<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'instapage');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '~sH8>Ab-e-_{b?N`+>M_hVOmv=t7qeBG:Z}F5gMByf|~VT73)?gq8xSM]<GJ|oXj');
define('SECURE_AUTH_KEY',  '/1-;9K@RKtYfuISFpMX| mtoCX-|pFhS/ 0DGo&2^g!l#Dw=>)$bk(;IZ!0MKBo?');
define('LOGGED_IN_KEY',    'OoXjS+;*{NI@j>VD(c}3_1fU:+^y0$8p46o;N4rjvt?s5wEu1R(SurZD>-H!PN}u');
define('NONCE_KEY',        '90fc!VJy=nml)sM@}i-mhABZ-Sd7hDD;p-S8P4TpGew+H-mVe1jVZXCT59p3UL m');
define('AUTH_SALT',        '8ruN_T;r#<4yV[fuV*&/x+Qx_=hk{K=# *8,26>OM*~%SWYhwx@|Fp%TR|.6a-Ra');
define('SECURE_AUTH_SALT', '.~]MFF&w^2ji||IedO_Fpyk!cO9uXM4?*v!BiiWIxMb+2U@YxKd@J:nSa%BWp^0=');
define('LOGGED_IN_SALT',   'g2-b*pCkO,Cm9z<r~Z>T)I[*])7+{noo-2#1dObv${n|>@zTk;q!(02$|7*|NEfL');
define('NONCE_SALT',       ']>G~7&ov4*_V]/~0E-yedYUw].e60cAcU7&,1s2=8@f0v2_R`<hE21xMv>i3fo8E');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
