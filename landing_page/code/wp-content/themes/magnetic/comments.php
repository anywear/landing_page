<?php
	if ('comments.php' == basename($_SERVER['SCRIPT_FILENAME']))
	{
		die('Please do not load this page directly. Thanks!');
	}

	if ( ! empty($post->post_password))
	{
		if ($_COOKIE['wp-postpass_'.COOKIEHASH] != $post->post_password)
		{
			?><p class="box warning aligncenter"><span><?php ether::lang('This post is password protected. Enter the password to view comments.'); ?></span></p><?php
			
			return;
		}
	}
	
	if ($post->comment_status == 'open')
	{
		ether::import('template.form.comment');
	}

	if (($comments) or ('open' == $post-> comment_status))
	{		
		ether::import('template.comments.default');
	}
?>
