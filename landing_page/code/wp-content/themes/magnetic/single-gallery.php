<?php get_header(); ?>

<?php
	global $post;
	
	if ($post->post_parent == 0)
	{
		ether::import('template.page.gallery-list');
	} else
	{
		ether::import('template.page.gallery-item');
	}
?>

<?php get_footer(); ?>
