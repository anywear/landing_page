				<?php
					$width = ether_header::get_option('width');
					$height = ether_header::get_option('height');
					
					if ( ! empty($width))
					{
						$width = intval($width);
					} else
					{
						$width = 970;
					}
					
					if ( ! empty($height))
					{
						$height = intval($height);
					} else
					{
						$height = 270;
					}

					$slides = ether_header::get_slides($width, $height);

					if (count($slides) > 0)
					{
				?>
				
				<script type="text/javascript">
					$j = jQuery.noConflict();
					
					$j( function()
					{
						$j('#main-heading div.nivo-slider').nivoSlider
						({
							effect: '<?php echo (ether_header::get_option('effect') == 'default' ? 'random' : ether_header::get_option('effect')); ?>',
							pauseTime: '<?php echo (ether_header::get_option('autoplay') == '0' ? 999999 : ether_header::get_option('autoplay') * 1000); ?>'
						});
					});
				</script>
				<div id="main-heading">
					<div class="nivo-slider-holder" style="width: <?php echo $width; ?>px; height: <?php echo $height; ?>px; margin: 0 auto 0; margin-top:21px;"> 
						<div class="nivo-slider"> 
							<?php foreach ($slides as $slide) { ?>
							<?php
								$url = ether::meta('url', TRUE, $slide['id']);
								
								if (empty($url))
								{
									$url = $slide['url'];
								}
							?>
							<a href="<?php echo ether::shadowbox_href($url); ?>"><img src="<?php echo $slide['image']; ?>" alt="<?php echo $slide['image_alt']; ?>" title="" width="<?php echo $slide['image_width']; ?>" height="<?php echo $slide['image_height']; ?>" /></a>
							<?php } ?>
						</div>
						<?php $slides = array(); foreach ($slides as $slide) { ?>
						<div id="nivo-slide<?php echo $slide['order']; ?>" class="nivo-html-caption">
							<?php echo $slide['content']; ?>
						</div>
						<?php } ?>
					</div>
				</div>
				
				<?php
					}
				?>
