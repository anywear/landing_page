				<?php
					$flickrs = ether::flickr_feed(ether_header::get_option('id'), 7);
					
					$flickr_template = array
					(
						array('width' => 100, 'height' => 75),
						array('width' => 115, 'height' => 81),
						array('width' => 125, 'height' => 90),
						array('width' => 145, 'height' => 100),
						array('width' => 125, 'height' => 90),
						array('width' => 115, 'height' => 81),
						array('width' => 100, 'height' => 75)
					);
					
					$count = count($flickrs);
	
					if ($count > 0)
					{
				?>
				<div id="main-heading">
					<div class="widget">
						<div class="flickr-feed">
							<ul class="items">
							<?php for ($i = 0; $i < $count; $i++) { $flickr = $flickrs[$i]; ?>
								<li><a class="frame-holder alignleft" href="<?php echo $flickr['image']; ?>" rel="lightbox"><img src="<?php echo $flickr['thumbnail']; ?>" alt="<?php echo $flickr['title']; ?>" width="<?php echo $flickr_template[$i]['width']; ?>" height="<?php echo $flickr_template[$i]['height']; ?>" /></a></li>
							<?php } ?>
							</ul>
						</div>
					</div>
				</div>
				<?php
					}
				?>
