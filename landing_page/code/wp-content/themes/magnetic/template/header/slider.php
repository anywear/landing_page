				<?php
					$slides = ether_header::get_slides(520, 295);

					if (count($slides) > 0)
					{
				?>
				
				<?php if (ether_header::get_option('autoplay') != '0') { ?><script type="text/javascript">
					$j = jQuery.noConflict();

					$j( function()
					{
						var header_interval = setInterval( function()
						{
							$j('#main-heading .slider-nav li.next').trigger('click');
						}, <?php echo (ether_header::get_option('autoplay') * 1000); ?>);
						
						$j('#main-headering .slider-nav li, #main-heading .slider-pagination li').click( function()
						{
							clearInterval(header_interval);
						});
					});
				</script><?php } ?>
				
				
				
				<div id="main-heading">
					<div class="fix_width"><div class="widget">
						<div class="success-stories">
							<ul class="items">
								<?php foreach ($slides as $slide) { ?>
								<?php
									$url = ether::meta('url', TRUE, $slide['id']);
								?>
								<li>
									<div class="media align<?php echo (ether_header::get_option('align') == 'left' ? 'right' : 'left'); ?>"><?php if ( ! empty($url)) { ?><a href="<?php echo ether::shadowbox_href($url); ?>"><?php } ?><img src="<?php echo $slide['image']; ?>" alt="<?php echo $slide['image_alt']; ?>" width="<?php echo $slide['']; ?>" height="<?php echo $slide['']; ?>" /><?php if ( ! empty($url)) { ?></a><?php } ?></div>
	<!-- 								<div class="desc">
										<h2><?php if ( ! empty($url)) { ?><a href="<?php echo ether::shadowbox_href($url); ?>"><?php } ?><?php echo $slide['title']; ?><?php if ( ! empty($url)) { ?></a><?php } ?></h2>
										<?php echo do_shortcode($slide['content']); ?>
									</div>
									-->								</li>
								<?php } ?>
							</ul>
							<ul class="slider-nav type-3">
								<li class="prev"><a href="#prev"><?php ether::lang('Prev'); ?></a></li>
								<li class="next"><a href="#next"><?php ether::lang('Next'); ?></a></li>
							</ul>
							<ul class="slider-pagination type-1">
								<?php
									$pagination = 1;
									
									foreach ($slides as $slide)
									{
										echo '<li><a href="#slide'.$pagination.'">'.$pagination.'</a></li>';
										$pagination++;
									}
								?>
							</ul>
						</div>
					</div>
					<div class="clear"></div>
					</div>
				</div>
				<?php
					}
				?>

