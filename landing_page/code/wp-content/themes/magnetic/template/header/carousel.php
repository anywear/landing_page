				<?php
					$width = ether_header::get_option('width');
					
					if ( ! empty($width))
					{
						$width = intval($width);
						$height = ($width / 620.0) * 380.0;
					} else
					{
						$width = 620;
						$height = 380;
					}

					$slides = ether_header::get_slides($width, $height);

					if (count($slides) > 0)
					{
				?>
				
				<script type="text/javascript">
					$j = jQuery.noConflict();
					
					$j( function()
					{
						$j('#main-heading ul.roundabout').roundabout
						({
							minScale: 0.65,
							childSelector: 'li',
							minOpacity: 1.0,
							easing: '<?php echo (ether_header::get_option('animation') == 'linear' ? 'swing': ether_header::get_option('animation')); ?>'
						});
						
						$j('#main-heading ul.roundabout li > a > img').css
						({
							'width': '100%',
							'height': '100%'
						});
						
						<?php if (ether_header::get_option('autoplay') != '0') { ?>
						var header_interval = setInterval( function()
						{
							$j('#main-heading ul.roundabout').roundabout_animateToPreviousChild();
						}, <?php echo (ether_header::get_option('autoplay') * 1000); ?>);
						
						$j('#main-heading ul.roundabout li').click( function()
						{
							clearInterval(header_interval);
						});
						<?php } ?>
					});
				</script>
				<div id="main-heading">
					<ul class="roundabout" style="width: <?php echo $width; ?>px; height: <?php echo $height; ?>px; height:340px;">
						<?php foreach ($slides as $slide) { ?>
						<li><a href="<?php echo $slide['url']; ?>"><img src="<?php echo $slide['image']; ?>" alt="<?php echo $slide['image_alt']; ?>" width="<?php echo $slide['image_width']; ?>" height="<?php echo $slide['image_height']; ?>" /></a></li>
						<?php } ?>
					</ul>
				</div>
				<?php
					}
				?>
