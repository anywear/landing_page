				<script type="text/javascript" src="<?php ether::path('media/piecemaker/swfobject.js'); ?>"></script>
				<script type="text/javascript">
					var flashvars = {};
					flashvars.cssSource = "<?php ether::path('media/piecemaker/piecemaker.css'); ?>";
					<?php
						$options = ether_header::$header_fields['piecemaker'];
						$url = '';
						
						foreach ($options as $option)
						{
							$key = str_replace('piecemaker_', '', $option);
							
							$url .= '%26'.$key.'='.ether_header::get_option($key);
						}
						
						function get_slide_id($a)
						{
							return $a[ether::config('prefix').(ether_header::$from_category ? 'category_' : '').'slides_id'];
						}
			
						$url .= '%26slides='.implode(',', array_map('get_slide_id', ether_header::$slides));
					?>
					flashvars.xmlSource = "<?php ether::path('ether/ether.php?piecemaker'.$url); ?>";

					var params = {};
					params.play = "true";
					params.menu = "true";
					params.scale = "showall";
					params.wmode = "transparent";
					params.allowfullscreen = "true";
					params.allowscriptaccess = "always";
					params.allownetworking = "all";

					swfobject.embedSWF('<?php ether::path('media/piecemaker/piecemaker.swf'); ?>', 'piecemaker', '980', '<?php echo (str_replace('px', '', ether_header::get_option('height')) + 100); ?>', '10', null, flashvars, params, null);
				</script>
				
				<div id="main-heading">
					<div class="slider" id="piecemaker">
					</div>
				</div>
