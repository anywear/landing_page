				<?php
					$tweets = ether::twitter_tweets(ether_header::get_option('username'), $count);

					if (count($tweets) > 0)
					{
				?>
				<div id="main-heading">
					<div class="widget">
						<div id="twitter_div">
							<ul id="twitter_update_list">
							<?php $counter = 0; foreach ($tweets as $tweet) { ?>
								<li><span><?php echo $tweet['tweet']; ?></span> <a href="<?php echo $tweet['link']; ?>"><?php echo $tweet['time']; ?></a></li>
								<?php $counter++; if ($counter >= 3) break; ?>
							<?php } ?>
							</ul>
						</div>
					</div>
				</div>
				<?php
					}
				?>
