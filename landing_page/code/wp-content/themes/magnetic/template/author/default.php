
					
					<?php
						global $author, $author_name;
						
						if (isset($_GET['author_name']))
						{
							$current_author = get_userdatabylogin($author_name);
						} else
						{
							$current_author = get_userdata(intval($author));
						}
						
						if (trim($current_author->first_name.' '.$current_author->last_name) != '')
						{
							$name = trim($current_author->first_name.' '.$current_author->last_name);
						} else
						{
							$name = $current_author->nicename;
						}
						
						$description = $current_author->description;
					?>
					<?php if (have_posts()) : ?>	
					<?php
						ether_theme::header('custom-text', array
						(
							'title' => __('Author').' '.$name,
							'content' => $description
						));
					?>

					<div class="main left">

						<div class="columns single">
							<div>
							
								<ul class="news">

									<?php while (have_posts()) : the_post(); ?>
									<li>
									
										<?php ether::import('template.post.default'); ?>
									</li>
									<?php endwhile; ?>
								</ul>		
								
								<ul class="pagination-2">
									<?php ether::locale(array
									(
										'pagination_prev' => '&laquo;',
										'pagination_next' => '&raquo;'
									)); ?>
									<?php ether::pagination(); ?>
								</ul>
							
							</div>

						</div>

					</div>

					<?php ether::import('template.sidebar.default'); ?>
					<?php else : ?>
					<?php ether::import('template.error.404'); ?>
					<?php endif; ?>
