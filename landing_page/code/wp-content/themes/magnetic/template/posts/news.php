					<?php
						global $more;
						$more = 0;
					?>

					<?php if (have_posts()) : ?>
					
					<?php ether::module_run('theme.setup'); ?>
					
					<div id="content" class="<?php echo ether::module_run('sidebar.get_align') == 'none' ? 'no-sidebar' : ether::module_run('sidebar.get_align').'-sidebar'; ?>">
						<?php ether::import('template.header.default'); ?>
						<div class="fix_width">
						<div class="main">
							<?php
								$per_page = get_option('posts_per_page');
								$paged = get_query_var('paged') ? get_query_var('paged') : 1;
								$offset = ($per_page * $paged) - $per_page;
								
								query_posts(array
								(
									'post_type' => 'news',
									'offset' => $offset,
									'posts_per_page' => $per_page, 
									'paged' => $paged
								));
							?>
							
							<ul class="news list">
								<?php while (have_posts()) : the_post(); ?>
								<li>
									<?php ether::import('template.post.default'); ?>
								</li>
								<?php endwhile; ?>
							</ul>		
									
							<ul class="post-nav">
								<?php ether::locale(array
								(
									'pagination_prev' => ether::langr('Newer'),
									'pagination_next' => ether::langr('Older')
								)); ?>
								
								<?php ether::pagination(FALSE, TRUE); ?>
							</ul>
							
							<?php wp_reset_query(); ?>
						</div>

						<?php ether::import('template.sidebar.default'); ?>
						<div class="clear"></div>
					</div>
					</div>
					
					<?php else: ?>
					<?php ether::import('404'); ?>
					<?php endif; ?>
