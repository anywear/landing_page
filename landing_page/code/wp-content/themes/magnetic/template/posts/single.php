
					<?php if (have_posts()) : the_post() ?>
					
					<?php ether::module_run('theme.setup'); ?>
					
					<div id="content" class="<?php echo ether::module_run('sidebar.get_align') == 'none' ? 'no-sidebar' : ether::module_run('sidebar.get_align').'-sidebar'; ?>">
						<?php ether::import('template.header.default'); ?>

						<div class="fix_width"><div class="main" style="padding-top:0px;">

							<ul class="news">
								<li>
									<?php ether::import('template.post.single'); ?>
								</li>
							</ul>		
									
							<ul class="post-nav">
								<?php ether::locale(array
								(
									'pagination_prev' => ether::langr('Previous post'),
									'pagination_next' => ether::langr('Next post')
								)); ?>
								
								<?php ether::pagination(FALSE, TRUE); ?>
							</ul>
							
							<?php ether::import('template.footer.post-single'); ?>
							
							<?php comments_template(); ?>	
								
						</div>
						
						<?php ether::import('template.sidebar.default'); ?>
						</div>
						<div class="clear"></div>
					</div>
					
					<?php else: ?>
					<?php ether::import('404'); ?>
					<?php endif; ?>
