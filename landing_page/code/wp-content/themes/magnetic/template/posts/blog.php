<?php
						global $more;
						$more = 0;
					?>
<?php if (have_posts()) : ?>
<?php ether::module_run('theme.setup'); ?>

<div id="content" class="<?php echo ether::module_run('sidebar.get_align') == 'none' ? 'no-sidebar' : ether::module_run('sidebar.get_align').'-sidebar'; ?>">
	<?php ether::import('template.header.default'); ?>
	<div class="fix_width">
		<div class="main">
			<ul class="news list">
				<?php while (have_posts()) : the_post(); ?>
				<li <?php post_class(); ?>>
					<?php ether::import('template.post.default'); ?>
				</li>
				<?php endwhile; ?>
			</ul>
			<ul class="post-nav">
				<?php ether::locale(array
				(
					'pagination_prev' => '<span>'.ether::langr('Newer').'</span>',
					'pagination_next' => '<span>'.ether::langr('Older').'</span>'
				)); ?>
				<?php ether::pagination(FALSE, TRUE); ?>
			</ul>
		</div>
		<?php ether::import('template.sidebar.default'); ?>
	</div>
	<div class="clear"></div>
</div>
<?php else: ?>
<?php ether::import('404'); ?>
<?php endif; ?>
