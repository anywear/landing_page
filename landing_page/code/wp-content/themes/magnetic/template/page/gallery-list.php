<?php if (have_posts()) : the_post() ?>
<?php ether::module_run('theme.setup'); ?>

<div id="content" class="<?php echo ether::module_run('sidebar.get_align') == 'none' ? 'no-sidebar' : ether::module_run('sidebar.get_align').'-sidebar'; ?>">
	<?php ether::import('template.header.default'); ?>
	<div class="fix_width">
		<div class="main simple_con2">
			<?php if (ether::meta('hide_title') != 'on') { ?>
			<h2>
				<?php the_title(); ?>
			</h2>
			<?php } ?>
			<?php the_content(); ?>
			<?php edit_post_link(ether::langr('Edit'), '<p>', '</p>'); ?>
			<?php ether_gallery::get_gallery(); ?>
		</div>
		<?php ether::import('template.sidebar.default'); ?>
	</div>
	<div class="clear"></div>
</div>
<?php else: ?>
<?php ether::import('404'); ?>
<?php endif; ?>
