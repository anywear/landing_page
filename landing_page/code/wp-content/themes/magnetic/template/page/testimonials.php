<?php if (have_posts()) : the_post() ?>
<?php ether::module_run('theme.setup'); ?>
<div id="content" class="<?php echo ether::module_run('sidebar.get_align') == 'none' ? 'no-sidebar' : ether::module_run('sidebar.get_align').'-sidebar'; ?>">
	<?php ether::import('template.header.default'); ?>
	<div class="fix_width"><div class="main">
		<?php if (ether::meta('hide_title') != 'on') { ?>
		<h2>
			<?php the_title(); ?>
		</h2>
		<?php } ?>
		<?php ether_gallery::get_gallery('top'); ?>
		<?php the_content(); ?>
		<ul class="testimonial-details">
			<li><?php echo ether::meta('author_name', TRUE); ?></li>
			<li><a href="<?php echo ether::meta('url', TRUE); ?>"><?php echo ether::meta('company_name', TRUE); ?></a></li>
		</ul>
		<?php ether_gallery::get_gallery('bottom'); ?>
		<?php edit_post_link(ether::langr('Edit'), '<p>', '</p>'); ?>
	</div>
	</div>
		
	<?php ether::import('template.sidebar.default'); ?>
	<div class="clear"></div>
</div>
<?php else: ?>
<?php ether::import('404'); ?>
<?php endif; ?>
