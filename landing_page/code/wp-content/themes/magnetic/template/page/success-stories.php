<?php if (have_posts()) : the_post() ?>
<?php ether::module_run('theme.setup'); ?>
<div id="content" class="<?php echo ether::module_run('sidebar.get_align') == 'none' ? 'no-sidebar' : ether::module_run('sidebar.get_align').'-sidebar'; ?>">
	<?php ether::import('template.header.default'); ?>
	<div class="fix_width"><div class="main">
		<?php if (ether::meta('hide_title') != 'on') { ?>
		<h2>
			<?php the_title(); ?>
		</h2>
		<?php } ?>
		<?php ether_gallery::get_gallery('top'); ?>
		<?php the_content(); ?>
		<?php
								$preview_image = ether::meta('preview_image', TRUE);
								
								if ( ! empty($preview_image)) {
									$image_base = ether::get_image_base($preview_image);
								?>
		<img src="<?php echo $image_base; ?>" alt="" class="frame aligncenter" />
		<?php
								}
							?>
		<?php ether_gallery::get_gallery('bottom'); ?>
		<?php edit_post_link(ether::langr('Edit'), '<p>', '</p>'); ?>
	</div>
	<?php ether::import('template.sidebar.default'); ?>
	</div>
	
		<div class="clear"></div>
</div>
<?php else: ?>
<?php ether::import('404'); ?>
<?php endif; ?>
