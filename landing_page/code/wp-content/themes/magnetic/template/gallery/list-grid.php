
					<?php
						if (ether_gallery::is_list())
						{
							$columns = ether_gallery::get_option('columns');
							$rows = ether_gallery::get_option('rows');

							$image_height = intval(str_replace('px', '', ether_gallery::get_option('image_height')));

							$margin = 48;
							$content_width = 980;

							if (ether_sidebar::get_align() != 'none')
							{
								$content_width = 680;
							}

							$content_width -= ($margin * $columns);
							$image_width = intval($content_width / $columns);

							ether_gallery::list_begin($columns * $rows);

							global $wp_query;
							//usort($wp_query->posts, array('ether_gallery', 'sort_featured'));
					?>
					<?php if (ether::config('use_quicksand')) { ?><script type="text/javascript">
						var gallery =
						{
							page: <?php $page = get_query_var('paged'); echo ( ! empty($page) ? intval($page) : 1); ?>,
							pages: <?php global $wp_query; echo intval(ceil($wp_query->found_posts / (intval($columns) * intval($rows)))); ?>
						};

						var vars = [], hash;
						var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');

						for (var i = 0; i < hashes.length; i++)
						{
							hash = hashes[i].split('=');
							vars.push(hash[0]);
							vars[hash[0]] = hash[1];
						}

						$j = jQuery.noConflict();

						$j( function()
						{
							$j('.gallery').wrap($j('<div />').css('position', 'relative'));

							if (gallery.pages > 1)
							{
								$j('.post-nav').remove();
								$j('.gallery').after($j('<ul class="post-nav"><li class="prev"><a href="#prev"><?php ether::lang('Previous'); ?></a></li><li class="next"><a href="#next"><?php ether::lang('Next'); ?></a></li></ul>'));

								var counter = 0;
								$j('.gallery > li').each( function()
								{
									$j(this).attr('data-id', 'index-' + counter);

									counter++;
								});

								$j('ul.post-nav li').click( function()
								{
									var next = 1;

									if ($j(this).hasClass('prev'))
									{
										next = gallery.page - 1;

										if (next < 1)
										{
											next = gallery.pages;
										}
									} else if ($j(this).hasClass('next'))
									{
										next = gallery.page + 1;

										if (next > gallery.pages)
										{
											next = 1;
										}
									}

									var old_url = window.location.href;
									var new_url = old_url;

									if (typeof vars['paged'] != 'undefined')
									{
										new_url = window.location.href.replace('page/' + gallery.page, 'page/' + next).replace('paged=' + gallery.page, 'paged=' + next);
									} else
									{
										new_url += (new_url.indexOf('?') != -1 ? '&paged=' + next : '/page/' + next + '/');
									}

									if (old_url != new_url)
									{
										$j('.post-nav').load_begin({ height: 50, path: config.images_dir, opacity: 0.0, image: 'loading-2.gif' });

										$j.get(new_url, function(data)
										{
											$j('.post-nav').load_end();
											var $items = $j(data).find('.gallery > li');
											var counter = 0;

											$items.each( function()
											{
												$j(this).attr('data-id', 'index-' + counter);
												counter++;
											});

											$j('.gallery').quicksand($items, { adjustHeight: 'dynamic' }, function()
											{
												$j('div.media-slider ul.items li').show();
												$j('div.media-slider').slider
												({
													items: 'ul.items',
													visible: 1,
													slide: 1,
													pagination: 'ul.slider-pagination'
												});

												Shadowbox.clearCache();
												Shadowbox.setup();

												$j('.featured-ribbon').each( function()
												{
													$j(this).css('top', $j(this).parent('.featured').height() / 2);
												});
											});

											gallery.page = next;
										});
									}

									return false;
								});
							}
						});
					</script><?php } ?>
					<ul class="gallery type-2">
					<?php $gallery_counter = 0; ?>
					<?php while (have_posts()) : the_post(); ?>
						<?php
							$gallery_counter++;
							$featured = (ether::meta('featured', TRUE, get_the_ID()) == 'yes');
						?>
						<li style="width: <?php echo (intval(100 / $columns) - 3).'%'; ?>">
							<div class="media-slider<?php if ($featured) echo ' featured'; ?>">
								<ul class="items">
									<?php
										$images = ether_gallery::get_images($image_width, $image_height, 4, TRUE, 1);

										foreach ($images as $image)
										{
											echo '<li><a href="'.$image['url'].'" rel="lightbox[gallery-'.$gallery_counter.']" class="frame-holder"><img src="'.$image['image'].'" alt="'.$image['image_alt'].'" width="'.$image['image_width'].'" height="'.$image['image_height'].'" /></a></li>';
										}
									?>
								</ul>
								<ul class="slider-pagination type-2">
									<?php
										$counter = 1;

										foreach ($images as $image)
										{
											echo '<li><a href="#slide'.$counter.'">'.$counter.'</a></li>';
											$counter++;
										}
									?>
								</ul>
								<?php if ($featured) echo '<div class="featured-ribbon"></div>'; ?>
							</div>

							<div class="desc">
								<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
								<?php the_excerpt(); ?>
							</div>
						</li>
					<?php endwhile; ?>
					</ul>

					<ul class="post-nav">
						<?php ether::locale(array
						(
							'pagination_prev' => ether::langr('Previous'),
							'pagination_next' => ether::langr('Next')
						)); ?>

						<?php ether::pagination(FALSE, TRUE); ?>
					</ul>

					<?php
							ether_gallery::list_end();
						}
					?>

