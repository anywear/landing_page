
					<?php
						$content_width = 980;
						
						if (ether_sidebar::get_align() != 'none')
						{
							$content_width = 680;
						}
						
						$width = intval(str_replace('%', '', ether_gallery::get_option('width')));
						$image_width = intval($content_width * ($width / 100.0));
						$image_height = intval(str_replace('px', '', ether_gallery::get_option('image_height')));
						
						$images = ether_gallery::get_images($image_width, $image_height);
						
						$style = 'width: '.ether_gallery::get_option('width').';';
						$class = '';
						$align_part = explode('-', ether_gallery::get_option('align'));
						
						if (count($align_part) > 1)
						{
							if ($align_part[1] == 'left')
							{
								$class = 'alignleft';
							} else if ($align_part[1] == 'right')
							{
								$class = 'alignright';
							} else if ($align_part[1] == 'center')
							{
								$class = 'aligncenter';
							}
						}
					?>					
					<div class="pip <?php echo $class; ?>" style="<?php echo $style; ?>">
						<div class="enlarged" style="height: <?php echo ether_gallery::get_option('image_height'); ?>;">
							<a href="<?php echo $images[0]['image']; ?>" rel="lightbox"><img src="<?php echo $images[0]['image']; ?>" alt="<?php echo $images[0]['image_alt']; ?>" height="100%" /></a>
						</div>
						<ul class="images align<?php echo ether_gallery::get_option('thumbs_align'); ?>">
							<?php
								foreach ($images as $image)
								{
									echo '<li><a href="'.$image['image'].'" class="frame-holder"><img src="'.$image['image'].'" alt="'.$image['image_alt'].'" width="50" height="50" /></a></li>';
								}
							?>
						</ul>
					</div>
