					
					<?php
						$columns = ether_gallery::get_option('columns');
						$rows = ether_gallery::get_option('rows');
						
						$width = intval(str_replace('%', '', ether_gallery::get_option('width')));
						$image_height = intval(str_replace('px', '', ether_gallery::get_option('image_height')));
						
						$margin = 48;
						$content_width = 980;
						
						if (ether_sidebar::get_align() != 'none')
						{
							$content_width = 680;
						}
						
						$content_width *= ($width / 100.0);
						$content_width -= ($margin * $columns);
						$image_width = intval($content_width / $columns);
						
						$images = ether_gallery::get_images($image_width, $image_height);
					?>
					<?php if (ether::config('use_quicksand')) { ?><script type="text/javascript">
						var gallery =
						{
							page: 1,
							pages: <?php echo intval(ceil(count($images) / intval($columns * $rows))); ?>,
							per_page: <?php echo intval($columns * $rows); ?>,
							images: null
						};

						$j = jQuery.noConflict();
						
						$j( function()
						{
							$j('.media1').parent('div').css('position', 'relative');

							if (gallery.pages > 1)
							{
								$j('.post-nav').remove();
								$j('.media1').after($j('<ul class="post-nav"><li class="prev"><a href="#prev"><?php ether::lang('Previous'); ?></a></li><li class="next"><a href="#next"><?php ether::lang('Next'); ?></a></li></ul>'));

								var counter = 0;
								$j('.media1 > li').each( function()
								{
									$j(this).attr('data-id', 'index-' + counter);
									
									counter++;
								});
								
								gallery.images = $j('.media1 li').clone();
								$j('.media1 li').remove();
								$j('.media1').append(gallery.images.clone().slice(0, gallery.per_page));
								
								$j('ul.post-nav li').click( function()
								{
									if ($j('.media1 li:animated').length == 0)
									{
										var next = 1;
										
										if ($j(this).hasClass('prev'))
										{
											next = gallery.page - 1;
											
											if (next < 1)
											{
												next = gallery.pages;
											}
										} else if ($j(this).hasClass('next'))
										{
											next = gallery.page + 1;
											
											if (next > gallery.pages)
											{
												next = 1;
											}
										}
										
										gallery.page = next;
										
										$j('.media1').quicksand(gallery.images.clone().slice((gallery.page - 1) * gallery.per_page, (gallery.page - 1) * gallery.per_page + gallery.per_page), { adjustHeight: 'dynamic' }, function()
										{
											Shadowbox.setup();
										});
									}
									
									return false;
								});
							}
						});
					</script><?php } ?>
					<?php
						$style = 'width: '.ether_gallery::get_option('width').';';
						$class = '';

						$align_part = explode('-', ether_gallery::get_option('align'));
						
						if (count($align_part) > 1)
						{
							if ($align_part[1] == 'left')
							{
								$class = 'alignleft';
							} else if ($align_part[1] == 'right')
							{
								$class = 'alignright';
							} else if ($align_part[1] == 'center')
							{
								$class = 'aligncenter';
							}
						}
					?>
					<div style="<?php echo $style; ?>" class="<?php echo $class; ?>">
						<ul class="media1">	
							<?php
								foreach ($images as $image)
								{
									echo '<li><a href="'.$image['url'].'" rel="lightbox[gallery]" class="frame-holder"><img src="'.$image['image'].'" alt="'.$image['image_alt'].'" width="'.$image['image_width'].'" height="'.$image['image_height'].'" /></a></li>';
								}
							?>
						</ul>
					</div>
