		<?php
			global $post, $user_ID, $user_email, $user_identity;
			$commenter = wp_get_current_commenter();
			extract($commenter);
		?>
		<div class="comment">
			<?php if (get_option('comment_registration') && ! $user_ID) { ?>
			<p class="box note aligncenter"><span><?php ether::lang('You must be %slogged in%s to post a comment.', '<a href="'.get_option('siteurl').'/wp-login.php?redirect_to='.get_permalink().'">', '</a>'); ?></span></p>
			<?php } else { ?>
			<h3><?php ether::lang('Leave a comment'); ?></h3>
		
			<form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post" id="comment"> 
				<fieldset> 
					<ul> 
						<li class="message"> 
							<textarea  rows="5" cols="20" name="comment" id="message"><?php ether::langx('Comment', 'comment form'); ?>...</textarea> 
						</li> 
						<li class="name"> 
							<input type="text" name="author" id="author" value="<?php echo ($user_ID ? $user_identity : ether::langx('Author', 'comment form', TRUE).'...'); ?>" <?php echo ($user_ID ? 'disabled="disabled" ' : ''); ?>/> 
						</li> 
						<li class="email"> 
							<input type="text" name="email" id="email" value="<?php echo ($user_ID ? $user_email : ether::langx('Email', 'comment form', TRUE).'...'); ?>" <?php echo ($user_ID ? 'disabled="disabled" ' : ''); ?>/> 
						</li> 
						<li class="submit"> 
							<span class="button default wide"><input type="submit" value="<?php ether::lang('Submit comment'); ?>" /></span> 
						</li> 
					</ul> 
				</fieldset>
				
				<?php comment_id_fields(); ?>
				<?php do_action('comment', $post->ID); ?> 
			</form>
		<?php } ?>
		</div>
