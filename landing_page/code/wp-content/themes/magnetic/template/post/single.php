
										<div class="header">
											<?php if (ether::meta('hide_title') != 'on') { ?>
											<h2><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
											<?php } ?>
											<ul class="meta">
												<li><?php ether::lang('Author'); ?>: <?php the_author_link(); ?></li>
												<?php global $post; if ($post->post_type == 'news') { ?>
												<li><?php ether::lang('Category'); ?>: <?php echo get_the_term_list($post->ID, 'news_category', '', ', '); ?>
												<?php } else { ?>
												<li><?php ether::lang('Category'); ?>: <?php the_category(', '); ?></li>
												<?php } ?>
												<?php
													ob_start();
													the_tags('', ', ');
													$tags = trim(ob_get_clean());
												?>
												<?php if ( ! empty($tags)) { ?><li><?php ether::lang('Tags'); ?>: <?php echo $tags; ?></li><?php } ?>
												<li class="alignright"><a href="<?php comments_link(); ?>"><?php comments_number(ether::langr('0 comments'), ether::langr('1 comment'), '% '.ether::langr('comments')); ?></a></li>
											</ul>
										</div>

										<?php ether_gallery::get_gallery('top'); ?>
										<?php the_content(); ?>
										<?php ether_gallery::get_gallery('bottom'); ?>
										<?php edit_post_link(ether::langr('Edit'), '<p>', '</p>'); ?>


