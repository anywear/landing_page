
										<div class="header">
											<h2><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
											<ul class="meta">
												<li><?php ether::lang('Author'); ?>: <?php the_author_link(); ?></li>
												<?php global $post; if ($post->post_type == 'news') { ?>
												<li><?php ether::lang('Category'); ?>: <?php echo get_the_term_list($post->ID, 'news_category', '', ', '); ?>
												<?php } else { ?>
												<li><?php ether::lang('Category'); ?>: <?php the_category(', '); ?></li>
												<?php } ?>
												<?php
													ob_start();
													the_tags('', ', ');
													$tags = trim(ob_get_clean());
												?>
												<?php if ( ! empty($tags)) { ?><li><?php ether::lang('Tags'); ?>: <?php echo $tags; ?></li><?php } ?>
												<li class="alignright"><a href="<?php comments_link(); ?>"><?php comments_number(ether::langr('0 comments'), ether::langr('1 comment'), '% '.ether::langr('comments')); ?></a></li>
												<li class="date"><?php the_date(); ?></li>
											</ul>
										</div>
										
										<?php
											$preview_image = ether::meta('preview_image', TRUE, get_the_ID());
											
											if ( ! empty($preview_image))
											{
												$preview_image = ether::get_image_thumbnail($preview_image, 155, 155);
											}
											
											$preview_alt = ether::meta('preview_alt', TRUE, $post->ID);
											$preview_title = ether::meta('preview_title', TRUE, $post->ID);
											
											if ( ! empty($preview_image))
											{
												echo '<a class="frame-holder preview alignleft" href="'.get_permalink().'"><img src="'.$preview_image.'" alt="'.$preview_alt.'" title="'.$preview_title.'" width="155" /></a>';
											}
										?>

										<?php the_content(ether::langr('read more').'...'); ?>
										<?php edit_post_link(ether::langr('Edit'), '<p>', '</p>'); ?>
