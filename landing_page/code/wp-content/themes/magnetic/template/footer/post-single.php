
<div class="columns news-corner">
	<div class="half">
		<h3>
			<?php ether::lang('Popular posts'); ?>
		</h3>
		<ul class="menu-2 news-overview">
			<?php global $post; ?>
			<?php ether::list_popular(5, FALSE, array('post_type' => $post->post_type)); ?>
		</ul>
	</div>
	<div class="half">
		<h3>
			<?php ether::lang('Share this post'); ?>
		</h3>
		<div class="addthis">
			<div class="addthis_toolbox addthis_default_style "> <a class="addthis_button_facebook_like" fb:like:layout="button_count"></a> <a class="addthis_button_tweet"></a> <a class="addthis_counter addthis_pill_style"></a> </div>
			<script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#username=<?php ether::option('social_addthis'); ?>"></script>
		</div>
		<h3>
			<?php ether::lang('Author'); ?>
			<?php the_author_link(); ?>
		</h3>
		<img src="http://0.gravatar.com/avatar/gravatar_thumb1.jpg?size=420" alt="Gravatar" width="78" height="78" class="alignright" />
		<p>
			<?php the_author_meta('description'); ?>
		</p>
	</div>
</div>
