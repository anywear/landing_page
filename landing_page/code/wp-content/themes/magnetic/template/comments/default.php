<?php
	global $post, $wp_query, $comments, $comment, $user_ID, $user_identity;
	$comments = $wp_query->comments;
	$commenter = wp_get_current_commenter();
	extract($commenter);
	
	$comment_iterator = 0;
	
	function comment_list($comment, $args, $depth)
	{
		global $comment_iterator;
		$comment_iterator++;
		$GLOBALS['comment'] = $comment;

		?><li id="comment-<?php comment_ID() ?>">
			<dl>
				<dt>
					<a class="author" href="<?php echo $comment->comment_author_url; ?>"><span class="avatar"><img src="http://0.gravatar.com/avatar/3d8cb0f4f498aa8ac1b35dc6c1dad237?size=420" alt="Gravatar" width="34" height="34" /></span><?php echo $comment->comment_author; ?></a>
					<?php comment_reply_link(array_merge($args, array('depth' => $depth, 'max_depth' => $args['max_depth'], 'respond_id' => 'comment'))) ?>
					<a href="#comment-<?php comment_ID() ?>" class="order"><?php echo $comment_iterator; ?></a>
					<span class="date"><?php echo get_comment_date().', '.get_comment_time(); ?></span>
				</dt>
					
				<dd>
					<?php if ($comment->comment_approved == '0') { ?>
					<p class="box note aligncenter"><span><?php ether::lang('Your comment is awaiting moderation.'); ?></span></p>
					<?php } ?>
					<?php comment_text() ?> 
					<?php edit_comment_link(ether::langr('Edit'),'<p>','</p>') ?>
				</dd>
			</dl>
		<?php
	}

	if ($comments) { ?>
	<ul class="comments">
		<?php wp_list_comments('type=comment&callback=comment_list'); ?>
	</ul>
	<?php } else
	{
		if ($post->comment_status == 'open')
		{
			?><p class="box info alignleft"><span><?php ether::lang('No comments yet.'); ?></span></p><?php
		} else
		{
			?><p class="box info alignleft"><span><?php ether::lang('Comments are closed.'); ?></span></p><?php
		}
	}
?>
