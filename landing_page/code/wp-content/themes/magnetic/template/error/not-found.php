					
					<?php ether_theme::header('custom-text'); ?>

					<div class="main left">

						<div class="columns single">
							<div>
							
								<ul class="news">
									<li>
										<p><?php ether::lang('The page you\'re looking for can\'t be found.'); ?></p>
										<ol>
											<?php
											$s = $wp_query->query_vars['name'];
											$s = preg_replace('/(.*)-(html|htm|php|asp|aspx)$/', '$1', $s);
											$posts = query_posts('post_type=any&name='.$s);
											$s = str_replace('-', ' ', $s);
											if (count($posts) == 0)
											{
												$posts = query_posts('post_type=any&s='.$s);
											}
											
											if (count($posts) > 0)
											{
											?>
											<li><?php ether::lang('Were you looking for one of the following posts or pages?'); ?>
												<ul>
												 <?php
													foreach ($posts as $post)
													{
														echo '<li><a href="'.get_permalink($post->ID).'">'.$post->post_title.'</a></li>';
													}
												?>
												</ul>
											</li>
											<?php
											}
											?>
											<li><?php ether::lang('Search for it'); ?>: <?php ether::import('template.form.search'); ?></li>
											<li><?php ether::lang('If you typed in a URL... make sure the spelling, cApitALiZaTiOn, and punctuation are correct. Then try reloading the page.'); ?></li>
											<li><?php ether::lang('Start over again at the homepage'); ?> - <a href="<?php ether::info('home'); ?>"><?php ether::lang('Home'); ?></a>.</li>
										</ol>
										
										<a href="#header"><span><?php ether::langx('Top', 'back to top link'); ?></span></a>
									</li>
									
								</ul>		
							
							</div>

						</div>

					</div>

					<?php ether_theme::sidebar(); ?>
