					
					<div id="content" class="no-sidebar">
						<div class="title"> 
							 
						</div> 
						<div class="main"> 
							<div class="errorpage"> 
								<h2>404</h2> 
								<p class="style-1"><?php ether::lang('Page not found', '404'); ?></p> 
								<h4><?php ether::lang('It seems that page you were looking for doesn\'t exist.<br />Try searching the site.', '404'); ?></h4> 
							</div> 
						</div> 
					</div>
