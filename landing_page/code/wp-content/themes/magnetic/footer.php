	<div id="footer">
<div class="fix_width">

		<div class="footer_top_box">
			<div class="corporate_bot_box">
				<div class="bottom_logo"><img src="<?php ether::path('media/images'); ?>/bot-logo.png" /></div>
				<div class="spacing_left">
					<h3>Corporate Office</h3>
					<div class="address_bot"> TechiesTown InfoTech<br />
						Office : 805 Samudra Annexe , Near Girish Cold Drinks , <br />
						Off C.G. Road , Ahmedabad - 380009 Gujarat , India </div>
					<h4>Contacts</h4>
					<div class="email_box_bot">
						<div class="email_title">Email:</div>
						<div class="email_id_bot"><a href="mailto:biz@techiestown.com">biz@techiestown.com</a><br />
							<a href="mailto:biz.techiestown@gmail.com">biz.techiestown@gmail.com</a></div>
					</div>
				</div>
			</div>
			<div class="bottom_services_box">
				<h3>Services</h3>
				<ul>
					<li><a href="http://192.168.1.12/rubyprogrammersindia_new/ror-development-and-consulting/
">Ruby on Rails Web Development</a></li>
					<li><a href="http://192.168.1.12/rubyprogrammersindia_new/rails-plugin-development/">Rails Plugin Development</a></li>
					<li><a href="http://192.168.1.12/rubyprogrammersindia_new/gems-development/">Gems Development</a></li>
					<li><a href="http://192.168.1.12/rubyprogrammersindia_new/grails-development/">GRails Development</a></li>
					<li><a href="http://192.168.1.12/rubyprogrammersindia_new/jruby-development/">jRuby Development</a></li>
					<li><a href="http://192.168.1.12/rubyprogrammersindia_new/iron-ruby-development/">iRon Ruby Development</a></li>
					<li><a href="http://192.168.1.12/rubyprogrammersindia_new/hire-dedicated-team/">Hire Dedicated Ruby Developers</a></li>
				</ul>
			</div>
			<div class="quick_contact_footer">
			<h3>Quick Contacts</h3>
				<div class="footer_social"><a href="#"><img src="<?php ether::path('media/images'); ?>/icon-yahoo.png" alt="Yahoo" title="Yahoo" />Yahoo<span>Yahoo</span></a></div>
				<div class="footer_social"><a href="#"><img src="<?php ether::path('media/images'); ?>/icon-gmail.png" alt="Gmail" title="Gmail" />Gmail<span>Gmail</span></a></div>
				<div class="footer_social"><a href="#"><img src="<?php ether::path('media/images'); ?>/icon-skype.png" alt="Skype" title="Skype" />Skype<span>Skype</span></a></div>
				<div class="footer_social"><a href="#"><img src="<?php ether::path('media/images'); ?>/icon-msn.png" alt="MSN" title="MSN" />MSN<span>MSN</span></a></div>
				<div class="footer_social"><a href="#"><img src="<?php ether::path('media/images'); ?>/icon-aol.png" alt="AOL" title="AOL" />AOL<span>AOL</span></a></div>
			</div>
			<div class="clear"></div>
		</div>
		
		<div id="copyright">
			<?php $footer_text = stripslashes(ether::option('footer_text')); if ( ! empty($footer_text)) { echo $footer_text; } else { ?>
			Copyright &copy; <?php echo date('Y'); ?>.
			<?php ether::lang('All rights reserved to'); ?>
			<a href="<?php ether::info('url'); ?>">
			<?php ether::info('name'); ?>
			</a>.
			<?php } ?>
		</div>
	</div>
	<div class="clear"></div>
</div>
<?php ether::footer(); ?>
</body></html>