<?php include('../modalbox/header.php'); ?>

	<fieldset class="ether"> 
		<ul>
			<li>
				<label class="alt-style-1"><?php ether::lang('Title'); ?></label>
				<input type="text" name="title" value="" />
			</li>
		</ul>		
	</fieldset>
	<fieldset class="ether">
		<ul>
			<li><label class="alt-style-1"><?php ether::lang('Select success stories'); ?></label>
		</ul>
		<ul style="overflow: auto; height: 200px;">
		<?php
			$stories = ether::get_posts(array('post_type' => 'success-stories', 'numberposts' => -1));
			
			foreach ($stories as $story)
			{
				?><li><label><input type="checkbox" name="id" value="<?php echo $story['id']; ?>" /> <?php echo $story['title']; ?></label></li><?php
			}
		?></ul>
	</fieldset>

<?php include('../modalbox/footer.php'); ?>
