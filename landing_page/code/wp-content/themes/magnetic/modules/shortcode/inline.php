<?php include('../modalbox/header.php'); ?>
	<script type="text/javascript">
		$( function()
		{
			$('select[name=icon]').change( function()
			{
				var path = '<?php ether::path('media/images/icons'); ?>/';
				$(this).prev('img').attr('src', path + $(this).children('option:selected').val() + '.png');
			});
		});
	</script>
	<fieldset class="ether"> 
		<ul>
			<li>
				<label class="alt-style-1"><?php ether::lang('Tooltip text'); ?></label>
				<input type="text" name="tooltip" value="" />
			</li>
			<li class="half">
				<label class="alt-style-1"><?php ether::lang('Icon'); ?></label>
				<select name="icon_align"><option value="none"><?php ether::lang('None'); ?></option><option value="left"><?php ether::lang('Left'); ?></option><option value="right"><?php ether::lang('Right'); ?></option></select>
			</li>
			<li class="half">
				<label class="alt-style-1"><?php ether::lang('Select icon'); ?></label>
				<img class="inline-icon" src="<?php ether::path('media/images/icons/download.png'); ?>" alt="" />
				<select name="icon">
					<option value="download">Download</option>
					<option value="pdf">PDF file</option>
					<option value="word">Word</option>
					<option value="email">Email</option>
					<option value="excel">Excel</option>
					<option value="presentation">Presentation</option>
					<option value="archive">Archive</option>
					<option value="link">Link</option>
					<option value="file">File</option>
					<option value="music">Music</option>
					<option value="video">Video</option>
					<option value="youtube">Youtube</option>
					<option value="vimeo">Vimeo</option>
					<option value="note">Note</option>
					<option value="image">Image</option>
					<option value="gallery">Gallery</option>
					<option value="own">Own icons</option>
				</select>
			</li>
			<li>
				<label class="alt-style-1"><?php ether::lang('Create link'); ?></label>
				<input type="text" name="url" value="" />
			</li>
		</ul>		
	</fieldset>

<?php include('../modalbox/footer.php'); ?>
