<?php include('../modalbox/header.php'); ?>

	<fieldset class="ether"> 
		<ul> 
			<li class="half"> 
				<label class="alt-style-1">Type</label> 
				<select name="type"><option value="info">Info</option><option value="note">Note</option><option value="warning">Warning</option><option value="download">Download</option></select> 
			</li> 
			<li class="half"> 
				<label class="alt-style-1">Align</label> 
				<select name="align"><option value="left">Left</option><option value="right">Right</option><option value="center">Center</option></select> 
			</li> 
			<li>
				<label class="alt-style-1">Paste link to icon to change default one</label>
				<input type="text" name="icon" value="" />
			</li>
			<li>
				<label class="alt-style-1">Width</label>
				<div class="slider width"></div>
				<input type="text" class="hidden" name="width" value="" />
				<small class="slider-min">0%</smalL>
				<small class="slider-max">100%</small>
				<div class="slider-value"></div>
			</li>
		</ul>		
	</fieldset>

<?php include('../modalbox/footer.php'); ?>
