<?php include('../modalbox/header.php'); ?>

	<fieldset class="ether"> 
		<ul> 
			<li class="half"> 
				<label class="alt-style-1"><?php ether::lang('Type'); ?></label> 
				<select name="type"><option value="info"><?php ether::lang('Info'); ?></option><option value="note"><?php ether::lang('Note'); ?></option><option value="warning"><?php ether::lang('Warning'); ?></option><option value="download"><?php ether::lang('Download'); ?></option></select> 
			</li> 
			<li class="half"> 
				<label class="alt-style-1"><?php ether::lang('Align'); ?></label> 
				<select name="align"><option value="left"><?php ether::lang('Left'); ?></option><option value="right"><?php ether::lang('Right'); ?></option><option value="center"><?php ether::lang('Center'); ?></option></select> 
			</li> 
			<li>
				<label class="alt-style-1"><?php ether::lang('Paste link to icon to change default one'); ?></label>
				<input type="text" name="icon" value="" />
			</li>
			<li>
				<label class="alt-style-1"><?php ether::lang('Width'); ?></label>
				<div class="slider width"></div>
				<input type="text" class="hidden" name="width" value="" />
				<small class="slider-min">0%</smalL>
				<small class="slider-max">100%</small>
				<div class="slider-value"></div>
			</li>
		</ul>		
	</fieldset>

<?php include('../modalbox/footer.php'); ?>
