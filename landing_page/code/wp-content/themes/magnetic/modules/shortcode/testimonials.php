<?php include('../modalbox/header.php'); ?>

	<fieldset class="ether"> 
		<ul>
			<li class="half">
				<label class="alt-style-1"><?php ether::lang('Title'); ?></label>
				<input type="text" name="title" value="" />
			</li>
			<li class="half">
				<label class="alt-style-1"><?php ether::lang('Number of words'); ?></label>
				<input type="text" name="words" value="" />
			<li>
				<label class="alt-style-1"><?php ether::lang('Columns'); ?></label>
				<div class="slider columns"></div>
				<input type="text" class="hidden" name="columns" value="4" />
				<small class="slider-min">1</small>
				<small class="slider-max">4</small>
				<div class="slider-value"></div>
			</li>
		</ul>		
	</fieldset>
	<fieldset class="ether">
		<ul>
			<li><label class="alt-style-1"><?php ether::lang('Select testimonials'); ?></label>
		</ul>
		<ul style="overflow: auto; height: 200px;">
		<?php
			$testimonials = ether::get_posts(array('post_type' => 'testimonials', 'numberposts' => -1));
			
			foreach ($testimonials as $testimonial)
			{
				?><li><label><input type="checkbox" name="id" value="<?php echo $testimonial['id']; ?>" /> <?php echo $testimonial['title']; ?></label></li><?php
			}
		?></ul>
	</fieldset>

<?php include('../modalbox/footer.php'); ?>
