<?php include('../modalbox/header.php'); ?>

	<fieldset class="ether"> 
		<ul>
			<li>
				<label class="alt-style-1"><?php ether::lang('URL'); ?></label>
				<input type="text" name="url" value="" />
			</li>
			<li class="half">
				<label class="alt-style-1"><?php ether::lang('Button align'); ?></label>
				<select name="align"><option value="left"><?php ether::lang('Left'); ?></option><option value="right"><?php ether::lang('Right'); ?></option><option value="center"><?php ether::lang('Center'); ?></option></select> 
			</li>
			<li class="half">
				<label class="alt-style-1"><?php ether::lang('Color'); ?></label>
				<input type="text" name="color" value="" />
			</li>
			<li>
				<label class="alt-style-1"><?php ether::lang('Width'); ?></label>
				<div class="slider width"></div>
				<input type="text" class="hidden" name="width" value="" />
				<small class="slider-min">0%</small>
				<small class="slider-max">100%</small>
				<div class="slider-value"></div>
			</li>
		</ul>		
	</fieldset>

<?php include('../modalbox/footer.php'); ?>
