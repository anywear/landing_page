<?php include('../modalbox/header.php'); ?>
	
	<script type="text/javascript">
		window.shortcode_content_callback = function(content)
		{
			var tmp_content = '';
			$('input[type=text]').each( function(i, input)
			{
				if ($(this).attr('name') == 'title')
				{
					tmp_content = ' [content title="' + $(this).val() + '"]';
				}
				
				if ($(this).attr('name') == 'content')
				{
					tmp_content += $(this).val() + '[/content]';
					content += tmp_content;
				}
			});
			
			return content;
		};
		
		$( function()
		{
			var toggle_form = $('fieldset.ether:eq(0) ul:first').clone();
			
			$('fieldset.ether li.add-toggle').click( function()
			{
				$('fieldset.ether:eq(0) ul:last').before(toggle_form.clone());

				return false;
			});
		});
	</script>
	<fieldset class="ether"> 
		<ul> 
			<li class="half"> 
				<label class="alt-style-1"><?php ether::lang('Toggle title'); ?></label>
				<input type="text" name="title" class="custom" />
			</li> 
			<li class="half"> 
				<label class="alt-style-1"><?php ether::lang('Content'); ?></label>
				<input type="text" name="content" class="custom" />
			</li>
		</ul>
		<ul> 
			<li class="add-toggle"><img src="<?php ether::path('ether/media/images/add-button.png'); ?>" alt="<?php ether::lang('Add toggle content'); ?>" /></li>
		</ul>		
	</fieldset>

<?php include('../modalbox/footer.php'); ?>
