<?php include('../modalbox/header.php'); ?>
	<script type="text/javascript">
		$( function()
		{
			$('select[name=style]').change( function()
			{
				var $list = $('ul.style');
				$list[0].className = '';
				$list.addClass('style').addClass($(this).children('option:selected').val());
			});
		});
	</script>
	<fieldset class="ether"> 
		<ul> 
			<li class="half"> 
				<label class="alt-style-1"><?php ether::lang('Select style'); ?></label> 
				<select name="style">
					<option value="check-1">Check 1</option>
					<option value="check-2">Check 2</option>
					<option value="check-3">Check 3</option>
					<option value="check-4">Check 4</option>
					<option value="check-5">Check 5</option>
					<option value="check-6">Check 6</option>
					<option value="arrow-1">Arrow 1</option>
					<option value="arrow-2">Arrow 2</option>
					<option value="arrow-3">Arrow 3</option>
					<option value="arrow-4">Arrow 4</option>
					<option value="arrow-5">Arrow 5</option>
					<option value="arrow-6">Arrow 6</option>
					<option value="star-1">Star 1</option>
					<option value="star-2">Star 2</option>
					<option value="star-3">Star 3</option>
					<option value="star-4">Star 4</option>
					<option value="star-5">Star 5</option>
					<option value="star-6">Star 6</option>
					<option value="clean-1">Clean 1</option>
					<option value="clean-2">Clean 2</option>
					<option value="clean-3">Clean 3</option>
					<option value="clean-4">Clean 4</option>
					<option value="clean-5">Clean 5</option>
					<option value="clean-6">Clean 6</option>
					<option value="exception-1">Exception 1</option>
					<option value="exception-2">Exception 2</option>
					<option value="exception-3">Exception 3</option>
					<option value="exception-4">Exception 4</option>
					<option value="exception-5">Exception 5</option>
					<option value="exception-6">Exception 6</option>
				</select> 
			</li> 
			<li class="half"> 
				<ul class="style check-1">
					<li>1</li>
					<li>2</li>
					<li>3</li>
				</ul>
			</li> 
			
		</ul>		
	</fieldset>

<?php include('../modalbox/footer.php'); ?>
