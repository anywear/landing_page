<?php include('../modalbox/header.php'); ?>

	<fieldset class="ether">
		<ul>
			<li><label class="alt-style-1"><?php ether::lang('Select pricing columns'); ?></label>
		</ul>
		<ul style="overflow: auto; height: 200px;">
		<?php
			$columns = ether::get_posts(array('post_type' => 'pricing-column', 'numberposts' => -1));
			
			foreach ($columns as $column)
			{
				?><li><label><input type="checkbox" name="id" value="<?php echo $column['id']; ?>" /> <?php echo $column['title']; ?> (ID: <?php echo $column['id']; ?>)</label></li><?php
			}
		?></ul>
	</fieldset>

<?php include('../modalbox/footer.php'); ?>
