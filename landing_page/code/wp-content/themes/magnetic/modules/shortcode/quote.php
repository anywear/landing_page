<?php include('../modalbox/header.php'); ?>

	<fieldset class="ether"> 
		<ul>
			<li class="half">
				<label class="alt-style-1"><?php ether::lang('Quote type'); ?></label>
				<select name="type"><option value="block"><?php ether::lang('Block quote'); ?></option><option value="pull"><?php ether::lang('Pull quote'); ?></option></select>
			</li>
			<li class="half">
				<label class="alt-style-1"><?php ether::lang('Template'); ?></label>
				<select name="style"><option value="1">Style 1</option><option value="2">Style 2</option></select>
			</li>
			<li class="half">
				<label class="alt-style-1"><?php ether::lang('Align'); ?></label> 
				<select name="align"><option value="left"><?php ether::lang('Left'); ?></option><option value="right"><?php ether::lang('Right'); ?></option><option value="center"><?php ether::lang('Center'); ?></option></select>
			</li>
			<li>
				<label class="alt-style-1"><?php ether::lang('Width'); ?></label>
				<div class="slider width"></div>
				<input type="text" class="hidden" name="width" value="" />
				<small class="slider-min">0%</small>
				<small class="slider-max">100%</small>
				<div class="slider-value"></div>
			</li>
		</ul>		
	</fieldset>

<?php include('../modalbox/footer.php'); ?>
