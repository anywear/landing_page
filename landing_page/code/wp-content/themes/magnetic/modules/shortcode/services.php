<?php include('../modalbox/header.php'); ?>

	<fieldset class="ether"> 
		<ul>
			<li>
				<label class="alt-style-1"><?php ether::lang('Number of words'); ?></label>
				<input type="text" name="words" value="" />
			</li>
			<li>
				<label class="alt-style-1"><?php ether::lang('Columns'); ?></label>
				<div class="slider columns"></div>
				<input type="text" class="hidden" name="columns" value="4" />
				<small class="slider-min">1</small>
				<small class="slider-max">4</small>
				<div class="slider-value"></div>
			</li>
		</ul>		
	</fieldset>
	<fieldset class="ether">
		<ul>
			<li><label class="alt-style-1"><?php ether::lang('Select services'); ?></label>
		</ul>
		<ul style="overflow: auto; height: 200px;">
		<?php
			$services = ether::get_posts(array('post_type' => 'service', 'numberposts' => -1));
			
			foreach ($services as $service)
			{
				?><li><label><input type="checkbox" name="id" value="<?php echo $service['id']; ?>" /> <?php echo $service['title']; ?></label></li><?php
			}
		?></ul>
	</fieldset>

<?php include('../modalbox/footer.php'); ?>
