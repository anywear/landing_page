<?php include('../modalbox/header.php'); ?>
	
	<script type="text/javascript" src="<?php ether::path('ether/media/scripts/jquery.ui.js'); ?>"></script>
	<script type="text/javascript" src="<?php ether::path('admin/media/scripts/columns.js'); ?>"></script>
	<link type="text/css" href="<?php ether::path('admin/media/stylesheets/easycolumn.css'); ?>" rel="stylesheet" />
	<style type="text/css">
		.hint { float: left; width: 100px; }
		.drag { width: 20px; height: 20px; background: #ff0000 !important; position: absolute; }
		#columns-wrapper { width: 100%; margin-bottom: 40px; }
		#columns-wrapper > .columns { position: relative; }
		#columns-wrapper > .columns > .drag { right: 0px; }
		#columns-wrapper > .columns.selected { border: 1px solid #000; }
		#columns-wrapper > .columns > .first { margin-left: 0 !important; }
		
		#columns-wrapper .columns { background: #ddd; min-height: 50px; }
		#columns-wrapper .columns .columns { background: #ccc }
		#columns-wrapper .columns > div { background: #bbb; min-height: 50px; }
		#columns-wrapper .columns .columns > div { background: #aaa; }
		#columns-wrapper .columns > div > * { background: #888; }
		#columns-wrapper .columns > div > *:before { content: ""; border: 1px solid #ddd; background: rgba(0,0,0,.5); color: #ddd; width: 100%; line-height: 20px; display: block; margin: 0 -100% -20px 0; z-index: 999; position: relative;font-size: 10pt !important; font-weight: bold;}
		#columns-wrapper .columns .columns:before { content: "";border: none; background: none; }
		#columns-wrapper .columns > .columns > div > *:before { content: "BAD NESTING! of: \'.columns\' -> .columns element can\'t be an immediate child of .columns"; background: rgba(200,0,0,.5)}
	</style>
	<!--<fieldset class="ether"> 
		<ul>
			<li class="half">
				<label class="alt-style-1">Quote type</label>
				<select name="type"><option type="block">Block quote</option><option type="pull">Pull quote</option></select>
			</li>
			<li class="half">
				<label class="alt-style-1">Template</label>
				<select name="style"><option value="1">Style 1</option><option value="2">Style 2</option></select>
			</li>
			<li class="half">
				<label class="alt-style-1">Align</label> 
				<select name="align"><option value="left">Left</option><option value="right">Right</option><option value="center">Center</option></select>
			</li>
			<li>
				<label class="alt-style-1">Width</label>
				<div class="slider width"></div>
				<input type="text" class="hidden" name="width" value="" />
				<small class="slider-min">0%</small>
				<small class="slider-max">100%</small>
				<div class="slider-value"></div>
			</li>
		</ul>		
	</fieldset>-->
	
	<fieldset>
		<select name="row">
			<option value="1">1 column</option>
			<option value="2">2 columns (50%, 50%)</option>
			<option value="3">2 columns (25%, 75%)</option>
			<option value="4">2 columns (33%, 66%)</option>
			<option value="5">3 columns (33%, 33%, 33%)</option>
			<option value="6">3 columns (50%, 25%, 25%)</option>
			<option value="7">4 columns (25%, 25%, 25%, 25%)</option>
		</select>
		<button name="addrow">Add row</button><button name="removerow">Remove row</button><button name="insertrows">Insert shortcode</button>
		<div id="columns-wrapper"></div>
	</fieldset>

<?php include('../modalbox/footer.php'); ?>
