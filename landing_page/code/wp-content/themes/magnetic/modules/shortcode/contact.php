<?php include('../modalbox/header.php'); ?>

	<fieldset class="ether"> 
		<ul>
			<li>
				<label class="alt-style-1"><?php ether::lang('Where to send emails?'); ?></label>
				<input type="text" name="email" value="" />
			</li>
			<li class="half">
				<label class="alt-style-1"><?php ether::lang('"Call us" text'); ?></label>
				<input type="text" name="callus_text" value="" />
			</li>
			<li class="half">
				<label class="alt-style-1"><?php ether::lang('"Call us" URL'); ?></label>
				<input type="text" name="callus_url" value="" />
			</li>
		</ul>
		<ul>
			<li class="half">
				<label class="alt-style-1"><?php ether::lang('Button text'); ?></label> 
				<input type="text" name="button_text" value="" />
			</li>
			<li class="half">
				<label class="alt-style-1"><?php ether::lang('Success message'); ?></label>
				<input type="text" name="success_message" value="" />
			</li>
		</ul>		
	</fieldset>

<?php include('../modalbox/footer.php'); ?>
