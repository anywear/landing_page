<?php include('../modalbox/header.php'); ?>

	<fieldset class="ether"> 
		<ul>
			<li>
				<label class="alt-style-1"><?php ether::lang('Paste link from YouTube, Vimeo et.'); ?></label>
				<input type="text" name="url" value="" />
			</li>
			<li class="half">
				<label class="alt-style-1"><?php ether::lang('Width'); ?></label>
				<input type="text" name="width" value="" />
			</li>
			<li class="half">
				<label class="alt-style-1"><?php ether::lang('Height'); ?></label>
				<input type="text" name="height" value="" />
			</li>
			<li class="half">
				<label class="alt-style-1"><?php ether::lang('Align'); ?></label> 
				<select name="align"><option value="left"><?php ether::lang('Left'); ?></option><option value="right"><?php ether::lang('Right'); ?></option><option value="center"><?php ether::lang('Center'); ?></option></select> 
			</li>
		</ul>		
	</fieldset>

<?php include('../modalbox/footer.php'); ?>
