<?php
	class ether_fonts_and_colors extends ether_module
	{
		public static $css;
		
		public static function init()
		{
			self::$css = '';
		}
		
		public static function is_google_font($name)
		{
			foreach (self::font_list() as $font => $options)
			{
				if ($name == $font AND $options['group'] == 'Google Font Directory')
				{
					return TRUE;
				}
			}
			
			return FALSE;
		}
					
		public static function get_google_font_variants($name)
		{
			$fonts = self::font_list();
			
			return $fonts[$name]['variants'];
		}
					
		public static function is_property($name, $element)
		{
			if (isset(self::$css[ether::config('prefix').$element.'_'.$name]))
			{
				$prop = self::$css[ether::config('prefix').$element.'_'.$name];
						
				if (isset($prop) AND ! empty($prop))
				{
					return TRUE;
				}
			}
					
			return FALSE;
		}
					
		public static function get_property($name, $element)
		{
			$prop = self::$css[ether::config('prefix').$element.'_'.$name];
					
			if (isset($prop) AND ! empty($prop))
			{
				return $prop;
			}
					
			return '';
		}
		
		public static function get_font_list()
		{
			self::$css = unserialize(ether::option('fonts-and-colors'));
			
			$google_fonts = array();
			
			$elements = array
			(
				'h1',
				'h2',
				'h3',
				'h4',
				'h5',
				'h6',
				'p'
			);

			foreach ($elements as $element)
			{
				if (self::is_property('font', $element))
				{
					$font = self::get_property('font', $element);

					if (self::is_google_font($font))
					{
						
						$variants = self::get_google_font_variants($font);
						$font_variants = 'regular';
							
						if (self::is_property('bold', $element) AND self::is_property('italic', $element) AND in_array('bolditalic', $variants))
						{
							$font_variants = 'bolditalic';
						} else if (self::is_property('bold', $element) AND in_array('bold', $variants))
						{
							$font_variants = 'bold';
						} else if (self::is_property('italic', $element) AND in_array('italic', $variants))
						{
							$font_variants = 'italic';
						}
						
						$google_fonts[] = 'http://fonts.googleapis.com/css?family='.str_replace(' ', '+', $font).':'.$font_variants.'&subset=latin';
					}
				}
			}
			
			return array_unique($google_fonts);
		}

		public static function get($data)
		{
			if (isset($data['custom-style']))
			{
				self::$css = unserialize(ether::option('fonts-and-colors'));

				$result = '';
				
				if (isset(self::$css[ether::config('prefix').'colors_use']) OR isset($data['preview']))
				{
					if ( ! isset($data['preview']))
					{
						if ( ! empty(self::$css[ether::config('prefix').'background_color']))
						{
							$result .= '#main-heading, #nav #left-bg, #nav #right-bg, #nav, #nav ul ul { background-color: '.self::$css[ether::config('prefix').'background_color'].'; }';
							$result .= '.tip:after { background-color: '.self::$css[ether::config('prefix').'background_color'].' !important; }';
							$result .= '.title h2, .title h3 { color: '.self::$css[ether::config('prefix').'background_color'].'; }';
						}
						
						if ( ! empty(self::$css[ether::config('prefix').'main_color']))
						{
							$result .= 'a, a:hover, a:focus, a:visited, a:link, h3 a, h4 a, h4, h6, .pricing .alt, #main-heading #twitter_update_list li a, blockquote.style-1 p, .services h4 a, .success-stories h4 a, .testimonials a, #twitter_update_list a { color: '.self::$css[ether::config('prefix').'main_color'].'; }';
							$result .= '.testimonials .read-more, .button.default { background-color: '.self::$css[ether::config('prefix').'main_color'].'; }';
							$result .= '.testimonials .read-more:after, .frame-holder:hover img { border-color: '.self::$css[ether::config('prefix').'main_color'].' transparent; }';
							$result .= '.right-sidebar .menu-1 li.current > a { border-right-color: '.self::$css[ether::config('prefix').'main_color'].' transparent; }';
							$result .= '.left-sidebar .menu-1 li.current > a { border-left-color: '.self::$css[ether::config('prefix').'main_color'].' transparent; }';
						}
					}
				}

				if (isset(self::$css[ether::config('prefix').'fonts_use']) OR isset($data['preview']))
				{
					$elements = array
					(
						'h1',
						'h2',
						'h3',
						'h4',
						'h5',
						'h6',
						'p'
					);
						
					$google_fonts = array();

					foreach ($elements as $element)
					{
						$result .= (isset($data['preview']) ? '.ether ' : '').$element.(isset($data['preview']) ? '.preview' : '').' {';
						
						if (self::is_property('color', $element))
						{
							$result .= ' color: '.self::get_property('color', $element).' !important;';
						}
						
						if (self::is_property('font', $element))
						{
							$result .= ' font-family: "'.self::get_property('font', $element).'" !important;';
						}
							
						if (self::is_property('justify', $element) OR self::is_property('center', $element) OR self::is_property('alignleft', $element) OR self::is_property('alignright', $element))
						{
							$align = 'left';
							
							if (self::is_property('justify', $element))
							{
								$align = 'justify';
							}
							
							if (self::is_property('center', $element))
							{
								$align = 'center';
							}
							
							if (self::is_property('alignright', $element))
							{
								$align = 'right';
							}
								
							if (self::is_property('alignleft', $element))
							{
								$align = 'left';
							}
								
							$result .= ' text-align: '.$align.' !important !important;';
						}
							
						if (self::is_property('all-uppercase', $element))
						{
							$result .= ' text-transform: uppercase !important;';
						}
							
						if (self::is_property('underline', $element))
						{
							$result .= ' text-decoration: underline !important;';
						}
							
						if (self::is_property('bold', $element))
						{
							$result .= ' font-weight: bold !important;';
						}
						
						if (self::is_property('italic', $element))
						{
							$result .= ' font-style: italic !important;';
						}
						
						if (self::is_property('kerning', $element))
						{
							$result .= ' letter-spacing: '.self::get_property('kerning', $element).'px !important;';
						}
						
						if (self::is_property('leading', $element))
						{
							$result .= ' line-height: '.self::get_property('leading', $element);
						}
						
						$result .=' } ';
						
						if ( ! isset($data['preview']))
						{
							if (self::is_property('font', $element))
							{
								$font = self::get_property('font', $element);
								
								if (self::is_google_font($font))
								{
									$variants = self::get_google_font_variants($font);
									$font_variants = 'regular';
									
									if (self::is_property('bold', $element) AND self::is_property('italic', $element) AND in_array('bolditalic', $variants))
									{
										$font_variants = 'bolditalic';
									} else if (self::is_property('bold', $element) AND in_array('bold', $variants))
									{
										$font_variants = 'bold';
									} else if (self::is_property('italic', $element) AND in_array('italic', $variants))
									{
										$font_variants = 'italic';
									}
									
									$google_fonts[] = 'http://fonts.googleapis.com/css?family='.str_replace(' ', '+', $font).':'.$font_variants.'&subset=latin';
								}
							}
						}
					}
				}
					
				header('Content-type: text/css');
				die($result);
			}
		}
		
		public static function font_list()
		{
			return array
			(
				'Arial' => array
				(
					'group' => 'Base Fonts',
					'name' => 'Arial'
				),
				'Helvetica' => array
				(
					'group' => 'Base Fonts',
					'name' => 'Helvetica'
				),
				'Times New Roman' => array
				(
					'group' => 'Base Fonts',
					'name' => 'Times New Roman'
				),
				'Times' => array
				(
					'group' => 'Base Fonts',
					'name' => 'Times'
				),
				'Courier New' => array
				(
					'group' => 'Base Fonts',
					'name' => 'Courier New'
				),
				'Courier' => array
				(
					'group' => 'Base Fonts',
					'name' => 'Courier New'
				),
				'Cantarell' => array
				(
					'group' => 'Google Font Directory',
					'name' => 'Cantarell',
					'variants' => array('regular', 'bold', 'italic', 'bolditalic')
				),
				'Cardo' => array
				(
					'group' => 'Google Font Directory',
					'name' => 'Cardo',
					'variants' => array('regular')
				),
				'Crimson Text' => array
				(
					'group' => 'Google Font Directory',
					'name' => 'Crimson Text',
					'variants' => array('regular')
				),
				'Cuprum' => array
				(
					'group' => 'Google Font Directory',
					'name' => 'Cuprum',
					'variants' => array('regular')
				),
				'Droid Sans' => array
				(
					'group' => 'Google Font Directory',
					'name' => 'Droid Sans',
					'variants' => array('regular', 'bold')
				),
				'Droid Sans Mono' => array
				(
					'group' => 'Google Font Directory',
					'name' => 'Droid Sans Mono',
					'variants' => array('regular')
				),
				'Droid Serif' => array
				(
					'group' => 'Google Font Directory',
					'name' => 'Droid Serif',
					'variants' => array('regular', 'bold', 'italic', 'bolditalic')
				),
				'IM Fell DW Pica' => array
				(
					'group' => 'Google Font Directory',
					'name' => 'IM Fell',
					'variants' => array('regular', 'italic')
				),
				'Inconsolata' => array
				(
					'group' => 'Google Font Directory',
					'name' => 'Inconsolata',
					'variants' => array('regular')
				),
				'Josefin Sans Std Light' => array
				(
					'group' => 'Google Font Directory',
					'name' => 'Josefin Sans Std Light',
					'variants' => array('regular')
				),
				'Lobster' => array
				(
					'group' => 'Google Font Directory',
					'name' => 'Lobster',
					'variants' => array('regular')
				),
				'Molengo' => array
				(
					'group' => 'Google Font Directory',
					'name' => 'Molengo',
					'variants' => array('regular')
				),
				'Neucha' => array
				(
					'group' => 'Google Font Directory',
					'name' => 'Neucha',
					'variants' => array('regular')
				),
				'Neuton' => array
				(
					'group' => 'Google Font Directory',
					'name' => 'Neuton',
					'variants' => array('regular')
				),
				'Nobile' => array
				(
					'group' => 'Google Font Directory',
					'name' => 'Nobile',
					'variants' => array('regular', 'bold', 'italic', 'bolditalic')
				),
				'OFL Sorts Mill Goudy TT' => array
				(
					'group' => 'Google Font Directory',
					'name' => 'OFL Sorts Mill Goudy TT',
					'variants' => array('regular', 'italic')
				),
				'Old Standard TT' => array
				(
					'group' => 'Google Font Directory',
					'name' => 'Old Standard TT',
					'variants' => array('regular', 'bold', 'italic')
				),
				'PT Sans' => array
				(
					'group' => 'Google Font Directory',
					'name' => 'PT Sans',
					'variants' => array('regular')
				),
				'Philosopher' => array
				(
					'group' => 'Google Font Directory',
					'name' => 'Philosopher',
					'variants' => array('regular')
				),
				'Reenie Beanie' => array
				(
					'group' => 'Google Font Directory',
					'name' => 'Reenie Beanie',
					'variants' => array('regular')
				),
				'Tangerine' => array
				(
					'group' => 'Google Font Directory',
					'name' => 'Tangerine',
					'variants' => array('regular', 'bold')
				),
				'Vollkorn' => array
				(
					'group' => 'Google Font Directory',
					'name' => 'Vollkorn',
					'variants' => array('regular', 'bold', 'italic', 'bolditalic')
				),
				'Yanone Kaffeesatz' => array
				(
					'group' => 'Google Font Directory',
					'name' => 'Yanone Kaffeesatz',
					'variants' => array('regular', 'bold')
				)
			);
		}
	}
?>
