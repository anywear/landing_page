<?php
	class ether_gallery extends ether_module
	{
		public static $template_parent = array();
		public static $template_child = array();
		public static $template_other = array();
		public static $align = array();
		public static $thumbs_align = array();
		public static $gallery_type = NULL;
		public static $gallery_options = array();
		public static $images = array();

		public static function init()
		{
			self::$template_parent = array
			(
				'list-simple' => array
				(
					'name' => ether::langx('List simple', 'parent gallery template name', TRUE)
				),
				'list-grid' => array
				(
					'name' => ether::langx('List grid', 'parent gallery template name', TRUE)
				),
				'list-portfolio' => array
				(
					'name' => ether::langx('List portfolio', 'parent gallery template name', TRUE)
				)
			);

			self::$template_child = array
			(
				'simple' => array
				(
					'name' => ether::langx('Simple', 'child gallery template name', TRUE)
				),
				'zoombox' => array
				(
					'name' => ether::langx('Zoombox', 'child gallery template name', TRUE)
				),
				'pip' => array
				(
					'name' => ether::langx('Picture in picture', 'child gallery template name', TRUE)
				)
			);

			self::$template_other = array
			(
				'none' => array
				(
					'name' => ether::langx('None', 'child gallery template name', TRUE)
				),
				'simple' => array
				(
					'name' => ether::langx('Simple', 'child gallery template name', TRUE)
				),
				'zoombox' => array
				(
					'name' => ether::langx('Zoombox', 'child gallery template name', TRUE)
				),
				'pip' => array
				(
					'name' => ether::langx('Picture in picture', 'child gallery template name', TRUE)
				)
			);

			self::$align = array
			(
				'top' => array('name' => ether::langx('Top', 'gallery position attribute', TRUE)),
				'top-left' => array('name' => ether::langx('Top left', 'gallery position attribute', TRUE)),
				'top-center' => array('name' => ether::langx('Top center', 'gallery position attribute', TRUE)),
				'top-right' => array('name' => ether::langx('Top Right', 'gallery position attribute', TRUE)),
				'bottom' => array('name' => ether::langx('Bottom', 'gallery position attribute', TRUE)),
				'bottom-left' => array('name' => ether::langx('Bottom left', 'gallery position attribute', TRUE)),
				'bottom-center' => array('name' => ether::langx('Bottom center', 'gallery position attribute', TRUE)),
				'bottom-right' => array('name' => ether::langx('Bottom right', 'gallery position attribute', TRUE))
			);

			self::$thumbs_align = array
			(
				'top' => array('name' => ether::langx('Top', 'gallery position attribute', TRUE)),
				'bottom' => array('name' => ether::langx('Bottom', 'gallery position attribute', TRUE)),
				'left' => array('name' => ether::langx('Left', 'gallery position attribute', TRUE)),
				'right' => array('name' => ether::langx('Right', 'gallery position attribute', TRUE))
			);
		}

		public static function get_gallery($align = NULL)
		{
			if (self::is_gallery())
			{
				$align_part = explode('-', self::get_option('align'));

				if ($align == NULL OR $align == $align_part[0] OR $align == self::get_option('align'))
				{
					ether::import('template.gallery.'.self::$gallery_type);
				}
			}
		}

		public static function is_gallery()
		{
			return ( ! empty(self::$gallery_type) AND self::$gallery_type != 'none');
		}

		public static function get_option($key)
		{
			if (isset(self::$gallery_options[$key]))
			{
				return self::$gallery_options[$key];
			}

			return NULL;
		}

		public static function get_images($image_width, $image_height, $count = -1, $use_preview = FALSE, $min_count = 1)
		{
			global $post;

			if (substr(self::$gallery_type, 0, 4) == 'list')
			{
				self::$images = unserialize(ether::meta('gallery_images', TRUE, $post->ID));
			}

			$images = array();
			$counter = 0;

			if ($use_preview)
			{
				$preview_image_base = NULL;
				$preview_image = ether::meta('preview_image', TRUE, $post->ID);
				$preview_alt = ether::meta('preview_alt', TRUE, $post->ID);

				if ( ! empty($preview_image))
				{
					$preview_image = ether::get_image_thumbnail($preview_image, $image_width, $image_height);
					$preview_image_base = ether::get_image_base($preview_image);

					$images[] = array
					(
						'image' => $preview_image,
						'image_alt' => $preview_alt,
						'image_width' => $image_width,
						'image_height' => $image_height,
						'url' => $preview_image_base
					);

					$counter++;
				}
			}

			if ($count == 1 AND $counter == $count)
			{
				return $images;
			}

			if ($count != -1 AND (empty(self::$images) OR count(self::$images) < $min_count - $counter))
			{
				$new_count = $min_count - count(self::$images) - $counter;

				for ($i = 0; $i < $new_count; $i++)
				{
					self::$images[] = NULL;
				}
			}

			foreach (self::$images as $img)
			{
				$image_base = NULL;
				$image = $img[ether::config('prefix').'gallery_image'];

				if (empty($image))
				{
					$image_base = ether::path('media/images/placeholders/placeholder.png', TRUE);
					$image = $image_base;
				} else
				{
					$image_base = ether::get_image_base($image);
					$image = ether::get_image_thumbnail($image, $image_width, $image_height);
				}

				$images[] = array
				(
					'image' => $image,
					'image_alt' => $img[ether::config('prefix').'gallery_alt'],
					'image_width' => $image_width,
					'image_height' => $image_height,
					'url' => $image_base
				);

				$counter++;

				if ($count != -1)
				{
					if ($counter >= $count)
					{
						break;
					}
				}
			}

			return $images;
		}

		public static function get_images_base()
		{
			global $post;

			if (substr(self::$gallery_type, 0, 4) == 'list')
			{
				self::$images = unserialize(ether::meta('gallery_images', TRUE, $post->ID));
			}

			$images = array();
			$counter = 0;

			foreach (self::$images as $img)
			{
				$image_width = 800;
				$image_height = 600;
				$image_base = NULL;
				$image = $img[ether::config('prefix').'gallery_image'];

				if (empty($image))
				{
					$image_base = ether::path('media/images/placeholders/placeholder.png', TRUE);

				} else
				{
					$image_base = ether::get_image_base($image);
					$size = ether::get_image_size($image_base);

					$image_width = $size['width'];
					$image_height = $size['height'];
				}

				$images[] = array
				(
					'image' => $image_base,
					'image_alt' => $img[ether::config('prefix').'gallery_alt'],
					'image_width' => $image_width,
					'image_height' => $image_height,
					'url' => $image_base
				);
			}

			return $images;
		}

		public static function setup()
		{
			self::$gallery_type = NULL;
			self::$gallery_options = array();
			self::$images = array();

			global $post;
			$id = NULL;
			$term = NULL;

			if (is_single())
			{
				$id = $post->ID;
			} else if (is_page() OR get_option('show_on_front') != 'posts')
			{
				if (is_page())
				{
					$id = $post->ID;
				} else if (is_home() AND get_option('show_on_front') != 'posts')
				{
					$id = get_option('page_for_posts');
				}
			}

			self::$gallery_type = ether::meta('gallery_template', TRUE, $id);

			if ( ! empty(self::$gallery_type) AND self::$gallery_type != 'none')
			{
				self::$gallery_options = array
				(
					'align' => ether::meta('gallery_align', TRUE, $id),
					'width' => ether::meta('gallery_width', TRUE, $id),
					'columns' => ether::meta('gallery_columns', TRUE, $id),
					'rows' =>  ether::meta('gallery_rows', TRUE, $id),
					'image_height' => ether::meta('gallery_image_height', TRUE, $id),
					'thumbs_align' => ether::meta('gallery_thumbs_align', TRUE, $id)
				);

				if (substr(self::$gallery_type, 0, 4) != 'list')
				{
					self::$images = unserialize(ether::meta('gallery_images'));

					if (empty(self::$images))
					{
						self::$images = array();
					}
				}
			}
		}

		public static function is_list()
		{
			global $post;

			return ($post->post_parent == 0);
		}

		public static function sort_featured($a, $b)
		{
			$a_featured = (ether::meta('featured', TRUE, $a->ID) == 'yes');
			$b_featured = (ether::meta('featured', TRUE, $b->ID) == 'yes');

			if ($a_featured != $b_featured)
			{
				return ($a_featured ? -1 : 1);
			}

			if ($a_featured AND $b_featured)
			{
				return -1;
			}

			return 0;
		}

		public static function list_begin($per_page = 10)
		{
			global $post;
			global $wp_query;

			if ($post->post_parent == 0)
			{
				$id = $post->ID;
				$paged = get_query_var('paged') ? get_query_var('paged') : 1;
				$offset = ($per_page * $paged) - $per_page;

				query_posts(array
				(
					'post_type' => 'gallery',
					'post_parent' => $id,
					'offset' => $offset,
					'posts_per_page' => $per_page,
					'paged' => $paged,
					'orderby' => 'date',
					'order' => 'DESC'
				));
			}
		}

		public static function list_end()
		{
			wp_reset_query();
		}
	}
?>
