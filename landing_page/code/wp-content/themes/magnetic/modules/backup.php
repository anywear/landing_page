<?php
	session_start();

	class ether_backup extends ether_module
	{
		protected static $tables;
		protected static $tables_reset;
		protected static $tables_rules;

		public static function init()
		{
			ether::bind('ether.post', array('ether_backup', 'post'));

			self::$tables = array
			(
				'commentmeta',
				'comments',
				'links',
				'options',
				'postmeta',
				'posts',
				'terms',
				'term_relationships',
				'term_taxonomy',
				//'usermeta',
				//'users',
				'ether_sidebars',
				'ether_sidebar_relations'
			);
			
			self::$tables_reset = array
			(
				'commentmeta',
				'comments',
				'links',
				'postmeta',
				'posts',
				'terms',
				'term_relationships',
				'term_taxonomy',
				'ether_sidebars',
				'ether_sidebar_relations'
			);
			
			self::$tables_rules = array
			(
				'commentmeta' => array('type' => 'insert', 'clean' => TRUE),
				'comments' => array('type' => 'insert', 'clean' => TRUE),
				'links' => array('type' => 'insert', 'clean' => TRUE),
				'options' => array('type' => 'update', 'base64' => array('option_value'), 'compare_key' => 'option_name', 'compare_index' => 1, 'value_key' => 'option_value', 'value_index' => 2, 'check_callback' => array('ether_backup', 'not_readonly')),
				'postmeta' => array('type' => 'insert', 'base64' => array('meta_value'), 'clean' => TRUE),
				'posts' => array('type' => 'insert', 'clean' => TRUE),
				'terms' => array('type' => 'insert', 'clean' => TRUE),
				'term_relationships' => array('type' => 'insert', 'clean' => TRUE),
				'term_taxonomy' => array('type' => 'insert', 'clean' => TRUE),
				'usermeta' => array('type' => 'insert'),
				'users' => array('type' => 'insert'),
				'ether_sidebars' => array('type' => 'insert', 'clean' => TRUE),
				'ether_sidebar_relations' => array('type' => 'insert', 'clean' => TRUE)
			);
		}
		
		public static function setup()
		{
			set_time_limit(0);
			ini_set('upload_max_size', '1000M');
			ini_set('post_max_size', '995M');
			ini_set('max_execution_time', 300);
		}
		
		public static function not_readonly($row)
		{
			$readonly = array
			(
				'siteurl',
				'home',
				'active_plugins',
				'admin_email'
			);

			if (in_array($row[1], $readonly))
			{
				return FALSE;
			}
			
			return TRUE;
		}

		public static function post($data)
		{
			if (is_user_logged_in() AND current_user_can('administrator'))
			{
				self::setup();

				header('Content-type: text/json');

				if (isset($data['backup']))
				{
					if (isset($data['init']))
					{
						$_SESSION['timestamp'] = date('Y-m-d-h-i-s');
						$_SESSION['table_index'] = 0;
						
						echo json_encode(array('backup' => TRUE, 'timestamp' => $_SESSION['timestamp'], 'next_table' => self::$tables[$_SESSION['table_index']]));
					} else
					{
						if (isset($_SESSION['timestamp']) AND ! empty($_SESSION['timestamp']) AND isset($data['timestamp']))
						{
							if ($data['timestamp'] == $_SESSION['timestamp'])
							{
								if (isset($data['table']))
								{
									if (ether::backup_table($data['table'], $_SESSION['timestamp'], self::$tables_rules[$data['table']]))
									{
										$_SESSION['table_index']++;
										
										if ($_SESSION['table_index'] < count(self::$tables))
										{
											echo json_encode(array('backup' => TRUE, 'table' => $data['table'], 'next_table' => self::$tables[$_SESSION['table_index']]));
										} else
										{
											$path = rtrim(ether::dir('backup/database/', TRUE), '/').'/'.$_SESSION['timestamp'].'/url';
											ether::write($path, get_site_url());

											echo json_encode(array('backup' => TRUE, 'database' => TRUE));
										}
									} else
									{
										echo json_encode(array('backup' => TRUE, 'error' => TRUE, 'table' => $data['table']));
									}
								} else if (isset($data['media']))
								{
									if (ether::backup_uploads($_SESSION['timestamp']))
									{
										echo json_encode(array('backup' => TRUE, 'media' => TRUE));
									} else
									{
										echo json_encode(array('backup' => TRUE, 'error' => TRUE, 'media' => FALSE));
									}
								}
							} else
							{
								echo '-2';
							}
						}
					}
				} else if (isset($data['restore']))
				{
					if (isset($data['init']) AND isset($data['timestamp']))
					{
						$path = rtrim(ether::dir('backup/database/', TRUE), '/').'/';
						$path .= $data['timestamp'];

						if (is_dir($path))
						{
							$_SESSION['timestamp'] = $data['timestamp'];
							$_SESSION['table_index'] = 0;
							
							echo json_encode(array('restore' => TRUE, 'timestamp' => $_SESSION['timestamp'], 'next_table' => self::$tables[$_SESSION['table_index']]));
						}
					} else
					{
						if (isset($_SESSION['timestamp']) AND ! empty($_SESSION['timestamp']) AND isset($data['timestamp']))
						{
							if ($data['timestamp'] == $_SESSION['timestamp'])
							{
								if (isset($data['table']))
								{
									if (ether::restore_table($_SESSION['timestamp'], $data['table'], self::$tables_rules[$data['table']]))
									{
										$_SESSION['table_index']++;
										
										if ($_SESSION['table_index'] < count(self::$tables))
										{
											echo json_encode(array('restore' => TRUE, 'table' => $data['table'], 'next_table' => self::$tables[$_SESSION['table_index']]));
										} else
										{
											$path = rtrim(ether::dir('backup/database/', TRUE), '/').'/'.$_SESSION['timestamp'].'/url';
											$url = trim(ether::read($path));
											$current_url = get_site_url();
											
											if ( ! empty($url) AND preg_match('|^http(s)?://[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i', $url) AND $url != get_site_url())
											{
												ether::update_url($url, $current_url);
												global $wpdb;

												$wpdb->query('UPDATE `'.$wpdb->options.'` SET `option_value`=REPLACE(`option_value`, \''.$url.'\', \''.$current_url.'\') WHERE `option_name`=\'ether_favicon\' OR `option_name`=\'ether_image_logo\'');
												$wpdb->query('UPDATE `'.$wpdb->postmeta.'` SET `meta_value`=REPLACE(`meta_value`, \''.$url.'\', \''.$current_url.'\') WHERE `meta_key`=\'_menu_item_url\' OR `meta_key`=\'ether_actionbar_html\' OR `meta_key`=\'ether_url\' OR `meta_key`=\'ether_preview_image\'');
												$galleries = $wpdb->get_results('SELECT `meta_id`, `meta_value` FROM `'.$wpdb->postmeta.'` WHERE `meta_key`=\'ether_gallery_images\'');
												
												foreach ($galleries as $gallery_meta)
												{
													$double_serialized = FALSE;

													$gallery = unserialize($gallery_meta->meta_value);
													
													if (is_string($gallery))
													{
														$gallery = unserialize($gallery);
														$double_serialized = TRUE;
													}

													if ( ! empty($gallery))
													{
														$count = count($gallery);
														
														for ($i = 0; $i < $count; $i++)
														{
															if (isset($gallery[$i]['ether_gallery_alt']))
															{
																$gallery[$i]['ether_gallery_alt'] = str_replace($url, $current_url, $gallery[$i]['ether_gallery_alt']);
															}
															
															if (isset($gallery[$i]['ether_gallery_image']))
															{
																$gallery[$i]['ether_gallery_image'] = str_replace($url, $current_url, $gallery[$i]['ether_gallery_image']);
															}
														}
														
														$wpdb->query('UPDATE `'.$wpdb->postmeta.'` SET `meta_value`=\''.serialize(($double_serialized ? serialize($gallery) : $gallery)).'\' WHERE `meta_id`=\''.$gallery_meta->meta_id.'\'');
													}
												}
											}

											echo json_encode(array('restore' => TRUE, 'database' => TRUE));
										}
									} else
									{
										echo json_encode(array('restore' => TRUE, 'error' => TRUE, 'table' => $data['table']));
									}
								} else if (isset($data['media']))
								{
									if (ether::restore_uploads($_SESSION['timestamp']))
									{
										echo json_encode(array('restore' => TRUE, 'media' => TRUE));
									} else
									{
										echo json_encode(array('restore' => TRUE, 'error' => TRUE, 'media' => FALSE));
									}
								}
							} else
							{
								echo '-2';
							}
						}
					}
				}
			} else
			{
				echo '-1';
			}
		}
	}

?>
