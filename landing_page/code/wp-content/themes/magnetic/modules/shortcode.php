<?php
	class ether_shortcode extends ether_module
	{
		private static $tabs = NULL;
		private static $toggle = NULL;
		private static $toggle_mode = FALSE;

		public static function init()
		{
			$shortcodes = array('alert', 'button', 'quote', 'testimonials', 'success_stories', '_list', 'video', 'divider', 'contact', 'services', 'inline', 'pricing_table', 'columns', 'half', 'third', 'fourth', 'two-third', 'three-fourth', 'twitter', 'tabs', 'tab', 'toggle', 'content');

			foreach ($shortcodes as $shortcode)
			{
				ether::register_shortcode(trim($shortcode, ' _-'), array('ether_shortcode', str_replace('-', '_', $shortcode)));
			}
		}

		public static function alert($args, $content)
		{
			extract(shortcode_atts(array
			(
				'type' => 'info',
				'width' => '100%',
				'align' => 'center',
				'background' => ''
			), $args));

			return '<div class="box '.$type.' align'.$align.'" style="width: '.$width.'"><span'.( ! empty($background) ? ' style="background-image: url('.$background.');"' : '').'>'.$content.'</span> <a class="close" href="#close">close</a></div>';
		}

		public static function button($args, $content)
		{
			extract(shortcode_atts(array
			(
				'url' => '',
				'align' => 'center',
				'color' => '',
				'width' => '',
				'style' => '',
			), $args));

			return '<a href="'.$url.'" class="button default'.( ! empty($style) ? '-'.$style : '').' align'.$align.'" style="'.( ! empty($color) ? 'background-color: '.$color.';' : '').( ! empty($width) ? 'width: '.$width.';' : '').'">'.$content.'</a>';
		}

		public static function quote($args, $content)
		{
			extract(shortcode_atts(array
			(
				'type' => 'block',
				'align' => 'center',
				'width' => '',
				'style' => '1'
			), $args));

			if ($type == 'block')
			{
				return '<blockquote class="style-1"'.( ! empty($width) ? ' style="width: '.$width.';"' : '').'>'.wpautop($content).'</blockquote>';
			} else
			{
				return '<q class="pull-quote style-'.$style.' align'.$align.'"'.( ! empty($width) ? ' style="width: '.$width.';"' : '').'><span>'.$content.'</span></q>';
			}
		}

		public static function inline($args, $content)
		{
			extract(shortcode_atts(array
			(
				'tooltip' => '',
				'icon_align' => 'none',
				'icon' => '',
				'url' => ''
			), $args));

			$content = trim(strip_tags($content));

			if ( ! empty($icon_align) AND $icon_align != 'none')
			{
				if ($icon_align == 'left')
				{
					$content = '<img class="inline-icon" src="'.ether::path('media/images/icons/'.$icon.'.png', TRUE).'" alt="Icon" /> '.$content;
				} else if ($icon_align == 'right')
				{
					$content = $content .= ' <img class="inline-icon" src="'.ether::path('media/images/icons/'.$icon.'.png', TRUE).'" alt="Icon" />';
				}
			}

			if ( ! empty($url))
			{
				$content = '<a href="'.$url.'"'.($tooltip != '' ? ' class="has-tooltip"' : '').'>'.$content.($tooltip != '' ? '<span class="tooltip">'.$tooltip.'</span>' : '').'</a>';
			} else
			{
				if ( ! empty($tooltip))
				{
					$content = '<span class="has-tooltip">'.$content.'<span class="tooltip">'.$tooltip.'</span></span>';
				}
			}

			return $content;
		}

		public static function testimonials($args, $content)
		{
			extract(shortcode_atts(array
			(
				'title' => '',
				'id' => '',
				'name' => '',
				'category_id' => '',
				'category_name' => '',
				'columns' => '',
				'author' => 'name-and-company',
				'words' => 100,
				'slider' => 'true'
			), $args));

			$select_by = 'id';
			$slugs = '';

			if ( ! empty($id))
			{
				$slugs = $id;
				$select_by = 'id';
			} else if ( ! empty($name))
			{
				$slugs = $name;
				$select_by = 'name';
			} else if ( ! empty($category_id))
			{
				$slugs = $category_id;
				$select_by = 'category_id';
			} else if ( ! empty($category_name))
			{
				$slugs = $category_name;
				$select_by = 'category_name';
			} else
			{
				$select_by = '';
			}

			if ( ! empty($select_by))
			{
				return ether_widget::testimonials($title, $slugs, '', $author, $slider == 'true', $columns, $select_by, $words);
			}

			return '';
		}

		public static function success_stories($args, $content)
		{
			extract(shortcode_atts(array
			(
				'title' => '',
				'id' => '',
				'name' => '',
				'category_id' => '',
				'category_name' => '',
				'large' => 'true'
			), $args));

			$select_by = 'id';
			$slugs = '';

			if ( ! empty($id))
			{
				$slugs = $id;
				$select_by = 'id';
			} else if ( ! empty($name))
			{
				$slugs = $name;
				$select_by = 'name';
			} else if ( ! empty($category_id))
			{
				$slugs = $category_id;
				$select_by = 'category_id';
			} else if ( ! empty($category_name))
			{
				$slugs = $category_name;
				$select_by = 'category_name';
			} else
			{
				$select_by = '';
			}

			if ( ! empty($select_by))
			{
				return ether_widget::success_stories($title, $slugs, '', $large == 'true', $select_by);
			}

			return '';
		}

		public static function _list($args, $content)
		{
			extract(shortcode_atts(array
			(
				'style' => 'check-1'
			), $args));

			preg_match_all('/<ul(.*)>/i', $content, $match);

			if (empty($match[0]))
			{
				$content_li = explode("\n", $content);
				$list = array();

				foreach ($content_li as $li)
				{
					$li = trim($li);

					if ( ! empty($li))
					{
						$list[] = $li;
					}
				}

				if (empty($list))
				{
					return $content;
				}

				return '<ul class="style '.ether::slug($style).'"><li>'.implode('</li><li>', $list).'</li></ul>';
			}

			return ether::set_attr('ul', 'class', 'style '.ether::slug($style), $content, TRUE);
		}

		public static function video($args, $content)
		{
			extract(shortcode_atts(array
			(
				'url' => '',
				'width' => '',
				'height' => '',
				'align' => 'center'
			), $args));

			$url = trim($url);

			preg_match('@^(?:http://)?(?:www.)?([^/]+)@i', $url, $matches);

			if ($matches[1] == 'video.google.com')
			{
				$params = explode('?', $url);
				parse_str(html_entity_decode($params[1]), $params);

				foreach ($params as $k => $v)
				{
					if (strtolower($k) == 'docid')
					{
						$url = 'http://video.google.com/googleplayer.swf?docid='.$v;

						break;
					}
				}
			} else if ($matches[1] == 'youtube.com')
			{
				$params = explode('?', $url);
				parse_str(html_entity_decode($params[1]), $params);

				foreach ($params as $k => $v)
				{
					if (strtolower($k) == 'v')
					{
						$url = 'http://www.youtube.com/embed/'.$v;

						break;
					}
				}
			} else if ($matches[1] == 'vimeo.com')
			{
				preg_match('/(\d+)/', $url, $id);

				if ( ! empty($id[1]))
				{
					$url = 'http://player.vimeo.com/video/'.$id[1];
				}
			} else if ($matches[1] == 'blip.tv')
			{
				preg_match('/file\/(\d+)\//', $url, $id);

				if ( ! empty($id[1]))
				{
					$url = 'http://blip.tv/play/'.$id[1];
				}
			}

			return '<iframe src="'.$url.'" class="align'.$align.' media" width="'.( ! empty($width) ? $width : '480').'" height="'.( ! empty($height) ? $height : '290').'" frameborder="0"></iframe>';
		}

		public static function divider($args, $content)
		{
			if (isset($args['top']))
			{
				return '<a href="#header" class="hr"><span>'.ether::langr('Top', 'divider').'</span></a>';
			}

			return '<span class="hr"></span>';
		}

		public static function contact($args, $content)
		{
			extract(shortcode_atts(array
			(
				'title' => '',
				'email' => ether::option('admin_email'),
				'callus_text' => '',
				'callus_url' => '',
				'button_text' => ether::langr('Send'),
				'success_message' => ether::langr('Message sent').'.'
			), $args));

			return ether_widget::contact($title, $email, $callus_text, $callus_url, $button_text, $success_message);
		}

		public static function services($args, $content)
		{
			extract(shortcode_atts(array
			(
				'columns' => 4,
				'words' => 55,
				'id' => ''
			), $args));

			$col = $columns;

			if ( ! empty($columns))
			{
				if ($columns > 1 AND $columns < 5)
				{
					$cols = array('two', 'three', 'four');

					$columns = ' '.$cols[$columns - 2].'-cols';
				} else
				{
					$columns = '';
				}
			}

			$output = '';

			$args = array
			(
				'post_type' => 'service',
				'text_opt' => 'content',
				'nopaging' => TRUE,
				'orderby' => 'custom'
			);

			$args['post__in'] = ether::slugify($id, TRUE);

			$services = ether::get_posts($args);
			$count = count($services);
			$counter = 0;

			for ($i = 0; $i < $count; $i++)
			{
				if ($i == 0 OR $i % $col == 0)
				{
					$output .= '<div class="services'.( ! empty($columns) ? $columns : '').'"><ul class="items">';
				}

				$service = $services[$i];

				$preview_image = ether::meta('preview_image', TRUE, $service['id']);
				$preview_alt = ether::meta('preview_alt', TRUE, $service['id']);

				if ( ! empty($preview_image))
				{
					$preview_image = ether::get_image_thumbnail($preview_image, 62, 49);
				}

				if (empty($preview_image))
				{
					$preview_image = ether::path('media/images/placeholders/services.png', TRUE);
				}

				$url = ether::meta('url', TRUE, $service['id']);

				$output .= '<li>';
				$output .= '<img src="'.$preview_image.'" alt="'.$preview_alt.'" width="62" height="49" />';
				$output .= '<div>';

				if ( ! empty($url))
				{
					$output .= '<h4><a href="'.$url.'">'.$service['title'].'</a></h4>';
				} else
				{
					$output .= '<h4>'.$service['title'].'</h4>';
				}

				$output .= wpautop($services['content']);
				//$output .= wpautop(ether::trim_words(strip_tags($service['content']), $words, TRUE));
				$output .= '</div>';
				$output .= '</li>';

				$counter++;

				if ($counter == $col)
				{
					$output .= '</ul>';
					$output .= '</div>';
					$counter = 0;
				}
			}

			if ($counter != 0)
			{
				$output .= '</ul>';
				$output .= '</div>';
			}

			return $output;
		}

		public static function pricing_table($args, $content)
		{
			extract(shortcode_atts(array
			(
				'id' => ''
			), $args));

			$output = '';

			$output .= '<table class="pricing" cellspacing="0"><tr>';

			$args = array
			(
				'post_type' => 'pricing-column',
				'text_opt' => 'content',
				'nopaging' => TRUE,
				'orderby' => 'custom'
			);

			$args['post__in'] = ether::slugify($id, TRUE);

			$columns = ether::get_posts($args);

			$width = intval(100 / count($columns));

			foreach ($columns as $column)
			{
				$output .= '<td width="'.$width.'%"><table cellspacing="0">';

				$output .= '<tr><th>'.$column['title'].'</th></tr>';
				$output .= '<tr><td>'.strip_tags(do_shortcode($column['content'])).'</td></tr>';

				$features = unserialize(ether::meta('features', TRUE, $column['id']));

				if ( ! is_array($features))
				{
					$features = array();
				}

				foreach ($features as $feature)
				{
					$output .= '<tr><td class="alt">';
					$output .= $feature[ether::config('prefix').'features_title'];
					$output .= '</td></tr>';
				}

				$output .= '<tr><td class="price">';
				$output .= '<sup>'.ether::meta('currency', TRUE, $column['id']).'</sup><strong>'.ether::meta('price', TRUE, $column['id']).ether::meta('term', TRUE, $column['id']).'</strong>';
				$output .= '</td></tr>';

				$color = ether::meta('button_color', TRUE, $column['id']);
				$output .= '<tr><td><a href="'.ether::meta('button_url', TRUE, $column['id']).'" class="aligncenter button default caps wide"'.( ! empty($color) ? ' style="background-color: '.$color.';"' : '').'>'.ether::meta('button_text', TRUE, $column['id']).'</a></td></tr>';

				$output .= '</table></td>';
			}

			$output .= '</tr></table>';

			return $output;
		}

		public static function columns($args, $content)
		{
			return '<div class="columns">'.do_shortcode($content).'</div>';
		}

		public static function half($args, $content)
		{
			return '<div class="half">'.do_shortcode($content).'</div>';
		}

		public static function third($args, $content)
		{
			return '<div class="third">'.do_shortcode($content).'</div>';
		}

		public static function fourth($args, $content)
		{
			return '<div class="fourth">'.do_shortcode($content).'</div>';
		}

		public static function two_third($args, $content)
		{
			return '<div class="two-third">'.do_shortcode($content).'</div>';
		}

		public static function three_fourth($args, $content)
		{
			return '<div class="three-fourth">'.do_shortcode($content).'</div>';
		}

		public static function twitter($args, $content)
		{
			extract(shortcode_atts(array
			(
				'username' => ether::option('social_twitter'),
				'count' => 10,
				'title' => ''
			), $args));

			return ether_widget::twitter($title, $username, $count, TRUE);
		}

		public static function tabs($args, $content)
		{
			extract(shortcode_atts(array
			(
				'current' => 1
			), $args));

			self::$tabs = array();

			do_shortcode($content);

			$tabs_nav = '<ul class="nav">';
			$tabs_content = '<ul class="content">';

			$counter = 0;

			foreach (self::$tabs as $tab)
			{
				$tabs_nav .= '<li'.(($counter + 1) == $current ? ' class="current"' : '').'><a href="#tab'.($counter + 1).'">'.$tab['title'].'</a></li>';
				$tabs_content .= '<li>'.wpautop($tab['content']).'</li>';
				$counter++;
			}

			$tabs_nav .= '</ul>';
			$tabs_content .= '</ul>';

			return '<div class="tabs">'.$tabs_nav.$tabs_content.'</div>';
		}

		public static function tab($args, $content)
		{
			extract(shortcode_atts(array
			(
				'title' => ether::langr('Tab')
			), $args));

			self::$tabs[] = array('title' => $title, 'content' => do_shortcode($content));
		}

		public static function toggle($args, $content)
		{
			self::$toggle = array();

			self::$toggle_mode = TRUE;
			do_shortcode($content);
			self::$toggle_mode = FALSE;

			// wordpress keeps adding unclosed <p> tag before each <a> tag without raw shortcode
			$toggle = ' <div class="toggle">';

			$counter = 0;

			foreach (self::$toggle as $toggle_content)
			{
				$toggle .= '<a href="#toggle'.($counter + 1).'"'.($toggle_content['closed'] ? ' class="closed"' : '').'><span>'.$toggle_content['title'].'</span></a>';
				$toggle .= '<div id="toggle'.($counter + 1).'" class="toggle-content">'.wpautop($toggle_content['content']).'</div>';

				$counter++;
			}

			$toggle .= '</div> ';

			return $toggle;
		}

		public static function content($args, $content)
		{
			if (self::$toggle_mode)
			{
				extract(shortcode_atts(array
				(
					'title' => ether::langr('Toggle'),
					'closed' => 1
				), $args));

				self::$toggle[] = array('title' => $title, 'content' => do_shortcode($content), 'closed' => $closed);
			} else
			{
				return $content;
			}
		}
	}
?>
