<?php
	class ether_header extends ether_module
	{
		public static $headers = array();
		public static $align = array();
		public static $autoplay = array();
		public static $nivo_effect = array();
		public static $carousel_effect = array();
		public static $piecemaker_transition = array();
		public static $onoff = array();
		public static $header_fields = array();
		public static $header_name = NULL;
		public static $header_options = array();
		public static $slides = array();
		public static $from_category = FALSE;
		
		public static function init()
		{
			self::$headers = array
			(
				'none' => array
				(
					'name' => ether::langx('None', 'header type', TRUE)
				),
				'slider' => array
				(
					'name' => ether::langx('Slider - Text + Image', 'header type', TRUE)
				),
				'nivo' => array
				(
					'name' => ether::langx('Slider - Nivo', 'header type', TRUE)
				),
				'carousel' => array
				(
					'name' => ether::langx('Slider - Carousel', 'header type', TRUE)
				),
				'piecemaker' => array
				(
					'name' => ether::langx('Slider - 3D', 'header type', TRUE)
				),
				'flickr' => array
				(
					'name' => ether::langx('Header - Flickr feed', 'header type', TRUE)
				),
				'twitter' => array
				(
					'name' => ether::langx('Header - Twitter', 'header type', TRUE)
				),
				'html' => array
				(
					'name' => ether::langx('Header - HTML', 'header type', TRUE)
				)
			);

			self::$align = array
			(
				'left' => array
				(
					'name' => ether::langx('Left', 'header align', TRUE)
				),
				'right' => array
				(
					'name' => ether::langx('Right', 'header align', TRUE)
				)
			);
			
			self::$autoplay = array
			(
				'0' => array('name' => ether::langx('Off', 'header autoplay option', TRUE)),
				'5' => array('name' => '5s'),
				'10' => array('name' => '10s'),
				'15' => array('name' => '15s'),
				'20' => array('name' => '20s'),
				'25' => array('name' => '25s')
			);

			self::$nivo_effect = array
			(
				'default' => array
				(
					'name' => ether::langx('Default', 'nivo header effect', TRUE)
				),
				'sliceDown' => array
				(
					'name' => ether::langx('Slice down', 'nivo header effect', TRUE)
				),
				'sliceDownLeft' => array
				(
					'name' => ether::langx('Slice down-left', 'nivo header effect', TRUE)
				),
				'sliceUp' => array
				(
					'name' => ether::langx('Slice up', 'nivo header effect', TRUE)
				),
				'sliceUpLeft' => array
				(
					'name' => ether::langx('Slice up-left', 'nivo header effect', TRUE)
				),
				'sliceUpDown' => array
				(
					'name' => ether::langx('Slice up-down', 'nivo header effect', TRUE)
				),
				'sliceUpDownLeft' => array
				(
					'name' => ether::langx('Slice up-down-left', 'nivo header effect', TRUE)
				),
				'fold' => array
				(
					'name' => ether::langx('Fold', 'nivo header effect', TRUE)
				),
				'fade' => array
				(
					'name' => ether::langx('Fade', 'nivo header effect', TRUE)
				),
				'random' => array
				(
					'name' => ether::langx('Random', 'nivo header effect', TRUE)
				)
			);
				
			self::$carousel_effect = array
			(
				'swing' => array('name' => ether::langx('Swing', 'carousel header effect', TRUE)),
				'bounce' => array('name' => ether::langx('Bounce', 'carousel header effect', TRUE)),
				'elastic' => array('name' => ether::langx('Elastic', 'carousel header effect', TRUE)),
				'smooth' => array('name' => ether::langx('Smooth', 'carousel header effect', TRUE)),
				'lunge' => array('name' => ether::langx('Lunge', 'carousel header effect', TRUE))
			);
				
			self::$piecemaker_transition = array
			(
				'linear' => array('name' => ether::langx('Linear', 'piecemaker header transition', TRUE)),
				'easeInSine' => array('name' => ether::langx('Ease in Sine', 'piecemaker header transition', TRUE)),
				'easeOutSine' => array('name' => ether::langx('Ease out Sine', 'piecemaker header transition', TRUE)),
				'easeInQuad' => array('name' => ether::langx('Ease in Quad', 'piecemaker header transition', TRUE)),
				'easeOutQuad' => array('name' => ether::langx('Ease out Quad', 'piecemaker header transition', TRUE)),
				'easeInCubic' => array('name' => ether::langx('Ease in Cubic', 'piecemaker header transition', TRUE)),
				'easeOutCubic' => array('name' => ether::langx('Ease out Cubic', 'piecemaker header transition', TRUE)),
				'easeInQuart' => array('name' => ether::langx('Ease in Quart', 'piecemaker header transition', TRUE)),
				'easeOutQuart' => array('name' => ether::langx('Ease out Quart', 'piecemaker header transition', TRUE)),
				'easeInQuint' => array('name' => ether::langx('Ease in Quint', 'piecemaker header transition', TRUE)),
				'easeOutQuint' => array('name' => ether::langx('Ease out Quint', 'piecemaker header transition', TRUE)),
				'easeInExpo' => array('name' => ether::langx('Ease in Expo', 'piecemaker header transition', TRUE)),
				'easeOutExpo' => array('name' => ether::langx('Ease out Expo', 'piecemaker header transition', TRUE)),
				'easeInCirc' => array('name' => ether::langx('Ease in Circ', 'piecemaker header transition', TRUE)),
				'easeOutCric' => array('name' => ether::langx('Ease out Circ', 'piecemaker header transition', TRUE)),
				'easeInElastic' => array('name' => ether::langx('Ease in Elastic', 'piecemaker header transition', TRUE)),
				'easeOutElastic' => array('name' => ether::langx('Ease out Elastic', 'piecemaker header transition', TRUE)),
				'easeInBack' => array('name' => ether::langx('Ease in Back', 'piecemaker header transition', TRUE)),
				'easeOutBack' => array('name' => ether::langx('Ease out Back', 'piecemaker header transition', TRUE)),
				'easeInBounce' => array('name' => ether::langx('Ease in Bounce', 'piecemaker header transition', TRUE)),
				'easeOutBounce' => array('name' => ether::langx('Ease out Bounce', 'piecemaker header transition', TRUE))
			);
				
			self::$onoff = array
			(
				'0' => array('name' => ether::langr('Off')),
				'1' => array('name' => ether::langr('On'))
			);
			
			self::$header_fields = array
			(
				'none' => array(),
				'slider' => array
				(
					'slider_align',
					'slider_autoplay'
				),
				'nivo' => array
				(
					'nivo_effect',
					'nivo_autoplay',
					'nivo_width',
					'nivo_height'
				),
				'carousel' => array
				(
					'carousel_mouse',
					'carousel_autoplay',
					'carousel_animation',
					'carousel_width'
				),
				'piecemaker' => array
				(
					'piecemaker_autoplay',
					'piecemaker_width',
					'piecemaker_height',
					'piecemaker_pieces',
					'piecemaker_time',
					'piecemaker_transition',
					'piecemaker_delay',
					'piecemaker_depth',
					'piecemaker_distance'
				),
				'flickr' => array
				(
					'flickr_id'
				),
				'twitter' => array
				(
					'twitter_username'
				),
				'html' => array
				(
					'html_code'
				)
			);
		}
		
		public static function get_header()
		{
			if (self::is_header())
			{
				ether::import('template.header.'.self::$header_name);
			}
		}
		
		public static function is_header()
		{
			return ( ! empty(self::$header_name) AND self::$header_name != 'none');
		}
		
		public static function get_option($name)
		{
			$key = ether::config('prefix').self::$header_name.'_'.$name;
			
			if (isset(self::$header_options[$key]))
			{
				return self::$header_options[$key];
			}
			
			return NULL;
		}
		
		public static function get_slides($image_width, $image_height, $ids = array())
		{
			$slides = array();

			function get_slide_id($a)
			{
				return $a[ether::config('prefix').(ether_header::$from_category ? 'category_' : '').'slides_id'];
			}
			
			if (empty($ids))
			{
				$ids = array_map('get_slide_id', self::$slides);
			}
			
			$posts = ether::get_posts(array('post_type' => 'slide', 'text_opt' => 'content', 'post__in' => $ids, 'nopaging' => TRUE, 'orderby' => 'custom'));
			
			foreach ($posts as $slide)
			{
				$preview_image = ether::meta('preview_image', TRUE, $slide['id']);
				$preview_alt = ether::meta('preview_alt', TRUE, $slide['id']);
				
				if ( ! empty($preview_image))
				{
					$preview_image = ether::get_image_thumbnail($preview_image, $image_width, $image_height);
					$preview_image_base = ether::get_image_base($preview_image);
				}
				
				if (empty($preview_image))
				{
					$preview_image = ether::path('media/images/placeholders/placeholder.png', TRUE);
					$preview_image_base = $preview_image;
				}
				
				$url = ether::meta('url', TRUE, $slide['id']);
				
				if (empty($url))
				{
					$url = $preview_image;
				}
				
				$slides[] = array
				(
					'id' => $slide['id'],
					'title' => $slide['title'],
					'content' => do_shortcode($slide['content']),
					'image' => $preview_image,
					'image_base' => $preview_image_base,
					'image_alt' => $preview_alt,
					'image_width' => $image_width,
					'image_height' => $image_height,
					'url' => $url
				);
			}
			
			return $slides;
		}

		public static function setup()
		{
			self::$header_name = NULL;
			self::$header_options = array();
			self::$slides = array();

			global $post;
			$id = NULL;
			$term = NULL;
			
			if (is_single())
			{
				$id = $post->ID;
				
				if ($post->post_type == 'post')
				{
					$term = 'category';
				} else if ($post->post_type == 'news')
				{
					$term = 'news_category';
				}
			} else if (is_page() OR get_option('show_on_front') != 'posts')
			{
				if (is_page())
				{
					$id = $post->ID;
				} else if (is_home() AND get_option('show_on_front') != 'posts')
				{
					$id = get_option('page_for_posts');
				}
			}
			
			self::$header_options = unserialize(ether::meta('header_options', TRUE, $id));
			self::$header_name = self::$header_options[ether::config('prefix').'header'];
			unset(self::$header_options[ether::config('prefix').'header']);
			
			if ((empty(self::$header_name) OR self::$header_name == 'inherited') AND ! empty($term))
			{
				self::$header_name = '';
				$terms = get_the_terms($id, $term);
				
				if (count($terms) > 0)
				{
					self::$from_category = TRUE;
					$category = array_shift($terms);
					
					self::$header_options = unserialize(ether::option('category_'.$category->term_id.'_header_options'));
					
					if ( ! empty(self::$header_options))
					{
						self::$header_name = self::$header_options[ether::config('prefix').'category_header'];
						unset(self::$header_options[ether::config('prefix').'category_header']);
						
						if ( ! empty(self::$header_name))
						{
							self::$slides = unserialize(ether::option('category_'.$category->term_id.'_slides'));
						}
					}
				}
			} else
			{
				if ( ! empty(self::$header_name) OR self::$header_name != 'inherited')
				{
					self::$slides = unserialize(ether::meta('slides'));
				}
			}
			
			if (empty(self::$slides))
			{
				self::$slides = array();
			}
		}
		
		public static function piecemaker_xml($data)
		{
			if (isset($data['piecemaker']))
			{
				$width = $data['width'];
				$height = $data['height'];
				
				if ( ! empty($width))
				{
					$width = intval($width);
				} else
				{
					$width = 900;
				}
					
				if ( ! empty($height))
				{
					$height = intval($height);
				} else
				{
					$height = 360;
				}
				
				$slides = self::get_slides($width, $height, explode(',', $data['slides']));
				
				$output = '<?xml version="1.0" encoding="utf-8"?>';
				$output .= '<Piecemaker>';
				$output .= '<Contents>';
				
				foreach ($slides as $slide)
				{
					$url = ether::meta('url', TRUE, $slide['id']);
					$output .= '<Image Source="'.$slide['image'].'" Title="'.$slide['image_alt'].'">
						<Text>'.htmlspecialchars($slide['content']).'</Text>
						'.( ! empty($url) ? '<Hyperlink URL="'.$url.'" Target="_blank" />' : '').'
					</Image>';
				}
				
				$output .= '</Contents>';
				$output .= '<Settings ImageWidth="'.$width.'" ImageHeight="'.$height.'" LoaderColor="0x333333" InnerSideColor="0x222222" SideShadowAlpha="0.8" DropShadowAlpha="0.7" DropShadowDistance="'.$data['distance'].'" DropShadowScale="0.95" DropShadowBlurX="40" DropShadowBlurY="4" MenuDistanceX="20" MenuDistanceY="50" MenuColor1="0x999999" MenuColor2="0x333333" MenuColor3="0xFFFFFF" ControlSize="100" ControlDistance="20" ControlColor1="0x222222" ControlColor2="0xFFFFFF" ControlAlpha="0.8" ControlAlphaOver="0.95" ControlsX="'.($width/2).'" ControlsY="'.($height/2).'" ControlsAlign="center" TooltipHeight="30" TooltipColor="0x222222" TooltipTextY="5" TooltipTextStyle="P-Italic" TooltipTextColor="0xFFFFFF" TooltipMarginLeft="5" TooltipMarginRight="7" TooltipTextSharpness="50" TooltipTextThickness="-100" InfoWidth="400" InfoBackground="0xFFFFFF" InfoBackgroundAlpha="0.95" InfoMargin="15" InfoSharpness="0" InfoThickness="0"'.( ! empty($data['autoplay']) ? ' Autoplay="'.$data['autoplay'].'"' : '').' FieldOfView="45"></Settings>';
				$output .= '<Transitions>';
				
				foreach ($slides as $slide)
				{
					$output .= '<Transition Pieces="'.$data['pieces'].'" Time="'.$data['time'].'" Transition="'.$data['transition'].'" Delay="'.$data['delay'].'" DepthOffset="'.$data['depth'].'" CubeDistance="30"></Transition>';
				}
				
				$output .= '</Transitions>';
				$output .= '</Piecemaker>';
				header('Content-Type: text/xml');
				die($output);
			}
		}
	}
?>
