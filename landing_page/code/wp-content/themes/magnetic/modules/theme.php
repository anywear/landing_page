<?php
	class ether_theme extends ether_module
	{
		public static $actionbar;
		public static $actionbar_html;

		public static function init()
		{
			ether::bind('ether.get', array('ether_fonts_and_colors', 'get'));
			ether::bind('ether.get', array('ether_header', 'piecemaker_xml'));
			
			ether::register_wysiwig_modalbox('List style', ether::langr('Change default list style by adding new amazing icons.'), ether::path('admin/media/images/wysiwig/list-style.png', TRUE), ether::path('modules/shortcode/list-style.php?name=list&title='.str_replace(' ', '+', ether::langr('List style')).'&tinymce=true', TRUE), 500, 182);
			ether::register_wysiwig_modalbox('Inline formatting', ether::langr('Highlight selected text with diffrent background color.'), ether::path('admin/media/images/wysiwig/inline-formatting.png', TRUE), ether::path('modules/shortcode/inline.php?name=inline&title='.str_replace(' ', '+', ether::langr('Inline formatting')).'&tinymce=true', TRUE), 500, 298);
			ether::register_wysiwig_modalbox('Create quotes', ether::langr('Create pull quote from selected text or add new block quote.'), ether::path('admin/media/images/wysiwig/quote.png', TRUE), ether::path('modules/shortcode/quote.php?name=quote&title='.str_replace(' ', '+', ether::langr('Create quotes')).'&tinymce=true', TRUE), 500, 288);
			ether::register_wysiwig_element('separator');
			ether::register_wysiwig_shortcode('Divider', ether::langr('Add simple horizontal line.'), ether::path('admin/media/images/wysiwig/divider.png', TRUE), '[divider]', '');
			ether::register_wysiwig_shortcode('Divider Top', ether::langr('Add horizontal line with link to top.'), ether::path('admin/media/images/wysiwig/divider-top.png', TRUE), '[divider top=true]', '');
			ether::register_wysiwig_element('separator');
			ether::register_wysiwig_modalbox('Toggle', ether::langr('Toggle content'), ether::path('admin/media/images/wysiwig/toggle.png', TRUE), ether::path('modules/shortcode/toggle.php?name=toggle&title='.str_replace(' ', '+', ether::langr('Create toggle content')).'&tinymce=true', TRUE), 500, 288);
			ether::register_wysiwig_modalbox('Tabs', ether::langr('Tabbed content'), ether::path('admin/media/images/wysiwig/tabs.png', TRUE), ether::path('modules/shortcode/tabs.php?name=tabs&title='.str_replace(' ', '+', ether::langr('Create tabbed content')).'&tinymce=true', TRUE), 500, 288);
			ether::register_wysiwig_element('separator');
			ether::register_wysiwig_modalbox('Contact form', ether::langr('Create simple contact form, choose where to send emails and change button text.'), ether::path('admin/media/images/wysiwig/contact-form.png', TRUE), ether::path('modules/shortcode/contact.php?name=contact&title='.str_replace(' ', '+', ether::langr('Contact form')).'&single=true&tinymce=true', TRUE), 500, 297);
			ether::register_wysiwig_modalbox('Insert video', ether::langr('Insert any video from YouTube, Vimeo, etc...'), ether::path('admin/media/images/wysiwig/video.png', TRUE), ether::path('modules/shortcode/video.php?name=video&title='.str_replace(' ', '+', ether::langr('Insert video')).'&single=true&tinymce=true', TRUE), 500, 298);
			ether::register_wysiwig_modalbox('Create button', ether::langr('Create button from selected text and style them with few clicks.'), ether::path('admin/media/images/wysiwig/button.png', TRUE), ether::path('modules/shortcode/button.php?name=button&title='.str_replace(' ', '+', ether::langr('Create button')).'&tinymce=true', TRUE), 500, 293);
			ether::register_wysiwig_modalbox('Alerts', ether::langr('Create alerts for downloads, notes, warnings and informations.'), ether::path('admin/media/images/wysiwig/alert.png', TRUE), ether::path('modules/shortcode/alert.php?name=alert&title='.str_replace(' ', '+', ether::langr('Alerts')).'&tinymce=true', TRUE), 500, 292);
			ether::register_wysiwig_modalbox('Pricing table', ether::langr('Insert pricing table by selecting pricing columns with few clicks.'), ether::path('admin/media/images/wysiwig/pricing-table.png', TRUE), ether::path('modules/shortcode/pricing-table.php?name=pricing_table&title='.str_replace(' ', '+', ether::langr('Pricing tables')).'&single=true&tinymce=true', TRUE), 500, 328);
			ether::register_wysiwig_element('separator');
			ether::register_wysiwig_modalbox('Testimonials', ether::langr('Insert any testimonials by selecting them in new window.'), ether::path('admin/media/images/wysiwig/testimonials.png', TRUE), ether::path('modules/shortcode/testimonials.php?name=testimonials&title='.str_replace(' ', '+', ether::langr('Testimonials')).'&single=true&tinymce=true', TRUE), 500, 500);
			ether::register_wysiwig_modalbox('Success stories', ether::langr('Insert any success story by selecting them in new window.'), ether::path('admin/media/images/wysiwig/success-stories.png', TRUE), ether::path('modules/shortcode/success-stories.php?name=success_stories&title='.str_replace(' ', '+', ether::langr('Success stories')).'&single=true&tinymce=true', TRUE), 500, 500);
			ether::register_wysiwig_modalbox('Services', ether::langr('Insert services, edit their layout and options.'), ether::path('admin/media/images/wysiwig/services.png', TRUE), ether::path('modules/shortcode/services.php?name=services&title='.str_replace(' ', '+', ether::langr('Services')).'&single=true&tinymce=true', TRUE), 500, 500);
			//ether::register_wysiwig_element('separator');
			//ether::register_wysiwig_modalbox('Columns', 'Generate columns', ether::path('admin/media/images/wysiwig/services.png', TRUE), ether::path('modules/shortcode/columns.php?name=columns&title=Columns&single=true', TRUE), 500, 500);
			
			ether::register_quicktag('1', '<div class="columns">', '</div>');
			ether::register_quicktag('1/2', '<div class="half">', '</div>');
			ether::register_quicktag('1/3', '<div class="third">', '</div>');
			ether::register_quicktag('2/3', '<div class="two-third">', '</div>');
			ether::register_quicktag('1/4', '<div class="fourth">', '</div>');
			ether::register_quicktag('3/4', '<div class="three-fourth">', '</div>');
		}
		
		public static function install()
		{
			ether::module_run('sidebar.install');
			
			ether::admin_reset();
		}
		
		public static function cleanup()
		{
			
		}
		
		/*public static function sidebar()
		{
			return ether::module_run('sidebar.get_sidebar');
		}
		
		public static function sidebar_align()
		{
			return ether::module_run('sidebar.get_align');
		}
		
		public static function header()
		{
			return ether::module_run('header.get_header');
		}*/
		
		public static function setup()
		{
			ether::module_run('header.setup');
			ether::module_run('sidebar.setup');
			ether::module_run('gallery.setup');
			
			global $post;
			$id = NULL;
			$term = NULL;
			
			if (is_single())
			{
				$id = $post->ID;
			} else if (is_page() OR get_option('show_on_front') != 'posts')
			{
				if (is_page())
				{
					$id = $post->ID;
				} else if (is_home() AND get_option('show_on_front') != 'posts')
				{
					$id = get_option('page_for_posts');
				}
			}
			
			self::$actionbar = ether::meta('actionbar', TRUE, $id);
			
			if (self::$actionbar == 'html')
			{
				self::$actionbar_html = ether::meta('actionbar_html', TRUE, $id);
			}
		}

		public static function custom_style()
		{
			$data = unserialize(ether::option('fonts-and-colors'));

			if (isset($data[ether::config('prefix').'fonts_use']) OR isset($data[ether::config('prefix').'colors_use']))
			{
				echo '<link href="'.ether::path('ether/ether.php?custom-style', TRUE).'" rel="stylesheet" type="text/css" />';
				
				if (isset($data[ether::config('prefix').'fonts_use']))
				{
					$fonts = ether_fonts_and_colors::get_font_list();
					
					foreach ($fonts as $font)
					{
						echo '<link href="'.$font.'" rel="stylesheet" type="text/css" />';
					}
				}
			}
		}
		
		public static function more_link($link, $text)
		{
			$link = preg_match('/href="(.+)" /', $link, $match);

			return ' <a class="more" href="'.$match[1].'">'.ether::langr('Read more').'...</a>';
		}
		
		public static function is_actionbar()
		{
			return ( ! empty(self::$actionbar) AND self::$actionbar == 'title');
		}
		
		public static function get_actionbar()
		{
			if (self::$actionbar AND self::$actionbar != 'none')
			{
				$output = '<div class="title title_belt"><div class="fix_width">';
				
				if (self::$actionbar == 'title')
				{
					$output .= '<h3>'.get_the_title().' <span class="date">'.get_the_date().'</span></h3>';
				} else if (self::$actionbar == 'html')
				{
					$output .=do_shortcode(self::$actionbar_html);
				}
				
				$output .= '</div></div>';
				
				return $output;
			}
		}
	}
?>
