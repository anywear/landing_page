<?php
	class ether_admin extends ether_module
	{
		public static function init()
		{
			ether::admin_panel('General', ether::langx('General', 'admin page title', TRUE));
			ether::admin_panel('Headers', ether::langx('Headers', 'admin page title', TRUE));
			ether::admin_panel('Sidebars', ether::langx('Sidebars', 'admin page title', TRUE));
			ether::admin_panel('Social integration', ether::langx('Social integration', 'admin page title', TRUE));
			ether::admin_panel('Custom code', ether::langx('Custom code', 'admin page title', TRUE));
			ether::admin_panel('Fonts & Colors', ether::langx('Fonts & Colors', 'admin page title', TRUE));
			ether::admin_panel('Import & Export', ether::langx('Backup & Restore', 'admin page title', TRUE));
			//ether::admin_panel('Cache');

			ether::admin_metabox('Sidebar', array('title' => ether::langx('Sidebar', 'metabox name', TRUE), 'permissions' => array('post', 'page', 'news', 'gallery'), 'context' => 'side', 'priority' => 'low'));
			ether::admin_metabox('Preview image', array('title' => ether::langx('Preview image', 'metabox name', TRUE), 'permissions' => array('post', 'news', 'gallery', 'success-stories', 'slide', 'service'), 'context' => 'side', 'priority' => 'low'));
			ether::admin_metabox('Gallery', array('title' => ether::langx('Gallery', 'metabox name', TRUE), 'permissions' => array('post', 'page', 'news', 'gallery', 'testimonials', 'success-stories')));
			ether::admin_metabox('Header', array('title' => ether::langx('Header', 'metabox name', TRUE), 'permissions' => array('post', 'page', 'news', 'gallery', 'testimonials', 'success-stories')));
			ether::admin_metabox('Testimonial', array('title' => ether::langx('Testimonial', 'metabox name', TRUE), 'permissions' => array('testimonials'), 'context' => 'side', 'priority' => 'low'));
			ether::admin_metabox('URL', array('title' => ether::langx('URL', 'metabox name', TRUE), 'permissions' => array('slide', 'service'), 'context' => 'side', 'priority' => 'low'));
			ether::admin_metabox('Features', array('title' => ether::langx('Features', 'metabox name', TRUE), 'permissions' => array('pricing-column')));
			ether::admin_metabox('Price', array('title' => ether::langx('Price', 'metabox name', TRUE), 'permissions' => array('pricing-column'), 'context' => 'side', 'priority' => 'low'));
			ether::admin_metabox('Button', array('title' => ether::langx('Button', 'metabox name', TRUE), 'permissions' => array('pricing-column'), 'context' => 'side', 'priority' => 'low'));
			ether::admin_metabox('Post options', array('title' => ether::langx('Post options', 'metabox name', TRUE), 'permissions' => array('post', 'page', 'news', 'gallery', 'testimonials', 'success-stories')));
		}
		
		public static function wp_admin_print_scripts()
		{
			wp_enqueue_script('jquery');
			wp_enqueue_script('theme.admin', ether::path('admin/media/scripts/admin.js', TRUE), array('jquery'), '1.0');
		}
	};
?>
