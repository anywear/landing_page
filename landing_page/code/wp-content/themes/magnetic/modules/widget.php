<?php
	class ether_widget extends ether_module
	{
		public static function twitter($title, $username, $count, $alt = FALSE)
		{
			$output = '';

			$output .= '<div class="widget">';
			
			if ($title != '')
			{
				$output .= '<h1 class="title_pagesub">'.$title.'</h1>';
			}
			
			if (empty($count))
			{
				$count = 10;
			}
			
			if ( ! empty($username))
			{
				$output .= '<div id="twitter_div"'.($alt ? ' class="type-2"' : '').'><ul id="twitter_update_list" class="items">';
				$tweets = ether::twitter_tweets($username, $count);

				foreach ($tweets as $tweet)
				{
					$output .= '<li><span>'.$tweet['tweet'].'</span> <a href="'.$tweet['link'].'">'.$tweet['time'].'</a></li>';
				}
					
				$output .= '</ul>';
				
				if ($alt)
				{
					$output .= '<a href="http://twitter.com/'.$username.'" class="button default-2 alignleft">'.ether::langr('Follow us on Twitter').'</a>';
					$output .= '<img class="chicken" src="'.ether::path('media/images/chicken.png', TRUE).'" alt="Twitter" />';
				}
				
				$output .= '<ul class="slider-nav type-'.($alt ? '5' : '2').'"><li class="prev"><a href="#prev">'.ether::langr('Prev').'</a></li><li class="next"><a href="#next">'.ether::langr('Next').'</a></li></ul></div>';
			}
			
			$output .= '</div>';
			
			return $output;
		}
		
		public static function flickr_feed($title, $flickrid, $count)
		{
			$output = '';
			
			$output .= '<div class="widget">';
			
			if ($title != '')
			{
				$output .= '<h1 class="title_pagesub">'.$title.'</h1>';
			}
			
			if (empty($count))
			{
				$count = 10;
			}
			
			$output .= '<div class="flickr-feed">';

			$output .= '<ul class="items">';
			
			$flickr_feed = ether::flickr_feed($flickrid, $count);

			foreach ($flickr_feed as $item)
			{
				$output .= '<li><a class="frame-holder alignleft" href="'.$item['link'].'"><img src="'.$item['image'].'" alt="'.$item['title'].'" title="'.$item['title'].'" width="100" height="80" /></a></li>';
			}
			
			$output .= '</ul>';
			$output .= '<ul class="slider-nav type-1"><li class="prev"><a href="#">'.ether::langr('Prev').'</a></li><li class="next"><a href="#">'.ether::langr('Next').'</a></li></ul>';
			$output .= '</div></div>';
			
			return $output;
		}

		public static function contact($title, $email, $callus_title, $callus_url, $button_text, $success_message = 'Message sent.')
		{
			$output = '';
			
			$output .= '<div class="widget">';
			
			if ($title != '')
			{
				$output .= '<h1 class="title_pagesub">'.$title.'</h1>';
			}
			
			if ($button_text != '')
			{
				$button_text = ether::langr('Send');
			}

			$output .= '<form class="contact" method="post"> 
				<fieldset> 
					<ul> 
						<li> 
							<input type="text" name="nick" value="" id="name" /> 
						</li> 
						<li> 
							<input type="text" name="email" value="" id="email" /> 
						</li> 
						<li> 
							<textarea rows="5" cols="20" name="message"></textarea> 
						</li> 
						<li class="button-holder">
							<div class="button alignleft default"><input type="submit" name="contact" value="'.$button_text.'" /></div>'.( ! empty($callus_title) ? '<div class="alt-contact"><a href="'.$callus_url.'">'.$callus_title.'</a></div>' : '').'
							<input type="hidden" name="'.base64_encode('email').'" value="'.base64_encode($email).'" />
							<input type="hidden" name="success-message" value="'.$success_message.'" />
						</li> 
					</ul> 
				</fieldset> 
			</form></div>';
				
			return $output;
		}

		public static function menu($title, $menu)
		{
			$output = '';
			
			if (function_exists('wp_nav_menu'))
			{
				$output = '<div class="widget">';
				
				if ($title != '')
				{
					$output .= '<h1 class="title_pagesub">'.$title.'</h1>';
				}
				
				$output .= '<ul class="menu-1">';
				$output .= ether::navigation('depth=3', $menu, FALSE, TRUE);
				$output .= '</ul></div>';
			}
			
			return $output;
		}

		public static function recent_posts($title = '', $slugs = '', $count = 5, $post_type = 'post', $select_by = 'category_id')
		{
			$output = '';
			
			$output .= '<div class="widget">';
			
			if ($title != '')
			{
				$output .= '<h1 class="title_pagesub">'.$title.'</h1>';
			}
			
			$output .= '<div class="popular"><ul class="items">';
			
			$args = array
			(
				'post_type' => $post_type
			);
			
			if ( ! empty($count))
			{
				$args['numberposts'] = $count;
			}
			
			if ( ! empty($slugs) AND $slugs != -1)
			{
				if ($select_by == 'category_name' OR $select_by == 'category_id')
				{
					$args['tax_query'] = array
					(
						'relation' => 'OR',
						array
						(
							'taxonomy' => ($post_type != 'post' ? $post_type.'_' : '').'category',
							'field' => ($select_by == 'category_name' ? 'slug' : 'id'),
							'terms' => ether::slugify($slugs, TRUE)
						)
					);
				} else if ($select_by == 'id')
				{
					$args['post__in'] = ether::slugify($slugs, TRUE);
				} else if ($select_by == 'name')
				{
					$args['name'] = ether::slugify($slugs);
				}
			}

			$posts = ether::get_posts($args);

			foreach ($posts as $post)
			{
				$preview_image = ether::meta('preview_image', TRUE, $post['id']);
				$preview_alt = ether::meta('preview_alt', TRUE, $post['id']);
				
				if ( ! empty($preview_image))
				{
					$preview_image = ether::get_image_thumbnail($preview_image, 215, 70);
				}
				
				if (empty($preview_image))
				{
					$preview_image = ether::path('media/images/placeholders/recent-posts.png', TRUE);
				}
	
				$output .= '<li><a href="'.$post['permalink'].'"><img src="'.$preview_image.'" alt="'.$preview_alt.'" width="215" height="70" /></a><h3><a href="'.$post['permalink'].'">'.$post['title'].'</a></h3></li>';
			}

			$output .= '</ul></div></div>';
			
			return $output;
		}

		public static function popular_posts($title, $slugs, $count = 5, $post_type = 'post', $select_by = 'category_id')
		{
			$output = '';
			
			$output .= '<div class="widget">';
			
			if ($title != '')
			{
				$output .= '<h1 class="title_pagesub">'.$title.'</h1>';
			}
			
			$output .= '<div class="popular"><ul class="items">';
			
			$args = array
			(
				'post_type' => $post_type,
				'orderby' => 'comment_count',
				'order' => 'DESC'
			);
			
			if ( ! empty($count))
			{
				$args['numberposts'] = $count;
			}
			
			if ( ! empty($slugs) AND $slugs != -1)
			{
				if ($select_by == 'category_name' OR $select_by == 'category_id')
				{
					$args['tax_query'] = array
					(
						'relation' => 'OR',
						array
						(
							'taxonomy' => ($post_type != 'post' ? $post_type.'_' : '').'category',
							'field' => ($select_by == 'category_name' ? 'slug' : 'id'),
							'terms' => ether::slugify($slugs, TRUE)
						)
					);
				} else if ($select_by == 'id')
				{
					$args['post__in'] = ether::slugify($slugs, TRUE);
				} else if ($select_by == 'name')
				{
					$args['name'] = ether::slugify($slugs);
				}
			}
			
			$posts = ether::get_posts($args);
			
			foreach ($posts as $post)
			{
				$preview_image = ether::meta('preview_image', TRUE, $post['id']);
				$preview_alt = ether::meta('preview_alt', TRUE, $post['id']);
				
				if ( ! empty($preview_image))
				{
					$preview_image = ether::get_image_thumbnail($preview_image, 215, 70);
				}
				
				if (empty($preview_image))
				{
					$preview_image = ether::path('media/images/placeholders/popular-posts.png', TRUE);
				}
	
				$output .= '<li><a href="'.$post['permalink'].'"><img src="'.$preview_image.'" alt="'.$preview_alt.'" width="215" height="70" /></a><h3><a href="'.$post['permalink'].'">'.$post['title'].'</a></h3></li>';
			}

			$output .= '</ul></div></div>';
			
			return $output;
		}
		
		public static function featured_posts($title, $category, $count = 5, $post_type = 'post', $select_by = 'category_id')
		{
			$output = '';
			
			$output .= '<div class="widget">';
			
			if ($title != '')
			{
				$output .= '<h1 class="title_pagesub">'.$title.'</h1>';
			}
			
			$output .= '<div class="featured"><ul class="items">';
			
			$args = array
			(
				'post_type' => $post_type,
				'text_opt' => 'excerpt'
			);
			
			if ( ! empty($count))
			{
				$args['numberposts'] = $count;
			}
			
			if ( ! empty($slugs) AND $slugs != -1)
			{
				if ($select_by == 'category_name' OR $select_by == 'category_id')
				{
					$args['tax_query'] = array
					(
						'relation' => 'OR',
						array
						(
							'taxonomy' => ($post_type != 'post' ? $post_type.'_' : '').'category',
							'field' => ($select_by == 'category_name' ? 'slug' : 'id'),
							'terms' => ether::slugify($slugs, TRUE)
						)
					);
				} else if ($select_by == 'id')
				{
					$args['post__in'] = ether::slugify($slugs, TRUE);
				} else if ($select_by == 'name')
				{
					$args['name'] = ether::slugify($slugs);
				}
			}
			
			$posts = ether::get_posts($args);

			foreach ($posts as $post)
			{
				$preview_image = ether::meta('preview_image', TRUE, $post['id']);
				$preview_alt = ether::meta('preview_alt', TRUE, $post['id']);
				
				if ( ! empty($preview_image))
				{
					$preview_image = ether::get_image_thumbnail($preview_image, 225, 70);
				}
				
				if (empty($preview_image))
				{
					$preview_image = ether::path('media/images/placeholders/featured-posts.png', TRUE);
				}
	
				$output .= '<li><img src="'.$preview_image.'" alt="'.$preview_alt.'" width="225" height="70" /><h3><a href="'.$post['permalink'].'">'.$post['title'].'</a></h3>'.$post['content'].'</li>';
			}

			$output .= '</ul><ul class="slider-nav type-1"><li class="prev"><a href="#prev">'.ether::langr('Prev').'</a></li><li class="next"><a href="#next">'.ether::langr('Next').'</a></li></ul></div></div>';
			
			return $output;
		}

		public static function testimonials($title, $slugs, $count, $author, $slider = TRUE, $columns = '', $select_by = 'category_id', $words = 100)
		{
			$output = '';
			
			$output .= '<div class="widget">';
			
			if ($title != '')
			{
				$output .= '<h1 class="title_pagesub">'.$title.'</h1>';
			}
			
			$col = $columns;

			if ( ! empty($columns))
			{
				if ($columns >= 1 AND $columns < 5)
				{
					$cols = array('one', 'two', 'three', 'four');
				
					$columns = ' '.$cols[$columns - 1].'-cols';
					
					$slider = FALSE;
				} else
				{
					$columns = '';
				}
			}
			
			if ($slider)
			{
				$output .= '<div class="testimonials'.(($slider AND empty($columns)) ? ' slider' : '').( ! empty($columns) ? $columns : '').'"><ul class="items">';
			}
			
			$args = array
			(
				'post_type' => 'testimonials',
				'text_opt' => 'excerpt',
				'nopaging' => TRUE
			);
			
			if ( ! empty($count))
			{
				$args['numberposts'] = $count;
			}
			
			if ( ! empty($slugs) AND $slugs != -1)
			{
				if ($select_by == 'category_name' OR $select_by == 'category_id')
				{
					$args['tax_query'] = array
					(
						'relation' => 'OR',
						array
						(
							'taxonomy' => 'testimonials_category',
							'field' => ($select_by == 'category_name' ? 'slug' : 'id'),
							'terms' => ether::slugify($slugs, TRUE)
						)
					);
				} else if ($select_by == 'id')
				{
					$args['post__in'] = ether::slugify($slugs, TRUE);
					$args['orderby'] = 'custom';
				} else if ($select_by == 'name')
				{
					$args['name'] = ether::slugify($slugs);
				}
			}
			
			$testimonials = ether::get_posts($args);
			$count = count($testimonials);
			$counter = 0;
			
			for ($i = 0; $i < $count; $i++)
			{
				$testimonial = $testimonials[$i];
				
				if ( ! $slider)
				{
					if ($i == 0 OR $i % $col == 0)
					{
						$output .= '<div class="testimonials'.(($slider AND empty($columns)) ? ' slider' : '').( ! empty($columns) ? $columns : '').'"><ul class="items">';
					}
				}

				$output .= '<li>';
				$output .= '<blockquote>'.wpautop(ether::trim_words(strip_tags($testimonial['content']), $words, TRUE)).'</blockquote>';
				
				if ($author == 'name' OR $author == 'name-and-company')
				{
					$output .= '<p class="meta"><a href="'.ether::meta('url', TRUE, $testimonial['id']).'"><strong>'.ether::meta('author_name', TRUE, $testimonial['id']).'</strong></a>';
				}
				
				if ($author == 'name-and-company')
				{
					$output .= ' '.ether::meta('company_name', TRUE, $testimonial['id']);
				}
				
				if ($author != 'hide')
				{
					$output .= '</p>';
				}
				
				$output .= '<a class="read-more" href="'.$testimonial['permalink'].'"><span>'.ether::langr('click to read whole testimonial').'</span></a>';
				$output .= '</li>';
				
				if ( ! $slider)
				{
					$counter++;

					if ($counter == $col)
					{
						$output .= '</ul></div>';
						$counter = 0;
					}
				}
			}
			
			if ($slider OR $counter != 0)
			{	
				$output .= '</ul>';
			}
			
			if ($slider AND ! $columns)
			{
				$output .= '<ul class="slider-nav type-1"><li class="prev"><a href="#prev">'.ether::langr('Prev').'</a></li><li class="next"><a href="#next">'.ether::langr('Next').'</a></li></ul>';
			}
			
			if ($slider OR $counter != 0)
			{
				$output .= '</div>';
			}
			
			$output .= '</div>';
			
			return $output;
		}
		

		public static function success_stories($title, $slugs, $count, $large = FALSE, $select_by = 'category_id')
		{
			$output = '';
			
			$output .= '<div class="widget">';
			
			if ($title != '' && ! $large)
			{
				$output .= '<h1 class="title_pagesub">'.$title.'</h1>';
			}
			
			$output .= '<div class="success-stories">';
			
			if ($title != '' && $large)
			{
				$output .= '<h1 class="title_pagesub">'.$title.'</h1>';
			}
			
			$output .= '<ul class="items">';
			
			$args = array
			(
				'post_type' => 'success-stories',
				'text_opt' => 'excerpt',
				'nopaging' => TRUE
			);
			
			if ( ! empty($count))
			{
				$args['numberposts'] = $count;
			}
			
			if ( ! empty($slugs) AND $slugs != -1)
			{
				if ($select_by == 'category_name' OR $select_by == 'category_id')
				{
					$args['tax_query'] = array
					(
						'relation' => 'OR',
						array
						(
							'taxonomy' => 'success-stories_category',
							'field' => ($select_by == 'category_name' ? 'slug' : 'id'),
							'terms' => ether::slugify($slugs, TRUE)
						)
					);
				} else if ($select_by == 'id')
				{
					$args['post__in'] = ether::slugify($slugs, TRUE);
					$args['orderby'] = 'custom';
				} else if ($select_by == 'name')
				{
					$args['name'] = ether::slugify($slugs);
				}
			}

			$stories = ether::get_posts($args);
			
			$counter = 1;
			$pagination = '';
			
			foreach ($stories as $story)
			{
				$preview_image = ether::meta('preview_image', TRUE, $story['id']);
				$preview_alt = ether::meta('preview_alt', TRUE, $story['id']);
				
				if ( ! empty($preview_image))
				{
					$preview_image = ether::get_image_thumbnail($preview_image, ($large ? 625 : 230), ($large ? 295 : 150));
					$preview_image_base = ether::get_image_base($preview_image);
					
					if ($large)
					{
						$preview_image = $preview_image_base;
					}
				}
				
				if (empty($preview_image))
				{
					$preview_image = ether::path('media/images/placeholders/success-story.png', TRUE);
					$preview_image_base = $preview_image;
				}
				
				$image_url = ether::meta('url', TRUE, $story['id']);
				$size = array();
				
				if ($large)
				{
					$size = ether::get_image_size($preview_image);
				}

				if ( ! empty($image_url))
				{
					$preview_image_base = $image_url;
				}
				
				$output .= '<li>';
				$output .= '<div class="desc"><h'.($large ? 4 : 3).'><a href="'.$story['permalink'].'">'.$story['title'].'</a></h'.($large ? 4 : 3).'>'.$story['content'].'<a href="#" class="view_alllink">View All</a></div>';
				$output .= '<a href="'.$preview_image_base.'" rel="lightbox" class="media'.($large ? ' ' : '').'"><img src="'.$preview_image.'" alt="'.$preview_alt.'" width="'.($large ? $size['width'] : 230).'" height="'.($large ? $size['height'] : 150).'" /></a>';
				$output .= '</li>';
			}
			
			$output .= '</ul>';
			$output .= '<ul class="slider-nav type-1"><li class="prev"><a href="#prev">'.ether::langr('Prev').'</a></li><li class="next"><a href="#next">'.ether::langr('Next').'</a></li></ul>';

			$output .= '</div></div>';
			
			return $output;
		}
		
		public static function search($title = '')
		{
			$output = '';
			
			$output .= '<div class="widget">';
			
			if ($title != '')
			{
				$output .= '<h1 class="title_pagesub">'.$title.'</h1>';
			}
			
			$output .= '<form class="search" action="'.ether::info('url', TRUE).'"> 
				<fieldset> 
					<ul> 
						<li> 
							<input type="text" name="s" value="" id="s" /> 
						</li>									
						<li> 
							<input type="submit" value="'.ether::langr('Search').'" />
						</li> 
					</ul> 
				</fieldset> 
			</form>';
			
			$output .= '</div>';
			
			return $output;
		}
		
		public static function archives($title)
		{
			$output = '';
			
			$output .= '<div class="widget">';
			
			
			if ($title != '')
			{
				$output .= '<h1 class="title_pagesub">'.$title.'</h1>';
			}
			
			$output .= '<ul class="menu-2 archives">';
			ob_start();
			ether::list_archives();
			$output .= trim(ob_get_clean());
				
			$output .= '</ul></div>';
			
			return $output;
		}
		
		public static function links($title)
		{
			$output = '';
			
			$output .= '<div class="widget">';
			
			
			if ($title != '')
			{
				$output .= '<h1 class="title_page">'.$title.'</h1>';
			}
			
			$output .= '<ul class="menu-2 links">';
			$output .= wp_list_bookmarks(array('categorize' => 0, 'echo' => 0, 'title_li' => '', 'title_before' => '', 'title_after' => ''));
			$output .= '</ul></div>';
			
			return $output;
		}
		
		public static function tags($title)
		{
			$output = '';
			
			$output .= '<div class="widget">';
			
			if ($title != '')
			{
				$output .= '<h1 class="title_pagesub">'.$title.'</h1>';
			}
			
			$output .= '<ul class="tags">';
			$tags = wp_tag_cloud(array('format' => 'array'));
			
			foreach ($tags as $tag)
			{
				preg_match('/(<a href=\'(.*)\'.*title=\'(.*) topic.*>)(.*)(<.*a>)/ismU', $tag, $match);
				 
				$output .= '<li><a href="'.$match[2].'"><span>'.$match[4].'</span><span>'.$match[3].'</span></a></li>';
			}
			
			$output .= '</ul>';
			$output .= '</div>';
			
			return $output;
		}
	}
			
	class ether_widget_tweets extends WP_Widget
	{
		public function __construct()
		{
			parent::__construct(FALSE, '&nbsp;'.ether::config('theme_name').' '.ether::langx('Twitter feed', 'widget name', TRUE), array());
		}
		
		public function widget($args, $instance)
		{
			echo ether_widget::twitter($instance['title'], $instance['username'], $instance['count']);
		}
		
		public function update($new_instance, $old_instance)
		{
			return $new_instance;
		}
		
		public function form($instance)
		{
			$title = esc_attr($instance['title']);
			$username = esc_attr($instance['username']);
			$count = esc_attr($instance['count']);
			
			echo '<p><label for="'.$this->get_field_name('title').'">'.ether::langr('Title').'</label><input type="text" class="widefat" name="'.$this->get_field_name('title').'" value="'.$title.'" /></p>';
			echo '<p><label for="'.$this->get_field_name('username').'">'.ether::langr('Enter username').'</label><input type="text" class="widefat" name="'.$this->get_field_name('username').'" value="'.$username.'" /></p>';
			echo '<p><label for="'.$this->get_field_name('count').'">'.ether::langr('Number of tweets').'</label></p>';
			echo '<p><select name="'.$this->get_field_name('count').'">';
			for ($i = 1; $i <= 10; $i++)
			{
				echo '<option value="'.$i.'"'.($count == $i ? ' selected="selected"' : '').'>'.$i.'</option>';
			}
			
			echo '</select></p>';
		}
	}
	
	class ether_widget_flickr_feed extends WP_Widget
	{
		public function __construct()
		{
			parent::__construct(FALSE, '&nbsp;'.ether::config('theme_name').' '.ether::langx('Flickr feed', 'widget name', TRUE), array());
		}
			
		public function widget($args, $instance)
		{
			echo ether_widget::flickr_feed($instance['title'], $instance['flickrid'], $instance['count']);
		}
			
		public function update($new_instance, $old_instance)
		{
			return $new_instance;
		}
			
		public function form($instance)
		{
			$title = esc_attr($instance['title']);
			$flickrid = esc_attr($instance['flickrid']);
			$count = esc_attr($instance['count']);
			
			echo '<p><label for="'.$this->get_field_name('title').'">'.ether::langr('Title').'</label><input type="text" class="widefat" name="'.$this->get_field_name('title').'" value="'.$title.'" /></p>';
			echo '<p><label for="'.$this->get_field_name('flickrid').'">'.ether::langr('Enter ID').'</label><input type="text" class="widefat" name="'.$this->get_field_name('flickrid').'" value="'.$flickrid.'" /></p>';
			echo '<p><label for="'.$this->get_field_name('count').'">'.ether::langr('Number of images').'</label></p>';
			echo '<p><select name="'.$this->get_field_name('count').'">';
			for ($i = 1; $i <= 10; $i++)
			{
				echo '<option value="'.$i.'"'.($count == $i ? ' selected="selected"' : '').'>'.$i.'</option>';
			}
			
			echo '</select></p>';
		}
	}

	class ether_widget_contact extends WP_Widget
	{
		public function __construct()
		{
			parent::__construct(FALSE, '&nbsp;'.ether::config('theme_name').' '.ether::langx('Contact form', 'widget name', TRUE), array());
		}
		
		public function widget($args, $instance)
		{			
			echo ether_widget::contact($instance['title'], $instance['email'], $instance['callus_title'], $instance['callus_url'], $instance['button_text'], $instance['success_message']);
		}
		
		public function update($new_instance, $old_instance)
		{
			return $new_instance;
		}
	
		public function form($instance)
		{
			$title = esc_attr($instance['title']);
			$email = esc_attr($instance['email']);
			$callus_title = esc_attr($instance['callus_title']);
			$callus_url = esc_attr($instance['callus_url']);
			$button_text = esc_attr($instance['button_text']);
			$success_message = esc_attr($instance['success_mesage']);
			
			echo '<p><label for="'.$this->get_field_name('title').'">'.ether::langr('Title').'</label><input type="text" class="widefat" name="'.$this->get_field_name('title').'" value="'.$title.'" /></p>
			<p><label for="'.$this->get_field_name('email').'">'.ether::langr('Where to send message?').'</label><input type="text" class="widefat" name="'.$this->get_field_name('email').'" value="'.$email.'" /></p>
			<p><label for="'.$this->get_field_name('callus_title').'">'.ether::langr('"Call us" title').'</label><input type="text" class="widefat" name="'.$this->get_field_name('callus_title').'" value="'.$callus_title.'" /></p>
			<p><label for="'.$this->get_field_name('callus_url').'">'.ether::langr('"Call us" URL').'</label><input type="text" class="widefat" name="'.$this->get_field_name('callus_url').'" value="'.$callus_url.'" /></p>
			<p><label for="'.$this->get_field_name('button_text').'">'.ether::langr('Button text').'</label><input type="text" class="widefat" name="'.$this->get_field_name('button_text').'" value="'.$button_text.'" /></p>
			<p><label for="'.$this->get_field_name('success_message').'">'.ether::langr('Success message').'</label><input type="text" class="widefat" name="'.$this->get_field_name('success_message').'" value="'.$success_message.'" /></p>';
		}
	}

	class ether_widget_menu extends WP_Widget
	{
		public function __construct()
		{
			parent::__construct(FALSE, '&nbsp;'.ether::config('theme_name').' '.ether::langx('Menu', 'widget name', TRUE), array());
		}
		
		public function widget($args, $instance)
		{
			echo ether_widget::menu($instance['title'], $instance['menu']);
		}
		
		public function update($new_instance, $old_instance)
		{
			return $new_instance;
		}
	
		public function form($instance)
		{
			$title = esc_attr($instance['title']);

			if (function_exists('wp_get_nav_menus'))
			{
				$menus = wp_get_nav_menus();
				echo '<p><label for="'.$this->get_field_name('title').'">'.ether::langr('Title').'</label><input type="text" class="widefat" name="'.$this->get_field_name('title').'" value="'.$title.'" /></p>';
				echo '<p><label for="'.$this->get_field_name('menu').'">'.ether::langr('Choose menu').'</label></p>
				<p><select name="'.$this->get_field_name('menu').'">';
				foreach ($menus as $menu)
				{
					echo '<option value="'.$menu->slug.'"'.(esc_attr($instance['menu']) == $menu->slug ? ' selected="selected"' : '').'>'.$menu->name.'</option>';
				}
				echo '</select></p>';
			}
		}
	}
			
	class ether_widget_recent_posts extends WP_Widget
	{
		public function __construct()
		{
			parent::__construct(FALSE, '&nbsp;'.ether::config('theme_name').' '.ether::langx('Recent posts', 'widget name', TRUE), array());
		}
		
		public function widget($args, $instance)
		{
			echo ether_widget::recent_posts($instance['title'], $instance['category'], $instance['count']);
		}
			
		public function update($new_instance, $old_instance)
		{
			return $new_instance;
		}
			
		public function form($instance)
		{
			$title = esc_attr($instance['title']);
			$count = esc_attr($instance['count']);
			$category = esc_attr($instance['category']);
			
			$categories = get_categories(array('type' => 'post', 'hierarchical' => FALSE));
			
			echo '<p><label for="'.$this->get_field_name('title').'">'.ether::langr('Title').'</label><input type="text" class="widefat" name="'.$this->get_field_name('title').'" value="'.$title.'" /></p>';
			echo '<p><label for="'.$this->get_field_name('category').'">'.ether::langr('Categories').'</label><input type="text" class="widefat" name="'.$this->get_field_name('category').'" value="'.$category.'" /></p>';
			/*echo '<p><select name="'.$this->get_field_name('category').'"><option value="-1"'.($category == -1 ? ' selected="selected"' : '').'>All</option>';
			
			foreach ($categories as $cat)
			{
				echo '<option value="'.$cat->slug.'"'.($category == $cat->slug ? ' selected="selected"' : '').'>'.$cat->cat_name.'</option>';
			}
			echo '</select></p>';*/
			echo '<p><label for="'.$this->get_field_name('count').'">'.ether::langr('Number of posts').'</label></p>';
			echo '<p><select name="'.$this->get_field_name('count').'">';
			for ($i = 1; $i <= 10; $i++)
			{
				echo '<option value="'.$i.'"'.($count == $i ? ' selected="selected"' : '').'>'.$i.'</option>';
			}
			
			echo '</select></p>';
		}
	}

	class ether_widget_popular_posts extends WP_Widget
	{
		public function __construct()
		{
			parent::__construct(FALSE, '&nbsp;'.ether::config('theme_name').' '.ether::langx('Popular posts', 'widget name', TRUE), array());
		}
		
		public function widget($args, $instance)
		{
			echo ether_widget::popular_posts($instance['title'], $instance['category'], $instance['count']);
		}
		
		public function update($new_instance, $old_instance)
		{
			return $new_instance;
		}
		
		public function form($instance)
		{
			$title = esc_attr($instance['title']);
			$count = esc_attr($instance['count']);
			$category = esc_attr($instance['category']);
			
			$categories = get_categories(array('type' => 'post', 'hierarchical' => FALSE));
			
			echo '<p><label for="'.$this->get_field_name('title').'">'.ether::langr('Title').'</label><input type="text" class="widefat" name="'.$this->get_field_name('title').'" value="'.$title.'" /></p>';
			echo '<p><label for="'.$this->get_field_name('category').'">'.ether::langr('Categories ID.').'</label><input type="text" class="widefat" name="'.$this->get_field_name('category').'" value="'.$category.'" /></p>';
			/*echo '<p><select name="'.$this->get_field_name('category').'"><option value="-1"'.($category == -1 ? ' selected="selected"' : '').'>All</option>';
			
			foreach ($categories as $cat)
			{
				echo '<option value="'.$cat->slug.'"'.($category == $cat->slug ? ' selected="selected"' : '').'>'.$cat->cat_name.'</option>';
			}
			echo '</select></p>';*/
			echo '<p><label for="'.$this->get_field_name('count').'">'.ether::langr('Number of posts').'</label></p>';
			echo '<p><select name="'.$this->get_field_name('count').'">';
			for ($i = 1; $i <= 10; $i++)
			{
				echo '<option value="'.$i.'"'.($count == $i ? ' selected="selected"' : '').'>'.$i.'</option>';
			}
			
			echo '</select></p>';
		}
	}
	
	class ether_widget_featured_posts extends WP_Widget
	{
		public function __construct()
		{
			parent::__construct(FALSE, '&nbsp;'.ether::config('theme_name').' '.ether::langx('Featured posts', 'widget name', TRUE), array());
		}
		
		public function widget($args, $instance)
		{
			echo ether_widget::featured_posts($instance['title'], $instance['category'], $instance['count']);
		}
		
		public function update($new_instance, $old_instance)
		{
			return $new_instance;
		}
		
		public function form($instance)
		{
			$title = esc_attr($instance['title']);
			$count = esc_attr($instance['count']);
			$category = esc_attr($instance['category']);
			
			$categories = get_categories(array('type' => 'post', 'hierarchical' => FALSE));
			
			echo '<p><label for="'.$this->get_field_name('title').'">'.ether::langr('Title').'</label><input type="text" class="widefat" name="'.$this->get_field_name('title').'" value="'.$title.'" /></p>';
			echo '<p><label for="'.$this->get_field_name('category').'">'.ether::langr('Categories ID').'</label><input type="text" class="widefat" name="'.$this->get_field_name('category').'" value="'.$category.'" /></p>';
			/*echo '<p><select name="'.$this->get_field_name('category').'"><option value="-1"'.($category == -1 ? ' selected="selected"' : '').'>All</option>';
			
			foreach ($categories as $cat)
			{
				echo '<option value="'.$cat->slug.'"'.($category == $cat->slug ? ' selected="selected"' : '').'>'.$cat->cat_name.'</option>';
			}
			echo '</select></p>';*/
			echo '<p><label for="'.$this->get_field_name('count').'">'.ether::langr('Number of posts').'</label></p>';
			echo '<p><select name="'.$this->get_field_name('count').'">';
			for ($i = 1; $i <= 10; $i++)
			{
				echo '<option value="'.$i.'"'.($count == $i ? ' selected="selected"' : '').'>'.$i.'</option>';
			}
			
			echo '</select></p>';
		}
	}
	
	class ether_widget_recent_news extends WP_Widget
	{
		public function __construct()
		{
			parent::__construct(FALSE, '&nbsp;'.ether::config('theme_name').' '.ether::langx('Recent news', 'widget name', TRUE), array());
		}
		
		public function widget($args, $instance)
		{
			echo ether_widget::recent_posts($instance['title'], $instance['category'], $instance['count'], 'news');
		}
			
		public function update($new_instance, $old_instance)
		{
			return $new_instance;
		}
			
		public function form($instance)
		{
			$title = esc_attr($instance['title']);
			$count = esc_attr($instance['count']);
			$category = esc_attr($instance['category']);
			
			$categories = get_categories(array('type' => 'news', 'hierarchical' => FALSE, 'taxonomy' => 'news_category'));
			
			echo '<p><label for="'.$this->get_field_name('title').'">'.ether::langr('Title').'</label><input type="text" class="widefat" name="'.$this->get_field_name('title').'" value="'.$title.'" /></p>';
			echo '<p><label for="'.$this->get_field_name('category').'">'.ether::langr('Categories ID').'</label><input type="text" class="widefat" name="'.$this->get_field_name('category').'" value="'.$category.'" /></p>';
			/*echo '<p><select name="'.$this->get_field_name('category').'"><option value="-1"'.($category == -1 ? ' selected="selected"' : '').'>All</option>';
			
			foreach ($categories as $cat)
			{
				echo '<option value="'.$cat->slug.'"'.($category == $cat->slug ? ' selected="selected"' : '').'>'.$cat->cat_name.'</option>';
			}
			echo '</select></p>';*/
			echo '<p><label for="'.$this->get_field_name('count').'">'.ether::langr('Number of posts').'</label></p>';
			echo '<p><select name="'.$this->get_field_name('count').'">';
			for ($i = 1; $i <= 10; $i++)
			{
				echo '<option value="'.$i.'"'.($count == $i ? ' selected="selected"' : '').'>'.$i.'</option>';
			}
			
			echo '</select></p>';
		}
	}

	class ether_widget_popular_news extends WP_Widget
	{
		public function __construct()
		{
			parent::__construct(FALSE, '&nbsp;'.ether::config('theme_name').' '.ether::langx('Popular news', 'widget name', TRUE), array());
		}
		
		public function widget($args, $instance)
		{
			echo ether_widget::popular_posts($instance['title'], $instance['category'], $instance['count'], 'news');
		}
		
		public function update($new_instance, $old_instance)
		{
			return $new_instance;
		}
		
		public function form($instance)
		{
			$title = esc_attr($instance['title']);
			$count = esc_attr($instance['count']);
			$category = esc_attr($instance['category']);
			
			$categories = get_categories(array('type' => 'news', 'hierarchical' => FALSE, 'taxonomy' => 'news_category'));
			
			echo '<p><label for="'.$this->get_field_name('title').'">'.ether::langr('Title').'</label><input type="text" class="widefat" name="'.$this->get_field_name('title').'" value="'.$title.'" /></p>';
			echo '<p><label for="'.$this->get_field_name('category').'">'.ether::langr('Categories ID').'</label><input type="text" class="widefat" name="'.$this->get_field_name('category').'" value="'.$category.'" /></p>';
			/*echo '<p><select name="'.$this->get_field_name('category').'"><option value="-1"'.($category == -1 ? ' selected="selected"' : '').'>All</option>';
			
			foreach ($categories as $cat)
			{
				echo '<option value="'.$cat->slug.'"'.($category == $cat->slug ? ' selected="selected"' : '').'>'.$cat->cat_name.'</option>';
			}
			echo '</select></p>';*/
			echo '<p><label for="'.$this->get_field_name('count').'">'.ether::langr('Number of posts').'</label></p>';
			echo '<p><select name="'.$this->get_field_name('count').'">';
			for ($i = 1; $i <= 10; $i++)
			{
				echo '<option value="'.$i.'"'.($count == $i ? ' selected="selected"' : '').'>'.$i.'</option>';
			}
			
			echo '</select></p>';
		}
	}
	
	class ether_widget_featured_news extends WP_Widget
	{
		public function __construct()
		{
			parent::__construct(FALSE, '&nbsp;'.ether::config('theme_name').' '.ether::langx('Featured news', 'widget name', TRUE), array());
		}
		
		public function widget($args, $instance)
		{
			echo ether_widget::featured_posts($instance['title'], $instance['category'], $instance['count'], 'news');
		}
		
		public function update($new_instance, $old_instance)
		{
			return $new_instance;
		}
		
		public function form($instance)
		{
			$title = esc_attr($instance['title']);
			$count = esc_attr($instance['count']);
			$category = esc_attr($instance['category']);
			
			$categories = get_categories(array('type' => 'news', 'hierarchical' => FALSE, 'taxonomy' => 'news_category'));
			
			echo '<p><label for="'.$this->get_field_name('title').'">'.ether::langr('Title').'</label><input type="text" class="widefat" name="'.$this->get_field_name('title').'" value="'.$title.'" /></p>';
			echo '<p><label for="'.$this->get_field_name('category').'">'.ether::langr('Categories ID.').'</label><input type="text" class="widefat" name="'.$this->get_field_name('category').'" value="'.$category.'" /></p>';
			/*echo '<p><select name="'.$this->get_field_name('category').'"><option value="-1"'.($category == -1 ? ' selected="selected"' : '').'>All</option>';
			
			foreach ($categories as $cat)
			{
				echo '<option value="'.$cat->slug.'"'.($category == $cat->slug ? ' selected="selected"' : '').'>'.$cat->cat_name.'</option>';
			}
			echo '</select></p>';*/
			echo '<p><label for="'.$this->get_field_name('count').'">'.ether::langr('Number of posts').'</label></p>';
			echo '<p><select name="'.$this->get_field_name('count').'">';
			for ($i = 1; $i <= 10; $i++)
			{
				echo '<option value="'.$i.'"'.($count == $i ? ' selected="selected"' : '').'>'.$i.'</option>';
			}
			
			echo '</select></p>';
		}
	}

	class ether_widget_testimonials extends WP_Widget
	{
		public function __construct()
		{
			parent::__construct(FALSE, '&nbsp;'.ether::config('theme_name').' '.ether::langx('Testimonials', 'widget name', TRUE), array());
		}
			
		public function widget($args, $instance)
		{
			echo ether_widget::testimonials($instance['title'], $instance['category'], $instance['count'], $instance['author']);
		}
			
		public function update($new_instance, $old_instance)
		{
			return $new_instance;
		}
		
		public function form($instance)
		{
			$title = esc_attr($instance['title']);
			$count = esc_attr($instance['count']);
			$category = esc_attr($instance['category']);
			$author = esc_attr($instance['author']);
			
			$categories = get_categories(array('type' => 'testimonials', 'hierarchical' => FALSE, 'taxonomy' => 'testimonials_category'));
			
			echo '<p><label for="'.$this->get_field_name('title').'">'.ether::langr('Title').'</label><input type="text" class="widefat" name="'.$this->get_field_name('title').'" value="'.$title.'" /></p>';
			echo '<p><label for="'.$this->get_field_name('categories').'">'.ether::langr('Categories ID').'</label><input type="text" class="widefat" name="'.$this->get_field_name('category').'" value="'.$category.'" /></p>';
			/*echo '<p><select name="'.$this->get_field_name('category').'"><option value="-1"'.($category == -1 ? ' selected="selected"' : '').'>All</option>';
			
			foreach ($categories as $cat)
			{
				echo '<option value="'.$cat->slug.'"'.($category == $cat->slug ? ' selected="selected"' : '').'>'.$cat->cat_name.'</option>';
			}
			echo '</select></p>';*/
			
			$author_options = array('name' => ether::langr('Name'), 'name-and-company' => ether::langr('Name & Company'), 'hide' => ether::langr('Hide'));
			echo '<p><label for="'.$this->get_field_name('author').'">Show author</label></p>';
			echo '<p><select name="'.$this->get_field_name('author').'">';
			foreach ($author_options as $option_key => $option_name)
			{
				echo '<option value="'.$option_key.'"'.($author == $option_key ? ' selected="selected"': '').'>'.$option_name.'</option>';
			}
			echo '</select></p>';
		}
	}
		
	class ether_widget_success_stories extends WP_Widget
	{
		public function __construct()
		{
			parent::__construct(FALSE, '&nbsp;'.ether::config('theme_name').' '.ether::langx('Success stories', 'widget name', TRUE), array());
		}
		
		public function widget($args, $instance)
		{
			echo ether_widget::success_stories($instance['title'], $instance['category'], $instance['count']);
		}
			
		public function update($new_instance, $old_instance)
		{
			return $new_instance;
		}
		
		public function form($instance)
		{
			$title = esc_attr($instance['title']);
			$count = esc_attr($instance['count']);
			$category = esc_attr($instance['category']);
			
			$categories = get_categories(array('type' => 'success-stories', 'hierarchical' => FALSE, 'taxonomy' => 'success-stories_category'));
			
			echo '<p><label for="'.$this->get_field_name('title').'">'.ether::langr('Title').'</label><input type="text" class="widefat" name="'.$this->get_field_name('title').'" value="'.$title.'" /></p>';
			echo '<p><label for="'.$this->get_field_name('category').'">'.ether::langr('Categories ID').'</label><input type="text" class="widefat" name="'.$this->get_field_name('category').'" value="'.$category.'" /></p>';
			/*echo '<p><select name="'.$this->get_field_name('category').'"><option value="-1"'.($category == -1 ? ' selected="selected"' : '').'>All</option>';
			
			foreach ($categories as $cat)
			{
				echo '<option value="'.$cat->slug.'"'.($category == $cat->slug ? ' selected="selected"' : '').'>'.$cat->cat_name.'</option>';
			}

			echo '</select></p>';*/
		}
	}
	
	class ether_widget_search extends WP_Widget
	{
		public function __construct()
		{
			parent::__construct(FALSE, '&nbsp;'.ether::config('theme_name').' '.ether::langx('Search', 'widget name', TRUE), array());
		}
		
		public function widget($args, $instance)
		{
			echo ether_widget::search($instance['title']);
		}
			
		public function update($new_instance, $old_instance)
		{
			return $new_instance;
		}
		
		public function form($instance)
		{
			$title = esc_attr($instance['title']);
			
			echo '<p><label for="'.$this->get_field_name('title').'">'.ether::langr('Title').'</label><input type="text" class="widefat" name="'.$this->get_field_name('title').'" value="'.$title.'" /></p>';
		}
	}
	
	class ether_widget_archives extends WP_Widget
	{
		public function __construct()
		{
			parent::__construct(FALSE, '&nbsp;'.ether::config('theme_name').' '.ether::langx('Archives', 'widget name', TRUE), array());
		}
		
		public function widget($args, $instance)
		{
			echo ether_widget::archives($instance['title']);
		}
		
		public function update($new_instance, $old_instance)
		{
			return $new_instance;
		}
		
		public function form($instance)
		{
			$title = esc_attr($instance['title']);
			
			echo '<p><label for="'.$this->get_field_name('title').'">'.ether::langr('Title').'</label><input type="text" class="widefat" name="'.$this->get_field_name('title').'" value="'.$title.'" /></p>';
		}
	}
	
	class ether_widget_links extends WP_Widget
	{
		public function __construct()
		{
			parent::__construct(FALSE, '&nbsp;'.ether::config('theme_name').' '.ether::langx('Links', 'widget name', TRUE), array());
		}
		
		public function widget($args, $instance)
		{
			echo ether_widget::links($instance['title']);
		}
		
		public function update($new_instance, $old_instance)
		{
			return $new_instance;
		}
		
		public function form($instance)
		{
			$title = esc_attr($instance['title']);
			
			echo '<p><label for="'.$this->get_field_name('title').'">'.ether::langr('Title').'</label><input type="text" class="widefat" name="'.$this->get_field_name('title').'" value="'.$title.'" /></p>';
		}
	}
	
	class ether_widget_tags extends WP_Widget
	{
		public function __construct()
		{
			parent::__construct(FALSE, '&nbsp;'.ether::config('theme_name').' '.ether::langx('Tags', 'widget name', TRUE), array());
		}
		
		public function widget($args, $instance)
		{
			echo ether_widget::tags($instance['title']);
		}
		
		public function update($new_instance, $old_instance)
		{
			return $new_instance;
		}
		
		public function form($instance)
		{
			$title = esc_attr($instance['title']);
			
			echo '<p><label for="'.$this->get_field_name('title').'">'.ether::langr('Title').'</label><input type="text" class="widefat" name="'.$this->get_field_name('title').'" value="'.$title.'" /></p>';
		}
	}

	register_widget('ether_widget_tweets');
	register_widget('ether_widget_contact');
	register_widget('ether_widget_menu');
	register_widget('ether_widget_recent_posts');
	register_widget('ether_widget_popular_posts');
	register_widget('ether_widget_featured_posts');
	register_widget('ether_widget_recent_news');
	register_widget('ether_widget_popular_news');
	register_widget('ether_widget_featured_news');
	register_widget('ether_widget_flickr_feed');
	register_widget('ether_widget_testimonials');
	register_widget('ether_widget_success_stories');
	register_widget('ether_widget_search');
	register_widget('ether_widget_archives');
	register_widget('ether_widget_links');
	register_widget('ether_widget_tags');
?>
