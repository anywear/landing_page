<?php
	$ether_root = dirname(dirname(dirname(__FILE__))).'/';
	$wp_root = dirname(dirname(dirname(dirname(dirname(dirname(__FILE__)))))).'/';
	
	require_once($wp_root.'wp-load.php');
	require_once($ether_root.'ether/ether.php');

	if ( ! is_user_logged_in() || ! current_user_can('edit_posts')) 
	{
		wp_die(__('Access denied'));
	}
	
	function disable_admin_bar()
	{
		return false;
	}
	
	add_filter('show_admin_bar' , 'my_function_admin_bar');
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>Shortcode</title>
		<?php
			wp_admin_css('css/global');
			wp_admin_css('css/wp-admin');
			wp_admin_css('css/media');
			wp_admin_css('css/colors-fresh');
			wp_admin_css('css/ie');
			wp_enqueue_script('jquery');
			wp_enqueue_script('utils');
		?>
		
		<script type="text/javascript" src="<?php ether::path('ether/media/scripts/jquery.js'); ?>"></script>
		<script type="text/javascript" src="<?php ether::path('ether/media/scripts/jquery.ui.js'); ?>"></script>
		<script type="text/javascript">
			$( function()
			{
				var vars = [], hash;
				var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');

				for (var i = 0; i < hashes.length; i++)
				{
					hash = hashes[i].split('=');
					vars.push(hash[0]);
					vars[hash[0]] = hash[1];
				}
				
				if (typeof vars['title'] != 'undefined')
				{
					$('title').text(vars['title'].replace(/\+/g, ' '));
				}

				$('.slider.width').slider
				({
					range: 'min',
					min: 0,
					max: 100,
					value: 100,
					slide: function(event, ui)
					{
						$('input[name=width]').val(ui.value + '%');
						$('div.slider-value').text(ui.value + '%');
						$('div.slider-value').css('left', $('div.slider-value').parent().width() * (ui.value / 100)  - $('div.slider-value').width() / 2);
						
						if (ui.value == 0 || ui.value == 100)
						{
							$('div.slider-value').hide();
						} else
						{
							$('div.slider-value').show();
						}
					}
				});
				
				$('.slider.columns').slider
				({
					range: 'min',
					min: 1,
					max: 4,
					value: 4,
					step: 1,
					slide: function(event, ui)
					{
						$('input[name=columns]').val(ui.value);
						$('div.slider-value').text(ui.value);
						$('div.slider-value').css('left', $('div.slider-value').parent().width() * ((ui.value - 1) / 3)  - $('div.slider-value').width() / 2);

						if (ui.value == 1 || ui.value == 4)
						{
							$('div.slider-value').hide();
						} else
						{
							$('div.slider-value').show();
						}
					}
				});

				if (typeof tinyMCEPopup != 'undefined')
				{
					tinyMCEPopup.resizeToInnerSize();
				
					$('form').submit( function()
					{
						return false;
					});

					$('input[name=insert]').click( function()
					{
						if (window.tinyMCE)
						{
							var selection = tinyMCE.activeEditor.selection.getContent();
							var shortcode_begin = '';
							var shortcode_end = '';
							var shortcode_args = '';
							var shortcode_content = '';

							var shortcode = vars['name'];
							var single = typeof vars['single'] != 'undefined';
							var checkboxes = {};
							
							$('input[type=text], select, textarea, input[type=hidden], input[type=checkbox]').each( function(i, input)
							{
								if ( ! $(this).hasClass('custom'))
								{
									if ($(this).is('input'))
									{
										if ($(this).attr('type') == 'checkbox')
										{
											if ($(this).is(':checked'))
											{
												var name = $(this).attr('name').replace(/[[]]/g, '');
												
												if (typeof checkboxes[name] == 'undefined')
												{
													checkboxes[name] = [];
												}

												checkboxes[name].push($(this).val());
											}
										} else
										{
											if ($(this).val() != '')
											{
												shortcode_args += ' ' + $(this).attr('name') + '="' + $(this).val() + '"';
											}
										}
									} else if ($(this).is('select'))
									{
										if ($(this).children('option:selected').val() != '')
										{
											shortcode_args += ' ' + $(this).attr('name') + '="' + $(this).children('option:selected').val() + '"';
										}
									} else if ($(this).is('textarea'))
									{
										if ($(this).text() != '')
										{
											shortcode_args += ' ' + $(this).attr('name') + '="' + $(this).text() + '"';
										}
									}
								}
							});
							
							$.each(checkboxes, function(k, arr)
							{
								shortcode_args += ' ' + k + '="' + checkboxes[k].join(', ') + '"';
							});
							
							shortcode_begin = '[' + shortcode + (shortcode_args != '' ? ' ' + $.trim(shortcode_args) : '') + ']';
							
							if ( ! single)
							{
								shortcode_end = '[/' + shortcode + ']';
							}
							
							if (typeof window.shortcode_content_callback != 'undefined')
							{
								shortcode_content = window.shortcode_content_callback(shortcode_content);
							}
							
							if (selection.length > 0)
							{
								var content = tinyMCE.activeEditor.selection.getContent();
						
								window.tinyMCE.execInstanceCommand('content', 'mceInsertContent', false,  ' ' + shortcode_begin + content + shortcode_content + shortcode_end + ' ');
							} else
							{
								window.tinyMCE.execInstanceCommand('content', 'mceInsertContent', false,  ' ' + shortcode_begin + shortcode_content + (shortcode_end != '' ? ' ' : '') + shortcode_end + ' ');
							}
					
							tinyMCEPopup.editor.execCommand('mceRepaint');
							tinyMCEPopup.close();
						}
						
						return false;
					});
				}
			});
		</script>
		<link rel="stylesheet" href="<?php ether::path('media/stylesheets/style.css'); ?>" />
		<link rel="stylesheet" href="<?php ether::path('ether/media/stylesheets/style.css'); ?>" media="all" />
		<link rel="stylesheet" href="<?php ether::path('ether/media/stylesheets/jquery.ui.css'); ?>" />
		<?php wp_head(); ?>
		<style type="text/css">
			html, body { margin: 0 !important; padding: 0 !important; background-color: #fff; }
			form.shortcode { margin: 0; }
			form.shortcode li { position: relative; }
			.hidden { display: none !important; }
			.inline-icon { position: absolute; top: 35px; right: -30px; }
		</style>
		
		<?php if (isset($_GET['tinymce'])) { ?>
		<script type="text/javascript" src="<?php echo get_option('siteurl') ?>/wp-includes/js/tinymce/tiny_mce_popup.js"></script>
		<script type="text/javascript" src="<?php echo get_option('siteurl') ?>/wp-includes/js/tinymce/utils/mctabs.js"></script>
		<script type="text/javascript" src="<?php echo get_option('siteurl') ?>/wp-includes/js/tinymce/utils/form_utils.js"></script>
		<?php } ?>
	</head>
	
	<body>
		<form method="post" class="shortcode">
