<?php include('../modalbox/header.php'); ?>

	<script type="text/javascript">
		$j = jQuery.noConflict();
		
		function filter_items(skip_first)
		{
			var filter = $j('input[name=search]').val();
			
			if (filter != '')
			{
				$j('ul.items').find('>li').filter(':not(:contains(' + filter + '))').hide();
				$j('ul.items').find('>li').filter(':contains(' + filter + ')').show();
			} else
			{
				$j('ul.items li').show();
			}
		}
		
		$j( function()
		{
			$j('input[name=search]').keyup( function()
			{
				filter_items();
			}).change( function()
			{
				filter_items();
			});
			
			$j('input[name=insert]').click( function()
			{
				var items = [];
				
				$j('input[type=checkbox]').each( function()
				{
					if ($j(this).is(':checked'))
					{
						items.push({ id: $j(this).val(), title: $j.trim($j(this).parent('label').text().replace(/"/g, '')), thumbnail: $j(this).parent('label').next('img').attr('src')});
					}
				});
				
				parent.window.send_to_editor(items);
				
				return false;
			});
		});
	
	</script>

	<fieldset class="ether"> 
		<ul>
			<li>
				<label><?php ether::lang('Search'); ?></label>
				<input type="text" name="search" value="" />
			</li>
		</ul>
		<ul class="items">
		<?php
			$slides = ether::get_posts(array('post_type' => 'slide', 'numberposts' => -1));
			
			foreach ($slides as $slide)
			{
				$thumbnail = ether::meta('preview_image', TRUE, $slide['id']);
				
				if ( ! empty($thumbnail))
				{
					$thumbnail = ether::get_image_thumbnail($thumbnail, 220, 165);
				} else
				{
					$thumbnail = ether::path('media/images/placeholders/placeholder.png', TRUE);
				}
				
				?><li><label><input type="checkbox" name="id" value="<?php echo $slide['id']; ?>" /> <?php echo $slide['title']; ?></label><img src="<?php echo $thumbnail; ?>" width="220" height="165" style="display: none;" /></li><?php
			}
		?></ul>
	</fieldset>
	
<?php include('../modalbox/footer.php'); ?>
