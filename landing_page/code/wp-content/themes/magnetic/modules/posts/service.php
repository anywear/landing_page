<?php
	class ether_service extends ether_post_type
	{
		public static function init()
		{
			ether::register_post('Service', array
			(
				'supports' => array('title', 'editor'),
				'public' => TRUE,
				'publicly_queryable' => FALSE,
				'rewrite' => FALSE,
				'has_archive' => FALSE, 
				'hierarchical' => FALSE,
				'menu_position' => 9,
				'menu_icon' => ether::path('admin/media/images/cpt/service-small.png', TRUE),
				'labels' => array
				(
					'name' => ether::langx('Services', 'CPT (Services) name', TRUE),
					'singular_name' => ether::langx('Service', 'CPT (Services) singular name', TRUE),
					'add_new' => ether::langx('Add new', 'CPT (Services)', TRUE),
					'add_new_item' => ether::langx('Add new service', 'CPT (Services)', TRUE),
					'edit_item' => ether::langx('Edit service', 'CPT (Services)', TRUE),
					'new_item' => ether::langx('New service', 'CPT (Services)', TRUE),
					'view_item' => ether::langx('View service', 'CPT (Services)', TRUE),
					'search_items' => ether::langx('Search services', 'CPT (Services)', TRUE),
					'not_found' =>  ether::langx('No services found', 'CPT (Services)', TRUE),
					'not_found_in_trash' => ether::langx('No services found in Trash', 'CPT (Services)', TRUE),
					'menu_name' => ether::langx('Services', 'CPT (Services) menu name', TRUE)
				)
			));
		}
	};
	
?>
