<?php
	class ether_slide extends ether_post_type
	{
		public static function init()
		{
			ether::register_post('Slide', array
			(
				'supports' => array('title', 'editor'),
				'public' => TRUE,
				'publicly_queryable' => FALSE,
				'rewrite' => FALSE,
				'has_archive' => FALSE, 
				'hierarchical' => FALSE,
				'menu_position' => 9,
				'menu_icon' => ether::path('admin/media/images/cpt/slide-small.png', TRUE),
				'labels' => array
				(
					'name' => ether::langx('Slides', 'CPT (Slides) name', TRUE),
					'singular_name' => ether::langx('Slide', 'CPT (Slides) singular name', TRUE),
					'add_new' => ether::langx('Add new', 'CPT (Slides)', TRUE),
					'add_new_item' => ether::langx('Add new slide', 'CPT (Slides)', TRUE),
					'edit_item' => ether::langx('Edit slide', 'CPT (Slides)', TRUE),
					'new_item' => ether::langx('New slide', 'CPT (Slides)', TRUE),
					'view_item' => ether::langx('View slide', 'CPT (Slides)', TRUE),
					'search_items' => ether::langx('Search slides', 'CPT (Slides)', TRUE),
					'not_found' =>  ether::langx('No slides found', 'CPT (Slides)', TRUE),
					'not_found_in_trash' => ether::langx('No slides found in Trash', 'CPT (Slides)', TRUE),
					'menu_name' => ether::langx('Slides', 'CPT (Slides) menu name', TRUE)
				)
			));
		}
	};
	
?>
