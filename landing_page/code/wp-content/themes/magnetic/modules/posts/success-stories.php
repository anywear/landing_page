<?php
	class ether_success_stories extends ether_post_type
	{
		public static function init()
		{
			ether::register_post('Success stories', array
			(
				'supports' => array('title', 'editor', 'excerpt'),
				'public' => TRUE,
				'publicly_queryable' => TRUE,
				'has_archive' => FALSE, 
				'hierarchical' => FALSE,
				'menu_position' => 13,
				'taxonomies' => array('success-stories_category'),
				'menu_icon' => ether::path('admin/media/images/cpt/success-stories-small.png', TRUE),
				'labels' => array
				(
					'name' => ether::langx('Success stories', 'CPT (Success stories) name', TRUE),
					'singular_name' => ether::langx('Succes story', 'CPT (Success stories) singular name', TRUE),
					'add_new' => ether::langx('Add new', 'CPT (Success stories)', TRUE),
					'add_new_item' => ether::langx('Add new success story', 'CPT (Success stories)', TRUE),
					'edit_item' => ether::langx('Edit success story', 'CPT (Success stories)', TRUE),
					'new_item' => ether::langx('New success story', 'CPT (Success stories)', TRUE),
					'view_item' => ether::langx('View success story', 'CPT (Success stories)', TRUE),
					'search_items' => ether::langx('Search success stories', 'CPT (Success stories)', TRUE),
					'not_found' =>  ether::langx('No success stories found', 'CPT (Success stories)', TRUE),
					'not_found_in_trash' => ether::langx('No success stories found in Trash', 'CPT (Success stories)', TRUE),
					'menu_name' => ether::langx('Success stories', 'CPT (Success stories) menu name', TRUE)
				)
			));
		}

		public static function wp_init()
		{
			register_taxonomy('success-stories_category', 'success-stories', array('hierarchical' => TRUE, 'label' => ether::langr('Categories'), 'query_var' => TRUE, 'rewrite' => TRUE));
		}
	};
	
?>
