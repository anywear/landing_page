<?php
	class ether_testimonials extends ether_post_type
	{
		public static function init()
		{
			ether::register_post('Testimonials', array
			(
				'supports' => array('title', 'editor', 'excerpt'),
				'public' => TRUE,
				'publicly_queryable' => TRUE,
				'has_archive' => FALSE, 
				'hierarchical' => FALSE,
				'menu_position' => 12,
				'taxonomies' => array('testimonials_category'),
				'menu_icon' => ether::path('admin/media/images/cpt/testimonials-small.png', TRUE),
				'labels' => array
				(
					'name' => ether::langx('Testimonials', 'CPT (Testimonials) name', TRUE),
					'singular_name' => ether::langx('Testimonial', 'CPT (Testimonials) singular name', TRUE),
					'add_new' => ether::langx('Add new', 'CPT (Testimonials)', TRUE),
					'add_new_item' => ether::langx('Add new testimonial', 'CPT (Testimonials)', TRUE),
					'edit_item' => ether::langx('Edit testimonial', 'CPT (Testimonials)', TRUE),
					'new_item' => ether::langx('New testimonial', 'CPT (Testimonials)', TRUE),
					'view_item' => ether::langx('View testimonial', 'CPT (Testimonials)', TRUE),
					'search_items' => ether::langx('Search testimonials', 'CPT (Testimonials)', TRUE),
					'not_found' =>  ether::langx('No testimonials found', 'CPT (Testimonials)', TRUE),
					'not_found_in_trash' => ether::langx('No testimonials found in Trash', 'CPT (Testimonials)', TRUE),
					'menu_name' => ether::langx('Testimonials', 'CPT (Testimonials) menu name', TRUE)
				)
			));
		}
		
		public static function get()
		{
			$posts = ether::get_posts(array
			(
				'post_type' => 'testimonials',
				'numberposts' => -1
			));
			
			print_r($posts);
		}
		
		public static function wp_init()
		{
			register_taxonomy('testimonials_category', 'testimonials', array('hierarchical' => TRUE, 'label' => ether::langr('Categories'), 'query_var' => TRUE, 'rewrite' => TRUE));
		}
	};
	
?>
