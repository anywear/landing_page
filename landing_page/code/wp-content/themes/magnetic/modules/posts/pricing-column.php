<?php
	class ether_pricing_column extends ether_post_type
	{
		public static function init()
		{
			$name = 'Pricing columns';
			ether::register_post('Pricing column', array
			(
				'supports' => array('title', 'editor'),
				'public' => TRUE,
				'publicly_queryable' => FALSE,
				'rewrite' => FALSE,
				'has_archive' => FALSE, 
				'hierarchical' => FALSE,
				'menu_position' => 12,
				'menu_icon' => ether::path('admin/media/images/cpt/pricing-column-small.png', TRUE),
				'labels' => array
				(
					'name' => ether::langx('Pricing columns', 'CPT (Pricing columns) name', TRUE),
					'singular_name' => ether::langx('Pricing column', 'CPT (Pricing columns) singular name', TRUE),
					'add_new' => ether::langx('Add new', 'CPT (Pricing columns)', TRUE),
					'add_new_item' => ether::langx('Add new pricing column', 'CPT (Pricing columns)', TRUE),
					'edit_item' => ether::langx('Edit pricing column', 'CPT (Pricing columns)', TRUE),
					'new_item' => ether::langx('New pricing column', 'CPT (Pricing columns)', TRUE),
					'view_item' => ether::langx('View pricing column', 'CPT (Pricing columns)', TRUE),
					'search_items' => ether::langx('Search pricing columns', 'CPT (Pricing columns)', TRUE),
					'not_found' =>  ether::langx('No pricing columns found', 'CPT (Pricing columns)', TRUE),
					'not_found_in_trash' => ether::langx('No pricing columns found in Trash', 'CPT (Pricing columns)', TRUE),
					'menu_name' => ether::langx('Pricing tables', 'CPT (Pricing columns) menu name', TRUE)
				)
			));
		}
	};
	
?>
