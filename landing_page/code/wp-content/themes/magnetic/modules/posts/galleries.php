<?php
	class ether_galleries extends ether_post_type
	{
		public static function init()
		{
			ether::register_post('Gallery', array
			(
				'supports' => array('title', 'editor', 'page-attributes', 'excerpt'),
				'public' => TRUE,
				'publicly_queryable' => TRUE,
				'rewrite' => array
				(
					'slug' => 'gallery',
					'with_front' => FALSE
				),
				'has_archive' => FALSE,
				'menu_position' => 5,
				'menu_icon' => ether::path('admin/media/images/cpt/galleries-small.png', TRUE),
				'labels' => array
				(
					'name' => ether::langx('Gallery', 'CPT (Gallery) name', TRUE),
					'singular_name' => ether::langx('Gallery', 'CPT (Gallery) singular name', TRUE),
					'add_new' => ether::langx('Add new', 'CPT (Gallery)', TRUE),
					'add_new_item' => ether::langx('Add new gallery', 'CPT (Gallery)', TRUE),
					'edit_item' => ether::langx('Edit gallery', 'CPT (Gallery)', TRUE),
					'new_item' => ether::langx('New gallery', 'CPT (Gallery)', TRUE),
					'view_item' => ether::langx('View gallery', 'CPT (Gallery)', TRUE),
					'search_items' => ether::langx('Search galleries', 'CPT (Gallery)', TRUE),
					'not_found' =>  ether::langx('No galleries found', 'CPT (Gallery)', TRUE),
					'not_found_in_trash' => ether::langx('No galleries found in Trash', 'CPT (Gallery)', TRUE),
					'menu_name' => ether::langx('Galleries', 'CPT (Gallery) menu name', TRUE)
				)
			));
		}
	};
	
?>
