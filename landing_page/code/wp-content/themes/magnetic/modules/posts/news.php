<?php
	class ether_news extends ether_post_type
	{
		public static function init()
		{
			ether::register_post('News', array
			(
				'supports' => array('title', 'editor', 'page-attributes', 'author', 'excerpt', 'custom-fields', 'comments'),
				'menu_position' => 4,
				'taxonomies' => array('post_tag', 'news_category'),
				'menu_icon' => ether::path('admin/media/images/cpt/news-small.png', TRUE),
				'custom_template' => TRUE,
				'labels' => array
				(
					'name' => ether::langx('News', 'CPT (News) name', TRUE),
					'singular_name' => ether::langx('Gallery', 'CPT (News) singular name', TRUE),
					'add_new' => ether::langx('Add new', 'CPT (News)', TRUE),
					'add_new_item' => ether::langx('Add new news', 'CPT (News)', TRUE),
					'edit_item' => ether::langx('Edit news', 'CPT (News)', TRUE),
					'new_item' => ether::langx('New news', 'CPT (News)', TRUE),
					'view_item' => ether::langx('View news', 'CPT (News)', TRUE),
					'search_items' => ether::langx('Search news', 'CPT (News)', TRUE),
					'not_found' =>  ether::langx('No news found', 'CPT (News)', TRUE),
					'not_found_in_trash' => ether::langx('No news found in Trash', 'CPT (News)', TRUE),
					'menu_name' => ether::langx('News', 'CPT (News) menu name', TRUE)
				)
			));
		}
		
		public static function wp_init()
		{
			register_taxonomy('news_category', 'news', array('hierarchical' => TRUE, 'label' => ether::langr('Categories'), 'query_var' => TRUE, 'rewrite' => TRUE));
		}
	};
	
?>
