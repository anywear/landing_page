<?php
	class ether_sidebar extends ether_module
	{
		protected static $sidebars;
		protected static $sidebar_relations;
		protected static $default;
		protected static $align;
		protected static $sidebar;
		protected static $sidebar_count;
		
		public static function init()
		{
			ether::bind('ether.ajax', array('ether_sidebar', 'ajax'));

			global $table_prefix;
			
			self::$sidebars = $table_prefix.ether::config('prefix').'sidebars';
			self::$sidebar_relations = $table_prefix.ether::config('prefix').'sidebar_relations';
			self::$default = 'Default';
			
			if (function_exists('register_sidebar'))
			{
				$sidebars = self::get_all();
				self::$sidebar_count = count($sidebars);
				array_unshift($sidebars, array( 'id' => NULL, 'name' => self::$default));
				
				foreach ($sidebars as $sidebar)
				{
					register_sidebar( array
					(
						'name' => $sidebar['name'],
						'before_widget' => '<div class="widget">',
						'after_widget' => '</div>',
						'before_title' => '<h2>',
						'after_title' => '</h2>'
					));
				}
			}
		}

		public static function setup()
		{
			global $post;
			$sidebar = NULL;
			$sidebar_name = ether::meta('sidebar');
			$sidebar_align = ether::meta('sidebar_align');
			$sidebar_default = ($sidebar_align == 'default');

			if (empty($sidebar_align) OR $sidebar_align == 'default')
			{
				$sidebar_align = 'right';
			}
			
			self::$align = $sidebar_align;	
			
			if ($sidebar_align != 'none')
			{
				if (empty($sidebar_name) OR $sidebar_name == 'inherited')
				{
					if (self::$sidebar_count == 0 AND (empty($sidebar_name) OR $sidebar_name == 'inherited'))
					{
						self::$sidebar = 'Default';
					} else
					{
						if (is_single())
						{
							$taxonomy = 'category';
							
							if ($post->post_type != 'post')
							{
								$taxonomy = $post->post_type.'_'.$taxonomy;
							}
							
							$ids = array();
							$taxonomies = wp_get_object_terms($post->ID, $taxonomy);
							
							foreach ($taxonomies as $tax)
							{
								$ids[] = $tax->term_id;
							}
		
							self::$sidebar = self::find('post', $post->ID, $ids);
						} else if (is_page() OR get_option('show_on_front') != 'posts')
						{
							$id = ether::get_id();
							
							/*if (is_page())
							{
								$id = $post->ID;
							} else if (is_home() AND get_option('show_on_front') != 'posts')
							{
								$id = get_option('page_for_posts');
							}*/
							
							self::$sidebar = self::find('page', $id);
						} else
						{
							self::$sidebar = self::find(NULL, NULL);
						}
					}
				} else
				{
					self::$sidebar = $sidebar_name;
				}
			}
			
			
			if ($sidebar_default AND empty(self::$sidebar))
			{
				self::$align = 'none';
			}
		}
		
		public static function get_sidebar()
		{
			if ( ! function_exists('dynamic_sidebar') || ! dynamic_sidebar(self::$sidebar))
			{
				ether::import('template.sidebar.empty');
			}
		}
		
		public static function get_align()
		{
			return self::$align;
		}
		
		public static function install()
		{
			global $wpdb;
			global $table_prefix;
			
			self::$sidebars = $table_prefix.ether::config('prefix').'sidebars';
			self::$sidebar_relations = $table_prefix.ether::config('prefix').'sidebar_relations';
			
			$wpdb->query
			('CREATE TABLE IF NOT EXISTS `'.self::$sidebars.'` (
				`id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
				`name` varchar(64) NOT NULL DEFAULT \'\',
				PRIMARY KEY (`id`)
			) ENGINE=MyISAM  DEFAULT CHARSET=utf8;');
			
			$wpdb->query
			('CREATE TABLE IF NOT EXISTS `'.self::$sidebar_relations.'` (
				`id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
				`sidebar_id` bigint(20) unsigned NOT NULL DEFAULT \'0\',
				`relation_type` varchar(20) NOT NULL DEFAULT \'\',
				`relation_id` bigint(20) unsigned NOT NULL DEFAULT \'0\',
				PRIMARY KEY (`id`)
			) ENGINE=MyISAM  DEFAULT CHARSET=utf8;');
		}
		
		public static function clean()
		{
			global $wpdb;
			global $table_prefix;
			
			//$wpdb->query('TRUNCATE TABLE `'.self::$sidebars.'`');
			//$wpdb->query('TRUNCATE TABLE `'.self::$sidebar_relations.'`');
		}
		
		public static function find($relation_type, $relation_id, $categories = array())
		{
			$relation_type = ether::clean($relation_type, TRUE, TRUE, TRUE);
			$relation_id = ether::clean($relation_id, TRUE, TRUE, TRUE);
			$sidebar_id = NULL;
			
			if ( ! empty($relation_type) AND ! empty($relation_id))
			{
				if ($relation_type == 'category' OR $relation_type == 'page' OR $relation_type == 'post')
				{
					if ($relation_type == 'post')
					{
						$relation = self::get_relation($relation_type, $relation_id);
						
						if (count($relation) == 0)
						{
							if ( ! empty($categories))
							{
								$relation = self::get_relation('category', $categories);

								if (count($relation) > 0)
								{
									$sidebar_id = $relation[0]['sidebar_id'];
								}
							}
						} else
						{
							$sidebar_id = $relation[0]['sidebar_id'];
						}
					} else
					{
						$relation = self::get_relation($relation_type, $relation_id);
						
						if (count($relation) > 0)
						{
							$sidebar_id = $relation[0]['sidebar_id'];
						}
					}
				}
			}
			
			return self::get_name($sidebar_id);
		}
		
		public static function get_name($id)
		{
			global $wpdb;
			
			$id = ether::clean($id, TRUE, TRUE, TRUE);
			
			if ( ! empty($id))
			{
				$sidebars = $wpdb->get_results('SELECT `name` FROM `'.self::$sidebars.'` WHERE `id`=\''.$id.'\'', ARRAY_A);
				
				if (count($sidebars) > 0)
				{
					return $sidebars[0]['name'];
				}
			}
			
			return '';
		}
		
		public static function get_all()
		{
			global $wpdb;
			global $table_prefix;
			
			return $wpdb->get_results('SELECT `id`, `name` FROM `'.self::$sidebars.'` LIMIT 0, 500', ARRAY_A);
		}
		
		public static function add($name)
		{
			global $wpdb;
			global $table_prefix;
			
			$name = ether::clean($name, TRUE, TRUE, TRUE);
			
			if ( ! empty($name))
			{
				$sidebars = $wpdb->get_results('SELECT `name` FROM `'.self::$sidebars.'` WHERE `name`=\''.$name.'\'');
				
				if (count($sidebars) > 0)
				{
					return FALSE;
				}
				
				$wpdb->insert(self::$sidebars, array('name' => $name), array('%s'));
			} else
			{
				return FALSE;
			}
			
			return TRUE;
		}
		
		public static function delete($id)
		{
			global $wpdb;
			
			$id = ether::clean($id, TRUE, TRUE, TRUE);
			
			if ( ! empty($id))
			{
				$wpdb->query('DELETE FROM `'.self::$sidebars.'` WHERE `id`=\''.$id.'\'');
				$wpdb->query('DELETE FROM `'.self::$sidebar_relations.'` WHERE `sidebar_id`=\''.$id.'\'');
				
				return TRUE;
			}
			
			return FALSE;
		}
		
		public static function edit($id, $name)
		{
			global $wpdb;
			
			$id = ether::clean($id, TRUE, TRUE, TRUE);
			$name = ether::clean($name, TRUE, TRUE, TRUE);
			
			if ( ! empty($id) AND ! empty($name))
			{
				$sidebars = $wpdb->get_results('SELECT `name` FROM `'.self::$sidebars.'` WHERE `name`=\''.$name.'\'');
				
				if (count($sidebars) > 0)
				{
					return FALSE;
				}
				
				$wpdb->update(self::$sidebars, array('name' => $name), array('id' => $id), array('%s'), array('%d'));
				
				return TRUE;
			}
			
			return FALSE;
		}
		
		public static function get_relation($relation_type, $relation_id)
		{
			global $wpdb;
			
			$relation_type = ether::clean($relation_type, TRUE, TRUE, TRUE);
			$relation_id = ether::clean($relation_id, TRUE, TRUE, TRUE);
			
			if ( ! empty($relation_type) AND ! empty($relation_id))
			{
				if ($relation_type == 'category' OR $relation_type == 'page' OR $relation_type == 'post')
				{
					if ( ! is_array($relation_id))
					{
						$relation_id = array($relation_id);
					}
					
					$relation_id = implode('\' AND `relation_type`=\''.$relation_type.'\') OR (`relation_id`=\'', $relation_id);

					return $wpdb->get_results('SELECT `id`, `sidebar_id` FROM `'.self::$sidebar_relations.'` WHERE (`relation_id`=\''.$relation_id.'\' AND `relation_type`=\''.$relation_type.'\') LIMIT 0, 500', ARRAY_A);
				}
			}
			
			return array();
		}
		
		public static function get_relation_by_id($id)
		{
			global $wpdb;
			
			$id = ether::clean($id, TRUE, TRUE, TRUE);

			return $wpdb->get_results('SELECT `id`, `sidebar_id`, `relation_type`, `relation_id` FROM `'.self::$sidebar_relations.'` WHERE `id`=\''.$id.'\' LIMIT 0, 500', ARRAY_A);
		}
		
		public static function get_relation_all()
		{
			global $wpdb;

			return $wpdb->get_results('SELECT `id`, `sidebar_id`, `relation_type`, `relation_id` FROM `'.self::$sidebar_relations.'` LIMIT 0, 500', ARRAY_A);
		}
		
		public static function is_relation($sidebar_id, $relation_type, $relation_id)
		{
			global $wpdb;
			
			$sidebar_id = ether::clean($sidebar_id, TRUE, TRUE, TRUE);
			$relation_type = ether::clean($relation_type, TRUE, TRUE, TRUE);
			$relation_id = ether::clean($relation_id, TRUE, TRUE, TRUE);
			
			if ($relation_type == 'category' OR $relation_type == 'page' OR $relation_type == 'post')
			{
				if ( ! empty($sidebar_id) AND ! empty($relation_id))
				{
					$sidebar_relations = $wpdb->get_results('SELECT `id` FROM `'.self::$sidebar_relations.'` WHERE `sidebar_id`=\''.$sidebar_id.'\' AND `relation_type`=\''.$relation_type.'\' AND `relation_id`=\''.$relation_id.'\'');
					if (count($sidebar_relations) > 0)
					{
						return TRUE;
					}
				}
			}
			
			return FALSE;
		}
		
		public static function add_relation($sidebar_id, $relation_type, $relation_id)
		{
			global $wpdb;

			$sidebar_id = ether::clean($sidebar_id, TRUE, TRUE, TRUE);
			$relation_type = ether::clean($relation_type, TRUE, TRUE, TRUE);
			$relation_id = ether::clean($relation_id, TRUE, TRUE, TRUE);
			
			if ($relation_type == 'category' OR $relation_type == 'page' OR $relation_type == 'post')
			{
				if ( ! empty($sidebar_id) AND ! empty($relation_id))
				{
					$sidebars = $wpdb->get_results('SELECT `id` FROM `'.self::$sidebars.'` WHERE `id`=\''.$sidebar_id.'\'');
				
					if (count($sidebars) == 0)
					{
						return FALSE;
					}
					
					$sidebar_relations = $wpdb->get_results('SELECT `id` FROM `'.self::$sidebar_relations.'` WHERE `sidebar_id`=\''.$sidebar_id.'\' AND `relation_type`=\''.$relation_type.'\' AND `relation_id`=\''.$relation_id.'\'');
					
					if (count($sidebar_relations) == 0)
					{
						$wpdb->insert(self::$sidebar_relations, array('sidebar_id' => $sidebar_id, 'relation_type' => $relation_type, 'relation_id' => $relation_id), array('%d', '%s', '%d'));
						
						return TRUE;
					}
				}
			}
			
			return FALSE;
		}
		
		public static function edit_relation_by_id($id, $relation_id)
		{
			global $wpdb;
			
			$id = ether::clean($id, TRUE, TRUE, TRUE);
			$relation_id = ether::clean($relation_id, TRUE, TRUE, TRUE);
			
			if ( ! empty($id) AND ! empty($relation_id))
			{
				$relations = $wpdb->get_results('SELECT `id` FROM `'.self::$sidebar_relations.'` WHERE `id`=\''.$id.'\'');
				
				if (count($relations) == 0)
				{
					return FALSE;
				}
				
				$wpdb->update(self::$sidebar_relations, array('relation_id' => $relation_id), array('id' => $id), array('%d'), array('%d'));
				
				return TRUE;
			}
			
			return FALSE;
		}
		
		public static function edit_relation($sidebar_id, $relation_type, $relation_id, $id)
		{
			global $wpdb;
			
			$sidebar_id = ether::clean($sidebar_id, TRUE, TRUE, TRUE);
			$relation_type = ether::clean($relation_type, TRUE, TRUE, TRUE);
			$relation_id = ether::clean($relation_id, TRUE, TRUE, TRUE);
			$id = ether::clean($id, TRUE, TRUE, TRUE);
			
			if (self::is_relation($sidebar_id, $relation_type, $relation_id))
			{
				$wpdb->update(self::$sidebar_relations, array('relation_id' => $id), array('sidebar_id' => $sidebar_id, 'relation_type' => $relation_type, 'relation_id' => $relation_id), array('%d'), array('%d', '%s', '%d'));
				
				return TRUE;
			}
			
			return FALSE;
		}
		
		public static function delete_relation($id)
		{
			global $wpdb;
			
			$id = ether::clean($id, TRUE, TRUE, TRUE);
			
			if ( ! empty($id))
			{
				$wpdb->query('DELETE FROM `'.self::$sidebar_relations.'` WHERE `id`=\''.$id.'\'');
				
				return TRUE;
			}
			
			return FALSE;
		}

		public static function ajax($data)
		{
			if (isset($data['sidebar']))
			{
				if (is_user_logged_in() AND current_user_can('edit_posts'))
				{
					$result = array
					(
						'error' => FALSE,
						'error_message' => '',
						'id' => NULL,
						'name' => NULL
					);
					
					$sidebar_id = $data['sidebar_id'];
					$relation_type = $data['relation_type'];
					$relation_id = $data['relation_id'];
					$relation_edit_id = $data['relation_edit_id'];
					
					$categories = get_terms(array('category', 'news_category', 'testimonials_category', 'success-stories_category'), array('orderby' => 'term_group'));
					//$categories = get_categories();
					$categories_by_id = array();			

					$count = count($categories);
					for ($i = 0; $i < $count; $i++)
					{
						if ( ! empty($categories[$i]->name))
						{
							$prefix = '';

							$taxonomies = array
							(
								'category' => 'Post',
								'news_category' => 'News',
								'testimonials_category' => 'Testimonials',
								'success-stories_category' => 'Success stories'
							);
							
							$prefix = $taxonomies[$categories[$i]->taxonomy];
							
							$categories_by_id[$categories[$i]->term_id] = ( ! empty($prefix) ? $prefix.' - ' : '').$categories[$i]->name;
						}
					}

					$found = FALSE;

					if ($relation_type == 'category')
					{
						if (isset($categories_by_id[$relation_edit_id]))
						{
							$result['id'] = $relation_edit_id;
							$result['name'] = $categories_by_id[$relation_edit_id];
							$found = TRUE;
						}
					} else if ($relation_type == 'page')
					{
						$page = get_posts(array('post_type' => array('page'), 'p' => $relation_edit_id));
						
						if (count($page) > 0)
						{
							$result['id'] = $page[0]->ID;
							$result['name'] = $page[0]->post_title;
							$result['type'] = $page[0]->post_type;
							$found = TRUE;
						}
					} else if ($relation_type == 'post')
					{
						$post = get_posts(array('post_type' => array_merge(array('post'), ether::post_types()), 'p' => $relation_edit_id));
						
						$types = array
						(
							'post' => 'Post',
							'news' => 'News',
							'testimonials' => 'Testimonials',
							'success-stories' => 'Success stories'
						);

						if (count($post) > 0)
						{
							$prefix = $types[$post[0]->post_type];
							
							$result['id'] = $post[0]->ID;
							$result['name'] = ( ! empty($prefix) ? $prefix.' - ' : '').$post[0]->post_title;
							$result['type'] = $post[0]->post_type;
							$found = TRUE;
						}
					}

					if ($found)
					{
						if ( ! self::is_relation($sidebar_id, $relation_type, $relation_edit_id))
						{
							self::edit_relation($sidebar_id, $relation_type, $relation_id, $relation_edit_id);
						} else
						{
							$result['error'] = TRUE;
							$result['error_message'] = '<div id="message" class="error"><p>'.ether::langr('There is \'%s\' with ID \'%s\' already.', $relation_type, ether::clean($relation_edit_id, TRUE, TRUE, TRUE)).'</p></div>';	
						}
					} else
					{
						$result['error'] = TRUE;
						$result['error_message'] = '<div id="message" class="error"><p>'.ether::langr('There isn\'t any \'%s\' with ID \'%s\'.', $relation_type, ether::clean($relation_edit_id, TRUE, TRUE, TRUE)).'</p></div>';
					}
					
					header('Content-type: application/json');
					die(json_encode($result));
				}
			}
		}

		public static function wp_admin_head()
		{
			if (isset($_GET['page']) AND isset($_GET['tab']))
			{
				if ($_GET['page'] == ether::slug(ether::config('theme_name')) AND $_GET['tab'] == 'sidebars')
				{
?><script type="text/javascript">
$j = jQuery.noConflict();

$j( function()
{
	var $sidebar_save = $j('div.buttons input.save');
	$sidebar_save.hide().prev('label').hide();
	
	var $sidebar_edit = $j('div.buttons input.change');
	
	$j('div.buttons input[name=relation_delete], div.buttons input[name=sidebar_delete]').click( function()
	{
		if ( ! confirm("<?php ether::lang('Are you sure you want to delete this item? \'Cancel\' to stop, \'OK\' to reset.'); ?>"))
		{
			return false;
		}
	});
	
	if ($j('div.buttons input.save').length > 0)
	{
		
		$j('fieldset.ether, ul.tapeworm, ul.tapeworm li, form, td, th, label, h5').click( function(e)
		{
			if (this == e.target)
			{
				$save = $j('div.buttons input.save:visible');
				
				if ($save.length > 0)
				{
					$j('div.error').remove();
					row = $save.parents('li').data('row');
					$this = $save;
				
					$this.parents('li').children('label').remove();
					$this.parents('li').prepend(row);

					$this.hide().prevAll('input.change').show();
					
					return false;
				}
			}
		});
	}
	
	$sidebar_edit.live('click', function()
	{
		$j(this).hide().nextAll('input.save').show();
		
		$row = $j(this).parents('li');
		row = $row.text().replace($j(this).parent().text(), '');
		$row.data('row', row);
		$row.data('id', row.split(' - ')[0].replace('ID', ''));
		
		$row.html($row.html().replace(row, ''));
		$row.prepend($j('<label />').text(row.split(' - ')[1]).prepend($j('<input />').attr('type', 'text').attr('name', 'relation_id').val(row.split(' - ')[0].replace('ID', ''))));

		return false;
	});
	
	$sidebar_save.live('click', function()
	{
		$j('div.error').remove();
		id = $j(this).parents('li').data('id');
		$this = $j(this);
		
		$j.ajax
		({
			url: ether.path + 'ether/ether.php',
			data: 'sidebar&sidebar_id=' + $this.prevAll('input[name=sidebar_id]').val() + '&relation_type=' + $this.prevAll('input[name=relation_type]').val() + '&relation_edit_id=' + $this.parents('li').children('label').children('input[name=relation_id]').val() + '&relation_id=' + id,
			success: function(data)
			{
				if (data.error)
				{
					$j(data.error_message).insertAfter($j('div.wrap').children('h2'));

					$this.parents('li').prepend($this.parents('li').data('row'));
					$this.parents('li').children('label').remove();
					$this.hide().prevAll('input.change').show();
				} else
				{
					$this.parents('li').children('label').remove();
					$this.parents('li').prepend('ID' + data.id + ' - ' + data.name);
					
					$this.hide().prevAll('input.change').show();
				}
			}
		});
		
		return false;
	});
});
</script><?php				
				}
			}
		}
	}
?>
