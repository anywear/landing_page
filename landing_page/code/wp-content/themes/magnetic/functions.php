<?php
	include(TEMPLATEPATH.'/ether/ether.php');

	if (file_exists(TEMPLATEPATH.'/'.get_locale().'.mo'))
	{
		load_textdomain('ether', TEMPLATEPATH.'/'.get_locale().'.mo');
	} else
	{
		if (file_exists(TEMPLATEPATH.'/default.mo'))
		{
			load_textdomain('ether', TEMPLATEPATH.'/default.mo');
		}
	}

	ether::config('theme_name', 'Magnetic');
	ether::config('theme_version', '1.1');
	ether::config('bc_delimiter', ' &raquo; ');
	ether::config('theme_activate_callback', array('ether_theme', 'install'));
	ether::config('theme_deactivate_callback', array('ether_theme', 'cleanup'));
	ether::config('hide_custom_fields', TRUE);
	ether::config('register_sidebar', FALSE);
	ether::config('more_link', FALSE);
	ether::config('debug', FALSE);
	ether::config('debug_wordpress', FALSE);
	ether::config('use_quicksand', TRUE);
	ether::config('cache', FALSE);
	ether::config('cache_lifetime', 43200);

	ether::begin();

	class ether_post_type extends ether_module
	{
		public static function wp_admin_head()
		{
			$type = str_replace(array('ether_', '_'), array('', '-'), self::get_class());

			if (isset($_GET['post_type']))
			{
				$_GET['post_type'] = str_replace('gallery', 'galleries', $_GET['post_type']);
				global $post_type;

				if ((isset($_GET['post_type']) AND $_GET['post_type'] == $type) OR $post_type == $type)
				{
					echo '<style type="text/css"> #icon-edit { background: transparent url('.ether::path('admin/media/images/cpt/'.$type.'-big.png', TRUE).') no-repeat; } </style>';
				}
			}
		}
	}

	ether::module('backup');
	ether::module('posts.news');
	ether::module('posts.testimonials');
	ether::module('posts.success-stories');
	ether::module('posts.slide');
	ether::module('posts.galleries');
	ether::module('posts.service');
	ether::module('posts.pricing-column');
	ether::module('contact');
	ether::module('fonts-and-colors');
	ether::module('widget');
	ether::module('shortcode');
	ether::module('sidebar');
	ether::module('header');
	ether::module('gallery');
	ether::module('admin');
	ether::module('theme');

	ether::end();

	add_action('admin_menu','menu_order');

	function menu_order()
	{
		global $menu;
		$count = count($menu);

		for ($i = 0; $i < $count; $i++)
		{
			if ($i == 20)
			{
				$menu[8] = $menu[7];
				$menu[7] = $menu[$i];
				unset($menu[$i]);
			} else if ($i == 10)
			{
				$menu[23] = $menu[$i];
				unset($menu[$i]);
			} else if ($i == 15)
			{
				$menu[24] = $menu[$i];
				unset($menu[$i]);
			}
		}
	}

	function custom_excerpt_more($more)
	{
		return '...';
	}

	add_filter('excerpt_more', 'custom_excerpt_more');

	add_action('post_submitbox_misc_actions', 'publish_metabox_featured');
	add_action('save_post', 'save_post_featured');

	function save_post_featured()
	{
		if ( ! empty($_POST) AND isset($_POST[ether::config('prefix').'featured']))
		{
			ether::handle_field($_POST, array
			(
				'select' => array
				(
					array
					(
						'name' => 'featured',
						'value' => '',
						'relation' => 'meta'
					)
				)
			));
		}
	}

	function publish_metabox_featured()
	{
		global $post_ID;

		$post = get_post($post_ID);

		echo '<script type="text/javascript">
			$j = jQuery.noConflict();

			$j( function()
			{
				$j(\'.misc-pub-section.featured a\').click( function()
				{
					$j(this).nextAll(\'p\').slideToggle();

					return false;
				});
			});
		</script><div class="misc-pub-section featured" style="border: 0; padding-top: 0; padding-bottom: 0;"><img src="'.ether::path('admin/media/images/featured.png', TRUE).'" alt="Featured" style="float: left; padding-right: 4px;" /><strong style="line-height: 15px;">'.ether::langr('Featured').'</strong> <a href="#edit_featured">'.ether::langr('Edit').'</a>
		<label class="screen-reader-text">'.ether::langr('Featured').'</label>
		<p style="display: none;">
		'.ether::make_field('featured', array('type' => 'select', 'options' => array('no' => array('name' => ether::langr('No')), 'yes' => array('name' => ether::langr('Yes'))), 'relation' => 'meta')).'
		</p></div>';
	}

	add_theme_support('automatic-feed-links');
	add_editor_style('media/stylesheets/editor.css', TRUE);

	add_filter('posts_orderby', 'featured_posts_order');
	add_action('pre_get_posts', 'featured_get_posts');

	function featured_get_posts()
	{
		global $wp_query;

		if ($wp_query->is_posts_page OR $wp_query->is_home)
		{
			$wp_query->set('meta_key', 'ether_featured');
		}
	}

	function featured_posts_order($orderby)
	{
		global $wpdb;
		global $wp_query;

		if ($wp_query->is_posts_page OR $wp_query->is_home)
		{
			$orderby = $wpdb->postmeta.'.meta_value DESC, '.$orderby;
		}

		return $orderby;
	}
?>
