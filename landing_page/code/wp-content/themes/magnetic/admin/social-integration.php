<?php

	class ether_panel_social_integration extends ether_panel
	{
		public static function init()
		{
	
		}
		
		public static function header()
		{
			
		}
		
		public static function reset()
		{
			ether::handle_field(array(), array
			(
				'text' => array
				(
					array
					(
						'name' => 'social_twitter',
						'value' => ''
					),
					array
					(
						'name' => 'social_flickr',
						'value' => ''
					),
					array
					(
						'name' => 'social_facebook',
						'value' => ''
					),
					array
					(
						'name' => 'social_addthis',
						'value' => ''
					)
				)
			));
		}
		
		public static function body()
		{
			$body = '';
			
			if (isset($_POST['save']))
			{
				ether::handle_field($_POST, array
				(
					'text' => array
					(
						array
						(
							'name' => 'social_twitter',
							'value' => ''
						),
						array
						(
							'name' => 'social_flickr',
							'value' => ''
						),
						array
						(
							'name' => 'social_facebook',
							'value' => ''
						),
						array
						(
							'name' => 'social_addthis',
							'value' => ''
						)
					)
				));
			}
			
			$body .= '<fieldset class="ether">
				<ul> 
					<li class="half"> 
						<label>'.ether::langr('Twitter username').'</label> 
						'.ether::make_field('social_twitter', array('type' => 'text')).'
					</li> 
					<li class="half"> 
						<label>'.ether::langr('Facebook username').'</label> 
						'.ether::make_field('social_facebook', array('type' => 'text')).'
					</li>
				</ul>		
			</fieldset>
			<fieldset class="ether">
				<ul> 
					<li class="half"> 
						<label>'.ether::langr('AddThis username').'</label> 
						'.ether::make_field('social_addthis', array('type' => 'text')).'
					</li> 
					<li class="half"> 
						<label>'.ether::langr('Flickr ID').'</label> 
						'.ether::make_field('social_flickr', array('type' => 'text')).'
					</li> 
				</ul>		
			</fieldset>';
			
			return $body;
		}
	}
?>
