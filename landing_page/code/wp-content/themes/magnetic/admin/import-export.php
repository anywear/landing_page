<?php
	class ether_panel_import_export extends ether_panel
	{
		
		public static function init()
		{

		}
		
		public static function save()
		{
			
		}
		
		public static function header()
		{
			echo '<script type="text/javascript">
				$j = jQuery.noConflict();
				
				var timestamp = "";
				var table_index = -1;
				var debug = false;
				
				if (typeof $_GET[\'debug\'] != \'undefined\')
				{
					debug = true;
				}

				function set_status(message, last)
				{
					$j(".log ul li").removeClass("current");
					var $li = $j("<li />");
					$li.text(message);
					
					if (typeof last == "undefined" && last !== true)
					{
						$li.addClass("current");
					}
					
					$j(".log ul").append($li);
				}
				
				function ajax_call(data, delay, message)
				{
					if (typeof delay == "undefined")
					{
						delay = 0;
					}
	
					if (delay > 0)
					{
						setTimeout( function()
						{
							$j.ajax({ data: data });
							
							if (typeof message != "undefined")
							{
								set_status(message);
							}
						}, delay);
					} else
					{
						$j.ajax({ data: data });
						
						if (typeof message != "undefined")
						{
							set_status(message);
						}
					}
				}

				$j( function()
				{
					$j.ajaxSetup
					({
						url: "'.ether::path('ether/ether.php', TRUE).'",
						type: "POST",
						timeout: 30000,
						success: function(data, status)
						{
							if (data == -1)
							{
								set_status("'.ether::langr('Access denied').'", true);
							} else if (data == -2)
							{
								set_status("'.ether::langr('Wrong timestamp').'", true);
							} else
							{
								if (typeof data.backup != "undefined")
								{
									if (typeof data.error != "undefined")
									{
										if (typeof data.table != "undefined")
										{
											set_status("'.ether::langr('Failed to backup table').' - " + data.table, true);
										}
									} else if (typeof data.database != "undefined")
									{
										set_status("'.ether::langr('Backuping database is done').'");
										
										ajax_call({ backup: true, timestamp: timestamp, media: true }, 1000, "'.ether::langr('Backuping media files').'...");
									} else if (typeof data.media != "undefined")
									{
										set_status("'.ether::langr('Backup is done').'", true);
										var $li = $j("<li />");
										$li.html("<a href=\"\">'.ether::langr('Click here to refresh').'</a>");
										$j(".log ul").append($li);
										$j(window).unbind("beforeunload");
									} else if (typeof data.timestamp != "undefined" && data.timestamp != "")
									{
										timestamp = data.timestamp;
										
										set_status("'.ether::langr('Preparing backup').' (" + data.timestamp + ")...");
										
										ajax_call({ backup: true, timestamp: timestamp, table: data.next_table }, 1000, "'.ether::langr('Backuping table').' - " + data.next_table);
									} else if (typeof data.table != "undefined" && data.table != "")
									{
										set_status(data.table + " - '.ether::langr('Done').'");
										
										ajax_call({ backup: true, timestamp: timestamp, table: data.next_table}, 1000, "'.ether::langr('Backuping table').' - " + data.next_table);
									}
								} else if (typeof data.restore != "undefined")
								{
									if (typeof data.error != "undefined")
									{
										if (typeof data.table != "undefined")
										{
											set_status("'.ether::langr('Failed to restore table').'" + data.table, true);
										}
									} else if (typeof data.database != "undefined")
									{
										set_status("'.ether::langr('Restoring database is done').'");
										
										ajax_call({ restore: true, timestamp: timestamp, media: true }, 1000, "'.ether::langr('Restoring media files').'...");
									} else if (typeof data.media != "undefined")
									{
										set_status("'.ether::langr('Restore is done').'", true);
										var $li = $j("<li />");
										$li.html("<a href=\"\">'.ether::langr('Click here to refresh').'</a>");
										$j(".log ul").append($li);
										$j(window).unbind("beforeunload");
									} else if (typeof data.timestamp != "undefined" && data.timestamp != "")
									{
										timestamp = data.timestamp;
										
										set_status("'.ether::langr('Preparing restore').' (" + data.timestamp + ")");
										
										ajax_call({ restore: true, timestamp: timestamp, table: data.next_table }, 1000, "'.ether::langr('Restoring table').' - " + data.next_table);
									} else if (typeof data.table != "undefined" && data.table != "")
									{
										set_status(data.table + " - '.ether::langr('Done').'");
										
										ajax_call({ restore: true, timestamp: timestamp, table: data.next_table}, 1000, "'.ether::langr('Restoring table').' - " + data.next_table);
									}
								} else
								{
									set_status(data, true);
								}
							}
						},
						error: function(xhr, status, err)
						{
							if (debug)
							{
								console.log(xhr.statusText);
								console.log(xhr.responseText);
								console.log(xhr.status);
								console.log(err);
							}

							if (status !== null)
							{
								if (status == "timeout")
								{
									set_status("'.ether::langr('Connection timed out').'", true);
								} else if (status == "abort")
								{
									set_status("'.ether::langr('Connection aborted').'", true);
								} else
								{
									set_status("'.ether::langr('Unknown error. Aborting...').' (" + status + ")", true);
								}
							}
						}
					});

					$j("input[name=backup]").click( function()
					{
						if (confirm("'.ether::langr('Are you sure you want to do this?').'"))
						{
							$j(".log").slideDown(1000, function()
							{
								$j(".backup input[type=submit]").attr("disabled", "disabled");
								
								ajax_call({backup: true, init: true}, 0, "'.ether::langr('Preparing backup').'...");
							});
							
							$j(window).bind("beforeunload", function(e)
							{
								return "'.ether::langr('Backup in progress - do you really want to leave?').'";
							});
						}

						return false;
					});
					
					$j("input[name=restore]").click( function()
					{
						if ($j("select[name=ether_backup_list] option").length > 0)
						{
							if (confirm("'.ether::langr('Network connection problems or other mistake during this process can result in a broken or entirely removed database! Are you sure you want to do this?').'"))
							{
								var timestamp = $j("select[name=ether_backup_list] option:selected").val();

								$j(".log ul li:not(:first)").remove();

								$j(".log").slideDown(1000, function()
								{
									$j(".backup input[type=submit]").attr("disabled", "disabled");
									
									ajax_call({restore: true, init: true, timestamp: timestamp}, 0, "'.ether::langr('Preparing restore').'...");
								});
								
								$j(window).bind("beforeunload", function(e)
								{
									return "'.ether::langr('Restore in progress - do you really want to leave?').'";
								});
							}
						}

						return false;
					});

					$j("input[name=sample-data]").click( function()
					{
						if (confirm("'.ether::langr('Make sure to create backup first! Network connection problems or other mistake during this process can result in a broken or entirely removed database! Are you sure you want to do this?').'"))
						{
							$j(".log ul li:not(:first)").remove();

							$j(".log").slideDown(1000, function()
							{
								$j(".backup input[type=submit]").attr("disabled", "disabled");
								
								ajax_call({restore: true, init: true, timestamp: "sample-data"}, 0, "'.ether::langr('Preparing import').'...");
							});
							
							$j(window).bind("beforeunload", function(e)
							{
								return "'.ether::langr('Importing sample data in progress - do you really want to leave?').'";
							});
						}

						return false;
					});
				});
			</script>
			<style type="text/css">
				fieldset.log ul li.current { padding-left: 20px; background: #fff url('.ether::path('admin/media/images/loading.gif', TRUE).') no-repeat left center; }
			</style>';
		}

		public static function body()
		{
			$body = '';
			
			if ( ! file_exists(ether::dir('backup/database', TRUE)))
			{
				$body .= '<div id="message" class="error"><p>'.ether::langr('Make sure that "%s" directory exists', ether::dir('backup/database', TRUE)).'</p></div>';
			} else
			{
				if ( ! is_writable(ether::dir('backup/database', TRUE)))
				{
					$body .= '<div id="message" class="error"><p>'.ether::langr('Make sure that "%s" directory is writable', ether::dir('backup/database', TRUE)).'</p></div>';
				}
			}
			
			$database_dir = array_reverse(scandir(ether::dir('backup/database/', TRUE)));
			$backup_list = array();
			
			foreach ($database_dir as $dir)
			{
				if ($dir != '.' AND $dir != '..' AND $dir != 'sample-data' AND substr($dir, 0, 1) != '.')
				{
					$backup_list[$dir] = array('name' => $dir);
				}
			}
			
			$body .='<fieldset class="ether backup">
				<ul>
					<li class="third">
						<label>'.ether::langr('Sample data').'</label>
						<p>'.ether::langr('If you are using our theme for the first time we strongly advice you to import sample data and read chapter in documentation about it').'.</p>
						<p>'.ether::langr('Importing sample data will feed your site with default pages, posts, news, media files, sidebars, settings, widgets etc.').'.</p>
						<div class="buttons">
							<input type="submit" name="sample-data" value="'.ether::langr('Start importing sample data').'" style="width: auto;" />
						</div>
					</li>
					<li class="third">
						<label>'.ether::langr('Create backup').'</label>
						<p>'.ether::langr('Create backup of entire website including custom settings in themes').'.</p>
						<div class="buttons">
							<input type="submit" name="backup" value="'.ether::langr('Create backup').'" />
						</div>
					</li>
					<li class="third">
						<label>'.ether::langr('Restore backup').'</label>
						<p>'.ether::langr('Select which backup you want to restore').'.</p>
						'.ether::make_field('backup_list', array('type' => 'select', 'options' => $backup_list)).'
						<div class="buttons">
							<input type="submit" name="restore" value="'.ether::langr('Restore').'" />
						</div>
					</li>
				</ul>		
			</fieldset>
			<fieldset class="ether log" style="display: none;">
				<ul>
					<li><label>'.ether::langr('Progress').'</label></li>
				</ul>
			</fieldset>';

			return $body;
		}
	}
?>
