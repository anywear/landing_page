<?php
	class ether_panel_fonts_colors extends ether_panel
	{
		protected static $options = '';
		protected static $elements = array
		(
			'h1',
			'h2',
			'h3',
			'h4',
			'h5',
			'h6',
			'p'
		);
			
		protected static $desc = array
		(
			'H1. Lorem ipsum dolor.',
			'H2. Lorem ipsum dolor.',
			'H3. Lorem ipsum dolor.',
			'H4. Lorem ipsum dolor.',
			'H5. Lorem ipsum dolor.',
			'H6. Lorem ipsum dolor.',
			'Paragraph</p><p class="preview">Lorem ipsum dolor sit amet enim. Etiam ullamcorper. Suspendisse a pellentesque dui, non felis. Maecenas malesuada elit lectus felis, malesuada ultricies.'
		);
		
		protected static $font_colors = array
		(
			'#0b0b0b',
			'#0b0b0b',
			'#0b0b0b',
			'#b54b2e',
			'#737373',
			'#b54b2e',
			'#393939'
		);
			
		public static function init()
		{

		}
		
		public static function save()
		{
			
		}

		public static function header()
		{
			if (isset($_POST['reset']))
			{
				ether::option('fonts-and-colors', '');
				self::$options = array();
			} else
			{
				if (isset($_POST['save']))
				{
					$data = ether::clean($_POST, TRUE, TRUE, TRUE);
					ether::option('fonts-and-colors', serialize($data));	
					self::$options = $data;
				} else
				{
					self::$options = unserialize(ether::option('fonts-and-colors'));
				}
			}

			$google_fonts_js_array = '';
			$google_fonts_variants_js_array = '';
			
			foreach (ether_fonts_and_colors::font_list() as $font => $options)
			{
				if ($options['group'] == 'Google Font Directory')
				{
					if ($google_fonts_js_array == '')
					{
						$google_fonts_js_array .= '\''.$font.'\'';
					} else
					{
						$google_fonts_js_array .= ', \''.$font.'\'';
					}
					
					if ($google_fonts_variants_js_array == '')
					{
						$google_fonts_variants_js_array .= '\''.implode(',', $options['variants']).'\'';
					} else
					{
						$google_fonts_variants_js_array .= ', \''.implode(',', $options['variants']).'\'';
					}
				}
			}
			
			foreach (self::$elements as $element)
			{
				$font = ether::config('prefix').$element.'_font';
				
				if (isset(self::$options[$font]) AND self::is_google_font(self::$options[$font]))
				{
					echo '<link href="http://fonts.googleapis.com/css?family='.str_replace(' ', '+', self::$options[$font]).':'.implode(',', self::get_google_font_variants(self::$options[$font])).'&subset=latin" rel="stylesheet" type="text/css" title="'.self::$options[$font].'" />';
				}
			}
			
			echo '<script src="http://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js" type="text/javascript"></script>';
			echo '<link rel="stylesheet" href="'.ether::path('admin/media/stylesheets/colorpicker.css', TRUE).'" media="all" />';
			echo '<link rel="stylesheet" href="'.ether::path('ether/ether.php?custom-style&preview=true', TRUE).'" media="all" />';
			echo '<script type="text/javascript" src="'.ether::path('admin/media/scripts/colorpicker.js', TRUE).'"></script>';
			
			echo '<script type="text/javascript">
				$j = jQuery.noConflict();
				
				var google_fonts = ['.$google_fonts_js_array.'];
				var google_fonts_variants = ['.$google_fonts_variants_js_array.'];
				
				function is_google_font(name)
				{
					var length = google_fonts.length;
					
					for (i = 0; i < length; i++)
					{
						if (name == google_fonts[i])
						{
							return true;
						}
					}
					
					return false;
				}
				
				function get_google_font_variants(name)
				{
					var length = google_fonts.length;
					
					for (i = 0; i < length; i++)
					{
						if (name == google_fonts[i])
						{
							return google_fonts_variants[i];
						}
					}
					
					return \'regular\';
				}

				function rgb2hex(rgb)
				{
					var rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
					
					function hex(x)
					{
						return ("0" + parseInt(x).toString(16)).slice(-2);
					}
					return hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
				}
				
				$j( function()
				{
					$j(\'select.font-family\').change( function()
					{
						$j(\'link.google-font\').remove();
						var font = $j(this).val();
						
						if (is_google_font(font))
						{
							variants = get_google_font_variants(font);

							WebFont.load
							({
								google:
								{
									families: [ font + \':\' + variants  ]
								}
							});
						}
						
						$j(this).parents(\'ul.tapeworm\').prev().css(\'font-family\', font + \' !important\');
					});
					
					$j(\'input.kerning\').change( function()
					{
						var kerning = $j(this).val();
						
						$j(this).parents(\'ul.tapeworm\').prev().css(\'letter-spacing\', kerning + \'px\');
					});
					
					$j(\'input.leading\').change( function()
					{
						var leading = $j(this).val();
						
						$j(this).parents(\'ul.tapeworm\').prev().css(\'line-height\', leading);
					});
					
					$j(\'input.bold\').click( function()
					{
						var bold = \'normal !important\';
						
						if ( ! $j(this).next(\'input[type=checkbox\]\').attr(\'checked\'))
						{
							bold = \'bold !important\';
						}
						
						$j(this).parents(\'fieldset.google-fonts\').children(\'.preview\').css(\'font-weight\', bold);
					});
					
					$j(\'input.italic\').click( function()
					{
						var italic = \'normal !important\';
						
						if ( ! $j(this).next(\'input[type=checkbox\]\').attr(\'checked\'))
						{
							italic = \'italic !important\';
						}
						
						$j(this).parents(\'fieldset.google-fonts\').children(\'.preview\').css(\'font-style\', italic);
					});
					
					$j(\'input.justify\').click( function()
					{
						var textalign = \'inherit !important\';
						
						if ( ! $j(this).next(\'input[type=checkbox\]\').attr(\'checked\'))
						{
							textalign = \'justify !important\';
						}
						
						$j(this).parents(\'fieldset.google-fonts\').children(\'.preview\').css(\'text-align\', textalign);
					});
					
					$j(\'input.center\').click( function()
					{
						var textalign = \'inherit !important\';
						
						if ( ! $j(this).next(\'input[type=checkbox\]\').attr(\'checked\'))
						{
							textalign = \'center !important\';
						}
						
						$j(this).parents(\'fieldset.google-fonts\').children(\'.preview\').css(\'text-align\', textalign);
					});
					
					$j(\'input.underline\').click( function()
					{
						var textdecoration = \'none !important\';
						
						if ( ! $j(this).next(\'input[type=checkbox\]\').attr(\'checked\'))
						{
							textdecoration = \'underline !important\';
						}
						
						$j(this).parents(\'fieldset.google-fonts\').children(\'.preview\').css(\'text-decoration\', textdecoration);
					});
					
					$j(\'input.all-uppercase\').click( function()
					{
						var texttransform = \'none !important\';
						
						if ( ! $j(this).next(\'input[type=checkbox\]\').attr(\'checked\'))
						{
							texttransform = \'uppercase !important\';
						}
						
						$j(this).parents(\'fieldset.google-fonts\').children(\'.preview\').css(\'text-transform\', texttransform);
					});
					
					$j(\'input.alignright\').click( function()
					{
						$j(this).parents(\'li\').children(\'input.alignleft\').trigger(\'click\');
						var textalign = \'inherit !important\';
						
						if ( ! $j(this).next(\'input[type=checkbox\]\').attr(\'checked\'))
						{
							textalign = \'right !important\';
						}
						
						$j(this).parents(\'fieldset.google-fonts\').children(\'.preview\').css(\'text-align\', textalign);
					});
					
					$j(\'input.alignleft\').click( function()
					{
						var textalign = \'inherit !important\';
						
						if ( ! $j(this).next(\'input[type=checkbox\]\').attr(\'checked\'))
						{
							textalign = \'left !important\';
						}
						
						$j(this).parents(\'fieldset.google-fonts\').children(\'.preview\').css(\'text-align\', textalign);
					});
					
					$j(\'input.color\').each( function()
					{
						$j(this).css(\'color\', $j(this).val());
						$j(this).css(\'background-color\', $j(this).val());
					});
					
					$j(\'input.color\').ColorPicker
					({
						onSubmit: function(hsb, hex, rgb, el)
						{
							$j(el).css(\'background\', \'#\' + hex);
							$j(el).css(\'color\', \'#\' + hex);
							$j(el).val(\'#\' + hex);
							$j(el).parents(\'ul.tapeworm\').prev().css(\'color\', \'#\' + hex);
							$j(el).ColorPickerHide();
						},
						onBeforeShow: function()
						{
							$j(this).ColorPickerSetColor($j(this).val());
						},
						onHide: function(picker, el)
						{
							$picker = $j(picker);
							color = $picker.data(\'colorpicker\');
						}
					}).bind(\'keyup\', function(e)
					{
						if (e.keyCode == 27)
						{
							$j(\'input.color\').ColorPickerHide();
						}
					});
					
					$j(\'ul.options li\').each( function()
					{
						if ($j(this).children(\'input[type=checkbox]\').attr(\'checked\'))
						{
							$j(this).addClass(\'active\');
						}
					});
					
					$j(\'ul.options li\').click( function()
					{
						$j(this).toggleClass(\'active\');
						
						if ($j(this).hasClass(\'active\'))
						{
							$j(this).children(\'input[type=checkbox]\').attr(\'checked\', \'checked\');
						} else
						{
							$j(this).children(\'input[type=checkbox]\').attr(\'checked\', \'\');	
						}
						
						return false;
					});
				});
			</script>';
		}
		
		public static function body()
		{
			$body = '';

			$body .= '<fieldset class="ether">
				<ul>
					<li>
						<label>'.ether::make_field('colors_use', array('type' => 'checkbox'), self::$options).' '.ether::langr('Use custom colors').'</label>
					</li>
				</ul>
			</fieldset>
			<fieldset class="ether">
				<ul>
					<li class="half"><label>'.ether::make_field('main_color', array('type' => 'text', 'class' => 'color', 'value' => '#cb806c'), (empty(self::$options) ? '#cb806c' : self::$options)).' '.ether::langr('Main color').'</label></li>
					<li class="half"><label>'.ether::make_field('background_color', array('type' => 'text', 'class' => 'color', 'value' => '#437ab1'), (empty(self::$options) ? '#437ab1' : self::$options)).' '.ether::langr('Background color').'</label></li>
				</ul>
			</fieldset>
			<fieldset class="ether">
				<ul>
					<li>
						<label>'.ether::make_field('fonts_use', array('type' => 'checkbox'), self::$options).' '.ether::langr('Use custom fonts').'</label>
					</li>
				</ul>
			</fieldset>';

			$count = count(self::$elements);
			for ($i = 0; $i < $count; $i++)
			{
				$body .= '<fieldset class="ether google-fonts">';
				$body .= '<'.self::$elements[$i].' class="preview">'.self::$desc[$i].'</'.self::$elements[$i].'>';
				$body .= '<ul class="tapeworm">';
				$body .= '<li>'.ether::make_field(self::$elements[$i].'_color', array('type' => 'text', 'class' => 'color', 'value' => self::$font_colors[$i]), (empty(self::$options) ? self::$font_colors[$i] : self::$options)).'</li>';
				$body .= '<li>';
				
				$body .= ether::make_field(self::$elements[$i].'_font', array('type' => 'select', 'class' => 'default font-family', 'options' => ether_fonts_and_colors::font_list()), self::$options);
				
				
				$body .= '</li>';
				$body .= '<li><label>'.ether::langr('Kerning').'</label>'.ether::make_field(self::$elements[$i].'_kerning', array('type' => 'text', 'class' => 'tiny kerning', 'value' => 0, 'use_default' => TRUE), self::$options).'</li>';
				$body .= '<li><label>'.ether::langr('Leading').'</label>'.ether::make_field(self::$elements[$i].'_leading', array('type' => 'text', 'class' => 'tiny leading', 'value' => 1.5, 'use_default' => TRUE), self::$options).'</li>';
				$body .= '</ul>';
				
				$body .= '<ul class="options">';
				$body .= '<li><input type="button" value="bold" class="bold" />'.ether::make_field(self::$elements[$i].'_bold', array('type' => 'checkbox'), self::$options).'</li>';
				$body .= '<li><input type="button" value="italic" class="italic" />'.ether::make_field(self::$elements[$i].'_italic', array('type' => 'checkbox'), self::$options).'</li>';
				$body .= '<li><input type="button" value="justify" class="justify" />'.ether::make_field(self::$elements[$i].'_justify', array('type' => 'checkbox'), self::$options).'</li>';
				$body .= '<li><input type="button" value="center" class="center" />'.ether::make_field(self::$elements[$i].'_center', array('type' => 'checkbox'), self::$options).'</li>';
				$body .= '<li><input type="button" value="underline" class="underline" />'.ether::make_field(self::$elements[$i].'_underline', array('type' => 'checkbox'), self::$options).'</li>';
				$body .= '<li><input type="button" value="all-uppercase" class="all-uppercase"/>'.ether::make_field(self::$elements[$i].'_all-uppercase', array('type' => 'checkbox'), self::$options).'</li>';
				$body .= '<li><input type="button" value="alignright" class="alignright"/>'.ether::make_field(self::$elements[$i].'_alignright', array('type' => 'checkbox'), self::$options).'</li>';
				$body .= '<li><input type="button" value="alignleft" class="alignleft"/>'.ether::make_field(self::$elements[$i].'_alignleft', array('type' => 'checkbox'), self::$options).'</li>';
				$body .= '</ul>';
				$body .= '</fieldset>';
			}
	
			return $body;
		}
		
		public static function is_google_font($name)
		{
			foreach (ether_fonts_and_colors::font_list() as $font => $options)
			{
				if ($name == $font AND $options['group'] == 'Google Font Directory')
				{
					return TRUE;
				}
			}
			
			return FALSE;
		}
		
		public static function get_google_font_variants($name)
		{
			$fonts = ether_fonts_and_colors::font_list();
			
			return $fonts[$name]['variants'];
		}
	}
?>
