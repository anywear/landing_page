<?php
	class ether_metabox_url extends ether_metabox
	{
		public static function init()
		{

		}
		
		public static function header()
		{
			
		}
		
		public static function save()
		{
			ether::handle_field($_POST, array
			(
				'text' => array
				(
					array
					(
						'name' => 'url',
						'value' => '',
						'relation' => 'meta'
					)
				)
			));
		}
		
		public static function body()
		{
			return '<fieldset class="ether"><ul><li>'.ether::make_field('url', array('type' => 'text', 'relation' => 'meta')).'</li></ul></fieldset>';
		}
	}
?>
