<?php
	class ether_metabox_button extends ether_metabox
	{
		public static function init()
		{

		}
		
		public static function header()
		{
			$header = '<link rel="stylesheet" href="'.ether::path('admin/media/stylesheets/colorpicker.css', TRUE).'" media="all" />';
			$header .= '<script type="text/javascript" src="'.ether::path('admin/media/scripts/colorpicker.js', TRUE).'"></script>';
			
			$header .= '<script type="text/javascript">
				$j = jQuery.noConflict();
				
				function rgb2hex(rgb)
				{
					rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
					
					function hex(x)
					{
						return ("0" + parseInt(x).toString(16)).slice(-2);
					}
					return hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
				}
				
				$j( function()
				{
					$j(\'input.color\').each( function()
					{
						$j(this).css(\'color\', $j(this).val());
						$j(this).css(\'background-color\', $j(this).val());
						$j(this).css(\'width\', \'23px\').height(\'23px\');
					});
					
					$j(\'input.color\').ColorPicker
					({
						onSubmit: function(hsb, hex, rgb, el)
						{
							$j(el).css(\'background\', \'#\' + hex);
							$j(el).css(\'color\', \'#\' + hex);
							$j(el).val(\'#\' + hex);
							$j(el).parents(\'ul.tapeworm\').prev().css(\'color\', \'#\' + hex);
							$j(el).ColorPickerHide();
						},
						onBeforeShow: function()
						{
							$j(this).ColorPickerSetColor($j(this).val());
						},
						onHide: function(picker, el)
						{
							$picker = $j(picker);
							color = $picker.data(\'colorpicker\');
						}
					}).bind(\'keyup\', function(e)
					{
						if (e.keyCode == 27)
						{
							$j(\'input.color\').ColorPickerHide();
						}
					});
				});
			</script>';
			
			return $header;
		}
		
		public static function save()
		{
			ether::handle_field($_POST, array
			(
				'text' => array
				(
					array
					(
						'name' => 'button_text',
						'value' => '',
						'relation' => 'meta'
					),
					array
					(
						'name' => 'button_url',
						'value' => '',
						'relation' => 'meta'
					),
					array
					(
						'name' => 'button_color',
						'value' => '',
						'relation' => 'meta'
					)
				)
			));
		}
		
		public static function body()
		{
			return '<fieldset class="ether"><ul><li><label class="fixed alt-style-1">'.ether::langr('Button text').'</label> 
				'.ether::make_field('button_text', array('type' => 'text', 'relation' => 'meta')).'</li>
				<li><label class="fixed alt-style-1">'.ether::langr('Button URL').'</label> 
				'.ether::make_field('button_url', array('type' => 'text', 'relation' => 'meta')).'</li>
				<li><label style="line-height: 33px;">'.ether::make_field('button_color', array('type' => 'text', 'class' => 'color', 'relation' => 'meta', 'style' => 'float: left; margin-right: 5px;')).' '.ether::langr('Button color').'</label></li>
				</ul></fieldset>
			';
		}
	}
?>
