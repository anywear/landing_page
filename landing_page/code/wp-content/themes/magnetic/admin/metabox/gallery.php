<?php
	function gallery_cmp($a, $b)
	{
		return ($a[ether::config('prefix').'gallery_order'] < $b[ether::config('prefix').'gallery_order'] ? -1 : 1);
	}
	
	class ether_metabox_gallery extends ether_metabox
	{
		public static function init()
		{

		}
		
		public static function header()
		{
			global $post;

			return '<link rel="stylesheet" href="'.ether::path('ether/media/stylesheets/jquery.ui.css', TRUE).'" />
			<script type="text/javascript" src="'.ether::path('ether/media/scripts/jquery.ui.js', TRUE).'"></script>
			<script type="text/javascript" src="'.ether::path('ether/media/scripts/tooltip.js', TRUE).'"></script>
			<script type="text/javascript">
				var $j = jQuery.noConflict();
				
				$j( function()
				{
					$j(\'.images .has-tooltip\').tooltip
					({
						tooltip: \'.tooltip\',
						x: -23,
						y: -15,
						offset: true,
						sub_width: false,
						sub_height: true,
						standalone: false
					});
					
					function slide_width(event, ui)
					{
						var $li = $j(ui.handle).parents(\'li\');

						$li.children(\'input[type=hidden]\').val(ui.value + \'%\');
						$li.children(\'div.slider-value\').text(ui.value + \'%\');
						$li.children(\'div.slider-value\').css(\'left\', $li.width() * ((ui.value / 75) - 0.25) - $li.children(\'div.slider-value\').width());
							
						if (ui.value == 25 || ui.value == 100)
						{
							$li.children(\'div.slider-value\').hide();
						} else
						{
							$li.children(\'div.slider-value\').show();
						}
					}

					$j(\'.slider.width\').slider
					({
						range: \'min\',
						min: 25,
						max: 100,
						value: 100,
						slide: slide_width
					});
					
					function slide_image_height(event, ui)
					{
						var $li = $j(ui.handle).parents(\'li\');

						$li.children(\'input[type=hidden]\').val(ui.value + \'px\');
						$li.children(\'div.slider-value\').text(ui.value + \'px\');
						$li.children(\'div.slider-value\').css(\'left\', $li.width() * ((ui.value / 815)) - $li.children(\'div.slider-value\').width());
							
						if (ui.value == 35 || ui.value == 850)
						{
							$li.children(\'div.slider-value\').hide();
						} else
						{
							$li.children(\'div.slider-value\').show();
						}
					}
					
					$j(\'.slider.image-height\').slider
					({
						range: \'min\',
						min: 35,
						max: 850,
						value: 220,
						slide: slide_image_height
					});
					
					function slide_columns(event, ui)
					{
						var $li = $j(ui.handle).parents(\'li\');

						$li.children(\'input[type=hidden]\').val(ui.value);
						$li.children(\'div.slider-value\').text(ui.value);
						$li.children(\'div.slider-value\').css(\'left\', $li.width() * ((ui.value - 1) / 9));
							
						if (ui.value == 1 || ui.value == 10)
						{
							$li.children(\'div.slider-value\').hide();
						} else
						{
							$li.children(\'div.slider-value\').show();
						}
					}
					
					$j(\'.slider.columns, .slider.rows\').slider
					({
						range: \'min\',
						min: 1,
						max: 10,
						value: 10,
						step: 1,
						slide: slide_columns
					});
					
					$j(\'.slider\').next(\'input[type=hidden]\').each( function()
					{
						if ($j(this).val() != \'\')
						{
							var val = $j(this).val().replace(\'%\', \'\').replace(\'px\', \'\');
							var type = $j(this).prev(\'.slider\')[0].className.split(\' \')[1];
							
							if (type == \'width\')
							{
								slide_width(null, { value: val, handle: $j(this).prev(\'.slider\') });
							} else if (type == \'image-height\')
							{
								slide_image_height(null, { value: val, handle: $j(this).prev(\'.slider\') });
							} else if (type == \'columns\' || type == \'rows\')
							{
								slide_columns(null, { value: val, handle: $j(this).prev(\'.slider\') });
							}
							
							$j(this).prev(\'.slider\').slider(\'value\', val);
						}
					});

					$j(\'.images .add_slide\').click( function()
					{
						var index = -1;

						$j(\'.images input.slider-image-index\').each( function()
						{
							if (parseInt($j(this).val()) > index)
							{
								index = parseInt($j(this).val());
							}
						});

						index += 1;
						
						$j(\'.images ul.slides\').append(\'\' + 
						\'<li>\' +
							\'<img src="\' + ether.path + \'ether/media/images/dragndrop-button.png" alt="Drag and Drop" class="dragndrop" />\' +
							\'<input type="text" name="ether_gallery_order[\' + index + \']" class="slider-image-index" value="" />\' +
							\'<input type="text" name="ether_gallery_alt[\' + index + \']" class="upload_image-\' + index + \' hidden" value="" style="display: none;" />\' +
							\'<input type="text" name="ether_gallery_image[\' + index + \']" class="upload_image-\' + index + \' hidden" value="" style="display: none;" />\' +
							\'<div class="title has-tooltip">\' +
								\'<a href="#" class="upload_image-\' + index + \'-filename"></a> <a href="#remove" class="remove_slide"><img src="\' + ether.path + \'ether/media/images/remove.png" alt="Remove" /></a>\' +
								\'<span class="tooltip"><img src="" class="upload_image-\' + index + \'" width="220" height="165" /></span>\' +
							\'</div>\' +
							\'<div class="buttons alignright single-row">\' +
								\'<input type="button" class="upload_image width-220 height-165" name="ether_upload_image-\' + index + \'" value="Change" />\' +
							\'</div>\' +
						\'</li>\');

						var $list = $j(\'.images ul.slides > li\');
			
						$list.each( function()
						{
							index = $list.index(this);
								
							$j(this).children(\'input.slider-image-index\').val(index);
						});

						$list.children(\'input.slider-image-index\').trigger(\'change\');

						return false;
					});
					
					var val = $j(\'select[name=ether_gallery_template] option:selected\').val();
						
					if (val == \'none\')
					{
						$j(\'fieldset.ether.images\').hide().prev(\'fieldset.ether\').hide();
					}
					
					if (val == \'list-portfolio\')
					{
						$j(\'div.slider.columns\').parents(\'li\').hide();
					}
					
					if (val == \'zoombox\' || val == \'pip\')
					{
						$j(\'div.slider.columns, div.slider.rows\').parents(\'li\').hide();
					}
					
					if (val != \'pip\')
					{
						$j(\'select[name=ether_gallery_thumbs_align]\').parents(\'fieldset.ether\').hide();
					}

					$j(\'select[name=ether_gallery_template]\').change( function()
					{
						var val = $j(this).children(\'option:selected\').val();
						
						if (val == \'none\')
						{
							$j(\'fieldset.ether.images\').hide().prev(\'fieldset.ether\').hide();
						} else
						{
							$j(\'fieldset.ether.images\').show().prev(\'fieldset.ether\').show();
						}
						
						if (val == \'zoombox\' || val == \'pip\')
						{
							$j(\'div.slider.columns, div.slider.rows\').parents(\'li\').hide();
						} else
						{
							if (val == \'list-portfolio\')
							{
								$j(\'div.slider.columns\').parents(\'li\').hide();
							} else
							{
								$j(\'div.slider.columns, div.slider.rows\').parents(\'li\').show();
							}
						}
						
						if (val != \'pip\')
						{
							$j(\'select[name=ether_gallery_thumbs_align]\').parents(\'fieldset.ether\').hide();
						} else
						{
							$j(\'select[name=ether_gallery_thumbs_align]\').parents(\'fieldset.ether\').show();
						}
						
						return false;
					});
					
					window.gallery_send_to_editor = window.send_to_editor;
					
					window.send_to_editor = function(html)
					{
						if (ether.upload_preview != null)
						{
							var url = \'\';
							
							if ($j(\'img\', html).length > 0)
							{
								url = $j(\'img\', html).attr(\'src\');
							} else
							{
								url = html;
							}
							
							$j(ether.upload_preview + \'-filename\').text(url.split(\'/\').pop());
						}
						
						window.gallery_send_to_editor(html);
					};
					
					$j(\'.images .remove_slide\').live(\'click\', function()
					{
						$j(this).parents(\'li\').remove();
						
						return false;
					});
				});
			</script>';
		}
		
		public static function save()
		{
			$images = array();
			$prefix = ether::config('prefix').'gallery_';
			
			ether::handle_field($_POST, array
			(
				'select' => array
				(
					array
					(
						'name' => 'gallery_template',
						'value' => '',
						'relation' => 'meta'
					),
					array
					(
						'name' => 'gallery_align',
						'value' => '',
						'relation' => 'meta'
					),
					array
					(
						'name' => 'gallery_thumbs_align',
						'value' => '',
						'relation' => 'meta'
					)
				),
				'hidden' => array
				(
					array
					(
						'name' => 'gallery_width',
						'value' => '100%',
						'relation' => 'meta'
					),
					array
					(
						'name' => 'gallery_columns',
						'value' => '3',
						'relation' => 'meta'
					),
					array
					(
						'name' => 'gallery_rows',
						'value' => '4',
						'relation' => 'meta'
					),
					array
					(
						'name' => 'gallery_image_height',
						'value' => '220px',
						'relation' => 'meta'
					)
				)
			));
				
			foreach ($_POST[$prefix.'order'] as $k => $v)
			{
				$images[$k] = array
				(
					$prefix.'order' => $_POST[$prefix.'order'][$k],
					$prefix.'image' => $_POST[$prefix.'image'][$k],
					$prefix.'alt' => $_POST[$prefix.'alt'][$k]
				);
			}

			global $post;

			ether::meta('gallery_images', serialize($images), $post->ID, TRUE);
			
			usort($images, 'gallery_cmp');
		}
		
		public static function body()
		{
			$body = '';
			global $post;

			$images = unserialize(ether::meta('gallery_images', TRUE, $post->ID));

			if ( ! is_array($images))
			{
				$images = array();
			}
			
			$body .= '<fieldset class="ether">
				<ul>
					<li class="half"><label class="fixed alt-style-1">'.ether::langr('Template').'</label>'.ether::make_field('gallery_template', array('type' => 'select', 'options' => ($post->post_type != 'gallery' ? ether_gallery::$template_other : ($post->post_parent == 0 ? ether_gallery::$template_parent : ether_gallery::$template_child)), 'relation' => 'meta')).'</li>
					'.(($post->post_parent != 0 OR $post->post_type != 'gallery') ? '<li class="half"><label class="fixed alt-style-1">'.ether::langr('Align').'</label>'.ether::make_field('gallery_align', array('type' => 'select', 'options' => ether_gallery::$align, 'relation' => 'meta')).'</li>' : '').'
				<ul>
			</fieldset>
			
			<fieldset class="ether">
				<ul>
					<li class="half"><label class="fixed alt-style-1">'.ether::langr('Thumbs align').'</label>'.ether::make_field('gallery_thumbs_align', array('type' => 'select', 'options' => ether_gallery::$thumbs_align, 'relation' => 'meta')).'</li>
				</ul>
			</fieldset>
			
			<fieldset class="ether">
				<ul>
					'.(($post->post_parent != 0 OR $post->post_type != 'gallery') ? '<li style="position: relative;"><label class="alt-style-1">'.ether::langr('Width').'</label>
						<div class="slider width"></div>
						'.ether::make_field('gallery_width', array('type' => 'hidden', 'class' => '', 'relation' => 'meta')).'
						<small class="slider-min">25%</small>
						<small class="slider-max">100%</small>
						<div class="slider-value"></div>
					</li>' : '').'
					<li style="position: relative;"><label class="alt-style-1">'.ether::langr('Columns').'</label>
						<div class="slider columns"></div>
						'.ether::make_field('gallery_columns', array('type' => 'hidden', 'class' => '', 'relation' => 'meta')).'
						<small class="slider-min">1</small>
						<small class="slider-max">10</small>
						<div class="slider-value"></div>
					</li>
					<li style="position: relative;"><label class="alt-style-1">'.ether::langr('Rows').'</label>
						<div class="slider rows"></div>
						'.ether::make_field('gallery_rows', array('type' => 'hidden', 'class' => '', 'relation' => 'meta')).'
						<small class="slider-min">1</small>
						<small class="slider-max">10</small>
						<div class="slider-value"></div>
					</li>
					<li style="position: relative;"><label class="alt-style-1">'.ether::langr('Image height').'</label>
						<div class="slider image-height"></div>
						'.ether::make_field('gallery_image_height', array('type' => 'hidden', 'class' => '', 'relation' => 'meta')).'
						<small class="slider-min">35px</small>
						<small class="slider-max">850px</small>
						<div class="slider-value"></div>
					</li>
				</ul>
			</fieldset>';
			
			if ($post->post_parent != 0 OR $post->post_type != 'gallery')
			{
				$body .= '<fieldset class="ether images">
					<h5 class="alt-style-1">'.ether::langr('Add images').'</h5> 
					<ul id="slider" class="slides">
					'; 
					

				$counter = 0;
				foreach ($images as $image)
				{
					$body .= '<li>
						<img src="'.ether::path('ether/media/images/dragndrop-button.png', TRUE).'" alt="Drag and Drop" class="dragndrop" />
						'.ether::make_field('gallery_order['.$counter.']', array('type' => 'text', 'class' => 'slider-image-index', 'value' => 'drag and drop'), $counter).'
						'.ether::make_field('gallery_alt['.$counter.']', array('type' => 'text', 'class' => 'upload_image-'.$counter.' hidden'), $image[ether::config('prefix').'gallery_alt']).'
						'.ether::make_field('gallery_image['.$counter.']', array('type' => 'text', 'class' => 'upload_image-'.$counter.' hidden'), $image[ether::config('prefix').'gallery_image']).'
						<div class="title has-tooltip">
							<a href="#" class="upload_image-'.$counter.'-filename">'.array_pop(explode('/', $image[ether::config('prefix').'gallery_image'])).'</a> <a href="#remove" class="remove_slide"><img src="'.ether::path('ether/media/images/remove.png', TRUE).'" alt="Remove" /></a>
							<span class="tooltip"><img src="'.$image[ether::config('prefix').'gallery_image'].'" class="upload_image-'.$counter.'" width="220" height="165" /></span>
						</div>
						<div class="buttons alignright single-row">
							'.ether::make_field('upload_image-'.$counter, array('type' => 'upload_image', 'width' => '220', 'height' => '165', 'value' => ether::langr('Change'))).'
						</div>
					</li>';
					
					$counter++;
				}

				$body .='</ul>
					<ul>
						<li> 
							<input class="add-image add_slide" type="button" value="'.ether::langr('Add image').'" /> 
						</li> 
					</ul> 
				</fieldset>';
			}
			
			return $body;
		}
	}
?>
