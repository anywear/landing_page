<?php
	class ether_metabox_sidebar extends ether_metabox
	{
		public static function init()
		{

		}
		
		public static function header()
		{
			
		}
		
		public static function save()
		{
			ether::handle_field($_POST, array
			(
				'select' => array
				(
					array
					(
						'name' => 'sidebar_align',
						'value' => 'right',
						'relation' => 'meta'
					),
					array
					(
						'name' => 'sidebar',
						'value' => '',
						'relation' => 'meta'
					)
				)
			));
		}
		
		public static function body()
		{
			$sidebars_all = ether::module_run('sidebar.get_all');
			$sidebars = array('default' => array('name' => ether::langx('Default', 'default sidebar name', TRUE)));
			
			foreach ($sidebars_all as $sidebar)
			{
				$sidebars[$sidebar['name']] = array('name' => $sidebar['name']);
			}
			
			$sidebars = array('inherited' => array('name' => ether::langx('Inherited', 'enable sidebar inheritance from global sidebar settings', TRUE))) + $sidebars;

			return '<fieldset class="ether"><ul><li><label class="fixed alt-style-1">'.ether::langr('Main sidebar').'</label> 
				'.ether::make_field('sidebar_align', array('type' => 'select', 'relation' => 'meta', 'options' => array('default' => array('name' => ether::langx('Default', 'sidebar option', TRUE)), 'none' => array('name' => ether::langx('None', 'sidebar option', TRUE)), 'left' => array('name' => ether::langx('Left', 'sidebar option', TRUE)), 'right' => array('name' => ether::langx('Right', 'sidebar option', TRUE))))).'</li>
				<li>
				'.ether::make_field('sidebar', array('type' => 'select', 'relation' => 'meta', 'options' => $sidebars)).'</li>
				<li class="half"><a href="admin.php?page='.ether::slug(ether::config('theme_name')).'&tab=sidebars">+ '.ether::langr('Add new sidebar').'</a></li><li class="half" style="text-align: right;"><a href="widgets.php">'.ether::langr('Manage sidebars').'</a></li></ul></fieldset>
			';
		}
	}
?>
