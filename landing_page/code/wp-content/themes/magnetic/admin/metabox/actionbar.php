<?php

	class ether_metabox_actionbar extends ether_metabox
	{
		public static function init()
		{

		}
		
		public static function header()
		{
			return '<script type="text/javascript">
				$j = jQuery.noConflict();
				
				$j( function()
				{
					$j(\'fieldset.ether.actionbar-options\').hide();
					
					var actionbar = $j(\'select[name=ether_actionbar]\').attr(\'selectedIndex\');
					
					if (actionbar == 2)
					{
						$j(\'fieldset.ether.actionbar-options\').show();
					}
					
					$j(\'select[name=ether_actionbar]\').change( function()
					{
						var actionbar = $j(this).attr(\'selectedIndex\');
						
						if (actionbar == 2)
						{
							$j(\'fieldset.ether.actionbar-options\').show();
						} else
						{
							$j(\'fieldset.ether.actionbar-options\').hide();
						}
					});
				});
			</script>';
		}
		
		public static function save()
		{
			ether::handle_field($_POST, array
			(
				'select' => array
				(
					array
					(
						'name' => 'actionbar',
						'value' => 'none',
						'relation' => 'meta'
					)
				),
				'textarea' => array
				(
					array
					(
						'name' => 'actionbar_html',
						'value' => '',
						'relation' => 'meta'
					)
				)
			));
		}
		
		public static function body()
		{
			$body = '';

			$body .= '<fieldset class="ether">
				<ul> 
					<li class="half"> 
						<label class="fixed alt-style-1">Action bar</label> 
						'.ether::make_field('actionbar', array('type' => 'select', 'options' => array('none' => array('name' => 'None'), 'title' => array('name' => 'Post title'), 'html' => array('name' => 'HTML')), 'relation' => 'meta')).'
					</li> 
				</ul>		
			</fieldset>

			<fieldset class="ether actionbar-options">
				<ul>
					<li><label class="fixed alt-style-1">HTML code</label>'.ether::make_field('actionbar_html', array('type' => 'textarea', 'rows' => 10, 'relation' => 'meta')).'</li>
				</ul>
			</fieldset>';

			return $body;
		}
	}
?>
