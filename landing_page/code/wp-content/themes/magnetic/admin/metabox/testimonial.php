<?php
	class ether_metabox_testimonial extends ether_metabox
	{
		public static function init()
		{

		}
		
		public static function header()
		{
			
		}
		
		public static function save()
		{
			ether::handle_field($_POST, array
			(
				'text' => array
				(
					array
					(
						'name' => 'author_name',
						'value' => '',
						'relation' => 'meta'
					),
					array
					(
						'name' => 'company_name',
						'value' => '',
						'relation' => 'meta'
					),
					array
					(
						'name' => 'url',
						'value' => '',
						'relation' => 'meta'
					)
				)
			));
		}
		
		public static function body()
		{
			return '<fieldset class="ether"><ul><li><label class="fixed alt-style-1">'.ether::langr('Author name').'</label> 
				'.ether::make_field('author_name', array('type' => 'text', 'relation' => 'meta')).'</li>
				<li><label class="fixed alt-style-1">'.ether::langr('Company name').'</label> 
				'.ether::make_field('company_name', array('type' => 'text', 'relation' => 'meta')).'</li>
				<li><label class="fixed alt-style-1">'.ether::langr('URL').'</label> 
				'.ether::make_field('url', array('type' => 'text', 'relation' => 'meta')).'</li>
				</ul></fieldset>
			';
		}
	}
?>
