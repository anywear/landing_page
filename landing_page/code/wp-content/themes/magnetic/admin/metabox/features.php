<?php
	function features_cmp($a, $b)
	{
		return ($a[ether::config('prefix').'features_order'] < $b[ether::config('prefix').'features_order'] ? -1 : 1);
	}

	class ether_metabox_features extends ether_metabox
	{
		public static function init()
		{

		}
		
		public static function header()
		{
			return '<script type="text/javascript">
				$j = jQuery.noConflict();
				
				$j( function()
				{
					$j(\'.features .add_slide\').click( function()
					{
						var index = -1;

						$j(\'.features input.slider-image-index\').each( function()
						{
							if (parseInt($j(this).val()) > index)
							{
								index = parseInt($j(this).val());
							}
						});
							
						index += 1;
	
						$j(\'.features ul.slides\').append(\'\' + 
						\'<li>\' +
							\'<img src="\' + ether.path + \'ether/media/images/dragndrop-button.png" alt="Drag and Drop" class="dragndrop" />\' +
							\'<input type="text" name="ether_features_order[\' + (index) + \']" class="slider-image-index" value="" />\' +
							\'<input type="text" name="ether_features_title[\' + (index) + \']" value="" class="two-third" /> <a href="#remove" class="remove_slide"><img src="\' + ether.path + \'ether/media/images/remove.png" alt="Remove" /></a>\' +
						\'</li>\');
							
						var $list = $j(\'.features ul.slides > li\');
			
						$list.each( function()
						{
							index = $list.index(this);
								
							$j(this).children(\'input.slider-image-index\').val(index);
						});
							
						$list.children(\'input.slider-image-index\').trigger(\'change\');
						
						return false;
					});
					
					$j(\'.features .remove_slide\').live(\'click\', function()
					{
						$j(this).parents(\'li\').remove();
						
						return false;
					});
				});
			</script>';
		}
		
		public static function save()
		{
			$features = array();
			$prefix = ether::config('prefix').'features_';
				
			foreach ($_POST[$prefix.'order'] as $k => $v)
			{
				$features[$k] = array
				(
					$prefix.'order' => $_POST[$prefix.'order'][$k],
					$prefix.'title' => $_POST[$prefix.'title'][$k]
				);
			}

			global $post;

			ether::meta('features', serialize($features), $post->ID, TRUE);

			usort($features, 'features_cmp');
		}
		
		public static function body()
		{
			$body = '';
			
			global $post;

			$features = unserialize(ether::meta('features', TRUE, $post->ID));

			if ( ! is_array($features))
			{
				$features = array();
			}

			$body .= '<fieldset class="ether features"> 
				<h5 class="alt-style-1">'.ether::langr('Add features').'</h5> 
				<ul id="slider" class="slides">
				'; 
				

			$counter = 0;
			foreach ($features as $feature)
			{
				$body .= '<li>
					<img src="'.ether::path('ether/media/images/dragndrop-button.png', TRUE).'" alt="Drag and Drop" class="dragndrop" />
					'.ether::make_field('features_order['.$counter.']', array('type' => 'text', 'class' => 'slider-image-index', 'value' => 'drag and drop'), $counter).'
					'.ether::make_field('features_title['.$counter.']', array('type' => 'text', 'class' => 'two-third'), $feature[ether::config('prefix').'features_title']).'  <a href="#remove" class="remove_slide"><img src="'.ether::path('ether/media/images/remove.png', TRUE).'" alt="Remove" /></a>
					
				</li>';
				
				$counter++;
			}

			$body .='</ul>
				<ul>
					<li> 
						<input class="add-image add_slide" type="button" value="'.ether::langr('Add image').'" /> 
					</li> 
				</ul> 
			</fieldset>';
		
			return $body;
		}
	}
?>
