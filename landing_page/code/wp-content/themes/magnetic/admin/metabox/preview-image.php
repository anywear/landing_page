<?php
	class ether_metabox_preview_image extends ether_metabox
	{
		public static function init()
		{

		}
		
		public static function header()
		{
			
		}
		
		public static function save()
		{
			ether::handle_field($_POST, array
			(
				'text' => array
				(
					array
					(
						'name' => 'preview_image',
						'value' => '',
						'relation' => 'meta'
					),
					array
					(
						'name' => 'preview_alt',
						'value' => '',
						'relation' => 'meta'
					),
					array
					(
						'name' => 'preview_title',
						'value' => '',
						'relation' => 'meta'
					)
				)
			));
		}
		
		public static function body()
		{
			$width = 266;
			$height = 56;
			
			global $post_type;
			
			if (isset($_GET['post_type']))
			{
				$post_type = $post_type;
			}
			
			if ($post_type == 'service')
			{
				$width = 62;
				$height = 49;
			}
			
			return '<fieldset class="ether"><ul>
				<li>'.ether::make_field('preview_image', array('type' => 'text', 'class' => 'hidden upload_preview_image', 'relation' => 'meta')).'
				'.ether::make_field('preview_alt', array('type' => 'text', 'class' => 'hidden upload_preview_image_alt', 'relation' => 'meta')).'
				'.ether::make_field('preview_title', array('type' => 'text', 'class' => 'hidden upload_preview_image_title', 'relation' => 'meta')).'</li>
				<li>'.ether::make_field('preview_image', array('type' => 'image', 'class' => 'upload_preview_image', 'width' => $width, 'height' => $height, 'relation' => 'meta', 'value' => ether::path('media/images/placeholders/preview-image.png', TRUE))).'</li>
				<li class="half" style="line-height: 32px;"><a href="#'.ether::config('prefix').'preview_image" class="remove_image">'.ether::langr('Remove image').'</a></li>
				<li class="half">'.ether::make_field('upload_preview_image', array('type' => 'upload_image', 'width' => $width, 'height' => $height, 'value' => ether::langr('Upload image'))).'</li>
			</ul></fieldset>';
		}
	}
?>
