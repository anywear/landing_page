<?php
	class ether_metabox_price extends ether_metabox
	{
		public static function init()
		{

		}
		
		public static function header()
		{
			
		}
		
		public static function save()
		{
			ether::handle_field($_POST, array
			(
				'text' => array
				(
					array
					(
						'name' => 'currency',
						'value' => '',
						'relation' => 'meta'
					),
					array
					(
						'name' => 'price',
						'value' => '',
						'relation' => 'meta'
					),
					array
					(
						'name' => 'term',
						'value' => '',
						'relation' => 'meta'
					)
				)
			));
		}
		
		public static function body()
		{
			return '<fieldset class="ether"><ul><li><label class="fixed alt-style-1">'.ether::langr('Currency').'</label> 
				'.ether::make_field('currency', array('type' => 'text', 'relation' => 'meta')).'</li>
				<li><label class="fixed alt-style-1">'.ether::langr('Price').'</label> 
				'.ether::make_field('price', array('type' => 'text', 'relation' => 'meta')).'</li>
				<li><label class="fixed alt-style-1">'.ether::langr('Term').'</label> 
				'.ether::make_field('term', array('type' => 'text', 'relation' => 'meta')).'</li>
				</ul></fieldset>
			';
		}
	}
?>
