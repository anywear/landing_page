<?php
	function slides_cmp($a, $b)
	{
		return ($a[ether::config('prefix').'slides_order'] < $b[ether::config('prefix').'slides_order'] ? -1 : 1);
	}

	class ether_metabox_header extends ether_metabox
	{
		public static function init()
		{

		}
		
		public static function header()
		{
			return '<script type="text/javascript" src="'.ether::path('ether/media/scripts/tooltip.js', TRUE).'"></script>
			<script type="text/javascript">
				$j = jQuery.noConflict();
				
				$j( function()
				{
					$j(\'.headers .has-tooltip\').tooltip
					({
						tooltip: \'.tooltip\',
						x: -23,
						y: -15,
						offset: true,
						sub_width: false,
						sub_height: true,
						standalone: false
					});
					
					$j(\'fieldset.ether.header-options\').hide();
					
					var header = $j(\'select[name=ether_header]\')[0].selectedIndex;
					
					if (header > 1)
					{
						$j(\'fieldset.ether.header-options\').eq(header - 2).show();
					}
					
					if (header < 2 || header > 5)
					{
						$j(\'fieldset.ether.headers\').hide();
					}
					
					$j(\'select[name=ether_header]\').change( function()
					{
						var header = $j(this)[0].selectedIndex;
						$j(\'fieldset.ether.header-options\').hide();
						
						if (header > 1)
						{
							$j(\'fieldset.ether.header-options\').eq(header - 2).show();
						}
						
						if (header > 1 && header < 6)
						{
							$j(\'fieldset.ether.headers\').show();
						} else
						{
							$j(\'fieldset.ether.headers\').hide();
						}
					});

					$j(\'.headers .add_slide\').click( function()
					{
						ether.custom_insert = true;
						tb_show(\'Add slide\', ether.path + \'modules/modalbox/slide.php?ether=true&TB_iframe=true\');
						
						return false;
					});
					
					$j(\'.headers .remove_slide\').live(\'click\', function()
					{
						$j(this).parents(\'li\').remove();
						
						return false;
					});
					
					window._send_to_editor = window.send_to_editor;
					
					window.send_to_editor = function(data)
					{
						if (ether.custom_insert != null)
						{
							if (data.length > 0)
							{
								var index = -1;
								
								$j(\'.headers input.slider-image-index\').each( function()
								{
									if (parseInt($j(this).val()) > index)
									{
										index = parseInt($j(this).val());
									}
								});
								
								index += 1;

								for (var i = 0; i < data.length; i++)
								{
									if ($j(\'input[name^="ether_slides_id"][value=\' + data[i].id + \']\').length == 0)
									{
										$j(\'.headers ul.slides\').append(\'\' + 
										\'<li>\' +
											\'<img src="\' + ether.path + \'ether/media/images/dragndrop-button.png" alt="Drag and Drop" class="dragndrop" />\' +
											\'<input type="text" name="ether_slides_order[\' + (index + i) + \']" class="slider-image-index" value="" />\' +
											\'<input type="text" name="ether_slides_id[\' + (index + i) + \']" class="hidden" value="\' + data[i].id + \'" style="display: none;" />\' +
											\'<input type="text" name="ether_slides_title[\' + (index + i) + \']" class="hidden" value="\' + data[i].title + \'" style="display: none;" />\' +
											\'<div class="title has-tooltip">\' +
												\'<a href="#">\' + data[i].title + \'</a> <a href="#remove" class="remove_slide"><img src="\' + ether.path + \'ether/media/images/remove.png" alt="Remove" /></a>\' +
												\'<span class="tooltip"><img src="\' + data[i].thumbnail + \'" width="220" height="165" /></span>\' +									
											\'</div>\' +
										\'</li>\');
									}
								}
							}
							
							var $list = $j(\'.headers ul.slides > li\');
			
							$list.each( function()
							{
								index = $list.index(this);
									
								$j(this).children(\'input.slider-image-index\').val(index);
							});
							
							$list.children(\'input.slider-image-index\').trigger(\'change\');
							tb_remove();
						} else
						{
							window._send_to_editor(data);
						}
					};
				});
			</script>';
		}
		
		public static function save()
		{
			$slides = array();
			$prefix = ether::config('prefix').'slides_';
				
			foreach ($_POST[$prefix.'order'] as $k => $v)
			{
				$slides[$k] = array
				(
					$prefix.'order' => $_POST[$prefix.'order'][$k],
					$prefix.'id' => $_POST[$prefix.'id'][$k],
					$prefix.'title' => $_POST[$prefix.'title'][$k]
				);
			}

			global $post;

			ether::meta('slides', serialize($slides), $post->ID, TRUE);
	
			$header_options = array();
				
			foreach (ether_header::$header_fields[$_POST[ether::config('prefix').'header']] as $option)
			{
				$header_options[ether::config('prefix').$option] = htmlspecialchars(stripslashes($_POST[ether::config('prefix').$option]));
			}
				
			$header_options[ether::config('prefix').'header'] = $_POST[ether::config('prefix').'header'];
			
			
			ether::meta('header_options', serialize($header_options), $post->ID, TRUE);

			usort($slides, 'slides_cmp');
		}
		
		public static function body()
		{
			$body = '';
			
			global $post;

			$slides = unserialize(ether::meta('slides', TRUE, $post->ID));

			if ( ! is_array($slides))
			{
				$slides = array();
			}
				
			$header_options = unserialize(ether::meta('header_options', TRUE, $post->ID));
			
			foreach ($header_options as $k => $v)
			{
				$header_options[$k] = htmlspecialchars_decode($v);
			}
			
			$headers = array('inherited' => array('name' => ether::langx('Inherited', 'enable header inheritance from global header settings', TRUE))) + ether_header::$headers;
			
			$body .= '<fieldset class="ether">
				<ul> 
					<li class="half"> 
						<label class="fixed alt-style-1">'.ether::langr('Header').'</label> 
						'.ether::make_field('header', array('type' => 'select', 'options' => $headers), $header_options).'
					</li> 
				</ul>		
			</fieldset>
			
			<fieldset class="ether header-options">
				<ul>
					<li class="half"><label class="fixed alt-style-1">'.ether::langr('Text align').'</label>'.ether::make_field('slider_align', array('type' => 'select', 'options' => ether_header::$align), $header_options).'</li>
					<li class="half"><label class="fixed alt-style-1">'.ether::langr('AutoPlay').'</label>'.ether::make_field('slider_autoplay', array('type' => 'select', 'options' => ether_header::$autoplay), $header_options).'</li>
				</ul>
			</fieldset>
			
			<fieldset class="ether header-options">
				<ul>
					<li class="half"><label class="fixed alt-style-1">'.ether::langr('Effect').'</label>'.ether::make_field('nivo_effect', array('type' => 'select', 'options' => ether_header::$nivo_effect), $header_options).'</li>
					<li class="half"><label class="fixed alt-style-1">'.ether::langr('AutoPlay').'</label>'.ether::make_field('nivo_autoplay', array('type' => 'select', 'options' => ether_header::$autoplay), $header_options).'</li>
				</ul>
				<ul>
					<li class="half"><label class="fixed alt-style-1">'.ether::langr('Width').'</label>'.ether::make_field('nivo_width', array('type' => 'text', 'value' => '970', 'use_default' => TRUE), $header_options).'</li>
					<li class="half"><label class="fixed alt-style-1">'.ether::langr('Height').'</label>'.ether::make_field('nivo_height', array('type' => 'text', 'value' => '270', 'use_default' => TRUE), $header_options).'</li>
				</ul>
			</fieldset>
			
			<fieldset class="ether header-options">
				<ul>
					<li class="half"><label class="fixed alt-style-1">'.ether::langr('AutoPlay').'</label>'.ether::make_field('carousel_autoplay', array('type' => 'select', 'options' => ether_header::$autoplay), $header_options).'</li>
					<li class="half"><label class="fixed alt-style-1">'.ether::langr('Animation').'</label>'.ether::make_field('carousel_animation', array('type' => 'select', 'options' => ether_header::$piecemaker_transition), $header_options).'</li>
				</ul>
				<ul>
					<li class="half"><label class="fixed alt-style-1">'.ether::langr('Width').'</label>'.ether::make_field('carousel_width', array('type' => 'text', 'value' => '620', 'use_default' => TRUE), $header_options).'</li>
				</ul>
			</fieldset>
			
			<fieldset class="ether header-options">
				<ul>
					<li class="half"><label class="fixed alt-style-1">'.ether::langr('Width').'</label>'.ether::make_field('piecemaker_width', array('type' => 'text', 'value' => '900', 'use_default' => TRUE), $header_options).'</li>
					<li class="half"><label class="fixed alt-style-1">'.ether::langr('Height').'</label>'.ether::make_field('piecemaker_height', array('type' => 'text', 'value' => '360', 'use_default' => TRUE), $header_options).'</li>
				</ul>
				<ul>
					<li class="half"><label class="fixed alt-style-1">'.ether::langr('Pieces').'</label>'.ether::make_field('piecemaker_pieces', array('type' => 'text', 'value' => '9', 'use_default' => TRUE), $header_options).'</li>
					<li class="half"><label class="fixed alt-style-1">'.ether::langr('Time').'</label>'.ether::make_field('piecemaker_time', array('type' => 'text', 'value' => '1.2', 'use_default' => TRUE), $header_options).'</li>
				</ul>
				<ul>
					<li class="half"><label class="fixed alt-style-1">'.ether::langr('Transition').'</label>'.ether::make_field('piecemaker_transition', array('type' => 'select', 'options' => ether_header::$piecemaker_transition), $header_options).'</li>
					<li class="half"><label class="fixed alt-style-1">'.ether::langr('Delay').'</label>'.ether::make_field('piecemaker_delay', array('type' => 'text', 'value' => '0.1', 'use_default' => TRUE), $header_options).'</li>
				</ul>
				<ul>
					<li class="half"><label class="fixed alt-style-1">'.ether::langr('Depth offset').'</label>'.ether::make_field('piecemaker_depth', array('type' => 'text', 'value' => '300', 'use_default' => TRUE), $header_options).'</li>
					<li class="half"><label class="fixed alt-style-1">'.ether::langr('Distance').'</label>'.ether::make_field('piecemaker_distance', array('type' => 'text', 'value' => '30', 'use_default' => TRUE), $header_options).'</li>
				</ul>
				<ul>
					<li class="half"><label class="fixed alt-style-1">'.ether::langr('AutoPlay').'</label>'.ether::make_field('piecemaker_autoplay', array('type' => 'select', 'options' => ether_header::$autoplay), $header_options).'</li>
				</ul>
			</fieldset>
			
			<fieldset class="ether header-options">
				<ul>
					<li><label class="fixed alt-style-1">'.ether::langr('Flickr ID').'</label>'.ether::make_field('flickr_id', array('type' => 'text', 'value' => ether::option('social_flickr'), 'use_default' => TRUE), $header_options).'</li>
				</ul>
			</fieldset>
			
			<fieldset class="ether header-options">
				<ul>
					<li><label class="fixed alt-style-1">'.ether::langr('Twitter username').'</label>'.ether::make_field('twitter_username', array('type' => 'text', 'value' => ether::option('social_twitter'), 'use_default' => TRUE), $header_options).'</li>
				</ul>
			</fieldset>
			
			<fieldset class="ether header-options">
				<ul>
					<li><label class="fixed alt-style-1">'.ether::langr('HTML code').'</label>'.ether::make_field('html_code', array('type' => 'textarea', 'rows' => 10), $header_options).'</li>
				</ul>
			</fieldset>
	
			<fieldset class="ether headers"> 
				<h5 class="alt-style-1">'.ether::langr('Add slides').'</h5> 
				<ul id="slider" class="slides">
				'; 
				

			$counter = 0;
			foreach ($slides as $slide)
			{
				$thumbnail = ether::meta('preview_image', TRUE, $slide[ether::config('prefix').'slides_id']);
				
				if ( ! empty($thumbnail))
				{
					$thumbnail = ether::get_image_thumbnail($thumbnail, 220, 165);
				} else
				{
					$thumbnail = ether::path('media/images/placeholders/placeholder.png', TRUE);
				}
				
				$body .= '<li>
					<img src="'.ether::path('ether/media/images/dragndrop-button.png', TRUE).'" alt="Drag and Drop" class="dragndrop" />
					'.ether::make_field('slides_order['.$counter.']', array('type' => 'text', 'class' => 'slider-image-index', 'value' => 'drag and drop'), $counter).'
					'.ether::make_field('slides_id['.$counter.']', array('type' => 'text', 'class' => 'hidden'), $slide[ether::config('prefix').'slides_id']).'
					'.ether::make_field('slides_title['.$counter.']', array('type' => 'text', 'class' => 'hidden'), $slide[ether::config('prefix').'slides_title']).'
					<div class="title has-tooltip">
						<a href="post.php?post='.$slide[ether::config('prefix').'slides_id'].'&action=edit">'.$slide[ether::config('prefix').'slides_title'].'</a> <a href="#remove" class="remove_slide"><img src="'.ether::path('ether/media/images/remove.png', TRUE).'" alt="Remove" /></a>
						<span class="tooltip"><img src="'.$thumbnail.'" width="220" height="165" /></span>
					</div>
				</li>';
				
				$counter++;
			}

			$body .='</ul>
				<ul>
					<li> 
						<input class="add-image add_slide" type="button" value="'.ether::langr('Add image').'" /> 
					</li> 
				</ul> 
			</fieldset>';
		
			return $body;
		}
	}
?>
