<?php
	class ether_panel_custom_code extends ether_panel
	{
		public static function init()
		{
			
		}
		
		public static function header()
		{
			
		}
		
		public static function reset()
		{
			ether::handle_field(array(), array
			(
				'textarea' => array
				(
					array
					(
						'name' => 'custom_style',
						'value' => ''
					),
					array
					(
						'name' => 'custom_script',
						'value' => ''
					),
					array
					(
						'name' => 'tracking_code',
						'value' => ''
					)
				)
			));
		}
		
		public static function body()
		{
			if (isset($_POST['save']))
			{
				ether::handle_field($_POST, array
				(
					'textarea' => array
					(
						array
						(
							'name' => 'custom_style',
							'value' => ''
						),
						array
						(
							'name' => 'custom_script',
							'value' => ''
						),
						array
						(
							'name' => 'tracking_code',
							'value' => ''
						)
					)
				));
			}
			
			return '<fieldset class="ether">
				<ul> 
					<li> 
						<label>'.ether::langr('Custom CSS').'</label> 
						'.ether::make_field('custom_style', array('type' => 'textarea', 'class' => 'auto', 'rows' => 10, 'cols' => 10)).'
					</li> 
				</ul>
				<ul> 
					<li> 
						<label>'.ether::langr('Custom Java Script').'</label> 
						'.ether::make_field('custom_script', array('type' => 'textarea', 'class' => 'auto', 'rows' => 10, 'cols' => 10)).'
					</li> 
				</ul>
				<ul> 
					<li> 
						<label>'.ether::langr('Tracking code').'</label> 
						'.ether::make_field('tracking_code', array('type' => 'textarea', 'class' => 'auto', 'rows' => 10, 'cols' => 10)).'
					</li> 
				</ul>		
		</fieldset> ';
		}
	}
?>
