<?php
	class ether_panel_sidebars extends ether_panel
	{
		public static function init()
		{

		}
		
		public static function header()
		{
			
		}
		
		public static function reset()
		{
			ether::module_run('sidebar.clean');
		}
		
		public static function body()
		{
			if ( ! isset($_POST['relation_edit']) OR isset($_POST['relation_edit_id']))
			{
				$body = '<fieldset class="ether"><h3>'.ether::langr('Assigned sidebars').'</h3>'; 
			}

			if (isset($_POST['sidebar_add']))
			{
				if (isset($_POST['sidebar_name']) AND ! empty($_POST['sidebar_name']))
				{
					ether::module_run('sidebar.add', $_POST['sidebar_name']);
				}
			}
			
			if (isset($_POST['relation_add']))
			{
				if ( ! empty($_POST['category_id']))
				{
					ether::module_run('sidebar.add_relation', $_POST['sidebar_id'], 'category', $_POST['category_id']);
				}
				
				if ( ! empty($_POST['page_id']))
				{
					$posts = get_posts(array('post_type' => array('page'), 'p' => $_POST['page_id'], 'nopaging' => TRUE));
					
					if (count($posts) > 0)
					{
						ether::module_run('sidebar.add_relation', $_POST['sidebar_id'], 'page', $_POST['page_id']);
					} else
					{
						$body .= '<div id="message" class="error"><p>'.ether::langr('There isn\'t any page with ID %s', ether::clean($_POST['page_id'], TRUE, TRUE, TRUE)).'.</p></div>';
					}
				}
				
				if ( ! empty($_POST['post_id']))
				{
					$posts = get_posts(array('post_type' => array_merge(array('post'), ether::post_types()), 'p' => $_POST['post_id'], 'nopaging' => TRUE));
					
					if (count($posts) > 0)
					{
						ether::module_run('sidebar.add_relation', $_POST['sidebar_id'], 'post', $_POST['post_id']);
					} else
					{
						$body .= '<div id="message" class="error"><p>'.ether::langr('There isn\'t any post with ID %s'. ether::clean($_POST['post_id'], TRUE, TRUE, TRUE)).'.</p></div>';
					}
				}
				
				if (empty($_POST['category_id']) AND empty($_POST['page_id']) AND empty($_POST['post_id']))
				{
					$body .= '<div id="message" class="error"><p>'.ether::langr('You must fill in one of the ID fields').'.</p></div>';
				}
			}
			
			if (isset($_POST['relation_delete']))
			{
				if ( ! empty($_POST['relation_delete']))
				{
					ether::module_run('sidebar.delete_relation', $_POST['relation_delete']);
				}
			}
			
			if (isset($_POST['sidebar_delete']))
			{
				if ( ! empty($_POST['sidebar_delete']))
				{
					ether::module_run('sidebar.delete', $_POST['sidebar_delete']);
				}
			}
			
			if (isset($_POST['relation_edit']) AND ! isset($_POST['relation_id']) AND ! isset($_POST['relation_edit_id']))
			{
				$relation = ether::module_run('sidebar.get_relation_by_id', $_POST['relation_edit']);
				$name = ucfirst($relation[0]['relation_type']);

				$body .= '<fieldset class="ether"> 
				<h5>'.ether::langr('Edit relation').'</h5> 
				<ul class="tapeworm"> 
					<li> 
						<label class="alt-style-1"><b>'.$name.' ID</b></label> 
						<input type="hidden" name="relation_id" value="'.$relation[0]['id'].'" /> 
						<input type="text" name="relation_edit_id" value="'.$relation[0]['relation_id'].'" /> 
					</li> 
					<li> 
						<input type="submit" name="relation_edit" value="'.ether::langr('Edit').'" /> 
					</li> 
				</ul>		
				</fieldset>';
			} else
			{
				$categories = get_terms(array('category', 'news_category', 'testimonials_category', 'success-stories_category'), array('orderby' => 'term_group'));
				//$categories = ether::get_categories();
				$categories_by_id = array();			

				$count = count($categories);
				for ($i = 0; $i < $count; $i++)
				{
					if ( ! empty($categories[$i]->name))
					{
						$prefix = '';

						$taxonomies = array
						(
							'category' => 'Post',
							'news_category' => 'News',
							'testimonials_category' => 'Testimonials',
							'success-stories_category' => 'Success stories'
						);
						
						$prefix = $taxonomies[$categories[$i]->taxonomy];
						
						$categories_by_id[$categories[$i]->term_id] = ( ! empty($prefix) ? $prefix.' - ' : '').$categories[$i]->name;
					}
				}
				
				if (isset($_POST['relation_edit']))
				{
					$relation = ether::module_run('sidebar.get_relation_by_id', $_POST['relation_id']);
					
					if (count($relation) > 0)
					{
						$found = FALSE;
						
						if ($relation[0]['relation_type'] == 'category')
						{
							if (isset($categories_by_id[$_POST['relation_edit_id']]))
							{
								$found = TRUE;
							}
						} else if ($relation[0]['relation_type'] == 'page')
						{
							if (count(get_posts(array('post_type' => array('page'), 'p' => $_POST['relation_edit_id']))) > 0)
							{
								$found = TRUE;
							}
						} else if ($relation[0]['relation_type'] == 'post')
						{
							if (count(get_posts(array('post_type' => array_merge(array('post'), ether::post_types()), 'p' => $_POST['relation_edit_id']))) > 0)
							{
								$found = TRUE;
							}
						}
						
						if ($found)
						{
							if ( ! ether::module_run('sidebar.is_relation', $relation[0]['sidebar_id'], $relation[0]['relation_type'], $_POST['relation_edit_id']))
							{
								ether::module_run('sidebar.edit_relation_by_id', $_POST['relation_id'], $_POST['relation_edit_id']);
							} else
							{
								$body .= '<div id="message" class="error"><p>'.ether::langr('There is %s with ID %s already', $relation[0]['relation_type'], ether::clean($_POST['relation_edit_id'], TRUE, TRUE, TRUE)).'.</p></div>';	
							}
						} else
						{
							$body .= '<div id="message" class="error"><p>'.ether::langr('There isn\'t any %s with ID %s', $relation[0]['relation_type'], ether::clean($_POST['relation_edit_id'], TRUE, TRUE, TRUE)).'.</p></div>';
						}
					}
				}
				
				$sidebars = ether::module_run('sidebar.get_all');
				$relations = ether::module_run('sidebar.get_relation_all');
				$relations_by_sidebar_id = array();
				$post_ids = array();
				$page_ids = array();
				$posts_by_id = array();
				$pages_by_id = array();
				$sidebars_by_id = array();
				
				$count = count($sidebars);
				for ($i = 0; $i < $count; $i++)
				{
					$sidebars_by_id[$sidebars[$i]['id']] = $sidebars[$i]['name'];
				}
				
				$count = count($relations);
				for ($i = 0; $i < $count; $i++)
				{
					$relations_by_sidebar_id[$relations[$i]['sidebar_id']][] = array
					(
						'id' => $relations[$i]['id'],
						'relation_type' => $relations[$i]['relation_type'],
						'relation_id' => $relations[$i]['relation_id'],
					);
					
					if ($relations[$i]['relation_type'] == 'post')
					{
						$post_ids[] = $relations[$i]['relation_id'];
					} else if ($relations[$i]['relation_type'] == 'page')
					{
						$page_ids[] = $relations[$i]['relation_id'];
					}
				}
				
				$posts = get_posts(array('post_type' => array_merge(array('post'), ether::post_types()), 'post__in' => $post_ids, 'nopaging' => TRUE));
				$pages = get_posts(array('post_type' => array('page'), 'post__in' => $page_ids, 'nopaging' => TRUE));
				
				$count = count($posts);
				for ($i = 0; $i < $count; $i++)
				{
					$posts_by_id[$posts[$i]->ID] = array
					(
						'id' => $posts[$i]->ID,
						'name' => $posts[$i]->post_title,
						'type' => $posts[$i]->post_type
					);
				}

				unset($posts);
				
				$count = count($pages);
				for ($i = 0; $i < $count; $i++)
				{
					$pages_by_id[$pages[$i]->ID] = array
					(
						'id' => $pages[$i]->ID,
						'name' => $pages[$i]->post_title,
						'type' => $pages[$i]->post_type
					);
				}

				unset($posts);

				$body .= '<table width="100%" border="0" cellspacing="0" cellpadding="0"> 
					<tr> 
						<th scope="col">'.ether::langr('Sidebar Name').'</th> 
						<th scope="col">'.ether::langr('Assigned categories').'</th> 
						<th scope="col">'.ether::langr('Assigned pages').'</th> 
						<th scope="col">'.ether::langr('Assigned posts').'</th> 
					</tr>';

				foreach ($sidebars_by_id as $id => $name)
				{
					$relation = $relations_by_sidebar_id[$id];
					$relation_count = count($relation);
					
					$body .= '<tr>';
					$body .= '<th scope="row">'.$name.'<div class="buttons"><label for="sidebar_delete">'.ether::langr('Delete').'</label><input type="submit" name="sidebar_delete" value="'.$id.'" class="delete" /></div></th>';
					$body .= '<td>';
					$counter = 0;
					$row = '';
					
					for ($i = 0; $i < $relation_count; $i++)
					{
						if ($relation[$i]['relation_type'] == 'category')
						{
							$category = $categories_by_id[$relation[$i]['relation_id']];
							$row .= '<li>ID'.$relation[$i]['relation_id'].' - '.$category;
							$row .= '<div class="buttons">';
							$row .= '<label for="relation_edit">'.ether::langr('Edit').'</label>';
							$row .= '<input type="hidden" name="sidebar_id" value="'.$id.'" />';
							$row .= '<input type="hidden" name="relation_type" value="'.$relation[$i]['relation_type'].'" />';
							$row .= '<input type="submit" name="relation_edit" value="'.$relation[$i]['id'].'" class="change" />';
							$row .= '<label for="relation_save">'.ether::langr('Save').'</label>';
							$row .= '<input type="submit" name="relation_save" value="'.$relation[$i]['id'].'" class="save" />';
							$row .= '<label for="relation_delete">'.ether::langr('Delete').'</label>';
							$row .= '<input type="submit" name="relation_delete" value="'.$relation[$i]['id'].'" class="delete" />';
							$row .= '</div>';
							$row .= '</li>';
							$counter++;
						}
					}
					
					if ($counter > 0)
					{
						$body .= '<ul>'.$row.'</ul>';
					}
					
					$body .= '</td>';
					$body .= '<td>';
					$counter = 0;
					$row = '';
					
					for ($i = 0; $i < $relation_count; $i++)
					{
						if ($relation[$i]['relation_type'] == 'page')
						{
							$page = $pages_by_id[$relation[$i]['relation_id']];
							$row .= '<li>ID'.$relation[$i]['relation_id'].' - '.$page['name'];
							$row .= '<div class="buttons">';
							$row .= '<input type="hidden" name="sidebar_id" value="'.$id.'" />';
							$row .= '<input type="hidden" name="relation_type" value="'.$relation[$i]['relation_type'].'" />';
							$row .= '<label for="relation_edit">'.ether::langr('Edit').'</label>';
							$row .= '<input type="submit" name="relation_edit" value="'.$relation[$i]['id'].'" class="change" />';
							$row .= '<label for="relation_save">'.ether::langr('Save').'</label>';
							$row .= '<input type="submit" name="relation_save" value="'.$relation[$i]['id'].'" class="save" />';
							$row .= '<label for="relation_delete">'.ether::langr('Delete').'</label>';
							$row .= '<input type="submit" name="relation_delete" value="'.$relation[$i]['id'].'" class="delete" />';
							$row .= '</div>';
							$row .= '</li>';
							$counter++;
						}
					}
					
					if ($counter > 0)
					{
						$body .= '<ul>'.$row.'</ul>';
					}
					
					$body .= '</td>';
					$body .= '<td>';
					$counter = 0;
					$row = '';
					
					for ($i = 0; $i < $relation_count; $i++)
					{
						if ($relation[$i]['relation_type'] == 'post')
						{
							$post = $posts_by_id[$relation[$i]['relation_id']];
							
							$types = array
							(
								'post' => 'Post',
								'news' => 'News',
								'testimonials' => 'Testimonials',
								'success-stories' => 'Success stories'
							);
							
							$prefix = $types[$post['type']];
							
							$row .= '<li>ID'.$relation[$i]['relation_id'].' - '.( ! empty($prefix) ? $prefix.' - ' : '').$post['name'];
							$row .= '<div class="buttons">';
							$row .= '<label for="relation_edit">'.ether::langr('Edit').'</label>';
							$row .= '<input type="hidden" name="sidebar_id" value="'.$id.'" />';
							$row .= '<input type="hidden" name="relation_type" value="'.$relation[$i]['relation_type'].'" />';
							$row .= '<input type="submit" name="relation_edit" value="'.$relation[$i]['id'].'" class="change" />';
							$row .= '<label for="relation_save">'.ether::langr('Save').'</label>';
							$row .= '<input type="submit" name="relation_save" value="'.$relation[$i]['id'].'" class="save" />';
							$row .= '<label for="relation_delete">'.ether::langr('Delete').'</label>';
							$row .= '<input type="submit" name="relation_delete" value="'.$relation[$i]['id'].'" class="delete" />';
							$row .= '</div>';
							$row .= '</li>';
							$counter++;
						}
					}
					
					if ($counter > 0)
					{
						$body .= '<ul>'.$row.'</ul>';
					}
					
					$body .= '</td>';
					$body .= '</tr>';
				}

				$body .= '</table> 
				</fieldset>		
				<fieldset class="ether">			
					<h5>'.ether::langr('New assign').'</h5> 
					<ul class="tapeworm"> 
						<li> 
							<label class="alt-style-1"><b>'.ether::langr('Select sidebar').'</b></label> 
							<select name="sidebar_id"><option></option>';
							
							$count = count($sidebars);
							
							for ($i = 0; $i < $count; $i++)
							{
								$body .= '<option value="'.$sidebars[$i]['id'].'">'.$sidebars[$i]['name'].'</option>';
							}
							
							$body .= '</select> 
						</li> 
						<li> 
							<span>and</span> 
							<label class="alt-style-1"><b>'.ether::langr('Assign category').'</b></label> 
							<select name="category_id"><option></option>';
							
							$count = count($categories);
							
							for ($i = 0; $i < $count; $i++)
							{
								if ( ! empty($categories[$i]->name))
								{
									$prefix = '';
									
									$taxonomies = array
									(
										'category' => 'Post',
										'news_category' => 'News',
										'testimonials_category' => 'Testimonials',
										'success-stories_category' => 'Success stories'
									);
									
									$prefix = $taxonomies[$categories[$i]->taxonomy];

									$body .= '<option value="'.$categories[$i]->term_id.'">'.( ! empty($prefix) ? $prefix.' - ' : '').$categories[$i]->name.'</option>';
								}
							}
							
							$body .= '</select>
						</li> 
						<li> 
							<span>or</span> 
							<label class="alt-style-1"><b>'.ether::langr('Page ID').'</b></label> 
							<input class="tiny" name="page_id" type="text" />  
						</li> 
						<li> 
							<span>or</span> 
							<label class="alt-style-1"><b>'.ether::langr('Post ID').'</b></label> 
							<input class="tiny" name="post_id" type="text" /> 
						</li> 
						<li> 
							<input type="submit" name="relation_add" value="'.ether::langr('Assign').'" /> 
						</li> 
					</ul> 
				</fieldset>		
				<fieldset class="ether"> 
					<h5>'.ether::langr('Create new sidebar').'</h5> 
					<ul class="tapeworm"> 
						<li> 
							<label class="alt-style-1"><b>'.ether::langr('Sidebar name').'</b></label> 
							<input type="text" name="sidebar_name" /> 
						</li> 
						<li> 
							<input type="submit" name="sidebar_add" value="'.ether::langr('Create').'" /> 
						</li> 
					</ul>		
				</fieldset>';
			}
			
			return $body;
		}
	}
?>
