<?php
	class ether_panel_general extends ether_panel
	{
		public static function init()
		{
			
		}
		
		public static function header()
		{
			
		}
		
		public static function reset()
		{
			ether::handle_field(array(), array
			(
				'text' => array
				(
					array
					(
						'name' => 'footer_text',
						'value' => '&copy '.date('Y').' Your Company Name. All rights reserved.'
					),
					array
					(
						'name' => 'favicon',
						'value' => ether::path('admin/media/images/favicon.png', TRUE)
					),
					array
					(
						'name' => 'html_logo_title',
						'value' => '<span>Magnetic</span><sup>TM</sup>'
					),
					array
					(
						'name' => 'image_logo',
						'value' => ''
					)
				),
				'checkbox' => array
				(
					array
					(
						'name' => 'use_image_logo',
						'value' => ''
					),
					array
					(
						'name' => 'use_favicon',
						'value' => ''
					),
					array
					(
						'name' => 'enable_switcher',
						'value' => ''
					)
				)
			));	
		}
		
		public static function body()
		{
			if (isset($_POST['save']))
			{
				ether::handle_field($_POST, array
				(
					'text' => array
					(
						array
						(
							'name' => 'footer_text',
							'value' => '&copy '.date('Y').' Your Company Name. All rights reserved.'
						),
						array
						(
							'name' => 'favicon',
							'value' => ether::path('admin/media/images/favicon.png', TRUE)
						),
						array
						(
							'name' => 'html_logo_title',
							'value' => '<span>Magnetic</span><sup>TM</sup>'
						),
						array
						(
							'name' => 'image_logo',
							'value' => ''
						)
					),
					'checkbox' => array
					(
						array
						(
							'name' => 'use_image_logo',
							'value' => ''
						),
						array
						(
							'name' => 'use_favicon',
							'value' => ''
						),
						array
						(
							'name' => 'enable_switcher',
							'value' => ''
						)
					)
				));
			}
			
			
			$body = '<fieldset class="ether">
				<ul>
					<li>
						<label>'.ether::langr('Customize text in logo').'</label>
						'.ether::make_field('html_logo_title', array('type' => 'text')).'
					</li>
				</ul>		
			</fieldset>
			<fieldset class="ether">
				<ul>
					<li>
						<label>'.ether::make_field('use_image_logo', array('type' => 'checkbox')).' '.ether::langr('I want to use image as a logo').'</label>
						'.ether::make_field('image_logo', array('type' => 'text', 'class' => 'hidden upload_image_logo')).'
						<p class="logo-preview-box">
							'.ether::make_field('image_logo', array('type' => 'image', 'class' => 'upload_image_logo', 'width' => 200, 'height' => 60)).'
						</p>
						<div class="buttons alignleft">
							'.ether::make_field('upload_image_logo', array('type' => 'upload_image', 'width' => 200, 'height' => 60, 'value' => ether::langr('Upload image'))).'
						</div>
					</li>
				</ul>
			</fieldset>
			<fieldset class="ether">
				<ul>
					<li>
						<label>'.ether::make_field('use_favicon', array('type' => 'checkbox')).' '.ether::langr('Use favicon').'</label>
						'.ether::make_field('favicon', array('type' => 'text', 'class' => 'hidden upload_favicon')).'
						<div class="favicon-preview-box">
							'.ether::make_field('favicon', array('type' => 'image', 'class' => 'upload_favicon', 'width' => 16, 'height' => 16)).'
							<div class="buttons alignright single-row">
								'.ether::make_field('upload_favicon', array('type' => 'upload_image', 'width' => 16, 'height' => 16, 'value' => ether::langr('Upload image'))).'
							</div>
						</div>
					</li>
				</ul>
			</fieldset>		
			<fieldset class="ether">		
				<ul>
					<li>
						<label>'.ether::langr('Footer text').'</label>
						'.ether::make_field('footer_text', array('type' => 'text', 'class' => 'auto')).'
					</li>
				</ul>		
			</fieldset>
			<fieldset class="ether">
				<ul>
					<li>
						<label>'.ether::make_field('enable_switcher', array('type' => 'checkbox')).' '.ether::langr('Enable switcher').'</label>
					</li>
				</ul>
			</fieldset>';
			
			return $body;
		}
	}
?>
