$j = jQuery.noConflict();

$j( function()
{
	function r()
	{
		return new Date().getTime();
	}
	
	$j('#columns-wrapper').sortable({
		connectWith: $j('#columns-wrapper'),
		handle: '.drag',
		cursor: 'move',
		placeholder: 'placeholder',
		forcePlaceholderSize: true,
		opacity: 0.4,
		stop: function(event, ui)
		{
			$j(ui.item).children('.drag').click();
		}
	});
	
	$j('button[name=addrow]').click( function()
	{
		var rowtype = $j('select[name=row]').val();
		var row = '';
		
		if (rowtype == 1)
		{
			row = '<div class="columns"><a class="drag"></a></div>';
		} else if (rowtype == 2)
		{
			row = '<div class="columns"><div class="half"></div><div class="half"></div><a class="drag"></a></div>';
		} else if (rowtype == 3)
		{
			row = '<div class="columns"><div class="fourth"></div><div class="three-fourth"></div><a class="drag"></a></div>';
		} else if (rowtype == 4)
		{
			row = '<div class="columns"><div class="third"></div><div class="two-third"></div><a class="drag"></a></div>';
		} else if (rowtype == 5)
		{
			row = '<div class="columns"><div class="third"></div><div class="third"></div><div class="third"></div><a class="drag"></a></div>';
		} else if (rowtype == 6)
		{
			row = '<div class="columns"><div class="half"></div><div class="fourth"></div><div class="fourth"></div><a class="drag"></a></div>';
		} else if (rowtype == 7)
		{
			row = '<div class="columns"><div class="fourth"></div><div class="fourth"></div><div class="fourth"></div><div class="fourth"></div><a class="drag"></a></div>';
		}

		$j('#columns-wrapper').append(row);
	
		return false;
	});
	
	$j('button[name=removerow]').click( function()
	{
		$j('.columns.selected').remove();
		
		return false;
	});
	
	if (typeof tinyMCEPopup != 'undefined')
	{
		tinyMCEPopup.resizeToInnerSize();

		$j('input[name=insert]').click( function()
		{
			console.log('asd');
			if (window.tinyMCE)
			{
				var selection = tinyMCE.activeEditor.selection.getContent();
				
				var shortcode = '';
				
				$j('#columns-wrapper .columns').each( function()
				{
					var html = $j(this).html();
					
					var cols_html = ['<div class="half"></div>', '<div class="third">', '<div class="fourth">', '<div class="two-third">', '<div class="three-fourth">'];
					var cols_shortcode = ['[half][/half]', '[third][/third]', '[fourth][/fourth]', '[two-third][/two-third]', '[three-fourth][/three-fourth]'];
					
					for (var i = 0; i < cols_html.length; i++)
					{
						html = html.replace(new RegExp(cols_html[i], 'g'), cols_shortcode[i]);
					}
					
					html = html.replace(/<div class="columns">/g, '[columns]');
					html = html.replace(/<\/div>/g, '[/columns]' + "\n");
					
					shortcode += html;
				});

				if (selection.length > 0)
				{
					var content = tinyMCE.activeEditor.selection.getContent();
			
					window.tinyMCE.execInstanceCommand('content', 'mceInsertContent', false,  ' ' + content + shortcode + ' ');
				} else
				{
					window.tinyMCE.execInstanceCommand('content', 'mceInsertContent', false,  ' ' + shortcode + ' ');
				}
		
				tinyMCEPopup.editor.execCommand('mceRepaint');
				tinyMCEPopup.close();
			}
			
			return false;
		});
	}
	
	$j('#columns-wrapper .columns').live('click', function(e)
	{
		if ($j(e.target).hasClass('columns'))
		{
			$j(this).siblings('.columns').removeClass('selected');
			$j(this).addClass('selected');
		} else
		{
			$j(this).parents('.columns').siblings('.columns').removeClass('selected');
			$j(this).parents('.columns').addClass('selected');
		}
		
		return false;
	});
	
	$j('#columns-wrapper > .columns').live('hover', function()
	{
		//$j(this).append('<div class="drag"></div>');
	}, function()
	{
		//$j(this).find('.drag').remove();
	});

	$j('#columns-wrapper > .columns > div').live('mouseenter', function(e)
	{
		if ($j(e.target).parent('.columns').length > 0)
		{
			$j(this).append('<a class="drag"></a>');
			
			if ($j(e.target).parent('.ui-sortable').length == 0)
			{
				$j(e.target).parent('.columns').sortable({
					connectWith: $j(e.target).parent('.columns'),
					handle: '.drag',
					cursor: 'move',
					placeholder: 'placeholder',
					forcePlaceholderSize: true,
					opacity: 0.4,
					start: function(event, ui)
					{
						var $items = $j(ui.item).parent('.columns').children('div');
						$items.removeClass('first');

						if ($items.eq(0).hasClass('ui-sortable-helper') || $items.index($j(ui.placeholder)) == 1)
						{
							$items.eq(1).addClass('first');
						} else
						{
							$items.eq(0).addClass('first');
						}
					},
					stop: function(event, ui)
					{
						$j(ui.item).find('.drag').click();
						var $items = $j(ui.item).parent('.columns').children('div');
						$items.removeClass('first');
					},
					change: function(event, ui)
					{
						var $items = $j(ui.item).parent('.columns').children('div');
						$items.removeClass('first');
						
						if ($items.eq(0).hasClass('ui-sortable-helper'))
						{
							$items.eq(1).addClass('first');
						} else
						{
							$items.eq(0).addClass('first');
						}
					}
				}).disableSelection();
			}
		}
	}).live('mouseleave', function(e)
	{
		$j(this).find('.drag').remove();
	});

	$j('#columns-wrapper > .columns').live('mousemove', function(e)
	{

	});
});
