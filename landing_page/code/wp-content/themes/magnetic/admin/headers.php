<?php
	function category_slides_cmp($a, $b)
	{
		return ($a[ether::config('prefix').'category_slides_order'] < $b[ether::config('prefix').'category_slides_order'] ? -1 : 1);
	}

	class ether_panel_headers extends ether_panel
	{
		public static function init()
		{
			
		}
		
		public static function reset()
		{
			$categories = get_terms(array('category', 'news_category'), array('orderby' => 'term_group'));
			$category_list = array();
			
			if (is_array($categories))
			{
				$count = count($categories);
				
				for ($i = 0; $i < $count; $i++)
				{
					if ( ! empty($categories[$i]->name))
					{
						ether::option('category_'.$categories[$i]->term_id.'_slides', '');
						ether::option('category_'.$categories[$i]->term_id.'_header_options', '');
					}
				}
			}
		}
		
		public static function header()
		{
			echo '<script type="text/javascript" src="'.ether::path('ether/media/scripts/tooltip.js', TRUE).'"></script>
			<script type="text/javascript">
				$j = jQuery.noConflict();
				
				$j( function()
				{
					$j(\'.headers .has-tooltip\').tooltip
					({
						tooltip: \'.tooltip\',
						x: -23,
						y: -15,
						offset: true,
						sub_width: false,
						sub_height: true,
						standalone: false
					});

					$j(\'fieldset.ether.options\').hide();
					
					var header = $j(\'select[name=ether_category_header]\')[0].selectedIndex;
					
					if (header > 0)
					{
						$j(\'fieldset.ether.options\').eq(header - 1).show();
					}
					
					if (header < 1 || header > 4)
					{
						$j(\'fieldset.ether.headers\').hide();
					}
					
					$j(\'select[name=ether_category_header]\').change( function()
					{
						var header = $j(this)[0].selectedIndex;
						$j(\'fieldset.ether.options\').hide();
						
						if (header > 0)
						{
							$j(\'fieldset.ether.options\').eq(header - 1).show();
						}
						
						if (header > 0 && header < 5)
						{
							$j(\'fieldset.ether.headers\').show();
						} else
						{
							$j(\'fieldset.ether.headers\').hide();
						}
					});
					
					$j(\'select[name=ether_category]\').change( function()
					{
						url = window.location.href;
						
						if ($_GET[\'category\'])
						{
							url = url.split(\'&category=\');
							url = url[0];
						}
						
						if ($_GET[\'category\'] != $j(this).val())
						{
							window.location = url + \'&category=\' + $j(this).val();
						}
					});

					$j(\'.headers .add_slide\').click( function()
					{
						ether.custom_insert = true;
						tb_show(\'Add slide\', ether.path + \'modules/modalbox/slide.php?ether=true&TB_iframe=true\');
						
						return false;
					});
					
					$j(\'.headers .remove_slide\').live(\'click\', function()
					{
						$j(this).parents(\'li\').remove();
						
						return false;
					});
					
					window._send_to_editor = window.send_to_editor;
					
					window.send_to_editor = function(data)
					{
						if (ether.custom_insert != null)
						{
							if (data.length > 0)
							{
								var index = -1;
								
								$j(\'.headers input.slider-image-index\').each( function()
								{
									if (parseInt($j(this).val()) > index)
									{
										index = parseInt($j(this).val());
									}
								});
								
								index += 1;

								for (var i = 0; i < data.length; i++)
								{
									if ($j(\'input[name^="ether_category_slides_id"][value=\' + data[i].id + \']\').length == 0)
									{
										$j(\'.headers ul.slides\').append(\'\' + 
										\'<li>\' +
											\'<img src="\' + ether.path + \'ether/media/images/dragndrop-button.png" alt="Drag and Drop" class="dragndrop" />\' +
											\'<input type="text" name="ether_category_slides_order[\' + (index + i) + \']" class="slider-image-index" value="" />\' +
											\'<input type="text" name="ether_category_slides_id[\' + (index + i) + \']" class="hidden" value="\' + data[i].id + \'" style="display: none;" />\' +
											\'<input type="text" name="ether_category_slides_title[\' + (index + i) + \']" class="hidden" value="\' + data[i].title + \'" style="display: none;" />\' +
											\'<div class="title has-tooltip">\' +
												\'<a href="#">\' + data[i].title + \'</a> <a href="#remove" class="remove_slide"><img src="\' + ether.path + \'ether/media/images/remove.png" alt="Remove" /></a>\' +
												\'<span class="tooltip"><img src="\' + data[i].thumbnail + \'" width="220" height="165" /></span>\' +	
											\'</div>\' +
										\'</li>\');
									}
								}
							}
							
							var $list = $j(\'.headers ul.slides > li\');
			
							$list.each( function()
							{
								index = $list.index(this);
									
								$j(this).children(\'input.slider-image-index\').val(index);
							});
							
							$list.children(\'input.slider-image-index\').trigger(\'change\');
							tb_remove();
						} else
						{
							window._send_to_editor(data);
						}
					};
				});
			</script>';
		}
		
		public static function body()
		{	
			$body = '';
			$categories = get_terms(array('category', 'news_category'), array('orderby' => 'term_group'));
			
			$category_list = array();
			$count = count($categories);

			for ($i = 0; $i < $count; $i++)
			{
				if ( ! empty($categories[$i]->name))
				{
					$category_list[$categories[$i]->term_id] = array
					(
						'name' => ($categories[$i]->taxonomy == 'category' ? 'Posts': 'News').' - '.$categories[$i]->name,
						'value' => $categories[$i]->term_id
					);
				}
			}
			
			$category_options = array();
			$category_slides = array();

			if ( ! isset($_GET['category']) OR empty($_GET['category']))
			{
				$category = current($category_list);

				$_GET['category'] = $category['value'];
			}

			if (isset($_POST['save']))
			{
				$category_slides = array();
				$prefix = ether::config('prefix').'category_slides_';
				
				foreach ($_POST[$prefix.'order'] as $k => $v)
				{
					$category_slides[$k] = array
					(
						$prefix.'order' => $_POST[$prefix.'order'][$k],
						$prefix.'id' => $_POST[$prefix.'id'][$k],
						$prefix.'title' => $_POST[$prefix.'title'][$k]
					);
				}

				ether::option('category_'.$_POST[ether::config('prefix').'category'].'_slides', serialize($category_slides));
	
				$category_options = array();
				
				foreach (ether_header::$header_fields[$_POST[ether::config('prefix').'category_header']] as $option)
				{
					$category_options[ether::config('prefix').$option] = htmlspecialchars(stripslashes($_POST[ether::config('prefix').$option]));
				}
				
				$category_options[ether::config('prefix').'category_header'] = $_POST[ether::config('prefix').'category_header'];
				
				ether::option('category_'.$_POST[ether::config('prefix').'category'].'_header_options', serialize($category_options));

				usort($category_slides, 'category_slides_cmp');
			} else
			{
				$category_slides = unserialize(ether::option('category_'.$_GET['category'].'_slides'));

				if ( ! is_array($category_slides))
				{
					$category_slides = array();
				}
				
				$category_options = unserialize(ether::option('category_'.$_GET['category'].'_header_options'));
			}
			
			foreach ($category_options as $k => $v)
			{
				$category_options[$k] = htmlspecialchars_decode($v);
			}
			
			$body .= '<fieldset class="ether">
				<h5>'.ether::langr('Header options').'</h5>		
				<p class="hint"><em>'.ether::langr('Choose category to edit options. Remember. If post or page will have its own options - added in post/page options - this setings will be ovverided.').'</em></p> 
				<ul> 
					<li class="half"> 
						<label class="fixed alt-style-1">'.ether::langr('Choose category').'</label> 
						'.ether::make_field('category', array('type' => 'select', 'options' => $category_list), $_GET['category']).'
					</li> 
					<li class="half"> 
						<label class="fixed alt-style-1">'.ether::langr('Header').'</label> 
						'.ether::make_field('category_header', array('type' => 'select', 'options' => ether_header::$headers), $category_options).'
					</li> 
				</ul>		
			</fieldset>
			
			<fieldset class="ether options">
				<ul>
					<li class="half"><label class="fixed alt-style-1">'.ether::langr('Text align').'</label>'.ether::make_field('slider_align', array('type' => 'select', 'options' => ether_header::$align), $category_options).'</li>
					<li class="half"><label class="fixed alt-style-1">'.ether::langr('AutoPlay').'</label>'.ether::make_field('slider_autoplay', array('type' => 'select', 'options' => ether_header::$autoplay), $category_options).'</li>
				</ul>
			</fieldset>
			
			<fieldset class="ether options">
				<ul>
					<li class="half"><label class="fixed alt-style-1">'.ether::langr('Effect').'</label>'.ether::make_field('nivo_effect', array('type' => 'select', 'options' => ether_header::$nivo_effect), $category_options).'</li>
					<li class="half"><label class="fixed alt-style-1">'.ether::langr('AutoPlay').'</label>'.ether::make_field('nivo_autoplay', array('type' => 'select', 'options' => ether_header::$autoplay), $category_options).'</li>
				</ul>
				<ul>
					<li class="half"><label class="fixed alt-style-1">'.ether::langr('Width').'</label>'.ether::make_field('nivo_width', array('type' => 'text', 'value' => '970', 'use_default' => TRUE), $category_options).'</li>
					<li class="half"><label class="fixed alt-style-1">'.ether::langr('Height').'</label>'.ether::make_field('nivo_height', array('type' => 'text', 'value' => '270', 'use_default' => TRUE), $category_options).'</li>
				</ul>
			</fieldset>
			
			<fieldset class="ether options">
				<ul>
					<li class="half"><label class="fixed alt-style-1">'.ether::langr('AutoPlay').'</label>'.ether::make_field('carousel_autoplay', array('type' => 'select', 'options' => ether_header::$autoplay), $category_options).'</li>
					<li class="half"><label class="fixed alt-style-1">'.ether::langr('Animation').'</label>'.ether::make_field('carousel_animation', array('type' => 'select', 'options' => ether_header::$piecemaker_transition), $category_options).'</li>
				</ul>
				<ul>
					<li class="half"><label class="fixed alt-style-1">'.ether::langr('Width').'</label>'.ether::make_field('carousel_width', array('type' => 'text', 'value' => '620', 'use_default' => TRUE), $category_options).'</li>
				</ul>
			</fieldset>
			
			<fieldset class="ether options">
				<ul>
					<li class="half"><label class="fixed alt-style-1">'.ether::langr('Width').'</label>'.ether::make_field('piecemaker_width', array('type' => 'text', 'value' => '900', 'use_default' => TRUE), $category_options).'</li>
					<li class="half"><label class="fixed alt-style-1">'.ether::langr('Height').'</label>'.ether::make_field('piecemaker_height', array('type' => 'text', 'value' => '360', 'use_default' => TRUE), $category_options).'</li>
				</ul>
				<ul>
					<li class="half"><label class="fixed alt-style-1">'.ether::langr('Pieces').'</label>'.ether::make_field('piecemaker_pieces', array('type' => 'text', 'value' => '9', 'use_default' => TRUE), $category_options).'</li>
					<li class="half"><label class="fixed alt-style-1">'.ether::langr('Time').'</label>'.ether::make_field('piecemaker_time', array('type' => 'text', 'value' => '1.2', 'use_default' => TRUE), $category_options).'</li>
				</ul>
				<ul>
					<li class="half"><label class="fixed alt-style-1">'.ether::langr('Transition').'</label>'.ether::make_field('piecemaker_transition', array('type' => 'select', 'options' => ether_header::$piecemaker_transition), $category_options).'</li>
					<li class="half"><label class="fixed alt-style-1">'.ether::langr('Delay').'</label>'.ether::make_field('piecemaker_delay', array('type' => 'text', 'value' => '0.1', 'use_default' => TRUE), $category_options).'</li>
				</ul>
				<ul>
					<li class="half"><label class="fixed alt-style-1">'.ether::langr('Depth offset').'</label>'.ether::make_field('piecemaker_depth', array('type' => 'text', 'value' => '300', 'use_default' => TRUE), $category_options).'</li>
					<li class="half"><label class="fixed alt-style-1">'.ether::langr('Distance').'</label>'.ether::make_field('piecemaker_distance', array('type' => 'text', 'value' => '30', 'use_default' => TRUE), $category_options).'</li>
				</ul>
				<ul>
					<li class="half"><label class="fixed alt-style-1">'.ether::langr('AutoPlay').'</label>'.ether::make_field('piecemaker_autoplay', array('type' => 'select', 'options' => ether_header::$autoplay), $category_options).'</li>
				</ul>
			</fieldset>
			
			<fieldset class="ether options">
				<ul>
					<li><label class="fixed alt-style-1">'.ether::langr('Flickr ID').'</label>'.ether::make_field('flickr_id', array('type' => 'text', 'value' => ether::option('social_flickr'), 'use_default' => TRUE), $category_options).'</li>
				</ul>
			</fieldset>
			
			<fieldset class="ether options">
				<ul>
					<li><label class="fixed alt-style-1">'.ether::langr('Twitter username').'</label>'.ether::make_field('twitter_username', array('type' => 'text', 'value' => ether::option('social_twitter'), 'use_default' => TRUE), $category_options).'</li>
				</ul>
			</fieldset>
			
			<fieldset class="ether options">
				<ul>
					<li><label class="fixed alt-style-1">'.ether::langr('HTML code').'</label>'.ether::make_field('html_code', array('type' => 'textarea', 'rows' => 10), $category_options).'</li>
				</ul>
			</fieldset>
	
			<fieldset class="ether headers"> 
				<h5 class="alt-style-1">'.ether::langr('Add slides').'</h5> 
				<ul id="slider" class="slides">
				'; 
				

			$counter = 0;
			foreach ($category_slides as $slide)
			{
				$thumbnail = ether::meta('preview_image', TRUE, $slide[ether::config('prefix').'slides_id']);
				
				if ( ! empty($thumbnail))
				{
					$thumbnail = ether::get_image_thumbnail($thumbnail, 220, 165);
				} else
				{
					$thumbnail = ether::path('media/images/placeholders/placeholder.png', TRUE);
				}

				$body .= '<li>
					<img src="'.ether::path('ether/media/images/dragndrop-button.png', TRUE).'" alt="Drag and Drop" class="dragndrop" />
					'.ether::make_field('category_slides_order['.$counter.']', array('type' => 'text', 'class' => 'slider-image-index', 'value' => 'drag and drop'), $counter).'
					'.ether::make_field('category_slides_id['.$counter.']', array('type' => 'text', 'class' => 'hidden'), $slide[ether::config('prefix').'category_slides_id']).'
					'.ether::make_field('category_slides_title['.$counter.']', array('type' => 'text', 'class' => 'hidden'), $slide[ether::config('prefix').'category_slides_title']).'
					<div class="title has-tooltip">
						<a href="#">'.$slide[ether::config('prefix').'category_slides_title'].'</a> <a href="#remove" class="remove_slide"><img src="'.ether::path('ether/media/images/remove.png', TRUE).'" alt="Remove" /></a>
						<span class="tooltip"><img src="'.$thumbnail.'" width="220" height="165" /></span>
					</div>
				</li>';
				
				$counter++;
			}

			$body .='</ul>
				<ul>
					<li> 
						<input class="add-image add_slide" type="button" value="'.ether::langr('Add image').'" /> 
					</li> 
				</ul> 
			</fieldset>';
		
			return $body;
		}
	}
?>
