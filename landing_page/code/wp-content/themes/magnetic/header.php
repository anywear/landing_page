<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes('xhtml'); ?>>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>
<?php ether::title(); ?>
</title>
<script type="text/javascript">document.getElementsByTagName('html')[0].className += ' js';</script>
<link rel="stylesheet" type="text/css" href="<?php ether::path('media/stylesheets/nivo-slider.css'); ?>" />
<link rel="stylesheet" type="text/css" href="<?php ether::path('media/stylesheets/style.css'); ?>" />
<link rel="stylesheet" type="text/css" href="<?php ether::path('media/stylesheets/easycolumn.css'); ?>" />


<!--[if IE 7]><link href="<?php ether::path('media/stylesheets/style-ie7.css'); ?>" rel="stylesheet" type="text/css" /><![endif]-->
<!--[if IE 8]><link href="<?php ether::path('media/stylesheets/style-ie8.css'); ?>" rel="stylesheet" type="text/css" /><![endif]-->
<link rel="stylesheet" type="text/css" href="<?php ether::path('media/scripts/libs/shadowbox/shadowbox.css'); ?>" />

<?php ether::script('jquery', 'media/scripts/libs/jquery.js'); ?>
<?php
			// for development
			/*ether::script('shadowbox', 'media/scripts/libs/shadowbox/shadowbox.js', array('jquery'));
			ether::script('jquery.superfish', 'media/scripts/libs/jquery.superfish.js', array('jquery')); 
			ether::script('jquery.nivo.slider', 'media/scripts/libs/jquery.nivo.slider.js', array('jquery'));
			ether::script('jquery.quicksand', 'media/scripts/libs/jquery.quicksand.js', array('jquery')); 
			ether::script('jquery.roundabout', 'media/scripts/libs/jquery.roundabout.js', array('jquery'));*/
			
			// for production
			ether::script('jquery.libs', 'media/scripts/libs/jquery.libs.js', array('jquery'));

			// for development
			/*ether::script('ether.placeholder', 'media/scripts/placeholder.js', array('jquery')); 
			ether::script('ether.slider', 'media/scripts/slider.js', array('jquery')); 
			ether::script('ether.tabs', 'media/scripts/tabs.js', array('jquery'));
			ether::script('ether.imgload', 'media/scripts/imgload.js', array('jquery')); 
			ether::script('ether.load', 'media/scripts/load.js', array('jquery')); 
			ether::script('ether.tooltip', 'media/scripts/tooltip.js', array('jquery'));*/
			
			// for production
			ether::script('ether.base', 'media/scripts/base.js', array('jquery'));
			ether::script('ether.config', 'media/scripts/config.js', array('jquery')); 
			ether::script('ether.common', 'media/scripts/common.js', array('jquery', 'jquery.libs', 'ether.base', 'ether.config'));
		?>
<?php ether_theme::custom_style(); ?>
<?php
		
		$use_image_logo = (ether::option('use_image_logo') == 'on' ? TRUE : FALSE);
		$use_favicon = (ether::option('use_favicon') == 'on' ? TRUE : FALSE);
		
		if ($use_favicon)
		{
			$favicon_url = ether::option('favicon');
			$favicon_ext = '';
	
			if ( ! empty($favicon_url))
			{
				$favicon_ext = array_pop(explode('.', $favicon_url));
			}
		}
		
		if ($use_image_logo) { ?>
<style type="text/css">
<?php $image_logo = ether::option('image_logo');
 $image_size = array();
 if ( ! empty($image_logo)) {
 $image_logo = ether::get_image_base($image_logo);
 $image_size = ether::get_image_size($image_logo);
 ?> #header h1 a {
background-image: url(<?php echo $image_logo;
?>);
width: <?php echo $image_size['width'];
?>px;
height: <?php echo $image_size['height'];
?>px;
}
 <?php
}
 ?>
</style>
<?php }
		if ($use_favicon AND ! empty($favicon_url)) { ?>
<link rel="icon" type="image/<?php echo $favicon_ext; ?>" href="<?php echo $favicon_url; ?>">
<?php } ?>
<?php if (ether::option('enable_switcher') == 'on') { ?>
<?php ether::script('ether.switcher', 'switcher/switcher.js', array('jquery')); ?>
<?php } ?>
<?php ether::header(); ?>
<script type="text/javascript">
			config.placeholder =
			{
				name: '<?php ether::lang('Name'); ?>...',
				author: '<?php ether::lang('Author'); ?>...',
				email: '<?php ether::lang('Email'); ?>...',
				message: '<?php ether::lang('Message'); ?>...',
				search: '<?php ether::lang('Search'); ?>...',
				s: '<?php ether::lang('Search'); ?>...',
				comment: '<?php ether::lang('Comment'); ?>...',
				nick: '<?php ether::lang('Name'); ?>...'
			};
			
			config.images_dir = '<?php ether::path('media/images'); ?>/';
		</script>
</head>
<body <?php body_class(); ?>>
<div class="top_bg">
	<div class="fix_width">
		<div id="header">
			<div class="header_right">
				<div class="telephont_header">
								
					<div class="tele_title">USA Direct No :</div>
					<div class="telephone_no_top">1 - (415) - 513 - 4480</div>					
					<div class="tele_title">India Direct No : </div>
					<div class="telephone_no_top">91 - (079) - 65237177</div>
					<div class="tele_title">Phone :</div>
					<div class="telephone_no_top">(+91) - 997 - 476 - 7177</div>		
					
				</div>
				<div class="social_icon_top">
					<ul class="tt-wrapper">
						<li><a class="tt-facebook" href="#"><span>Facebook</span></a></li>
						<li><a class="tt-gplus" href="#"><span>Google Plus</span></a></li>
						<li><a class="tt-twitter" href="#"><span>Twitter</span></a></li>
						<li><a class="tt-linkedin" href="#"><span>LinkedIn</span></a></li>
					</ul>
				</div>
				<div id="nav">
					<ul>
						<?php ether::navigation('sort_column=menu_order&depth=5', 'header', FALSE); ?>
					</ul>
				</div>
			</div>
			<div class="logo"><a href="http://192.168.1.12/rubyprogrammersindia_new/"><img src="<?php ether::path('media/images'); ?>/logo.png" alt="Ruby Programmers India" title="Ruby Programmers India" /></a></div>
		</div>
	</div>
	<div class="clear"></div>
</div>
