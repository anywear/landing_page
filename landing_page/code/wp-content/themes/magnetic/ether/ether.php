<?php
/**
 * Wordpress Ether Framework
 * 
 * License
 * Copyright (c) 2010-2011, Por Design. All rights reserved.
 *
 * @author Por Design <contact@pordesign.eu>
 * @version 1.2.0
 *
 * Licensed for commercial use for Magnetic Premium Wordpress Theme
 */

defined('ETHER_VERSION') OR define('ETHER_VERSION', '1.2.0');
defined('ETHER_ROOT') OR define('ETHER_ROOT', dirname(__FILE__).'/');
defined('ETHER_THEME_ROOT') OR define('ETHER_THEME_ROOT', dirname(dirname(__FILE__)).'/');
defined('ETHER_WP_ROOT') OR define('ETHER_WP_ROOT', dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/');

if (basename(__FILE__) == basename($_SERVER['SCRIPT_FILENAME']) AND ( ! defined('ABSPATH') OR ! defined('TEMPLATEPATH')))
{
	require_once(ETHER_WP_ROOT.'wp-load.php');
	
	if ( ! empty($_GET))
	{
		ether::trigger('ether.get', array($_GET));
	}
	
	if ( ! empty($_POST))
	{
		ether::trigger('ether.post', array($_POST));
	}
	
	if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')
	{
		ether::trigger('ether.ajax', array($_GET));
	}
	
	exit;
}

if ( ! class_exists('ether'))
{
	class ether
	{
		private static $config;
		private static $locale;
		private static $bc_links = 0;
		private static $log_buffer;
			
		protected static $title;
		protected static $menu_title;
		protected static $slug;
		protected static $permission;
		protected static $controller;
		protected static $panels = array();
		protected static $modules = array();
		protected static $panel = '';
		protected static $metaboxes = array();
		protected static $metabox_permissions = array();
		protected static $custom_post_types = array();
		protected static $bindings = array();
		protected static $shortcodes = array();
		protected static $quicktags = array();
		protected static $wysiwig = array();
		protected static $csv = array();

		public static function init()
		{
			self::$config = array
			(
				'theme_name' => 'Ether Theme',
				'theme_version' => '1.0',
				'ext' => 'php',
				'dir' => basename(dirname(dirname(__FILE__))),
				'prefix' => 'ether_',
				'sidebar_title' => '<h3>%</h3>',
				'sidebar_container' => '<div class="section">%</div>',
				'more_link' => TRUE,
				'register_sidebar' => TRUE,
				'register_menu' => TRUE,
				'post_autop' => TRUE,
				'page_autop' => TRUE,
				'image_autop' => FALSE,
				'post_texturize' => TRUE,
				'page_texturize' => TRUE,
				'hide_custom_fields' => FALSE,
				'theme_activate_callback' => NULL,
				'theme_deactivate_callback' => NULL,
				'upload_dir' => 'ether',
				'upload_url' => '',
				'image_editor_filter' => array('ether', 'image_editor_filter'),
				'debug' => FALSE,
				'debug_wordpress' => FALSE,
				'excluded_categories' => array(),
				'search_custom_post' => TRUE,
				'pagination' => TRUE,
				'thumb_prefix' => '-thumb',
				'cache_lifetime' => 43200,
				'cache' => FALSE
			);

			self::$locale = array
			(
				'pagination_first' => '&laquo;&laquo; '.__('First'),
				'pagination_last' => __('Last').' &raquo;&raquo;',
				'pagination_prev' => '&laquo; '.__('Previous'),
				'pagination_next' => __('Next').' &raquo;',
				'search' => __('Search'),
				'author' => __('Author'),
				'tag' => __('Tag'),
				'blog_sidebar' => __('Blog'),
				'post_sidebar' => __('Post'),
				'page_sidebar' => __('Page'),
				'menu_header' => __('Header'),
				'menu_footer' => __('Footer')
			);
			
			self::$csv = array
			(
				'terminated' => "\n",
				'separator' => ',',
				'enclosed' => '"',
				'escaped' => '\\'
			);
			
			set_error_handler(array('ether', 'error_handler'));
			
			add_action('wp_footer', array('ether', 'log_footer'));
			add_action('admin_footer', array('ether', 'log_footer'));
			add_action('wp_head', array('ether', 'log_header'));
			add_action('admin_head', array('ether', 'log_header'));
			add_action('init', array('ether', 'cache_begin'));
			add_action('shutdown', array('ether', 'cache_end'));
			
			self::bind('ether.get', array('ether', 'wysiwig_script'));
		}
		
		public static function cache_begin()
		{

		}
		
		public static function cache_end()
		{

		}
		
		public static function log_header()
		{
			if (self::config('debug'))
			{
				$body = '';
				
				echo $body;
			}
		}
		
		public static function log_footer()
		{
			if (self::config('debug'))
			{
				$body = '';
				
				$body .= '<script type="text/javascript">';

				foreach (self::$log_buffer as $log_line)
				{
					$body .= 'console.log(\''.addslashes($log_line['message']).'\', \''.$log_line['type'].'\', \''.$log_line['file'].'\', \''.$log_line['line'].'\');';
				}
				
				$body .= '</script>';
				
				echo $body;
			}
		}
		
		public static function log($message, $type = 'debug', $file = NULL, $line = NULL)
		{
			if (self::config('debug'))
			{
				$trace = debug_backtrace();
				
				if ($file == NULL)
				{
					$file = $trace[0]['file'];
				}
				
				if ($line == NULL)
				{
					$line = $trace[0]['line'];
				}
				
				self::$log_buffer[] = array('type' => $type, 'message' => $message, 'file' => $file, 'line' => $line);
			}
		}
		
		public static function dump($key, $value = NULL)
		{
			if (self::config('debug'))
			{
				$trace = debug_backtrace();
				$file = $trace[0]['file'];
				$line = $trace[0]['line'];
				
				$dump = '<pre>'.str_replace("\n", '<br />', var_export(($value !== NULL ? $value : $key), TRUE)).'</pre>';
				
				self::log('Dump'.(is_string($key) ? ' '.$key : '').' '.$dump, 'dump', $file, $line);
			}
		}
		
		public static function error_handler($code, $message, $file, $line)
		{
			$type = array
			(
				1 => 'error',
				2 => 'warning',
				4 => 'error',
				8 => 'warning',
				16 => 'error',
				32 => 'warning',
				64 => 'error',
				128 => 'warning',
				256 => 'error',
				512 => 'warning',
				1024 => 'warning',
				2048 => 'error'
			);
			
			if ( ! self::config('debug_wordpress'))
			{
				$theme_dir = dirname(dirname(__FILE__));

				if (substr($file, 0, strlen($theme_dir)) != $theme_dir)
				{
					return;
				}
			}
			
			self::log($message, $type[$code], $file, $line);
		}

		public static function bind($event, $callback)
		{
			if ( ! isset(self::$bindings[$event]))
			{
				self::$bindings[$event] = array();
			}
			
			self::$bindings[$event][] = $callback;
		}

		public static function trigger($event, $args = array())
		{
			if (isset(self::$bindings[$event]))
			{
				foreach (self::$bindings[$event] as $callback)
				{
					call_user_func_array($callback, $args);
				}
			}
		}
		
		public static function begin()
		{
			if (self::config('debug'))
			{
				error_reporting(E_ALL);
			} else
			{
				error_reporting(0);
			}
			
			$uploads = wp_upload_dir();

			if ($uploads['error'] !== FALSE)
			{
				wp_die($uploads['error']);
			}

			$upload_dir = $uploads['basedir'];
			
			if (is_writable($uploads['basedir']))
			{
				$upload_dir .= '/'.self::config('upload_dir');
				if (file_exists($upload_dir))
				{
					if ( ! is_writable($upload_dir))
					{
						wp_die('Make sure that "'.$upload_dir.'" is writable.');
					}
				} else
				{
					mkdir($upload_dir);
				}
			} else
			{
				wp_die('Make sure that "'.$uploads['basedir'].'" is writable.');
			}
			
			self::config('upload_url', $uploads['baseurl'].'/'.self::config('upload_dir'));
			self::config('upload_dir', $upload_dir);
			
			load_theme_textdomain(trim(self::config('prefix'), '_'), self::path('locale', TRUE));

			add_action('login_head', array('ether', 'admin_header'));
			
			remove_action('wp_head', 'wp_generator');
			remove_action('wp_head', 'rsd_link');
			remove_action('wp_head', 'wlwmanifest_link');

			if (self::config('register_sidebar') AND function_exists('register_sidebar'))
			{
				$sidebars = array(self::$locale['blog_sidebar'], self::$locale['post_sidebar'], self::$locale['page_sidebar']);
				
				$sidebar_title = explode('%', self::config('sidebar_title'));
				$sidebar_container = explode('%', self::config('sidebar_container'));

				foreach ($sidebars as $sidebar)
				{
					register_sidebar( array
					(
						'name' => $sidebar,
						'before_widget' => $sidebar_container[0],
						'after_widget' => $sidebar_container[1],
						'before_title' => $sidebar_title[0],
						'after_title' => $sidebar_title[1]
					));
				}
			}
			
			if (self::config('more_link'))
			{
				add_filter('the_content_more_link', array('ether', 'more_link'), 10, 2);
			}
			
			if (self::config('register_menu') AND function_exists('register_nav_menu'))
			{
				register_nav_menu('header', self::$locale['menu_header']);
				register_nav_menu('footer', self::$locale['menu_footer']);
			}
			
			if (function_exists('wp_register_sidebar_widget'))
			{
				wp_register_sidebar_widget(self::$locale['search'], array('ether', 'widget_search'));
			}

			add_filter('the_content', 'shortcode_unautop');
			add_filter('disable_captions', array('ether', 'captions'));
			add_filter('the_content', array('ether', 'formatter'), 99);
			add_filter('tiny_mce_version', array('ether', 'wysiwig_tinymce_version'));
			add_filter('image_send_to_editor', array('ether', 'image_send_to_editor'), 10, 8);
			add_filter('media_send_to_editor', array('ether', 'media_send_to_editor'), 10, 3);
			add_filter('media_upload_form_url', array('ether', 'media_upload_form_url'), 10, 2);
			add_filter('media_upload_panels', array('ether', 'media_upload_panels'));
			add_filter('widget_text', 'do_shortcode');
			add_filter('widget_text', 'shortcode_unautop');
			
			remove_filter('the_content', 'wpautop');
			remove_filter('the_content', 'wptexturize');
			
			add_action('wp_dashboard_setup', array('ether', 'dashboard_widget_setup'));
			add_action('plugins_loaded', create_function('', 'global $ether_wysiwig; $ether_wysiwig = new ether_wysiwig();'));
			add_action('edit_form_advanced', array('ether', 'quicktags_init'));
			add_action('edit_page_form', array('ether', 'quicktags_init'));
			add_action('init', array('ether', 'custom_post_init'));
			add_action('init', array('ether', 'wysiwig_init'));
			add_action('template_redirect', array('ether', 'custom_post_template_redirect'));
			add_action('save_post', array('ether', 'custom_post_template_proccess'));
			add_action('admin_init', array('ether', 'custom_post_template_register'), 10);

			add_action('import_end', array('ether', 'import_end'));
			
			add_action('pre_get_posts', array('ether', 'exclude_categories'));

			if (self::config('hide_custom_fields'))
			{
				add_action('do_meta_boxes', array('ether', 'hide_custom_fields'), 10, 3);  
			}

			add_action('parse_query', array('ether', 'custom_post_pagination_request'));
		}
		
		// fix for custom post type pagination redirecting
		public static function no_canonical($url)
		{
        return FALSE;
		}
		
		public static function custom_post_pagination_request($request)
		{
			if ((isset($request->query_vars['post_type']) AND in_array($request->query_vars['post_type'], array_keys(self::$custom_post_types))) AND $request->is_singular === TRUE AND $request->current_post == -1 AND $request->is_paged === TRUE)
			{
				add_filter('redirect_canonical', array('ether', 'no_canonical'));
			}
			
			return $request;
		}
		
		public static function login_url()
		{
			echo home_url();
		}
		
		public static function register_quicktag($name, $tag_open, $tag_close)
		{
			self::$quicktags[] = array('name' => $name, 'tag_open' => $tag_open, 'tag_close' => $tag_close);
		}
		
		public static function register_wysiwig_element($name)
		{
			if ($name == 'separator' OR $name == '|')
			{
				self::$wysiwig[] = array('type' => 'separator', 'name' => 'separator');
			}
		}
		
		public static function register_wysiwig_modalbox($name, $desc, $icon, $url, $width = 500, $height = 300)
		{
			self::$wysiwig[] = array('type' => 'modalbox', 'name' => $name, 'desc' => $desc, 'icon' => $icon, 'url' => $url, 'width' => $width, 'height' => $height);
		}
		
		public static function register_wysiwig_shortcode($name, $desc, $icon, $tag_open, $tag_close)
		{
			self::$wysiwig[] = array('type' => 'shortcode', 'name' => $name, 'desc' => $desc, 'icon' => $icon, 'tag_open' => $tag_open, 'tag_close' => $tag_close);
		}
		
		public static function quicktags_init()
		{
			$output = '<script type="text/javascript">';
			$output .= 'var quicktags_length = edButtons.length - 1;';
			$output .= 'jQuery( function() {';
			$output .= 'jQuery(\'#ed_toolbar\').append(\'<br />\');';

			$counter = 1;
			
			foreach (self::$quicktags as $quicktag)
			{
				$output .= 'edButtons[edButtons.length] = new edButton(\'ether_ed_'.self::slug($quicktag['name']).'\', \''.$quicktag['name'].'\', \''.$quicktag['tag_open'].'\', \''.$quicktag['tag_close'].'\');';
				$output .= 'jQuery(\'#ed_toolbar\').append(\'<input type="button" id="ether_ed_'.self::slug($quicktag['name']).'" class="ed_button" onclick="edInsertTag(edCanvas, quicktags_length + '.$counter.');" value="'.$quicktag['name'].'" />\');';
				$counter++;
			}
			
			$output .= '});';
			$output .= '</script>';
			
			echo $output;
		}

		public static function dashboard_widget_setup()
		{
			global $wp_meta_boxes;
			 
			wp_add_dashboard_widget('ether', self::langr('Ether Board'), array('ether', 'dashboard_widget'));
			$dashboard_widgets = $wp_meta_boxes['dashboard']['normal']['core'];
			$ether_widget = array('ether' => $dashboard_widgets['ether']);
			unset($dashboard_widgets['ether']);
			$wp_meta_boxes['dashboard']['normal']['core'] = array_merge($ether_widget, $dashboard_widgets);
		}
		
		public static function dashboard_widget()
		{
			$body = '';
			$body .= '<script type="text/javascript">
				jQuery( function()
				{
					jQuery(\'div.ether-content\').hide();
					
					var script = jQuery(\'<script>\').load( function()
					{
						jQuery(this).remove();
					}).attr(\'src\', \'http://ether.pordesign.eu/api.php?callback=ether_init&theme_name='.urlencode(self::config('theme_name')).'&theme_version='.urlencode(self::config('theme_version')).'&url='.urlencode(self::info('url', TRUE)).'\');
						jQuery(\'body\').append(script);
				});
				
				function ether_init(data)
				{
					jQuery(\'div.ether-content\').html(data[\'content\']).slideDown();
				}
			</script>';
			
			$body .= '<div class="ether-content"><p>Loading...</p></div>';
			
			echo $body;
		}
		
		public static function register_shortcode($name, $callback, $fields = array(), $group = 'Shortcodes')
		{
			if ( ! isset(self::$shortcodes[$group]))
			{
				self::$shortcodes[$group] = array();
			}
			
			self::$shortcodes[$group][$name] = $fields;
			add_shortcode($name, $callback);
		}
		
		public static function register_post($name, $args)
		{
			if ( ! isset($args['labels']) OR empty($args['labels']))
			{
				$args['labels'] = array
				(
					'name' => $name,
					'singular_name' => $name,
					'add_new' => 'Add new',
					'add_new_item' => 'Add new '.$name,
					'edit_item' => 'Edit '.$name,
					'new_item' => 'New '.$name,
					'view_item' => 'View '.$name,
					'search_items' => 'Search '.$name,
					'not_found' =>  'No '.strtolower($name).' found',
					'not_found_in_trash' => 'No '.strtolower($name).' found in Trash', 
					'parent_item_colon' => '',
					'menu_name' => $name
				);
			}
			
			self::$custom_post_types[self::slug($name)] = $args;
		}
		
		public static function post_types()
		{
			return array_keys(self::$custom_post_types);
		}
		
		public static function custom_post_init()
		{
			foreach (self::$custom_post_types as $type => $args)
			{			
				if ( ! is_array($args))
				{
					parse_str($args, $args);
				}

				$defaults = array
				(
					'public' => TRUE,
					'publicly_queryable' => TRUE,
					'show_ui' => TRUE,
					'query_var' => TRUE,
					'rewrite' => TRUE,
					'capability_type' => 'post',
					'has_archive' => TRUE, 
					'hierarchical' => TRUE,
					'menu_position' => NULL,
					'supports' => array('title', 'editor', 'page-attributes', 'author'),
					'can_export' => TRUE,
					'rewrite' => array
					(
						'slug' => $type,
						'with_front' => TRUE
					)
				);
				
				$args = array_merge($defaults, $args);

				register_post_type($type, $args);
			}
		}
		
		public static function custom_post_template_redirect()
		{
			if ( ! empty(self::$custom_post_types))
			{
				global $wp_query;
				
				$id = $wp_query->get_queried_object_id();
				
				$template = get_post_meta($id, '_wp_page_template', TRUE);
				
				if ($template AND 'default' !== $template)
				{
					$file = STYLESHEETPATH.'/'.$template;
					
					if (is_file($file))
					{
						require_once $file;
						exit;
					}
				}
			}
		}
			
		public static function custom_post_template_proccess()
		{
			if ( ! empty(self::$custom_post_types))
			{
				global $post;
				
				$clean_id = (isset($_POST['ID']) ? intval($_POST['ID']) : 0);
			
				if ( ! empty($_POST['page_template']) AND array_key_exists($post->post_type, self::$custom_post_types))
				{
					$page_templates = get_page_templates();
					
					if ($page_template != 'default' AND ! in_array($_POST['page_template'], $page_templates))
					{
						if ($wp_error)
						{
							return new WP_Error('invalid_page_template', self::langr('The page template is invalid.'));
						} else
						{
							return 0;
						}
					}
						
					update_post_meta($clean_id, '_wp_page_template', $_POST['page_template']);
				}
			}
		}
			
		public static function custom_post_template_register()
		{
			foreach (self::$custom_post_types as $type => $args)
			{
				if (isset($args['custom_template']) AND $args['custom_template'])
				{
					add_meta_box('custom-template', 'Template', array('ether', 'custom_post_template_metabox'), $type, 'side', 'low');
				}
			}
		}
		
		public static function custom_post_template_metabox()
		{
			global $post;
			
			$post_type_object = get_post_type_object($post->post_type);
			
			if (count(get_page_templates()) > 0)
			{
				$template = get_post_meta($post->ID, '_wp_page_template', TRUE);
				echo '<p><strong>'.self::langr('Template').'</strong></p><label class="screen-reader-text" for="page_template">'.self::langr('Page Template').'</label><select name="page_template" id="page_template"><option value="default">'.self::langr('Default Template').'</option>';
				page_template_dropdown($template);
				echo '</select>';
			}
		}
		
		public static function register_sidebar($name, $options = array())
		{
			$title = explode('%', isset($options['title']) ? $options['title'] : self::config('sidebar_title'));
			$container = explode('%', isset($options['container']) ? $options['container'] : self::config('sidebar_container'));

			register_sidebar( array
			(
				'name' => $name,
				'before_widget' => isset($container[0]) ? $container[0] : '',
				'after_widget' => isset($container[1]) ? $container[1] : '',
				'before_title' => isset($title[0]) ? $title[0] : '',
				'after_title' => isset($title[1]) ? $title[1] : ''
			));
		}
		
		public static function sidebar($name = NULL)
		{
			if (self::config('register_sidebar') AND $name == NULL)
			{
				if (is_page())
				{
					$name = self::$locale['page_sidebar'];
				} else if (is_single())
				{
					$name = self::$locale['post_sidebar'];
				} else
				{
					$name = self::$locale['blog_sidebar'];
				}
				
				return dynamic_sidebar($name);
			} else
			{
				return dynamic_sidebar($name);
			}
		}
		
		public static function header()
		{
			if (is_singular())
			{
				wp_enqueue_script('comment-reply');
			}
			
			echo '<link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="'.self::rss(TRUE).'" />';
			
			echo '<script type="text/javascript">if (typeof ether == \'undefined\') var ether = {}; ether.path = \''.self::info('url', TRUE).'/wp-content/themes/'.self::config('dir').'/\';</script>';
			
			wp_head();
			
			if (self::option('custom_style') != '')
			{
				echo '<style type="text/css">'.stripslashes(self::option('custom_style')).'</style>';
			}
			
			if (self::option('custom_script') != '')
			{
				echo '<script type="text/javascript">'.stripslashes(self::option('custom_script')).'</script>';
			}
		}
		
		public static function exclude_categories()
		{
			if (is_home() OR is_feed() OR (is_archive() AND ! is_category()))
			{
				$categories = array_unique(self::config('excluded_categories'));
				
				if ( ! empty($categories))
				{
					global $wp_query;
					
					$wp_query->query_vars['cat'] .= '-'.implode(',-', $categories);
				}
			}	
		}
		
		public static function footer()
		{
			wp_footer();
			
			if (self::option('tracking_code') != '')
			{
				echo stripslashes(self::option('tracking_code'));
			}
		}
		
		public static function clean_database($meta = TRUE, $options = TRUE)
		{
			if (self::config('prefix') != '')
			{
				global $wpdb;
				
				$prefix = self::config('prefix');
				$prefix_length = strlen($prefix);
				
				if ($meta)
				{
					$wpdb->query('DELETE FROM `'.$wpdb->postmeta.'` WHERE SUBSTRING(`meta_key`, 1, '.$prefix_length.')=\''.$prefix.'\'');
				}
				
				if ($options)
				{
					$wpdb->query('DELETE FROM `'.$wpdb->options.'` WHERE SUBSTRING(`option_name`, 1, '.$prefix_length.')=\''.$prefix.'\'');
				}
			}
		}
		
		public static function update_url($current_url, $new_url)
		{
			global $wpdb;
			
			$wpdb->query('UPDATE `'.$wpdb->options.'` SET `option_value`=REPLACE(`option_value`, \''.$current_url.'\', \''.$new_url.'\') WHERE `option_name`=\'home\' OR `option_name`=\'siteurl\';');
			$wpdb->query('UPDATE `'.$wpdb->posts.'` SET `guid`=REPLACE(`guid`, \''.$current_url.'\', \''.$new_url.'\');');
			$wpdb->query('UPDATE `'.$wpdb->posts.'` SET `post_content`=REPLACE(`post_content`, \''.$current_url.'\', \''.$new_url.'\');');
		}
		
		public static function end()
		{			
			if ( ! empty($_GET))
			{
				self::trigger('get', array($_GET));
			}
			
			if ( ! empty($_POST))
			{
				self::trigger('post', array($_POST));
			}
			
			if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')
			{
				self::trigger('ajax', array($_GET));
			}

			add_filter('pre_get_posts', array('ether', 'search_filter'));
			
						
			foreach (self::$modules as $module)
			{
				self::import('modules.'.$module);
			}
			
			foreach (self::$modules as $module)
			{
				$module_node = explode('.', $module);
				$module_name = array_pop($module_node);
				$module_class = 'ether_'.str_replace('-', '_', $module_name);
				
				call_user_func_array(array($module_class, 'set_class'), array($module_class));
				self::module_run($module_name.'.__module');
				self::module_run($module_name.'.init');
			}
			
			if (is_admin())
			{
				self::admin_init();
				
				if (basename($_SERVER['SCRIPT_FILENAME']) == 'themes.php')
				{
					if (isset($_GET['activated']) AND $_GET['activated'] == 'true')
					{
						if (self::config('theme_activate_callback') != NULL)
						{
							call_user_func(self::config('theme_activate_callback'));
						}
						
						global $wp_rewrite;
						
						$wp_rewrite->flush_rules();
					}

					if (isset($_GET['action']) AND $_GET['action'] == 'activate' AND isset($_GET['template']) AND $_GET['template'] != self::config('dir'))
					{
						if (self::config('theme_deactivate_callback') != NULL)
						{
							call_user_func(self::config('theme_deactivate_callback'));
						}
					}
				}
				
				if (self::config('cache'))
				{
					if (isset($_POST) AND ! empty($_POST))
					{
						clean_cache();
					}
				}
			}
		}
		
		public static function search_filter($query)
		{
			if ($query->is_search)
			{
				$types = array('post', 'page');
				
				if (self::config('search_custom_post') === TRUE)
				{
					$query->set('post_type', array_merge($types, array_keys(self::$custom_post_types)));
				} else if (is_array(self::config('search_custom_post')))
				{
					$query->set('post_type', array_merge($types, self::config('search_custom_post')));
				}
			}
		}
		
		public static function config($key, $value = NULL)
		{
			if ($value !== NULL OR (is_array($key) AND empty($value)))
			{
				if (is_array($key))
				{
					foreach($key as $k => $v)
					{
						self::config($k, $v);
					}
				} else
				{
					self::$config[$key] = $value;
				}
			} else
			{
				if (isset(self::$config[$key]))
				{
					return self::$config[$key];
				}
				
				return FALSE;
			}
		}
		
		public static function lang()
		{
			$args = func_get_args();
			$format = array_shift($args);

			vprintf(__($format, trim(self::config('prefix'), '_')), $args);
		}
		
		public static function langr()
		{
			$args = func_get_args();
			$format = array_shift($args);

			return vsprintf(__($format, trim(self::config('prefix'), '_')), $args);
		}
		
		public static function langx($message, $context, $return = FALSE)
		{
			$output = _x($message, $context, trim(self::config('prefix'), '_'));
			
			if ($return)
			{
				return $output;
			}
			
			echo $output;
		}
		
		public static function langn($singular, $plural, $count, $return = FALSE)
		{
			$output = sprintf(_n($singular, $plural, $count), $count);
			
			if ($return)
			{
				return $output;
			}
			
			echo $output;
		}
		
		public static function locale($key, $value = NULL)
		{
			if ( ! empty($value) OR (is_array($key) AND empty($value)))
			{
				if (is_array($key))
				{
					foreach($key as $k => $v)
					{
						self::locale($k, $v);
					}
				} else
				{
					self::$locale[$key] = $value;
				}
			} else
			{
				if (isset(self::$locale[$key]))
				{
					return self::$locale[$key];
				}
				
				return FALSE;
			}
		}

		public static function import($path)
		{
			$once = FALSE;
			
			if (substr($path, 0, 1) == '!')
			{
				$once = TRUE;
				$path = trim($path, '!');
			}
			
			$path = str_replace('.', trim('/', '.'), $path);
			
			if (file_exists(TEMPLATEPATH.'/'.$path.'.'.self::config('ext')))
			{
				if ($once)
				{
					include_once(TEMPLATEPATH.'/'.$path.'.'.self::config('ext'));
				} else
				{
					include(TEMPLATEPATH.'/'.$path.'.'.self::config('ext'));
				}
			} else if (file_exists(TEMPLATEPATH.'/ether/'.$path.'.'.self::config('ext')))
			{
				if ($once)
				{
					include_once(TEMPLATEPATH.'/ether/'.$path.'.'.self::config('ext'));
				} else
				{
					include(TEMPLATEPATH.'/ether/'.$path.'.'.self::config('ext'));
				}
			} else
			{
				
			}
		}

		public static function clean_cache()
		{
			$path = TEMPLATEPATH.'/ether/cache/';
			$files = array();
			
			$scan_results = array_diff(scandir($path), array('.', '..'));
				
			foreach($scan_results as $result)
			{
				if (is_file($path.$result))
				{
					unlink($path.$result);
				}
			}
		}

		public static function cache($key, $content = NULL)
		{
			$path = dirname(__FILE__).'/cache';

			if ( ! is_writable($path) OR ! self::config('cache'))
			{
				return FALSE;
			}
			
			$file = dirname(__FILE__).'/cache/'.md5($key);
			
			if ($content != NULL)
			{
				file_put_contents($file, serialize($content));
			} else
			{
				if (file_exists($file) AND is_file($file))
				{
					if ((filemtime($file) + self::config('cache_lifetime')) > time())
					{
						return unserialize(file_get_contents($file));
					}
				}
				
				return FALSE;
			}
			
			return TRUE;
		}
		
		public static function formatter($content)
		{
			$new_content = '';
			$pattern_full = '{(\[raw\].*?\[/raw\])}is';
			$pattern_contents = '{\[raw\](.*?)\[/raw\]}is';
			$pieces = preg_split($pattern_full, $content, -1, PREG_SPLIT_DELIM_CAPTURE);

			foreach ($pieces as $piece)
			{
				if (preg_match($pattern_contents, $piece, $matches))
				{
					$new_content .= $matches[1];
				} else
				{
					$piece = self::autop($piece);
					preg_match_all('/(<a.*>)?<img[^>]+>(<\/a>)?/i', $piece, $images);
					
					foreach ($images[0] as $image)
					{
						preg_match('/a (.*?)class="(.*?)(frame-holder)[\ |"]/i', $image, $a_frame);
						preg_match('/a (.*?)class="(.*?)(align.*?)[\ |"](.*?)><img/i', $image, $a_align);
						preg_match('/img (.*?)class="(.*?)(align.*?)[\ |"]/i', $image, $img_align);
							
						if (isset($a_frame[3]) AND isset($img_align[3]))
						{
							if (isset($a_align[3]))
							{
								$classes = str_replace($a_align[3], $img_align[3], $a_align[0]);
								$piece = str_replace($a_align[0], $classes, $piece);
							} else
							{
								$classes = $a_frame[0].$img_align[3].(substr($a_frame[0], -1) == ' ' ? ' ' : '');
								$piece = str_replace($a_frame[0], $classes, $piece);
							}
						}	
					}
					
					// disable image wrapping by paragraph
					preg_match_all('/<p>(<a.*>)?<img[^>]+>(<\/a>)?<\/p>/i', $piece, $images);

					if ( ! self::config('image_autop') AND count($images[0]) > 0)
					{
						foreach ($images[0] as $image)
						{
							$piece = str_replace($image, self::strip($image, '<a><img><span>'), $piece);
						}
					}
					
					$new_content .= self::texturize($piece);
				}
			}
			
			$new_content = str_replace(array('<p></p>', '<p>&nbsp;</p>'), array('', ''), $new_content);

			return $new_content;
		}
		
		public static function captions()
		{
			return TRUE;
		}
		
		public static function autop($text)
		{
			global $post;
			
			if (($post->post_type == 'post' AND ! self::config('post_autop')) OR ($post->post_type == 'page' AND ! self::config('page_autop')) OR get_post_meta($post->ID, self::config('prefix').'disable_autop', TRUE) == '1')
			{
				return $text;
			} else
			{
				return wpautop($text);
			}
		}
		
		public static function texturize($text)
		{
			global $post;
			
			if (($post->post_type == 'post' AND ! self::config('post_texturize')) OR ($post->post_type == 'page' AND ! self::config('page_texturize')) OR get_post_meta($post->ID, self::config('prefix').'disable_texturize', TRUE) == '1')
			{
				return $text;
			} else
			{
				return wptexturize($text);
			}
		}
		
		public static function hide_custom_fields($type, $context, $post)
		{
			foreach (array( 'normal', 'advanced', 'side' ) as $context)
			{
				$post_types = array_merge(array('post', 'page'), array_keys(self::$custom_post_types));
				
				foreach ($post_types as $type)
				{
					remove_meta_box('postcustom', $type, $context);
				}
			}
		}
		
		public static function image_editor_filter($html, $id, $caption, $title, $align, $url, $size, $alt)
		{
			$output = '';
			
			preg_match('/(href=[\'"])(.*?)([\'"])/i', $html, $href);
			preg_match('/(src=[\'"])(.*?)([\'"])/i', $html, $src);
			preg_match('/(width=[\'"])(.*?)([\'"])/i', $html, $width);
			preg_match('/(height=[\'"])(.*?)([\'"])/i', $html, $height);
			
			$href = (isset($href[2]) ? $href[2] : NULL);
			$src = (isset($src[2]) ? $src[2] : NULL);
			$width = (isset($width[2]) ? $width[2] : NULL);
			$height = (isset($height[2]) ? $height[2] : NULL);
			
			if ( ! empty($href) AND ! empty($src))
			{
				$output .= '<a href="'.self::shadowbox_href($href).'" class="frame-holder align'.$align.'">';
			}
			
			if ( ! empty($src))
			{
				$output .= '<img src="'.$src.'" '.(empty($href) ? 'class="frame align'.$align.'" ' : '').'alt="'.$alt.'"'.( ! empty($width) ? ' width="'.$width.'"' : '').( ! empty($height) ? ' height="'.$height.'"' : '').' title="'.$title.'" />';
			}

			if ( ! empty($href) AND ! empty($src))
			{
				$output .= '</a>';
			}
			
			if ( ! empty($output))
			{
				return $output;
			}
			
			return $html;
		}
		
		public static function image_send_to_editor($html, $id, $caption, $title, $align, $url, $size, $alt)
		{
			$output = '';

			if (isset($_GET['ether']) AND $_GET['ether'] == 'true')
			{
				if (isset($_GET['width']) AND isset($_GET['height']))
				{
					$uploads = wp_upload_dir();
					
					preg_match('/attachment_id=(\d+)/i', $url, $attachment);
					
					if (count($attachment) > 0)
					{
						$url = wp_get_attachment_url($attachment[1]);
					}
					
					$image = $url;
					$thumbnail = $url;

					preg_match_all('/(src=[\'|"])(.*?)([\'|"])/i', $html, $src);
					
					if (count($src[2]) > 0)
					{
						$thumbnail = $src[2][0];
					}
					
					$thumbnail_path = str_replace($uploads['baseurl'], $uploads['basedir'], $thumbnail);
					$thumbnail_size = getimagesize($thumbnail_path);
					
					$width = explode('_', $_GET['width']);
					$height = explode('_', $_GET['height']);
					$width_count = count($width);
					$height_count = count($height);
					
					if ($width_count == $height_count)
					{
						$thumbnail_url = '';
						
						for ($i = 0; $i < $width_count; $i++)
						{
							if ($i == 0)
							{
								$thumbnail_url = self::get_image_thumbnail($image, $width[$i], $height[$i]);
							} else
							{
								self::get_image_thumbnail($image, $width[$i], $height[$i]);
							}
							
							if ($width[$i] == $thumbnail_size[0] AND $height[$i] == $thumbnail_size[1])
							{
								$thumbnail_url = $thumbnail;
							}
						}
						
						$output = $thumbnail_url;
					} else
					{
						$output = self::get_image_thumbnail($image, $width[0], $height[0]);
					}
				} else
				{
					$output = $url;
				}
				
				if ( ! empty($output))
				{
					if (isset($_GET['output']) AND $_GET['output'] == 'html')
					{
						return '<a href="'.self::shadowbox_href($url).'" class="frame-holder align'.$align.'"><img src="'.$output.'" alt="'.$alt.'" title="'.$title.'" /></a>';
					} else
					{
						return $output;
					}
				}
			}
			
			if (self::config('image_editor_filter') != NULL)
			{
				$html = call_user_func_array(self::config('image_editor_filter'), array($html, $id, $caption, $title, $align, $url, $size, $alt));
			}

			return $html;
		}
		
		public static function media_send_to_editor($html, $id, $attachment)
		{
			$output = '';

			if (isset($_GET['ether']) AND $_GET['ether'] == 'true' AND ( ! isset($_GET['type']) OR $_GET['type'] != 'image'))
			{
				$output = $html;
				
				if ( ! empty($output))
				{
					if (isset($_GET['output']) AND $_GET['output'] == 'html')
					{
						return $output;
					} else
					{
						preg_match_all('/(href=[\'|"])(.*?)([\'|"])/i', $html, $href);
					
						if (count($href[2]) > 0)
						{
							return $href[2][0];
						}
					}
				}
			}
			
			return $html;
		}
		
		public static function media_upload_form_url($url, $type)
		{
			if (isset($_GET['ether']) AND $_GET['ether'] == 'true')
			{
				$url .= '&ether=true';
				
				if (isset($_GET['width']))
				{
					$url .= '&width='.$_GET['width'];
				}
				
				if (isset($_GET['height']))
				{
					$url .= '&height='.$_GET['height'];
				}
				
				if (isset($_GET['output']))
				{
					$url .= '&output='.$_GET['output'];
				}
			}
			
			return $url;
		}
		
		public static function media_upload_panels($panels)
		{
			return array('type' => self::langr('Choose File'), 'library' => self::langr('Media Library'), 'gallery' => self::langr('Gallery'));
		}
		
		public static function is_sidebar($name)
		{
			if ( ! function_exists('dynamic_sidebar') OR ! dynamic_sidebar(self::$locale[$name.'_sidebar']))
			{
				return FALSE;
			}
			
			return TRUE;
		}

		public static function more_link($more_link, $more_link_text)
		{
			$link = preg_match('/href="(.+)" /', $more_link, $match);

			return '<a class="more" href="'.$match[1].'">'.$more_link_text.'</a>';
		}
		
		public static function widget_search()
		{
			self::import('searchform');
		}
		
		public static function title()
		{
			if ( ! is_home() AND ! is_front_page())
			{
				wp_title('', TRUE);
				echo ' - ';
			}
			
			$description = strip_tags(self::info('description', TRUE));
			
			echo self::info('name', TRUE);
			
			if ($description != NULL)
			{
				echo ' - '.$description;
			}
		}
		
		public static function url($url, $return = FALSE)
		{
			$url = self::info('url', TRUE).'/'.trim($url, '/').'/';
			
			if ($return)
			{
				return $url;
			} else
			{
				echo $url;
			}
		}

		public static function path($path, $return = FALSE)
		{
			$path = self::info('template_directory', TRUE).'/'.trim($path, '/');
			
			if ($return)
			{
				return $path;
			} else
			{
				echo $path;
			}
		}
		
		public static function script($handle, $path, $deps = array(), $version = FALSE, $in_footer = FALSE)
		{
			wp_enqueue_script($handle, ether::path($path, TRUE), $deps, $version, $in_footer);
		}
		
		public static function dir($dir, $return = FALSE)
		{
			$dir = rtrim(TEMPLATEPATH.'/'.$dir, '/');
			
			if ($return)
			{
				return $dir;
			} else
			{
				echo $dir;
			}
		}

		public static function slug($string)
		{
			$string = preg_replace('/[^a-zA-Z0-9\/_|+ -]/', '', $string);
			$string = strtolower(trim($string, '-'));
			$string = preg_replace('/[_|+ -]+/', '-', $string);
			$string = preg_replace('/(\/)+/', '/', $string);

			return rtrim($string, '/');
		}
		
		public static function trim_words($content, $limit, $ignore_end = FALSE) 
		{ 
			$text = strtok($content, ' '); 
			$total_words = count(explode(' ', $content));
			$output = '';
			$words = 0;
			
			while ($text)
			{
				$output .= ' '.$text;
				$words++;
				
				if ($words >= $limit)
				{
					if ($ignore_end)
					{
						if ($words < $total_words)
						{
							$output .= '...';
						}

						break;
					} else
					{
						if (substr($text, -1) == '!' OR substr($text, -1) == '.')
						{
							break;
						}
					}
				}
				
				$text = strtok(' ');
			}
			
			return ltrim($output);
		}

		public static function strip($data, $tags = NULL)
		{
			$regexp = '#\s*<(/?\w+)\s+(?:on\w+\s*=\s*(["\'\s])?.+?\(\1?.+?\1?\);?\1?|style=["\'].+?["\'])\s*>#is';
			
			return preg_replace($regexp, '<${1}>', strip_tags($data, $tags));
		}
		
		public static function strip_only($data, $tags)
		{
			if ( ! is_array($tags))
			{
				$tags = (strpos($str,'>') !== FALSE ? explode('>', str_replace('<', '', $tags)) : array($tags));
				
				if (end($tags) == '')
				{
					array_pop($tags);
				}
			}
			
			foreach($tags as $tag)
			{
				$data = preg_replace('#</?'.$tag.'[^>]*>#is', '', $data);
			}
			
			return $data;
		}

		public static function strip_slashes($data)
		{
			if (is_array($data))
			{
				foreach ($data as $key => $value)
				{
					$data[$key] = self::strip_slashes($value);
				}
					
				return $data;
			} else
			{
				$pattern = array('\\\'', '\\"', '\\\\', '\\0');
				$replace = array('', '', '', '');

				return stripslashes(str_replace($pattern, $replace, $data));
			}
		}
				
		public static function add_slashes($data)
		{
			$pattern = array('\\\'', '\\"', '\\\\', '\\0');
			$replace = array('', '', '', '');
			
			if (preg_match('/[\\\\\'"\\0]/', str_replace($pattern, $replace, $data)))
			{
				return addslashes($data);
			} else
			{
				return $data;
			}
		}
		
		public static function escape_string($str)
		{ 
			$len = strlen($str);
			$count = 0;
			$output = '';
			
			for ($i = 0; $i < $len; $i++)
			{
				switch ($c = $str[$i])
				{
					case '"':
						if ($count % 2 == 0)
						{
							$output .= '\\';
						}
						
						$count = 0;
						$output .= $c;
					break;

					case '\'':
						if ($count % 2 == 0)
						{
							$output .= '\\';
						}
						
						$count = 0;
						$output .= $c;
					break;

					case '\\':
						$count++;
						$output .= $c;
					break;
					
					default:
						$count = 0;
						$output .= $c;
				}
			}

			return $output; 
		}

		public static function clean($data, $strip_tags = FALSE, $htmlspecialchars = FALSE, $addslashes = TRUE, $whitespace = TRUE)
		{
			if (is_array($data))
			{
				foreach ($data as $key => $value)
				{
					$data[$key] = self::clean($value, $strip_tags, $htmlspecialchars, $whitespace);					
				}

				return $data;
			} else
			{
				if ($strip_tags)
				{
					$data = self::strip($data);
				}
				
				if ($htmlspecialchars)
				{
					$data = htmlspecialchars($data);
				}
				
				if ($addslashes)
				{
					$data = self::add_slashes($data);
				}
				
				if ( ! $whitespace)
				{
					$data = str_replace(array('\r\n', '\n', '\r'), '', $data);
				}

				if ( ! is_numeric($data))
				{
					$data = mysql_escape_string($data);
				}
					
				return $data;
			}
		}
		
		public static function http_get($url)
		{
			if (function_exists('curl_init'))
			{
				$curl = curl_init();
				curl_setopt($curl, CURLOPT_URL, $url);
				curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
				$content = curl_exec($curl);
				curl_close($curl);
			} else
			{
				$content = file_get_contents($url);
			}

			return $content;
		}
		
		public static function info($tag, $return = FALSE)
		{
			$cache = self::cache('ether::info::'.$tag, NULL);
			
			if ($cache !== FALSE)
			{
				if ($return)
				{
					return $cache;
				} else
				{
					echo $cache;
				}
				
				return;
			}
			
			$info = get_bloginfo($tag);
			
			self::cache('ether::info::'.$tag, $info);
			
			if ($return)
			{
				return $info;
			} else
			{
				echo $info;
			}
		}
		
		public static function option($key, $value = NULL)
		{
			if ($value === NULL)
			{
				$cache = self::cache('ether::option::'.$key, NULL);
				
				if ($cache !== FALSE)
				{
					return $cache;
				}
				
				$option = get_option(self::config('prefix').$key);
				
				self::cache('ether::option::'.$key, $option);
				
				return $option;
			} else
			{
				update_option(self::config('prefix').$key, $value);
				
				self::cache('ether::option::'.$key, $value);
			}
			
			return '';
		}
		
		public static function meta($key, $single = TRUE, $id = NULL, $update = FALSE)
		{
			if ($id == NULL AND get_option('show_on_front') != 'posts')
			{
				if (is_home() AND get_option('show_on_front') != 'posts')
				{
					$id = get_option('page_for_posts');	
				}
			}
			
			if ( ! $update)
			{
				$cache = self::cache('ether::meta::'.$key.$single, NULL);
				
				if ($cache !== FALSE)
				{
					return $cache;
				}
			
				global $post;
				
				$fields = get_post_meta($id != NULL ? $id : $post->ID, self::config('prefix').$key, $single);

				self::cache('ether::meta'.$key.$single, $fields);
				
				if ($key == 'url' AND $single AND ! empty($fields) AND substr($fields, 0, 7) != 'http://')
				{
					$fields = 'http://'.$fields;
				}
				
				return $fields;
			} else
			{
				global $post;

				update_post_meta($id != NULL ? $id : $post->ID, self::config('prefix').$key, $single);

				self::cache('ether::meta'.$key, $single);
			}
		}

		public static function feedburner_count($username = NULL, $separator = '.')
		{
			if ($username == NULL)
			{
				$username = self::option('social_feedburner');
			}
			
			$count = '0';
			
			if ($username != '')
			{
				$cache = self::cache('feedburner_subscribers', NULL);
			
				if ($cache !== FALSE)
				{
					return $cache;
				}
				
				$feedburner = self::http_get('http://feedburner.google.com/api/awareness/1.0/GetFeedData?uri='.$username);
				$begin = 'circulation="';
				$end = '"';
				$parts = explode($begin, $feedburner);
				$page = $parts[1];
				$parts = explode($end, $page);
				$count = $parts[0];
				
				if ($count == '')
				{
					$count = '0';
				}
			}
			
			if (strlen($count) > 3)
			{
				$length = strlen($count);
				$count = substr($count, 0, $length - 3).$separator.substr($count, $length - 3, 3);
			}
			
			self::cache('feedburner_subscribers', $count);

			return $count;
		}

		public static function youtube_channel($username = NULL)
		{
			if ($username == NULL)
			{
				$username = self::option('social_youtube');
			}
			
			$code = array();
			
			if ($username != '')
			{
				$cache = self::cache('youtube_channel', NULL);
				
				if ($cache !== FALSE)
				{
					return $cache;
				}
				
				$channel = self::http_get('http://gdata.youtube.com/feeds/api/users/'.$username.'/uploads?max-results=1&v=2&alt=json');
				$channel = json_decode($channel);
				
				$code[0]['url'] = $channel->feed->entry[0]->link[0]->href;
				$title = (array)$channel->feed->entry[0]->title;
				$code[0]['title'] = $title['$t'];
			}
			
			self::cache('youtube_channel', $code);
			
			return $code;
		}

		public static function twitter_count($username = NULL, $separator = '')
		{
			if ($username == NULL)
			{
				$username = self::option('social_twitter');
			}
			
			$count = '0';

			if ($username != '')
			{
				
				$cache = self::cache('twitter_followers', NULL);
			
				if ($cache !== FALSE)
				{
					return $cache;
				}
				
				$twitter = self::http_get('http://twitter.com/users/show/'.$username.'.xml');
				$begin = '<followers_count>';
				$end = '</followers_count>';
				$parts = explode($begin, $twitter);
				$page = $parts[1];
				$parts = explode($end, $page);
				$count = $parts[0];

				if ($count == '')
				{
					$count = '0';
				}
			}
			
			if (strlen($count) > 3)
			{
				$length = strlen($count);
				$count = substr($count, 0, $length - 3).$separator.substr($count, $length - 3, 3);
			}
			
			self::cache('twitter_followers', $count);
			
			return $count;
		}

		public static function twitter_time($time)
		{
			$delta = time() - $time;
		  
			if ($delta < 60)
			{
				return 'less than a minute ago';
			} else if ($delta < 120)
			{
				return 'about a minute ago';
			} else if ($delta < (45 * 60))
			{
				return floor($delta / 60).' minutes ago';
			} else if ($delta < (90 * 60))
			{
				return 'about an hour ago';
			} else if ($delta < (24 * 60 * 60))
			{
				return 'about '.floor($delta / 3600).' hours ago';
			} else if ($delta < (48 * 60 * 60))
			{
				return '1 day ago';
			} else
			{
				return floor($delta / 86400).' days ago';
			}
		}
		
		public static function flickr_feed($id = NULL, $count = 1)
		{
			$feed = array();
			
			if ($id == NULL)
			{
				$id = self::option('social_flickr');
			}
			
			if ($id != '')
			{
				$cache = self::cache('flickr_feed', NULL);
				
				if ($cache !== FALSE)
				{
					return $cache;
				}

				$flickr = self::http_get('http://api.flickr.com/services/feeds/photos_public.gne?id='.$id.'&format=php_serial');
				$flickr = unserialize($flickr);

				for ($i = 0; $i < $count; $i++)
				{
					$feed[] = array
					(
						'title' => $flickr['items'][$i]['title'],
						'link' => $flickr['items'][$i]['url'],
						'image' => $flickr['items'][$i]['photo_url'],
						'thumbnail' => $flickr['items'][$i]['t_url']
					);
				}
			}
			
			self::cache('flickr_feed', $feed);
			
			return $feed;
		}
		
		public static function google_map($location = NULL, $width = NULL, $height = NULL, $return = FALSE)
		{
			if ($location == NULL)
			{
				$location = self::option('social_location');
			}
			
			if ($width == NULL)
			{
				$width = '100%';
			}
			
			if ($height == NULL)
			{
				$height = '100%';
			}

			$location = htmlspecialchars($location);
			
			$map = '<iframe width="'.$width.'" height="'.$height.'" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com/maps?q='.$location.'&z=14&output=embed"></iframe>';
			
			if ($return)
			{
				return $map;
			}
			
			echo $map;
		}

		public static function twitter_tweets($username = NULL, $count = 1)
		{
			$tmp_cache_enable = self::config('cache');
			// force cache
			self::config('cache', TRUE);
			
			if ($username == NULL)
			{
				$username = self::option('social_twitter');
			}
			
			$tweets = array();
			
			if ($username != '')
			{
				$cache = self::cache('twitter_tweets', NULL);
				
				if ($cache !== FALSE AND ! empty($cache))
				{
					self::config('cache', $tmp_cache_enable);
					
					return $cache;
				}
				
				$twitter = self::http_get('http://twitter.com/statuses/user_timeline/'.$username.'.json?count='.$count);
				
				if ( ! empty($twitter))
				{
					$twitter = json_decode($twitter);
					
					if ( ! $twitter->error)
					{
						foreach ($twitter as $tweet)
						{
							$tweet->text = strip_tags($tweet->text);
							
							$regexps = array
							(
								'link'  => '/[a-z]+:\/\/[a-z0-9-_]+\.[a-z0-9-_:~%&\?\+#\/.=]+[^:\.,\)\s*$]/i',
								'at'    => '/(^|[^\w]+)\@([a-zA-Z0-9_]{1,15}(\/[a-zA-Z0-9-_]+)*)/',
								'hash'  => '/(^|[^&\w\'\"]+)\#([a-zA-Z0-9_]+)/'
							);
						 
							foreach ($regexps as $name => $re)
							{
								$tweet->text = preg_replace_callback($re, array('ether', 'parse_tweet_'.$name), $tweet->text);
							}
							
							if (trim($tweet->text) != '')
							{
								array_push($tweets, array
								(
									'tweet' => $tweet->text,
									'time' => self::twitter_time(strtotime(str_replace('+0000', '', $tweet->created_at))),
									'link' => 'http://twitter.com/'.$username.'/statuses/'.$tweet->id
								));
							}
						}
					}
				} else
				{
					return $cache;
				}
			}
			
			self::cache('twitter_tweets', $tweets);
			self::config('cache', $tmp_cache_enable);

			return $tweets;
		}

		private static function clean_tweet($tweet)
		{
			$regexps = array
			(
				'link'  => '/[a-z]+:\/\/[a-z0-9-_]+\.[a-z0-9-_:~%&\?\+#\/.=]+[^:\.,\)\s*$]/i',
				'at'    => '/(^|[^\w]+)\@([a-zA-Z0-9_]{1,15}(\/[a-zA-Z0-9-_]+)*)/',
				'hash'  => '/(^|[^&\w\'\"]+)\#([a-zA-Z0-9_]+)/'
			);
		 
			foreach ($regexps as $name => $re)
			{
				$tweet = preg_replace_callback($re, array(self, 'parse_tweet_'.$name), $tweet);
			}
		 
			return $tweet;
		}
		 
		private static function parse_tweet_link($m)
		{
			return '<a href="'.$m[0].'">'.((strlen($m[0]) > 25) ? substr($m[0], 0, 24).'...' : $m[0]).'</a>';
		}
		 
		private static function parse_tweet_at($m)
		{
			return $m[1].'@<a href="http://twitter.com/'.$m[2].'">'.$m[2].'</a>';
		}

		private static function parse_tweet_hash($m)
		{
			return $m[1].'#<a href="http://search.twitter.com/search?q=%23'.$m[2].'">'.$m[2].'</a>';
		}
		
		public static function share($service, $title = NULL, $url = NULL, $return = FALSE)
		{
			$share_title = urlencode(trim(($title != NULL ? $title : wp_title('', FALSE, ''))));
			$share_url = urlencode(trim(($url != NULL ? $url : ('http'.(isset($_SERVER['HTTPS'] )? 's' : '').'://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']))));	
			
			switch ($service)
			{
				case 'facebook':
					$url = 'http://www.facebook.com/share.php?u='.$share_url.'&t='.$share_title;
				break;
				
				case 'twitter':
					$url = 'http://twitter.com/home?status='.$share_title.'%20'.$share_url;
				break;
				
				case 'digg':
					$url = 'http://digg.com/submit?phase=2&url='.$share_url.'&title='.$share_title;
				break;
				
				case 'reddit':
					$url = 'http://reddit.com/submit?url='.$share_url.'&title='.$share_title;
				break;
				
				case 'stumbleupon':
					$url = 'http://www.stumbleupon.com/submit?url='.$share_url.'&title='.$share_title;
				break;
				
				case 'delicious':
					$url = 'http://del.icio.us/post?url='.$share_url.'&title='.$share_title;
				break;
				
				case 'addthis':
					$url = 'http://www.addthis.com/bookmark.php';
				break;
			}
			
			if ($return)
			{
				return $url;
			}
			
			echo $url;
		}
		
		public static function rss($return = FALSE)
		{
			$feedburner = self::option('social_feedburner');
			$url = 'http://feeds.feedburner.com/';
			
			if ($feedburner != '')
			{
				$url .= $feedburner;
			} else
			{
				$url = self::info('rss2_url', TRUE);
			}
			
			if ($return)
			{
				return $url;
			}
			
			echo $url;
		}
		
		public static function shadowbox_href($url)
		{
			$url = trim($url);

			preg_match('@^(?:http://)?(?:www.)?([^/]+)@i', $url, $matches);
				
			if ($matches[1] == 'video.google.com')
			{
				$params = explode('?', $url);
				parse_str(html_entity_decode($params[1]), $params);
					
				foreach ($params as $k => $v)
				{
					if (strtolower($k) == 'docid')
					{
						$url = 'http://video.google.com/googleplayer.swf?docid='.$v;

						return $url.'" rel="shadowbox;width=480;height=290;player=iframe';
					}
				}
			} else if ($matches[1] == 'youtube.com')
			{
				$params = explode('?', $url);
				parse_str(html_entity_decode($params[1]), $params);
				
				foreach ($params as $k => $v)
				{
					if (strtolower($k) == 'v')
					{
						$url = 'http://www.youtube.com/embed/'.$v;

						return $url.'" rel="shadowbox;width=480;height=290;player=iframe';
					}
				}
			} else if ($matches[1] == 'vimeo.com')
			{
				preg_match('/(\d+)/', $url, $id);
				
				if ( ! empty($id[1]))
				{
					$url = 'http://player.vimeo.com/video/'.$id[1];
					
					return $url.'" rel="shadowbox;width=480;height=290;player=iframe';
				}
			} else if ($matches[1] == 'blip.tv')
			{
				preg_match('/file\/(\d+)\//', $url, $id);
				
				if ( ! empty($id[1]))
				{
					$url = 'http://blip.tv/play/'.$id[1];
					
					return $url.'" rel="shadowbox;width=480;height=290;player=iframe';
				}
			}
			
			preg_match('/(?i)\.(jpg|png|gif)$/', $url, $ext);
			
			if ( ! empty($ext))
			{
				return $url.'" rel="shadowbox';
			}
			
			return $url;
		}
		
		public static function avatar($id_or_email, $size, $default, $return = FALSE)
		{
			if ($id_or_email === NULL)
			{
				global $post;
				
				$id_or_email = get_the_author_meta('user_email');
			}
			
			$gravatar = get_avatar($id_or_email, $size, $default);
			
			preg_match('/(src=[\'"])(.*?)([\'"])/i', $gravatar, $src);
			
			if ($return)
			{
				return $src[2];
			}
			
			echo $src[2];
		}
		
		public static function sitemap($show_posts = TRUE, $show_pages = TRUE, $posts_first = FALSE, $return = FALSE)
		{
			$sitemap = self::cache('ether::sitemap', NULL);
			
			if ($sitemap !== FALSE)
			{
				echo $sitemap;
				return;
			}
			
			$sitemap = '<li><a href="'.self::info('url', TRUE).'">'.self::langr('Home').'</a></li>';
			
			if ($posts_first)
			{
				if ($show_posts)
				{
					$sitemap .= wp_get_archives(array
					(
						'type' => 'postbypost',
						'echo' => 0
					));
				}
				
				if ($show_pages)
				{
					$sitemap .= wp_list_pages(array
					(
						'echo' => 0,
						'title_li' => ''
					));
				}
			} else
			{
				if ($show_pages)
				{
					$sitemap .= wp_list_pages(array
					(
						'echo' => 0,
						'title_li' => ''
					));
				}
				
				if ($show_posts)
				{
					$sitemap .= wp_get_archives(array
					(
						'type' => 'postbypost',
						'echo' => 0
					));
				}
			}
			
			if ($return)
			{
				self::cache('ether::sitemap', $sitemap);
				
				return $sitemap;
			}

			echo $sitemap;
			
			self::cache('ether::sitemap', $sitemap);
		}
		
		public static function get_categories()
		{
			return get_categories(array('hide_empty' => FALSE, 'hierarchical' => FALSE));	
		}
		
		public static function get_comments($args)
		{
			$result = array();
			
			if ( ! is_array($args))
			{
				parse_str($args, $args);
			}
			
			$defaults = array();
			$args = array_merge($defaults, $args);
			
			$comments = get_comments($args);
			
			foreach ($comments as $comment)
			{
				array_push($result, array
				(
					'id' => $comment->comment_ID,
					'author' => $comment->comment_author,
					'url' => $comment->comment_author_url,
					'date' => $comment->comment_date,
					'content' => $comment->comment_content
				));
			}
			
			return $result;
		}
		
		public static function get_id()
		{
			global $post;
			
			$id = NULL;
			
			if (is_single())
			{
				$id = $post->ID;
			} else if (is_page() OR get_option('show_on_front') != 'posts')
			{
				if (is_home() AND get_option('show_on_front') != 'posts')
				{
					$id = get_option('page_for_posts');
				} else
				{
					$id = $post->ID;
				}
			}
			
			return $id;
		}
		
		public static function get_posts($args = 'numberposts=1&post_type=post&text_opt=excerpt', $time_format = 'F j, Y')
		{
			global $post;

			$result = array();
			
			if ( ! is_array($args))
			{
				parse_str($args, $args);
			}
			
			$defaults = array('text_opt' => 'NONE');
			$args = array_merge($defaults, $args);

			$custom_order = FALSE;
			
			if (isset($args['post__in']) AND ! empty($args['post__in']) AND $args['orderby'] == 'custom')
			{
				unset($args['orderby']);
				$custom_order = TRUE;
			}

			$posts = get_posts($args);
			$result = array();
			
			if ( ! empty($posts))
			{
				foreach ($posts as $post)
				{
					setup_postdata($post);
					array_push($result, array
					(
						'id' => get_the_ID(),
						'permalink' => get_permalink(),
						'title' => get_the_title(),
						'author' => get_the_author(),
						'date' => get_the_time($time_format),
						'content' => ($args['text_opt'] == 'excerpt' ? wpautop(get_the_excerpt()) : ($args['text_opt'] == 'content' ? apply_filters('the_content', get_the_content()) : ''))
					));
				}
			}
			
			if ($custom_order)
			{
				$posts_ = array();
				$ids = array();
				$ids_ = $args['post__in'];
				$counter = 0;
				
				foreach ($ids_ as $k => $v)
				{
					$ids[$v] = $counter;
					$counter++;
				}
				
				foreach ($result as $p)
				{
					$posts_[intval($ids[$p['id']])] = $p;
				}
				
				$result = $posts_;
				
				ksort($result);
			}

			wp_reset_query();
			
			return $result;
		}

		public static function get_child_pages($id, $args = 'text_opt=NONE')
		{
			$args = $args.'&post_type=page&post_parent='.$id;

			return self::get_posts($args);
		}

		public static function get_pages($args = 'title_li=&sort_column=menu_order&depth=1&echo=0')
		{
			$data = wp_list_pages($args);

			preg_match_all('/class\=\"(.*)\".*><a.*href\=\"(.*)\" title.*>(.*)\<\/a>/', $data, $result);
			$count = count($result[0]);
			
			$pages = array();
			
			for ($i = 0; $i < $count; $i++)
			{
				$pages[$i] = array
				(
					'class' => $result[1][$i],
					'href' => $result[2][$i],
					'title' => $result[3][$i]
				);
			}
			
			return $pages;
		}
		
		public static function find_posts($query)
		{
			$query = preg_replace('/(.*)-(html|htm|php|asp|aspx)$/', '$1', $query);
			$posts = query_posts('post_type=any&name='.$query);
			$query = str_replace('-', ' ', $query);
			
			if (count($posts) == 0)
			{
				$posts = query_posts('post_type=any&s='.$query);
			}
			
			wp_reset_query();
			
			return $posts;
		}
		
		public static function list_similar($count = 5, $return = FALSE)
		{
			global $post;
			
			$last_post = $post;
			
			$query = preg_replace('/(.*)-(html|htm|php|asp|aspx)$/', '$1', $post->post_name);
			
			$posts = self::get_posts(array
			(
				'post_type' => 'post',
				'orderby' => 'date',
				'name' => $query,
				'numberposts' => $count,
				'post__not_in' => array($post->ID)
			));
			
			if (count($posts) == 0)
			{
				$posts = self::get_posts(array
				(
					'post_type' => 'post',
					'orderby' => 'date',
					'name' => str_replace('-', ' ', $query),
					'numberposts' => $count,
					'post__not_in' => array($post->ID)
				));
			}
			
			if ($return)
			{
				return $posts;
			}
			
			$result = '';
			
			foreach ($posts as $post)
			{
				$result .= '<li><a href="'.$post['permalink'].'">'.$post['title'].'</a></li>';
			}
			
			$post = $last_post;
				
			echo $result;
		}
		
		public static function list_random($count = 5, $return = FALSE)
		{
			$posts = self::get_posts(array
			(
				'post_type' => 'post',
				'orderby' => 'rand',
				'numberposts' => $count
			));
			
			if ($return)
			{
				return $posts;
			}
			
			$result = '';
			
			foreach ($posts as $post)
			{
				$result .= '<li><a href="'.$post['permalink'].'">'.$post['title'].'</a></li>';
			}
				
			echo $result;
		}
		
		public static function list_popular($count = 5, $return = FALSE, $args = array())
		{
			$args = array_merge(array
			(
				'post_type' => 'post',
				'orderby' => 'comment_count',
				'order' => 'DESC',
				'numberposts' => $count
			), $args);
			
			$posts = self::get_posts($args);
			
			if ($return)
			{
				return $posts;
			}
			
			$result = '';
			
			foreach ($posts as $post)
			{
				$result .= '<li><a href="'.$post['permalink'].'">'.$post['title'].'</a></li>';
			}
				
			echo $result;
		}
		
		public static function list_posts($count = 10, $return = FALSE)
		{
			$posts = self::get_posts(array
			(
				'post_type' => 'post',
				'orderby' => 'date',
				'order' => 'DESC',
				'numberposts' => $count
			));
			
			if ($return)
			{
				return $posts;
			}
			
			$result = '';
			
			foreach ($posts as $post)
			{
				$result .= '<li><a href="'.$post['permalink'].'">'.$post['title'].'</a></li>';
			}
				
			echo $result;
		}
		
		public static function list_categories($args = 'title_li=')
		{
			wp_list_categories($args);
		}
		
		public static function list_archives($args = 'type=monthly')
		{
			wp_get_archives($args);
		}
		
		public static function list_tags($args = 'order=ASC&number=20')
		{
			$tags = get_tags($args);
			
			$output = '';
			
			foreach ($tags as $tag)
			{
				$output .= '<li><a href="'.get_tag_link($tag->term_id).'" title="'.self::langr('View all posts in %s', $tag->name).'">'.$tag->name.'</a></li>';
			}
			
			echo $output;
		}

		public static function navigation($args = 'sort_column=menu_order', $menu = 'header', $show_home = TRUE, $return = FALSE)
		{
			if ( ! is_array($args))
			{
				parse_str($args, $args);
			}
			
			$defaults = array
			(
				'depth' => 0,
				'sort_column' => 'menu_order',
				'fallback_cb' => NULL
			);
			
			$args = array_merge($defaults, $args);
			
			$nav = '';
			
			if ( ! isset($args['class']))
			{
				$args['class'] = '';
			}
			
			if (function_exists('wp_nav_menu'))
			{
				ob_start();
				wp_nav_menu('fallback_cb='.$args['fallback_cb'].'&sort_column='.$args['sort_column'].'&container=&depth='.$args['depth'].'&menu='.$menu.'&theme_location='.$menu.'&menu_id=&menu_class='.$args['class']);
				$wp_nav = trim(ob_get_clean());
				
				preg_match('/^(<ul.*>)(.*)(<\/ul>)\z/ismU', $wp_nav, $elements);
				
				if (isset($elements[2]))
				{
					$nav .= str_replace('current-menu-item', 'current', $elements[2]);
				}
			} else
			{
				$pages = get_pages('depth='.$args['depth'].'&meta_key=menu&meta_value=exclude');
				$page_exclude = array();

				foreach ($pages as $page)
				{
					$page_exclude[] = $page->ID;
				}

				$nav .= str_replace('current_page_item', 'current', wp_list_pages('title_li=&sort_column='.$args['sort_column'].'&depth='.$args['depth'].'&echo=0&'.($menu == 'header' ? '' : 'meta_key=menu&meta_value='.$menu.'&').'exclude='.implode(',', $page_exclude).'&exclude_tree='.implode(',', $page_exclude)));
			}
			
			if ( ! empty($nav))
			{
				if ( ! function_exists('wp_nav_menu') AND empty($nav))
				{
					$nav = call_user_func($args['fallback_cb']);
				}
				
				$last_occurrance = strripos($nav, 'class="');
				$last_before = substr($nav, 0, ($last_occurrance + 7));
				$last_after = substr($nav, ($last_occurrance + 7), strlen($nav));
				
				$nav = $last_before.'last-child '.$last_after;
			}
			
			if (empty($nav) OR $show_home)
			{
				$nav = '<li'.(empty($nav) ? ' class="' : '').((is_home() OR is_front_page()) ? ( ! empty($nav) ? ' class="' : '').'current'.( ! empty($nav) ? '"' : ' ') : '').(empty($nav) ? 'last-child"' : '').'><a href="'.self::info('home', TRUE).'">Home</a></li>'.$nav;
			}

			if ($return)
			{
				return $nav;
			}
			
			echo $nav;
		}
		
		public static function is_paged()
		{
			global $wp_query;
			
			if (self::config('pagination'))
			{
				if ( ! is_single())
				{
					$is_paged = get_query_var('paged');

					return ! empty($is_paged) OR $is_paged === 0;
				} else
				{
					$output = '';
					ob_start();
					previous_post_link('%link', self::$locale['pagination_prev']);
					$output .= trim(ob_get_clean());
					ob_start();
					next_post_link('%link', self::$locale['pagination_next']);
					$output .= trim(ob_get_clean());
					
					return ! empty($output);
				}
			}
			
			return FALSE;
		}
		
		public static function pagination($list = TRUE, $prevnext = TRUE, $firstlast = FALSE, $return = FALSE)
		{
			global $wp_query;

			$page = get_query_var('paged');
			$page = ! empty($page) ? intval($page) : 1;

			$posts_per_page = intval(get_query_var('posts_per_page'));
			$pages = intval(ceil($wp_query->found_posts / $posts_per_page));
			
			$output = '';
			
			if ( ! is_single())
			{
				if ($pages > 1)
				{
					if ($firstlast)
					{
						$output .= '<li class="first"><a href="'.get_pagenum_link(1).'">'.self::$locale['pagination_first'].'</a></li>';
					}
					
					if ($prevnext AND $page > 1)
					{
						$output .= '<li class="prev"><a href="'.get_pagenum_link($page - 1).'">'.self::$locale['pagination_prev'].'</a></li>';
					}
					
					if ($list)
					{
						for ($i = 1; $i <= $pages; $i++)
						{
							if ($i == $page)
							{
								$output .= '<li class="current"><a href="'.get_pagenum_link($i).'">'.$i.'</a></li>';
							} else
							{
								$output .= '<li><a href="'.get_pagenum_link($i).'">'.$i.'</a></li>';
							}
						}
					}

					if ($prevnext AND $page < $pages)
					{
						$output .= '<li class="next"><a href="'.get_pagenum_link($page + 1).'">'.self::$locale['pagination_next'].'</a></li>';
					}
					
					if ($firstlast)
					{
						$output .= '<li class="last"><a href="'.get_pagenum_link($pages).'">'.self::$locale['pagination_last'].'</a></li>';
					}
				}
			} else
			{
				if ($prevnext)
				{
					ob_start();
					previous_post_link('%link', self::$locale['pagination_prev']);
					$output .= '<li class="prev">'.trim(ob_get_clean()).'</li>';
					ob_start();
					next_post_link('%link', self::$locale['pagination_next']);
					$output .= '<li class="next">'.trim(ob_get_clean()).'</li>';
				}
			}
			
			if ($return)
			{
				return $output;
			}
			
			echo $output;
		}
		
		public static function mail($title, $message, $name, $from, $to = NULL, $copy = FALSE)
		{
			$message = '
				<html>
					<head>
						<title>'.$title.'</title>
					</head>
					<body>
						'.$message.'
					</body>
				</html>
			';

			$headers  = 'MIME-Version: 1.0'."\n";
			$headers .= 'Content-type: text/html; charset=utf-8'."\n";
			$headers .= 'To: '.$to."\n";
			$headers .= 'From: '.$name.' <'.$from.'>'."\n";
			$headers .= 'Reply-To: '.$name.' <'.$from.'>'."\n";
			
			if ($copy)
			{
				$headers .= 'Cc: '.$from."\n";
				$headers .= 'Bcc: '.$from."\n";
			}
			
			if ($to == NULL)
			{
				$to = self::info('admin_email', TRUE);
			}

			mail($to, $title, $message, $headers);
		}

		public static function get_image_size($url)
		{
			preg_match_all('/('.self::config('thumb_prefix').')(\d+.)(x)(\d+.)(\.(jpg|jpeg|gif|png))/is', basename($thumbnail_url), $matches);
			$result = array('width' => 800, 'height' => 600);
			
			$uploads = wp_upload_dir();
			$base_path = '';

			if (count($matches[0][0]) > 0)
			{
				$base_url = str_replace($matches[0][0], $matches[5][0], $thumbnail_url);
				$base_path = self::config('upload_dir').'/'.basename($base_url);
			} else
			{
				$base_path = self::config('upload_dir').'/'.basename($url);
			}
			
			if (file_exists($base_path))
			{
				list($width, $height, $type, $attr) = getimagesize($base_path);
					
				$result['width'] = $width;
				$result['height'] = $height;
			}
			
			return $result;
		}

		public static function import_end()
		{

		}
		
		public static function find_image($url)
		{
			$uploads = wp_upload_dir();
			
			if (substr($url, 0, strlen($uploads['base_url'])) == $uploads['base_url'])
			{
				return $url;
			}
			
			$name = basename($url);
			$found = FALSE;
			
			$dir = opendir($uploads['base_dir']);
			
			// exclude ether directory here
			while ($file = readdir($dir))
			{
				if (is_file($uploads['base_dir'].'/'.$file))
				{
					if (basename($file) == $name)
					{
						copy($uploads['base_dir'].'/'.$file, self::config('upload_dir').'/'.basename($file));
						
						return self::config('upload_url').'/'.basename($file);
					}
				}
			}
			
			closedir($dir);
			
			return self::get_remote_image($url);
		}
		
		public static function get_remote_image($url)
		{
			// download image to local server
			$url_data = parse_url($url);

			if ($url_data['host'] != $_SERVER['HTTP_HOST'])
			{
				$remote_image = self::http_get($url);

				self::write(rtrim(self::config('upload_dir'), '/').'/'.basename($url), $remote_image, FALSE);
				
				return rtrim(self::config('upload_url'), '/').'/'.basename($url);
			}
			
			return $url;
		}
		
		public static function get_image_base($thumbnail_url)
		{
			/*$url_data = parse_url($thumbnail_url);

			if ($url_data['host'] != $_SERVER['HTTP_HOST'])
			{
				return $thumbnail_url;
			}*/
			
			//$thumbnail_url = self::get_remote_image($thumbnail_url);

			preg_match_all('/('.self::config('thumb_prefix').')(\d+.)(x)(\d+.)(\.(jpg|jpeg|gif|png))/is', basename($thumbnail_url), $matches);

			if (count($matches[0][0]) > 0)
			{
				$uploads = wp_upload_dir();
				$base_url = str_replace($matches[0][0], $matches[5][0], $thumbnail_url);
				$base_path = self::config('upload_dir').'/'.basename($base_url);

				if (file_exists($base_path))
				{
					return $base_url;
				}
			}
			
			return $thumbnail_url;
		}
		
		public static function get_image_thumbnail($base_url, $width, $height)
		{
			/*$url_data = parse_url($base_url);

			if ($url_data['host'] != $_SERVER['HTTP_HOST'])
			{
				return $base_url;
			}*/
			
			//$base_url = self::get_remote_image($base_url);
			$uploads = wp_upload_dir();
			
			$image = $base_url;
			$image_path = str_replace($uploads['baseurl'], $uploads['basedir'], $image);		
			$image_size = getimagesize($image_path);
			$width = intval($width);
			$height = intval($height);
			
			if ($width == 0 OR empty($width))
			{
				$width = $image_size[0];
			}
			
			if ($height == 0 OR empty($height))
			{
				$height = $image_size[1];
			}

			if ($width == $image_size[0] AND $height == $image_size[1])
			{
				return $image;
			}
			
			preg_match_all('/('.self::config('thumb_prefix').')(\d+.)(x)(\d+.)(\.(jpg|jpeg|gif|png))/is', basename($image), $matches);

			if ( ! empty($matches) AND ! empty($matches[0]) AND count($matches[0][0]) > 0)
			{
				if ($width == $matches[2][0] AND $height == $matches[4][0])
				{
					return $image;
				} else
				{
					return self::get_image_thumbnail(self::get_image_base($base_url), $width, $height);
				}
			}

			$pathinfo = pathinfo($image_path);
			$ext = $pathinfo['extension'];
			$filename = basename($image_path, '.'.$ext).self::config('thumb_prefix').$width.'x'.$height.'.'.$ext;

			if (file_exists(self::config('upload_dir').'/'.$filename))
			{
				return self::config('upload_url').'/'.$filename;
			}
					
			if ( ! file_exists(self::config('upload_dir').'/'.basename($image_path)))
			{
				copy($image_path, self::config('upload_dir').'/'.basename($image_path));
			}

			return str_replace($uploads['basedir'], $uploads['baseurl'], self::thumbnail($image_path, $width, $height, TRUE, $width, $height, self::config('upload_dir').'/'.$filename));
		}
		
		public static function clean_thumbnails()
		{
			$path = self::config('upload_dir').'/';
			$files = array();
			
			$scan_results = array_diff(scandir($path), array('.', '..'));
				
			foreach($scan_results as $result)
			{
				if (is_file($path.$result))
				{
					unlink($path.$result);
				}
			}
		}
		
		public static function thumbnail($path, $width, $height, $ratio = TRUE, $crop_width = NULL, $crop_height = NULL, $output_path = NULL)
		{
			$changed = FALSE;
			
			$ext = strtolower(end(explode('.', $path)));
			
			if ($ext == 'jpg' OR $ext == 'jpeg') 
			{
				$img = @imagecreatefromjpeg($path);
			}  else if ($ext == 'png') 
			{
				$img = @imagecreatefrompng($path);
			}  else if ($ext == 'gif') 
			{
				$img = @imagecreatefromgif($path);
			}
			  
			$x = imagesx($img);
			$y = imagesy($img);
			$size = getimagesize($path);
			  
			if ($width != NULL AND $height != NULL)
			{
				$wandh = TRUE;
			} else
			{
				$wandh = FALSE;
			}

			if ($width != NULL OR $height != NULL)
			{
				if ($width == NULL)
				{
					$width = $size[0];
				}
			
				if ($height == NULL)
				{
					$height = $size[1];
				}
			
				if ($width != $size[0])
				{
					$ratio_x = $x / $width;
				} else
				{
					$ratio_x = 1;
				}
				
				if ($height != $size[1])
				{
					$ratio_y = $y / $height;
				} else
				{
					$ratio_y = 1;
				}
				
				if ($ratio)
				{
					if ($wandh)
					{
						if ($ratio_y > $ratio_x)
						{
							$height = $y * ($width / $x);
						} else
						{
							$width = $x * ($height / $y);
						}
					} else
					{
						if ($ratio_y < $ratio_x)
						{
							$height = $y * ($width / $x);
						} else
						{
							$width = $x * ($height / $y);
						}
					}
				}

				$new_img = imagecreatetruecolor($width, $height);
				
				if ($size[2] == IMAGETYPE_GIF OR $size[2] == IMAGETYPE_PNG)
				{
					$index = imagecolortransparent($img);
					
					if ($index >= 0)
					{
						$color = imagecolorsforindex($img, $index);
						$index = imagecolorallocate($new_img, $color['red'], $color['green'], $color['blue']);
						imagefill($new_img, 0, 0, $index);
						imagecolortransparent($new_img, $index);
					} elseif ($size[2] == IMAGETYPE_PNG)
					{
						imagealphablending($new_img, FALSE);
						$color = imagecolorallocatealpha($new_img, 0, 0, 0, 127);
						imagefill($new_img, 0, 0, $color);
						imagesavealpha($new_img, TRUE);
					}
				}
				
				imagecopyresampled($new_img, $img, 0, 0, 0, 0, $width, $height, $x, $y);
				imagedestroy($img);
				$img = $new_img;
				$changed = TRUE;
			}
			
			if ($width == NULL)
			{
				$width = $x;
			}
			
			if ($height == NULL)
			{
				$height = $y;
			}
			
			if ($crop_width != NULL OR $crop_height != NULL)
			{
				if ($crop_width == NULL)
				{
					$crop_width = $width;
				}
				
				if ($crop_height == NULL)
				{
					$crop_height = $height;
				}

				$new_img = imagecreatetruecolor($crop_width, $crop_height);
				
				if ($size[2] == IMAGETYPE_GIF OR $size[2] == IMAGETYPE_PNG)
				{
					$index = imagecolortransparent($img);
					
					if ($index >= 0)
					{
						$color = imagecolorsforindex($img, $index);
						$index = imagecolorallocate($new_img, $color['red'], $color['green'], $color['blue']);
						imagefill($new_img, 0, 0, $index);
						imagecolortransparent($new_img, $index);
					} elseif ($size[2] == IMAGETYPE_PNG)
					{
						imagealphablending($new_img, FALSE);
						$color = imagecolorallocatealpha($new_img, 0, 0, 0, 127);
						imagefill($new_img, 0, 0, $color);
						imagesavealpha($new_img, TRUE);
					}
				}
					
				$x = ($width - $crop_width) / 2;
				$y = ($height - $crop_height) / 2;
				imagecopyresampled($new_img, $img, 0, 0, $x, $y, $crop_width, $crop_height, $crop_width, $crop_height);
				imagedestroy($img);
				$img = $new_img;
					
				$width = $crop_width;
				$height = $crop_height;
				$changed = TRUE;
			}

			if ($output_path != NULL)
			{
				$path = explode('.', $output_path);
				array_pop($path);
				$path = implode('.', $path).'.'.$ext;
			} else
			{		
				$path = explode('.', $path);
				array_pop($path);
				$path = implode('.', $path).(int)$width.'x'.(int)$height.'.'.$ext;
			}
			
			if ($ext == 'jpg' OR $ext == 'jpeg')
			{
				imagejpeg($img, $path, 100);
			} else if ($ext == 'png')
			 {
				imagepng($img, $path);
			} else if ($ext == 'gif')
			{
				imagegif($img, $path);
			}
			
			return $path;
		}

		public static function handle_field($data, $rules, $prefix = NULL)
		{
			//$data = self::clean($data, FALSE, FALSE, FALSE, FALSE);
			
			if ($prefix === NULL)
			{
				$prefix = rtrim(self::config('prefix'), '_').'_';
			} else
			{
				if ($prefix != '')
				{
					$prefix = rtrim($prefix, '_').'_';
				}
			}

			foreach ($rules as $rule => $fields)
			{
				foreach ($fields as $field)
				{
					if ( ! isset($field['relation']))
					{
						$field['relation'] = 'option';
					}
					
					if (isset($data[$prefix.$field['name']]))
					{
						if ($field['relation'] == 'option')
						{
							$current = get_option($prefix.$field['name']);
						} else if ($field['relation'] == 'meta')
						{
							global $post;
							
							$current = get_post_meta($post->ID, $prefix.$field['name'], TRUE);
						}
						
						if ($data[$prefix.$field['name']] != $current)
						{
							if ($field['relation'] == 'option')
							{
								update_option($prefix.$field['name'], $data[$prefix.$field['name']]);
							} else if ($field['relation'] == 'meta')
							{
								global $post;
								
								update_post_meta($post->ID, $prefix.$field['name'], $data[$prefix.$field['name']]);
							}
						} else if (empty($data[$prefix.$field['name']]) AND empty($current))
						{
							if ($field['relation'] == 'option')
							{
								update_option($prefix.$field['name'], $field['value']);
							} else if ($field['relation'] == 'meta')
							{
								global $post;
								
								update_post_meta($post->ID, $prefix.$field['name'], $field['value']);	
							}
						}
					} else
					{
						if ($field['relation'] == 'option')
						{
							update_option($prefix.$field['name'], $field['value']);	
						} else if ($field['relation'] == 'meta')
						{
							global $post;
							
							update_post_meta($post->ID, $prefix.$field['name'], $field['value']);
						}
					}
				}
			}
		}
		
		public static function make_field($name, $rules = array(), $data = NULL, $prefix = NULL)
		{
			if ($prefix === NULL)
			{
				$prefix = rtrim(self::config('prefix'), '_').'_';
			} else
			{
				if ($prefix != '')
				{
					$prefix = rtrim($prefix, '_').'_';
				}
			}
			
			if ( ! isset($rules['relation']))
			{
				$rules['relation'] = 'option';
			}
			
			$value = '';
			
			if ($rules['type'] != 'submit')
			{
				if ($data === NULL)
				{
					if ($rules['relation'] == 'option')
					{
						$value = get_option($prefix.$name);
					} else if ($rules['relation'] == 'meta')
					{
						global $post;
						
						$value = get_post_meta($post->ID, $prefix.$name, TRUE);
					}
				} else
				{
					if (is_array($data))
					{
						if (isset($data[$prefix.$name]))
						{
							$value = $data[$prefix.$name];
						}
					} else
					{
						$value = $data;
					}
				}
				
				if ( ! empty($value))
				{
					$value = htmlspecialchars(stripslashes($value));
				}
			}
			
			if ( ! isset($rules['value']))
			{
				$rules['value'] = '';
			}
			
			if (empty($value) AND isset($rules['use_default']) AND $rules['use_default'])
			{
				$value = htmlspecialchars(stripslashes($rules['value']));
			}
			
			if ($rules['type'] == 'text')
			{
				return '<input type="text" name="'.$prefix.$name.'"'.(isset($rules['id']) ? ' id="'.$rules['id'].'"' : '').(isset($rules['class']) ? ' class="'.$rules['class'].'"' : '').' value="'.$value.'"'.(isset($rules['style']) ? ' style="'.$rules['style'].'"' : '').' />';
			} else if ($rules['type'] == 'hidden')
			{
				return '<input type="hidden" name="'.$prefix.$name.'"'.(isset($rules['id']) ? ' id="'.$rules['id'].'"' : '').(isset($rules['class']) ? ' class="'.$rules['class'].'"' : '').' value="'.$value.'"'.(isset($rules['style']) ? ' style="'.$rules['style'].'"' : '').' />';
			} else if ($rules['type'] == 'textarea')
			{
				return '<textarea name="'.$prefix.$name.'"'.(isset($rules['cols']) ? ' cols="'.$rules['cols'].'"' : '').(isset($rules['rows']) ? ' rows="'.$rules['rows'].'"' : '').(isset($rules['id']) ? ' id="'.$rules['id'].'"' : '').(isset($rules['class']) ? ' class="'.$rules['class'].'"' : '').''.(isset($rules['style']) ? ' style="'.$rules['style'].'"' : '').'>'.$value.'</textarea>';
			} else if ($rules['type'] == 'select')
			{			
				$select = '<select name="'.$prefix.$name.'"'.(isset($rules['id']) ? ' id="'.$rules['id'].'"' : '').(isset($rules['class']) ? ' class="'.$rules['class'].'"' : '').' value="'.$value.'"'.(isset($rules['style']) ? ' style="'.$rules['style'].'"' : '').'>';
				
				if (isset($rules['options']))
				{
					$group = '';
					$last_group = '';
					
					foreach ($rules['options'] as $option_key => $option_value)
					{
						if (isset($option_value['group']) AND $option_value['group'] != $group)
						{
							if ($group != '')
							{
								$select .= '</optgroup>';
							}
							
							$group = $option_value['group'];
							
							$select .= '<optgroup label="'.$group.'">';
						}
						
						$select .= '<option value="'.$option_key.'"'.($option_key == $value ? ' selected="selected"' : '').'>'.$option_value['name'].'</option>';
					}
				}
				
				if ($group != '')
				{
					$select .= '</optgroup>';
				}
				
				$select .= '</select>';
				
				return $select;
			} else if ($rules['type'] == 'checkbox')
			{
				return '<input type="checkbox" name="'.$prefix.$name.'"'.(isset($rules['id']) ? ' id="'.$rules['id'].'"' : '').(isset($rules['class']) ? ' class="'.$rules['class'].'"' : '').($value == 'on' ? ' checked="checked"' : '').''.(isset($rules['style']) ? ' style="'.$rules['style'].'"' : '').' />';
			} else if ($rules['type'] == 'submit')
			{
				$value = $rules['value'];
				
				return '<input type="submit" name="'.$prefix.$name.'"'.(isset($rules['id']) ? ' id="'.$rules['id'].'"' : '').(isset($rules['class']) ? ' class="'.$rules['class'].'"' : '').' value="'.$value.'"'.(isset($rules['style']) ? ' style="'.$rules['style'].'"' : '').' />';
			} else if ($rules['type'] == 'image')
			{
				return '<img src="'.($value != '' ? $value : $rules['value']).'" alt="'.(isset($rules['alt']) ? $rules['alt'] : '').'"'.(isset($rules['width']) ? ' width="'.$rules['width'].'"' : '').(isset($rules['height']) ? ' height="'.$rules['height'].'"' : '').(isset($rules['id']) ? ' id="'.$rules['id'].'"' : '').(isset($rules['class']) ? ' class="'.$rules['class'].'"' : '').''.(isset($rules['style']) ? ' style="'.$rules['style'].'"' : '').' />';
			} else if ($rules['type'] == 'upload_image')
			{
				return '<input type="button" name="'.$prefix.$name.'" class="upload_image'.(isset($rules['width']) ? ' width-'.$rules['width'] : '').(isset($rules['height']) ? ' height-'.$rules['height'] : '').'" value="'.$rules['value'].'"'.(isset($rules['style']) ? ' style="'.$rules['style'].'"' : '').' />';
			} else if ($rules['type'] == 'upload_media')
			{
				return '<input type="button" name="'.$prefix.$name.'" class="upload_media" value="'.$rules['value'].'"'.(isset($rules['style']) ? ' style="'.$rules['style'].'"' : '').' />';
			}
		}
		
		public static function snippet($filename, $data = array())
		{
			$trace = debug_backtrace();
			$file = explode(self::config('dir').'/admin', $trace[0]['file']);
			$file = '/'.str_replace('.php', '', trim($file[1], '/')).'/'.$filename.'.html';

			if (file_exists(self::dir('ether/admin/snippets/', TRUE).$file))
			{
				$snippet = self::read(self::dir('ether/admin/snippets/', TRUE).$file);
			} else if (file_exists(self::dir('admin/snippets/', TRUE).$file))
			{
				$snippet = self::read(self::dir('admin/snippets/', TRUE).$file);	
			}
			
			foreach ($data as $k => $v)
			{
				$snippet = str_replace('{$'.$k.'}', $v);
			}
			
			return $snippet;
		}
		
		// file managment
		public static function read($path)
		{
			if ( ! file_exists($path))
			{
				return FALSE;
			}
					
			if (function_exists('file_get_contents'))
			{
				return file_get_contents($path);
			}
					
			if ( ! $fp = @fopen($path, FOPEN_READ))
			{
				return FALSE;
			}
				
			flock($fp, LOCK_SH);
			$data =& fread($fp, filesize($path));	
			flock($fp, LOCK_UN);
			fclose($fp);
				
			return $data;
		}
			
		public static function write($path, $data, $append = FALSE)
		{
			$flag = $append ? 'a' : 'w';

			if ( ! $fp = fopen($path, $flag))
			{
				return FALSE;
			}
			
			if ( ! $append)
			{
				chmod($path, 0755);
			}

			flock($fp, LOCK_EX);
			fwrite($fp, $data);
			flock($fp, LOCK_UN);
			fclose($fp);
				
			return TRUE;
		}
		
		public static function module($name)
		{
			if ( ! in_array($name, self::$modules))
			{
				self::$modules[] = $name;
			}
		}

		public static function module_run()
		{
			$args = func_get_args();
			$name = array_shift($args);
			$node = explode('.', $name);
			
			if (count($node) == 2)
			{
				$node[0] = str_replace('-', '_', $node[0]);

				if (self::module_exists($node[0]))
				{
					return call_user_func_array(array('ether_'.$node[0], $node[1]), $args);
				}
			}
			
			return FALSE;
		}
		
		public static function module_exists($name)
		{
			$name = str_replace('-', '_', $name);

			return class_exists('ether_'.$name);
		}
		
		public static function wysiwig_script($data)
		{
			if (isset($data['wysiwig']))
			{
				if (is_user_logged_in() AND current_user_can('edit_posts'))
				{
					header('Content-Type: text/javascript; charset=utf-8');
					$script = '';
					$script .= '(function()
{ function ether_wysiwig_modalbox(ed, url, width, height) { ed.windowManager.open({ file: url, width: width, height: height, inline: 1 }, { plugin_url: ether.path }); }

function ether_wysiwig_shortcode(ed, tag_open, tag_close) { var content = tinyMCE.activeEditor.selection.getContent(); tinyMCE.activeEditor.selection.setContent(tag_open + content + tag_close); }
	tinymce.create(\'tinymce.plugins.ether_wysiwig\',
	{
		init: function(ed, url)
		{';
		
		foreach (self::$wysiwig as $wysiwig)
		{
			if ($wysiwig['type'] == 'modalbox')
			{
				$script .= 'ed.addCommand(\''.self::slug($wysiwig['name']).'_cmd\', function() { ether_wysiwig_modalbox(ed, \''.$wysiwig['url'].'\', '.$wysiwig['width'].', '.$wysiwig['height'].'); });';
			} else if ($wysiwig['type'] == 'shortcode')
			{
				$script .= 'ed.addCommand(\''.self::slug($wysiwig['name']).'_cmd\', function() { ether_wysiwig_shortcode(ed, \''.$wysiwig['tag_open'].'\', \''.$wysiwig['tag_close'].'\'); });';
			}
		}
		
		foreach (self::$wysiwig as $wysiwig)
		{
			if ($wysiwig['type'] != 'separator')
			{
				$script .= 'ed.addButton(\''.self::slug($wysiwig['name']).'\', {
				title: \''.$wysiwig['desc'].'\',
				cmd: \''.self::slug($wysiwig['name']).'_cmd\',
				image: \''.$wysiwig['icon'].'\'
				});';
			}
		}
		
		$script .= '

		}
	});
	
	tinymce.PluginManager.add(\'ether_wysiwig\', tinymce.plugins.ether_wysiwig);
	
})();';
					echo str_replace(array("\t", "\n"), '', $script);
				}
			}
		}
		
		public static function wysiwig_init()
		{
			if (get_user_option('rich_editing'))
			{
				add_filter('mce_external_plugins', array('ether', 'wysiwig_plugins'), 5);
				add_filter('mce_buttons_3', array('ether', 'wysiwig_buttons'), 5);
			}
		}
		
		public static function wysiwig_plugins($plugins)
		{
			$plugins['ether_wysiwig'] = self::path('ether/ether.php?wysiwig', TRUE);
			
			return $plugins;
		}
		
		public static function wysiwig_buttons($buttons)
		{
			foreach (self::$wysiwig as $button)
			{
				array_push($buttons, self::slug($button['name']));
			}
	
			return $buttons;
		}
		
		public static function shortcodes_tinymce_version($version)
		{
			return ++$version;
		}
		
		public static function slugify($input, $output_array = FALSE)
		{
			if ( ! is_array($input))
			{
				$input = explode(',', $input);
			}
			
			$output = array();
			
			foreach ($input as $part)
			{
				$output[] = ether::slug(trim($part));
			}
			
			if ( ! $output_array)
			{
				return implode(',', $output);
			}
			
			return $output;
		}
		
		public static function set_attr($element, $attr, $value, $html, $append = FALSE)
		{
			preg_match('/(<'.$element.'(.*)>)(.*)(<.*'.$element.'>)/ismU', $html, $elements);

			if (empty($elements))
			{
				return $html;
			}
			
			if ( ! empty($elements[2]))
			{
				preg_match('/('.$attr.'=[\'|"])(.*?)([\'|"])/i', $elements[1], $attributes);
			
				if ( ! empty($attributes[2]))
				{
					$once = 1;
					
					if ($append)
					{
						return str_replace($attributes[0], $attributes[1].$attributes[2].( ! empty($attributes[2]) ? ' ' : '').$value.$attributes[3], $html, $once);
					}
					
					return str_replace($attributes[0], $attributes[1].$value.$attributes[3], $html, $once);
				}
			}
			
			$element = implode(' '.$attr.'="'.$value.'">', explode('>', $elements[1]));
			
			$once = 1;

			return str_replace($elements[1], $element, $html, $once);
		}
		
		// admin area

		public static function admin_init()
		{
			self::$title = self::config('theme_name');
			self::$menu_title = self::config('theme_name');
			self::$permission = 'edit_themes';
			self::$slug = self::slug(self::$title);
					
			add_action('admin_menu', array('ether', 'admin_create'));
			add_action('admin_head', array('ether', 'admin_header'));
				
			add_action('admin_print_scripts', array('ether', 'admin_scripts'));
			add_action('admin_print_styles', array('ether', 'admin_styles'));
			
			//self::admin_panel('Ether');
			self::$panel = (isset($_GET['tab']) ? self::clean($_GET['tab']) : '');
				
			if (empty(self::$panel))
			{
				self::$panel = self::slug(self::$panels[0]['name']);
			}
				
			self::import('!admin.'.self::$panel);
			self::$controller = 'ether_panel_'.str_replace(array('/', '--', '-'), array('', '-', '_'), self::$panel);

			if (class_exists(self::$controller))
			{
				$controller = self::$controller;
				call_user_func(array($controller, 'init'));
			}
				
			add_action('admin_menu', array('ether', 'admin_metabox_create'));
		}

		public static function admin_create()
		{
			add_menu_page(self::$title, self::$menu_title, self::$permission, self::$slug, array('ether', 'admin_body'));
		}

		public static function admin_metabox_create()
		{
			foreach (self::$metaboxes as $metabox_name => $metabox_options)
			{
				$metabox = self::slug($metabox_name);
				self::import('!admin.metabox.'.$metabox);
				$metabox_class = str_replace('-', '_', $metabox);

				if (class_exists('ether_metabox_'.$metabox_class))
				{
					$metabox_class = 'ether_metabox_'.$metabox_class;
					call_user_func_array(array($metabox_class, 'set_class'), array($metabox_class));
					self::$metabox_permissions[$metabox_class] = $metabox_options['permissions'];

					call_user_func(array($metabox_class, 'init'));

					$post_types = array_merge(array('post', 'page'), array_keys(self::$custom_post_types));

					foreach ($metabox_options['permissions'] as $permission)
					{
						if (substr($permission, 0, 3) != 'id:' AND substr($permission, 0, 9) != 'template:')
						{
							add_meta_box($metabox, isset($metabox_options['title']) ? $metabox_options['title'] : $metabox_name, array($metabox_class, 'body_init'), $permission, $metabox_options['context'], $metabox_options['priority']);
						} else
						{
							foreach($post_types as $type)
							{
								add_meta_box($metabox, isset($metabox_options['title']) ? $metabox_options['title'] : $metabox_name, array($metabox_class, 'body_init'), $type, $metabox_options['context'], $metabox_options['priority']);
							}
						}
					}

					add_action('save_post', array($metabox_class, 'save_init'));
					add_action('admin_head', array($metabox_class, 'header_init'));	
					
					add_action('admin_print_scripts', array($metabox_class, 'scripts_init'));
					add_action('admin_print_styles', array($metabox_class, 'styles_init'));
				}
			}
		}
		
		public static function admin_metabox_permission($class)
		{
			return self::$metabox_permissions[$class];
		}
		
		public static function admin_reset()
		{
			foreach (self::$panels as $panel)
			{
				$panel = self::slug($panel['name']);

				self::import('!admin.'.$panel);
				$controller = 'ether_panel_'.str_replace('-', '_', $panel);
				
				if (class_exists($controller))
				{
					call_user_func(array($controller, 'reset'));
				}
			}

			foreach (self::$metaboxes as $metabox_name => $metabox_permissions)
			{
				$metabox = self::slug($metabox_name);
				self::import('!admin.metabox.'.$metabox);
				$metabox_class = str_replace('-', '_', $metabox);

				if (class_exists('ether_metabox_'.$metabox_class))
				{
					$metabox_class = 'ether_metabox_'.$metabox_class;
					call_user_func(array($metabox_class, 'reset'));
				}
			}
		}

		public static function admin_panel($name, $title = '')
		{
			if ($name == 'index')
			{
				return;
			}
			
			if (empty($title))
			{
				$title = $name;
			}
			
			if ( ! in_array($name, self::$panels))
			{
				self::$panels[] = array('name' => $name, 'title' => $title);
			}
		}

		public static function admin_metabox($name, $options = array())
		{
			$defaults = array('permissions' => array('post', 'page'), 'context' => 'normal', 'priority' => 'high');

			if ( ! array_key_exists($name, self::$metaboxes))
			{
				self::$metaboxes[$name] = array_merge($defaults, $options);
			}
		}

		public static function admin_scripts()
		{
			wp_enqueue_script('jquery');
			wp_enqueue_script('media-upload');
			wp_enqueue_script('thickbox');
			wp_enqueue_script('jquery-ui-core');
			wp_enqueue_script('jquery-ui-sortable');
			wp_enqueue_script('jquery-ui-draggable');
			wp_enqueue_script('jquery.sort', self::path('ether/media/scripts/jquery.sort.js', TRUE), array('jquery'), '1.0');
			wp_enqueue_script('ether.common', self::path('ether/media/scripts/common.js', TRUE), array('jquery', 'jquery.sort', 'jquery-ui-core', 'jquery-ui-draggable', 'jquery-ui-draggable', 'media-upload', 'thickbox'), '1.0');
		}

		public static function admin_styles()
		{
			wp_enqueue_style('thickbox');
		}

		public static function admin_body()
		{
			$body = '';
			
			if (isset($_POST['save']))
			{
				if (class_exists(self::$controller))
				{
					$controller = self::$controller;
					$body .= call_user_func(array($controller, 'save'));
				}	
			}
			
			if (isset($_POST['reset']))
			{
				if (class_exists(self::$controller))
				{
					$controller = self::$controller;
					$body .= call_user_func(array($controller, 'reset'));
				}
			}

			$body .= '<div class="wrap">';
			$body .= '<div id="icon-ether-panel" class="icon32"><br /></div>';
			$body .= '<h2><strong>'.self::config('theme_name').'</strong> '.self::langr('Theme Options').'</h2>';
			$body .= '<ul id="ether-tabs">';
				
			foreach (self::$panels as $panel)
			{
				$panel_slug = str_replace(array('/', '--'), array('', '-'), self::slug($panel['name']));
				$body .= '<li'.(self::$panel == $panel_slug ? ' class="current"' : '').'><a href="?page='.self::$slug.'&tab='.$panel_slug.'">'.$panel['title'].'</a></li>';
			}

			$body .= '</ul>';
	
			$body .= '<form method="post">';
			
			if (class_exists(self::$controller))
			{
				$controller = self::$controller;
				$body .= call_user_func(array($controller, 'body'));
			}
			
			$body .= '<fieldset class="ether submit">';
				
			if (self::$panel != 'import-export')
			{
				$body .= '<ul>
						<li>	
							<div class="buttons alignright single-row">
								<input type="submit" name="reset" value="'.self::langr('Reset all').'" />
								<input type="submit" name="save" value="'.self::langr('Save all changes').'" />
							</div>
						</li>
					</ul>';
			}

			$body .= '</fieldset></form>';
				
			$body .= '</div>';
			echo $body;
		}

		public static function admin_header()
		{
			echo '<link rel="stylesheet" href="'.self::path('ether/media/stylesheets/style.css', TRUE).'" media="all" />';
			$use_favicon = (ether::option('use_favicon') == 'on' ? TRUE : FALSE);
			
			if ($use_favicon)
			{
				$favicon_url = ether::option('favicon');
				$favicon_ext = array_pop(explode('.', $favicon_url));
				
				echo '<link rel="icon" type="image/'.$favicon_ext.'" href="'.$favicon_url.'">';
			}
			
			if (self::config('admin_logo') != FALSE)
			{
				echo '<style type="text/css"> #header-logo, #wphead h1 a { display: none !important; } #wphead h1 { background-image: url(\''.self::config('admin_logo').'\') !important; background-position: 5px 10px; background-repeat: no-repeat; width: 181px; height: 32px; }</style>';	
			}

			if (self::config('login_logo') != FALSE)
			{
				echo '<style type="text/css">body.login h1 a { background-image: url(\''.self::config('login_logo').'\') !important; width: 326px !important; height: 53px !important; } </style>';	
			}
			
			echo '<script type="text/javascript">
				var $_GET = [], hash;
				var hashes = window.location.href.slice(window.location.href.indexOf(\'?\') + 1).split(\'&\');
				
				for(var i = 0; i < hashes.length; i++)
				{
					hash = hashes[i].split(\'=\');
					$_GET.push(hash[0]);
					$_GET[hash[0]] = hash[1];
				}
			</script>';
			
			echo '<script type="text/javascript">var ether = { path: \''.self::info('url', TRUE).'/wp-content/themes/'.self::config('dir').'/\', upload_preview: null, custom_insert: null, prefix: \''.self::config('prefix').'\' };</script>';
				
			if (class_exists(self::$controller))
			{
				$controller = self::$controller;
				
				call_user_func(array($controller, 'header'));
			}
			
			add_editor_style('ether/media/stylesheets/wysiwig.css');
		}

		public static function copy($src, $dst)
		{
			if (is_file($src))
			{
				return copy($src, $dst);
			}
			
			if ( ! is_dir($dst))
			{
				mkdir($dst, 0755);
			}

			$dir = dir($src);
			
			while (FALSE !== ($item = $dir->read()))
			{
				if ($item == '.' OR $item == '..')
				{
					continue;
				}
				
				self::copy($src.'/'.$item, $dst.'/'.$item);
			}
			
			$dir->close();
			
			return TRUE;
		}
		
		public static function backup_uploads($timestamp = NULL)
		{
			$uploads = wp_upload_dir();
			$uploads = $uploads['basedir'];
			
			$date = date('Y-m-d-h-i-s');
			
			if ($timestamp != NULL)
			{
				$date = $timestamp;
			}

			$path = rtrim(self::dir('backup/uploads/', TRUE), '/').'/';
			mkdir($path.$date, 0755);
			
			return self::copy($uploads, $path.$date);
		}

		public static function backup_table($table, $timestamp = NULL, $rules = array())
		{
			$date = date('Y-m-d-h-i-s');
			
			if ($timestamp != NULL)
			{
				$date = $timestamp;
			}

			$path = rtrim(self::dir('backup/database/', TRUE), '/').'/';
			
			if ( ! is_dir($path.$date))
			{
				mkdir($path.$date, 0755);
			}

			global $table_prefix;
			global $wpdb;

			$result = mysql_query('SELECT * FROM `'.$table_prefix.$table.'`');
			
			if (mysql_errno != 0)
			{
				return FALSE;
			}

			$fields = mysql_num_fields($result);
			$field_names = array();
			
			$output = '';
			$line = '';

			for ($i = 0; $i < $fields; $i++)
			{
				$line .= self::$csv['enclosed'].str_replace(self::$csv['enclosed'], self::$csv['escaped'].self::$csv['enclosed'], stripslashes(mysql_field_name($result, $i))).self::$csv['enclosed'];
				$line .= self::$csv['separator'];
				
				$field_names[] = stripslashes(mysql_field_name($result, $i));
			}

			$output = trim(substr($line, 0, -1));
			$output .= self::$csv['terminated'];
			
			while ($row = mysql_fetch_array($result))
			{
				$line = '';
				
				for ($j = 0; $j < $fields; $j++)
				{
					if ($row[$j] == '0' OR $row[$j] != '')
					{
						// double serialization bug in wordpress
						/*if (unserialize($row[$j]) !== FALSE AND is_string(unserialize($row[$j])) AND unserialize(unserialize($row[$j])) !== FALSE)
						{
							$row[$j] = unserialize($row[$j]);
						}*/

						if (self::$csv['enclosed'] == '')
						{
							if (isset($rules['base64']) AND in_array($field_names[$j], $rules['base64']))
							{
								$line .= base64_encode($row[$j]);
							} else
							{
								$line .= $row[$j];
							}
						} else
						{
							if (isset($rules['base64']) AND in_array($field_names[$j], $rules['base64']))
							{
								$line .= self::$csv['enclosed'].str_replace(self::$csv['enclosed'], self::$csv['escaped'].self::$csv['enclosed'], base64_encode($row[$j])).self::$csv['enclosed'];
							} else
							{
								$line .= self::$csv['enclosed'].str_replace(self::$csv['enclosed'], self::$csv['escaped'].self::$csv['enclosed'], $row[$j]).self::$csv['enclosed'];
							}
						}
					} else
					{
						$line .= '';
					}
					
					if ($j < $fields-1)
					{
						$line .= self::$csv['separator'];
					}
				}
				
				$output .= $line;
				$output .= self::$csv['terminated'];
			}

			self::write($path.$date.'/'.$table.'.csv', $output);
			
			return TRUE;
		}
		
		public static function restore_uploads($timestamp)
		{
			$uploads = wp_upload_dir();
			$uploads = $uploads['basedir'];
			
			$path = rtrim(self::dir('backup/uploads/', TRUE), '/').'/';

			return self::copy($path.$timestamp, $uploads);
		}

		public static function restore_table($timestamp, $table, $rules = array())
		{
			$path = rtrim(self::dir('backup/database/', TRUE), '/').'/';
			$path .= $timestamp;

			global $table_prefix;
			global $wpdb;

			$rows = array();

			if (file_exists($path.'/'.$table.'.csv'))
			{
				if (function_exists('fgetcsv'))
				{
					$csv = fopen($path.'/'.$table.'.csv', 'r');

					while (($row = fgetcsv($csv, 1000)) !== FALSE)
					{
						$rows[] = $row;
					}
					
					fclose($csv);
				} else
				{
					$input = self::read($path.'/'.$table.'.csv');

					if ($input[strlen($input)-1] != self::$csv['terminated'])
					{
						$input .= self::$csv['terminated'];
					}
					
					$length = strlen($input);
					$row = array();
					$item = 0;
					$quoted = FALSE;
					
					for ($i = 0; $i < $length; $i++)
					{
						$ch = $input[$i];
						
						if ($ch == self::$csv['enclosed'])
						{
							$quoted = ! $quoted;
						}
						
						if ($ch == self::$csv['terminated'] AND ! $quoted)
						{
							for ($k = 0; $k < count($row); $k++)
							{
								if ($row[$k] != '' AND $row[$k][0] == self::$csv['enclosed'])
								{
									$row[$k] = substr($row[$k], 1, strlen($row[$k]) - 2);
								}
								
								$row[$k] = str_replace(str_repeat(self::$csv['enclosed'], 2), self::$csv['enclosed'], $row[$k]);
							}
							
							$rows[] = $row;
							$row = array('');
							$item = 0;
						} else if ($ch == self::$csv['separator'] AND ! $quoted)
						{
							$row[++$item] = '';
						} else
						{
							$row[$item] .= $ch;
						}
					}
				}
				
				if (isset($rules['clean']) AND $rules['clean'])
				{
					mysql_query('TRUNCATE TABLE `'.$table_prefix.$table.'`');
				}
				
				$fields = array_shift($rows);
				
				if ($rules['type'] != 'insert')
				{
					array_shift($fields);
				}
				
				foreach ($rows as $row)
				{
					if ($rules['type'] != 'insert')
					{
						array_shift($row);
					}
					
					$fields_count = count($fields);

					if ($rules['type'] == 'update')
					{
						if (isset($rules['check_callback']) && ! empty($rules['check_callback']))
						{
							if ( ! call_user_func_array($rules['check_callback'], array($row)))
							{
								continue;
							}
						}

						$update_fields = array();
						$fields_count = count($fields);

						if (isset($rules['base64']) AND ! empty($rules['base64']))
						{
							for ($i = 0; $i < $fields_count; $i++)
							{
								if (in_array($fields[$i], $rules['base64']))
								{
									$row[$i] = base64_decode($row[$i]);
								}
							}
						}
						
						for ($i = 0; $i < $fields_count; $i++)
						{
							$row[$i] = self::escape_string($row[$i]);
						}
						
						/*for ($i = 0; $i < $fields_count; $i++)
						{
							if (unserialize($row[$i]) !== FALSE AND is_string(unserialize($row[$i])) AND unserialize(unserialize($row[$i])) !== FALSE)
							{
								$row[$i] = unserialize($row[$i]);
							}
						}*/
						
						for ($i = 0; $i < $fields_count; $i++)
						{
							$update_fields[] = '`'.$fields[$i].'`=\''.$row[$i].'\'';
						}
						
						if (isset($rules['value_key']) AND isset($rules['value_index']))
						{
							mysql_query('INSERT INTO `'.$table_prefix.$table.'` (`'.implode('`, `', $fields).'`) VALUES (\''.implode('\', \'', $row).'\') ON DUPLICATE KEY UPDATE `'.$rules['value_key'].'`=\''.$row[$rules['value_index']].'\'');
						} else
						{
							mysql_query('INSERT INTO `'.$table_prefix.$table.'` (`'.implode('`, `', $fields).'`) VALUES (\''.implode('\', \'', $row).'\')');
						}
					} else
					{
						$fields_count = count($fields);
						
						if (isset($rules['base64']) AND ! empty($rules['base64']))
						{
							for ($i = 0; $i < $fields_count; $i++)
							{
								if (in_array($fields[$i], $rules['base64']))
								{
									$row[$i] = base64_decode($row[$i]);
								}
							}
						}
						
						for ($i = 0; $i < $fields_count; $i++)
						{
							$row[$i] = self::escape_string($row[$i]);
						}
						
						/*for ($i = 0; $i < $fields_count; $i++)
						{
							if (unserialize($row[$i]) !== FALSE AND is_string(unserialize($row[$i])) AND unserialize(unserialize($row[$i])) !== FALSE)
							{
								$row[$i] = unserialize($row[$i]);
							}
						}*/
						
						
						mysql_query('INSERT INTO `'.$table_prefix.$table.'` (`'.implode('`, `', $fields).'`) VALUES (\''.implode('\', \'', $row).'\')');
					}
				}
				
				return TRUE;
			}
			
			return FALSE;
		}
	}
	
	class ether_module
	{
		public static $class = 'ether_module';
		public static $actions = array('init', 'wp_head', 'admin_init', 'admin_head', 'wp_print_scripts', 'wp_print_styles', 'admin_print_scripts', 'admin_print_styles', 'wp_footer', 'admin_footer'); 
		
		public static function get_class()
		{
			if (function_exists('get_called_class'))
			{
				return get_called_class();
			} else
			{
				$objects = array();
				$traces = debug_backtrace();

				foreach ($traces as $trace)
				{
					if (isset($trace['function']) AND substr($trace['function'], 0, 14) == 'call_user_func' AND isset($trace['args'][0][0]))
					{
						return $trace['args'][0][0];
					} else if (isset($trace['object']))
					{
						if (is_object($trace['object']))
						{
							$objects[] = $trace['object'];
						}
					}
				}
				
				if (count($objects))
				{
					return get_class($objects[0]);
				}
			}
		}
		
		public static function set_class($class)
		{
			self::$class = $class;
		}
		
		public static function __module()
		{
			foreach (self::$actions as $action)
			{
				if (substr($action, 0, 3) != 'wp_')
				{
					add_action($action, array(self::$class, 'wp_'.$action));
				} else
				{
					add_action($action, array(self::$class, $action));
				}
			}
		}

		public static function init()
		{
			
		}
		
		public static function wp_init()
		{
			
		}
		
		public static function wp_head()
		{
			
		}
		
		public static function wp_admin_init()
		{
			
		}
		
		public static function wp_admin_head()
		{
			
		}
		
		public static function wp_print_scripts()
		{
			
		}
		
		public static function wp_print_styles()
		{
			
		}
		
		public static function wp_admin_print_scripts()
		{
			
		}
		
		public static function wp_admin_print_styles()
		{
			
		}
		
		public static function wp_admin_footer()
		{
			
		}
		
		public static function wp_footer()
		{
			
		}
	}

	class ether_panel extends ether_module
	{
		public static function init()
		{
			
		}
		
		public static function header()
		{
			
		}
		
		public static function body()
		{
			
		}
		
		public static function scripts()
		{
			
		}
		
		public static function styles()
		{
			
		}
		
		public static function save()
		{
			
		}
		
		public static function reset()
		{
			
		}
		
		public static function __module()
		{
			
		}
	}

	class ether_metabox extends ether_module
	{	
		public static function init()
		{

		}
		
		public static function header_init()
		{
			$class = self::get_class();
			
			if (call_user_func(array($class, 'is_permitted')))
			{
				echo call_user_func(array($class, 'header'));
			}
		}
		
		public static function header()
		{
			
		}
		
		public static function body_init()
		{
			$class = self::get_class();
			
			if (call_user_func(array($class, 'is_permitted')))
			{
				echo call_user_func(array($class, 'body'));
			}
		}
		
		public static function body()
		{
			
		}
		
		public static function scripts_init()
		{
			$class = self::get_class();
			
			if (call_user_func(array($class, 'is_permitted')))
			{
				call_user_func(array($class, 'scripts'));
			}
		}
		
		public static function scripts()
		{
			
		}
		
		public static function styles_init()
		{
			$class = self::get_class();
			
			if (call_user_func(array($class, 'is_permitted')))
			{
				call_user_func(array($class, 'styles'));
			}
		}
		
		public static function styles()
		{
			
		}
		
		public static function reset()
		{
			
		}
		
		public static function save_init()
		{
			$class = self::get_class();
			
			if (call_user_func(array($class, 'is_permitted')))
			{
				call_user_func(array($class, 'save'));
			}
		}
		
		public static function save()
		{
			
		}
		
		public static function is_permitted()
		{
			$class = self::get_class();
			$permissions = ether::admin_metabox_permission($class);

			global $post;
			
			$permitted = FALSE;
			
			if (isset($post))
			{
				foreach ($permissions as $permission)
				{
					if ($permission == $post->post_type)
					{
						$permitted = TRUE;
					} else if (substr($permission, 0, 9) == 'template:')
					{
						$template = substr($permission, 9);
						
						if ($template == get_post_meta($post->ID, '_wp_page_template', TRUE))
						{
							$permitted = TRUE;
						}
					} else if (substr($permission, 0, 3) == 'id:')
					{
						$id = substr($permission, 3);
						
						if ($id == $post->ID)
						{
							$permitted = TRUE;
						}
					}
				}
			}
			
			return $permitted;
		}
		
		public static function __module()
		{
			
		}
	}

	ether::init();
}
?>
