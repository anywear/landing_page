(function()
{
	tinymce.create('tinymce.plugins.ether_wysiwig',
	{
		init: function(ed, url)
		{
			ed.addCommand('mceether_wysiwig', function()
			{
				ed.windowManager.open
				({
					file: ether.path + 'template/misc/shortcodes.php',
					width: 500 + ed.getLang('ether_wysiwig.delta_width', 0),
					height: 300 + ed.getLang('ether_wysiwig.delta_height', 0),
					inline: 1
				},
				{
					plugin_url: ether.path
				});
			});
			/*ed.addCommand('mceilcPHP', function(){
                ilc_sel_content = tinyMCE.activeEditor.selection.getContent();
                tinyMCE.activeEditor.selection.setContent('[php]' + ilc_sel_content + '[/php]');
            })*/
			ed.addButton('fu', { title: 'asd', 'cmd': '', image: '' });

			ed.addButton('ether_wysiwig',
			{
				title: 'ether_wysiwig.desc',
				cmd: 'mceether_wysiwig',
				image: ether.path + 'ether/media/images/shortcodes.gif'
			});
			
			ed.addButton('ether_wysiwig2',
			{
				title: 'ether_shas',
				cmd: 'mceether_wysiwig',
				image: ether.path + 'ether/media/images/shortcodes.gif'
			});

		}
	});
	
	tinymce.PluginManager.add('ether_wysiwig', tinymce.plugins.ether_wysiwig);
	
})();
