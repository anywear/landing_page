$j = jQuery.noConflict();

$j( function()
{
	$html_logo = $j('input[name=' + ether.prefix + 'use_html_logo]');
	$image_logo = $j('input[name=' + ether.prefix + 'use_image_logo]');
	
	$html_logo.change( function()
	{
		if ($j(this).attr('checked') && $image_logo.attr('checked'))
		{
			$image_logo.attr('checked', '')
		}
	});
	
	$image_logo.change( function()
	{
		if ($j(this).attr('checked') && $html_logo.attr('checked'))
		{
			$html_logo.attr('checked', '');
		}
	});
	
	$j('input[name=reset]').click( function()
	{
		if ( ! confirm('Are you sure you want to reset settings on this page? \'Cancel\' to stop, \'OK\' to reset.'))
		{
			return false;
		}
	});
	
	$j('.ether img').error( function()
	{ 
		$j(this).hide();
	});
	
	$j('.ether img').load( function()
	{
		$j(this).show();
	});
	
	$j.fn.cattr = function(key, value, attribute)
	{
		if (typeof attribute == 'undefined')
		{
			attribute = 'className';
		}

		var $object = $j(this).eq(0);
		var class_name = '';

		if (key != null)
		{
			var classes = $object[0][attribute].split(' ');

			for (i = 0; i < classes.length; i++)
			{ 
				if (classes[i].substr(0, key.length) == key)
				{
					class_name = classes[i];
				}
			}
		}

		if (typeof value == 'undefined' || value == null)
		{
			return class_name.substr(key.length+1);	
		} else
		{
			if (class_name != '')
			{
				$object.attr(attribute, $object.attr(attribute).replace(class_name, key + '-' + value));
			} else
			{
				$object.attr(attribute, $object.attr(attribute) + ' ' + key + '-' + value);	
			}
		}
		
		return this;
	};
	
	$j('.hidden').hide();
	
	$j('ul.slides').sortable
	({
		handle: '.dragndrop',
		update: function(event, ui)
		{
			var $list = $j('ul.slides > li');
			
			$list.each( function()
			{
				index = $list.index(this);
					
				$j(this).children('input.slider-image-index').val(index);
			});
			
			$list.children('input.slider-image-index').trigger('change');
		}
	});
	
	$j('ul.slides input.slider-image-index').live('change', function()
	{
		var $list = $j(this).parents('ul.slides').children('li');
		
		$list.sortElements( function(a, b)
		{
			if ($j(a).children('input.slider-image-index').val() == $j(b).children('input.slider-image-index').val())
			{
				return 1;
			}
			
			return $j(a).children('input.slider-image-index').val() > $j(b).children('input.slider-image-index').val() ? 1 : -1;
		});

		$list.each( function()
		{
			index = $list.index(this);
				
			$j(this).children('input.slider-image-index').val(index);
		});
	});
	
	$j('div.custom-thumbs-options input[type=checkbox]').live('change', function()
	{
		if ($j(this).attr('checked'))
		{
			$j(this).parent('label').nextAll().show();
		} else
		{
			$j(this).parent('label').nextAll().hide();
		}
	});

	$j('div.images-holder > ul.images').each( function()
	{
		$j(this).children('li').not(':first').hide();
		
		$thumbs = $j(this).children('li').children('div.custom-thumbs-options');
		$checkbox = $thumbs.children('label').children('input[type=checkbox]');
		
		if ( ! $checkbox.attr('checked'))
		{
			$checkbox.parent('label').nextAll().hide();
		}
		
	});
	
	$j('a.toggle-images-holder').live('click', function()
	{
		$j(this).toggleClass('open');
		$j(this).next('ul.images').children('li').not(':first').toggle();
		
		return false;
	});
	
	$j('ul.slides input.remove-image').live('click', function()
	{
		if ( ! confirm('Are you sure you want to delete this item? \'Cancel\' to stop, \'OK\' to reset.'))
		{
			return false;
		}
		
		$j(this).parents('li').remove();
		$list = $j('ul.slides > li');
		
		$list.each( function()
		{
			index = $list.index(this);
			$j(this).children('input.slider-image-index').val(index);
		});
	});
	
	window.wp_send_to_editor = window.send_to_editor;

	window.send_to_editor = function(html)
	{
		/*if (ether.custom_insert != null)
		{
			tb_remove();
			ether.custom_insert = null;
		} else */if (ether.upload_preview != null)
		{
			var url = '';
			var alt = '';
			var title = '';

			if ($j('img', html).length > 0)
			{
				url = $j('img', html).attr('src');
				alt = $j('img', html).attr('alt');
				title = $j('img', html).attr('title');
			} else
			{
				url = html;
			}

			var $dst = $j(ether.upload_preview);
			var $alt = $j(ether.upload_preview + '_alt');
			var $title = $j(ether.upload_preview + '_title');

			$dst.each( function()
			{
				if ($j(this).is('img'))
				{
					$j(this).attr('src', url).show();
				} else if ($j(this).is('a'))
				{
					$j(this).attr('href', url);
				} else if ($j(this).is('input'))
				{
					$j(this).val(url);
				}
			});
			
			$alt.each( function()
			{
				if ($j(this).is('img'))
				{
					$j(this).attr('alt', alt).show();
				} else if ($j(this).is('a'))
				{
					$j(this).text(alt);
				} else if ($j(this).is('input'))
				{
					$j(this).val(alt);
				}
			});
			
			$title.each( function()
			{
				if ($j(this).is('img'))
				{
					$j(this).attr('title', title).show();
				} else if ($j(this).is('a'))
				{
					$j(this).attr('title', title);
				} else if ($j(this).is('input'))
				{
					$j(this).val(title);
				}
			});
			
			tb_remove();
			ether.upload_preview = null;
		} else
		{
			window.wp_send_to_editor(html);
		}
	};
	
	$j('.remove_image').click( function()
	{
		var name = $j(this).attr('href').replace('#', '').replace(ether.prefix, '');

		$j('input.upload_' + name).val('');
		$j('input.upload_' + name + '_alt').val('');
		$j('input.upload_' + name + '_title').val('');

		$j('img.upload_' + name).attr('src', ether.path + 'media/images/placeholders/preview-image.png');
		
		return false;
	});
	
	$j('.upload_image').live('click', function()
	{
		if ($j(this).is('input') && $j(this).attr('type') == 'button')
		{
			var name = $j(this).attr('name');
			var width = $j(this).cattr('width');
			var height = $j(this).cattr('height');
			ether.upload_preview = '.' + name.replace(ether.prefix, '');
			
			tb_show('', 'media-upload.php?&type=image&post_id=0&ether=true&output=html&width=' + width + '&height=' + height + '&TB_iframe=true');
			
			return false;
		}
	});
	
	$j('.upload_media').live('click', function()
	{
		if ($j(this).is('input') && $j(this).attr('type') == 'button')
		{
			var name = $j(this).attr('name');
			ether.upload_preview = '.' + name.replace(ether.prefix, '');
			
			tb_show('', 'media-upload.php?&post_id=0&ether=true&TB_iframe=true');
			
			return false;
		}
	});
	
	$j('.info-box, .toggle-info-box').addClass('js');
	
	$j('.toggle-info-box').hover( function()
	{
		$j(this).next('.info-box').not(':visible').stop().fadeIn();
	}, function()
	{
		$j(this).next('.info-box').animate
		({
			'opacity': 1.0,
		}, 500, function()
		{
			$j(this).fadeOut();
		});
	});
	
	$j('.info-box').hover( function()
	{
		$j(this).stop().show().css('opacity', 1);
	}, function()
	{
		$j(this).fadeOut();
	});
});
