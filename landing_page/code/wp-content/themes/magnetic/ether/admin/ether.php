<?php
	class ether_panel_ether extends ether_panel
	{
		public static function init()
		{
	
		}
		
		public static function header()
		{
			
		}
		
		public static function save()
		{

		}
			
		public static function reset()
		{

		}	
		
		public static function body()
		{
			$body = '';
			
			$body .= '<script type="text/javascript">
				jQuery( function()
				{
					jQuery(\'#poststuff .postbox .inside\').hide();
					
					var script = jQuery(\'<script>\').load( function()
					{
						jQuery(this).remove();
					}).attr(\'src\', \'http://ether.pordesign.eu/api.php?callback=ether_init&version='.urlencode(ETHER_VERSION).'&theme_name='.urlencode(ether::config('theme_name')).'&theme_version='.urlencode(ether::config('theme_version')).'&url='.urlencode(ether::info('url', TRUE)).'\');
						jQuery(\'body\').append(script);
				});
				
				function ether_init(data)
				{
					jQuery(\'#poststuff .postbox .inside\').html(data[\'content\']).slideDown();
				}
			</script>';
			
			if (isset($_POST[ether::config('prefix').'cleanup']))
			{
				ether::clean_database(TRUE, FALSE);
				ether::clean_cache();
				ether::clean_thumbnails();
				ether::admin_reset();
				
				$body .= '<div class="updated"><p>Theme cleaned!.</p></div>';
			}
			
			$body .= '<fieldset class="ether"><p>This theme is powered by Ether Framework. You can <a href="http://ether.pordesign.eu/">visit the homepage here</a>.</p>
					
			<div id="poststuff">
				<div class="postbox">
					<h3 class="hndle"><span>Ether Board</span></h3>
					<div class="inside">
						<p>Loading...</p>
					</div>
				</div>
			</div>
			<div id="ether_content"></div></fieldset>';	
			
			$body .= '<fieldset class="ether">
				<h5>Ether options</h5>
				<p class="hint"><em>Clean cache data, post meta, thumbnails and options related to this theme.</em></p>
				<ul> 
					<li> 
						'.ether::make_field('cleanup', array('type' => 'submit', 'value' => 'Clean up')).'
					</li> 
				</ul>
			</fieldset>';
			
			return $body;
		}
	}
?>
