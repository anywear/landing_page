<?php

	class ether_panel_cache extends ether_panel
	{
		public static function init()
		{
	
		}
		
		public static function header()
		{
			
		}
		
		public static function save()
		{
			ether::handle_field($_POST, array
			(
				'checkbox' => array
				(
					array
					(
						'name' => 'enable_cache',
						'value' => ''
					)
				),
				'select' => array
				(
					array
					(
						'name' => 'cache_lifetime',
						'value' => '3600'
					)
				)
			));
		}
			
		public static function reset()
		{
			ether::handle_field(array(), array
			(
				'checkbox' => array
				(
					array
					(
						'name' => 'enable_cache',
						'value' => ''
					)
				),
				'select' => array
				(
					array
					(
						'name' => 'cache_lifetime',
						'value' => '3600'
					)
				)
			));
		}	
		
		public static function body()
		{
			$body = '';
			
			if ( ! file_exists(ether::dir('ether/cache', TRUE)))
			{
				$body .= '<div id="message" class="error"><p>Make sure that "'.ether::dir('ether/cache', TRUE).'" directory exists.</p></div>';
			} else
			{
				if ( ! is_writable(ether::dir('ether/cache', TRUE)))
				{
					$body .= '<div id="message" class="error"><p>Make sure that "'.ether::dir('ether/cache', TRUE).'" directory is writable.</p></div>';
				}
			}
			
			if (isset($_POST[ether::config('prefix').'clear_cache']))
			{
				ether::clean_cache();
				$body .= '<div class="updated"><p>Cache cleared.</p></div>';
			}

			$cache_lifetime = array
			(
				'3600' => array('name' => '1 Hour'),
				'7200' => array('name' => '2 Hours'),
				'18000' => array('name' => '5 Hours'),
				'43200' => array('name' => '12 Hours'),
				'86400' => array('name' => '1 Day'),
				'172800' => array('name' => '2 Days'),
				'432000' => array('name' => '5 Days')
			);
			
			$body .= '<fieldset class="ether">		
				<ul> 
					<li> 
						<label>'.ether::make_field('enable_cache', array('type' => 'checkbox')).' Enable cache</label>
					</li> 
					<li> 
						<label>Cache lifetime</label>
						'.ether::make_field('cache_lifetime', array('type' => 'select', 'options' => $cache_lifetime)).'
					</li>
				</ul>
			</fieldset>
			<fieldset class="ether">		
				<ul> 
					<li> 
						<label>Clear cache</label>
						'.ether::make_field('clear_cache', array('type' => 'submit', 'value' => 'Clear cache')).'
					</li> 
				</ul>
			</fieldset>';
			
			return $body;
		}
	}
?>
