/**
 * Base
 *
 * Copyright (c) 2010 Por Design (pordesign.eu)
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
 *
 */
(function($)
{
	$.fn.placeholder = function(options)
	{
		$(this).each( function()
		{
			if ($(this).attr('name') != '' && $(this).attr('name') in options)
			{
				var placeholder = options[$(this).attr('name')];

				$(this).data('placeholder', placeholder);
				
				if ($(this).val() == '')
				{
					$(this).val(placeholder);
					$(this).addClass('placeholder');
				}
			} else
			{
				$(this).data('placeholder', '');
			}
		});

		$(this).focus( function()
		{
			if ($(this).val() == $(this).data('placeholder'))
			{
				$(this).val('');
				$(this).removeClass('placeholder');
			}
		});
			
		$(this).blur( function()
		{
			if ($(this).val() == '')
			{
				$(this).val($(this).data('placeholder'));
				$(this).addClass('placeholder');
			}
		});

		$(this).parents('form').submit( function()
		{
			$(this).find('input, textarea').each( function()
			{
				if ($(this).data('placeholder') != '')
				{
					if ($(this).val() == $(this).data('placeholder'))
					{
						$(this).val('');
					}
				}
				
				$(this).blur();
			});
		});

		return this;
	}
})(jQuery);

(function($)
{
	$.fn.slider = function(options)
	{
		var $sliders = $(this);
		
		var options = $.extend
		({
			nav: 'ul.nav',
			pagination: 'ul.pagination',
			items: 'ul.items',
			item: options.item,
			visible: 3,
			slide: 1,
			speed: 200,
			auto_width: true,
			width: null,
			height: null,
			wrapper_class: 'slider-wrapper',
			fade: false,
			auto_slide: false,
			auto_delay: 500,
			auto_direction: 'right',
			easing: 'swing',
			auto_height: true,
			auto_height_parent: false,
			count: null,
			axis: 'x',
			on_show: null
		}, options);
		
		$sliders.each( function()
		{
			var $slider = $(this);
			
			var $slider_nav = $(this).children(options.nav);
			var $slider_pagination = $(this).children(options.pagination);
			var $slider_items = $(this).children(options.items);
				
			$slider_items.after($slider_items.clone().hide());
				
			var $slider_items_content = $(this).children(options.items).eq(1);
			
			$slider_items.css
			({
				'position': 'absolute',
				'width': (options.axis == 'x' ? 9999 : 'auto'),
				'height': (options.axis == 'y' ? 9999 : 'auto')
			}).wrap
			(
				$('<div />').addClass(options.wrapper_class).css
				({
					'position': 'relative',
					'overflow': 'hidden',
					'height': (options.axis == 'x' ? $slider_items.outerHeight(true) : 'auto'),
					'width': (options.auto_width ? 'auto': options.width)
				})
			).children(options.item).css
			({
				'float': 'left'
			});
			
			if (options.count == null)
			{
				options.count = $slider_items.children(options.item).length;
			}
				
			$slider_items.children(options.item).remove();
			$slider_items.data('last-index', options.visible);
			
			var height = 0;
			
			for (i = 0; i < options.visible; i++)
			{
				$item = $slider_items_content.children(options.item).eq(i).clone();
				
				if (options.on_show != null)
				{
					options.on_show($item);
				}
				
				$slider_items.append($item);
				
				if (options.auto_height)
				{
					if (options.axis == 'x')
					{
						if ($slider_items.children(options.item).last().outerHeight(true) > height)
						{
							height = $slider_items.children(options.item).last().outerHeight(true);
						}
					} else
					{
						height += $slider_items.children(options.item).last().outerHeight(true);
					}
				}
			}

			if (options.auto_height && height != 0)
			{
				if (options.auto_height_parent)
				{
					$slider_items.parent('div.' + options.wrapper_class).parent().height(height);
				} else
				{
					$slider_items.parent('div.' + options.wrapper_class).height(height);
				}
			}
			
			if ( ! options.auto_height && options.height != null)
			{
				$slider_items.parent('div.' + options.wrapper_class).height(options.height);
			}
			
			slider_item = function()
			{
				var $slider = $(this).data('slider');
				var options = $slider.data('options');
				var $slider_pagination = $slider.children(options.pagination);
				var $slider_items = $slider.children('div.' + options.wrapper_class).children(options.items);				
				var $slider_items_content = $slider.children(options.items);
				var item = $slider_pagination.children(options.item).index(this);
				
				if (options.count > options.visible)
				{
					if ($slider_items.is(':not(:animated)'))
					{
						var start_index = $slider_items.data('last-index') - 1;
						var height = 0;

						if (item < start_index)
						{
							var $item = $slider_items_content.children(options.item).eq(item).clone();
							
							if (options.on_show)
							{
								options.on_show($item);
							}
							
							$slider_items.prepend($item);
								
							if (options.auto_height)
							{
								if (options.axis == 'x')
								{
									if ($slider_items.children(options.item).first().outerHeight(true))
									{
										height = $slider_items.children(options.item).first().outerHeight(true);
									}
								} else
								{
									height += $slider_items.children(options.item).first().outerHeight(true);
								}
							}
								
							$slider_items.data('last-index', (item + 1 >= options.count ? 0 : item + 1));
						} else if (item > start_index)
						{
							var $item = $slider_items_content.children(options.item).eq(item).clone();
							
							if (options.on_show != null)
							{
								options.on_show($item);
							}
							
							$slider_items.append($item);
							
							if (options.auto_height)
							{
								if (options.axis == 'x')
								{
									if ($slider_items.children(options.item).last().outerHeight(true))
									{
										height = $slider_items.children(options.item).last().outerHeight(true);
									}
								} else
								{
									height += $slider_items.children(options.item).last().outerHeight(true);
								}
							}
							
							$slider_items.data('last-index', (item + 1 >= options.count ? 0 : item + 1));
						}
						
						if (item != start_index)
						{
							$slider_pagination.children(options.item).removeClass('current').eq(item).addClass('current');
							
							if (item < start_index)
							{
								$slider_items.css((options.axis == 'x' ? 'left' : 'top'), -(options.axis == 'x' ? $slider_items.children(options.item).outerWidth(true) : $slider_items.children(options.item).outerHeight(true)) * options.slide);
							}
								
							if (options.fade)
							{
								if (item < start_index)
								{
									$slider_items.children(options.item).last().fadeOut(options.speed);
								} else
								{
									$slider_items.children(options.item).first().fadeOut(options.speed);
								}
							}
							
							if (options.auto_height && height != 0)
							{
								if (options.auto_height_parent)
								{
									$slider_items.parent('div.' + options.wrapper_class).parent().animate
									({
										'height': height
									}, options.speed);
								} else
								{
									$slider_items.parent('div.' + options.wrapper_class).animate
									({
										'height': height
									}, options.speed);
								}
							}
							
							if (item < start_index)
							{
								if (options.axis == 'x')
								{
									$slider_items.animate
									({
										'left': '+=' + $slider_items.children(options.item).outerWidth(true) * options.slide
									}, options.speed, options.easing, function()
									{
										for (i = 0; i < options.slide; i++)
										{
											$slider_items.children(options.item).last().remove();
										}
									});
								} else
								{
									$slider_items.animate
									({
										'top': '+=' + $slider_items.children(options.item).outerHeight(true) * options.slide
									}, options.speed, options.easing, function()
									{
										for (i = 0; i < options.slide; i++)
										{
											$slider_items.children(options.item).last().remove();
										}
									});
								}
							} else
							{
								if (options.axis == 'x')
								{
									$slider_items.animate
									({
										'left': '-=' + $slider_items.children(options.item).outerWidth(true) * options.slide
									}, options.speed, options.easing, function()
									{
										$slider_items.css('left', 0);

										for (i = 0; i < options.slide; i++)
										{
											$slider_items.children(options.item).first().remove();
										}
									});
								} else
								{
									$slider_items.animate
									({
										'top': '-=' + $slider_items.children(options.item).outerHeight(true) * options.slide
									}, options.speed, options.easing, function()
									{
										$slider_items.css('top', 0);

										for (i = 0; i < options.slide; i++)
										{
											$slider_items.children(options.item).first().remove();
										}
									});
								}
							}
						}
					}
				}
					
				return false;
			};
				
			slide_left = function()
			{
				var $slider = $(this).data('slider');
				var options = $slider.data('options');
				var $slider_nav = $slider.children(options.nav);
				var $slider_pagination = $slider.children(options.pagination);
				var $slider_items = $slider.children('div.' + options.wrapper_class).children(options.items);				
				var $slider_items_content = $slider.children(options.items);

				if (options.count > options.visible)
				{
					if ($slider_items.is(':not(:animated)'))
					{
						var start_index = $slider_items.data('last-index') - options.visible - 1;
						var height = 0;
						
						for (i = start_index; i > start_index - options.slide ; i--)
						{
							if (i < 0)
							{
								var index = $slider_items_content.children(options.item).length + i;
							} else
							{
								var index = i;
							}
							
							var $item = $slider_items_content.children(options.item).eq(index).clone();
							
							if (options.on_show != null)
							{
								options.on_show($item);
							}
			
							$slider_items.prepend($item);
							
							if (options.auto_height)
							{
								if (options.axis == 'x')
								{
									if ($slider_items.children(options.item).first().outerHeight(true))
									{
										height = $slider_items.children(options.item).first().outerHeight(true);
									}
								} else
								{
									height += $slider_items.children(options.item).first().outerHeight(true);
								}
							}
							
							$slider_items.data('last-index', index + options.visible);
						}
						
						$slider_pagination.children(options.item).removeClass('current').eq(index).addClass('current');
						
						$slider_items.css((options.axis == 'x' ? 'left' : 'top'), -(options.axis == 'x' ? $slider_items.children(options.item).outerWidth(true) : $slider_items.children(options.item).outerHeight(true)) * options.slide);
							
						if (options.fade)
						{
							for (i = $slider_items.children(options.item).length - 1; i > $slider_items.children(options.item).length - options.slide - 1; i--)
							{
								$slider_items.children(options.item).eq(i).fadeOut(options.speed);
							}
						}
						
						if (options.auto_height && height != 0)
						{
							if (options.auto_height_parent)
							{
								$slider_items.parent('div.' + options.wrapper_class).parent().animate
								({
									'height': height
								}, options.speed);
							} else
							{
								$slider_items.parent('div.' + options.wrapper_class).animate
								({
									'height': height
								}, options.speed);
							}
						}
						
						if (options.axis == 'x')
						{
							$slider_items.animate
							({
								'left': '+=' + $slider_items.children(options.item).outerWidth(true) * options.slide
							}, options.speed, options.easing, function()
							{
								for (i = 0; i < options.slide; i++)
								{
									$slider_items.children(options.item).last().remove();
								}
							});
						} else
						{
							$slider_items.animate
							({
								'top': '+=' + $slider_items.children(options.item).outerHeight(true) * options.slide
							}, options.speed, options.easing, function()
							{
								for (i = 0; i < options.slide; i++)
								{
									$slider_items.children(options.item).last().remove();
								}
							});
						}
					}
				}
					
				return false;
			};
			
			slide_right = function()
			{
				var $slider = $(this).data('slider');
				var options = $slider.data('options');
				var $slider_nav = $slider.children(options.nav);
				var $slider_items = $slider.children('div.' + options.wrapper_class).children(options.items);
				var $slider_items_content = $slider.children(options.items);
				
				if (options.count > options.visible)
				{
					if ($slider_items.is(':not(:animated)'))
					{
						var start_index = $slider_items.data('last-index');
						var height = 0;
						
						for (i = start_index; i < start_index + options.slide ; i++)
						{
							if (i >= $slider_items_content.children(options.item).length)
							{
								var index = i - $slider_items_content.children(options.item).length;
							} else
							{
								var index = i;
							}
							
							var $item = $slider_items_content.children(options.item).eq(index).clone();
							
							if (options.on_show != null)
							{
								options.on_show($item);
							}
							
							$slider_items.append($item);
							
							if (options.auto_height)
							{
								if (options.axis == 'x')
								{
									if ($slider_items.children(options.item).last().outerHeight(true))
									{
										height = $slider_items.children(options.item).last().outerHeight(true);
									}
								} else
								{
									height += $slider_items.children(options.item).last().outerHeight(true);
								}
							}
							
							$slider_items.data('last-index', index + 1);
						}
						
						$slider_pagination.children(options.item).removeClass('current').eq(index).addClass('current');
							
						if (options.fade)
						{
							for (i = 0; i < options.slide; i++)
							{
								$slider_items.children(options.item).eq(i).fadeOut(options.speed);
							}
						}
						
						if (options.auto_height && height != 0)
						{
							if (options.auto_height_parent)
							{
								$slider_items.parent('div.' + options.wrapper_class).parent().animate
								({
									'height': height
								}, options.speed);
							} else
							{
								$slider_items.parent('div.' + options.wrapper_class).animate
								({
									'height': height
								}, options.speed);
							}
						}
						
						if (options.axis == 'x')
						{
							$slider_items.animate
							({
								'left': '-=' + $slider_items.children(options.item).outerWidth(true) * options.slide
							}, options.speed, options.easing, function()
							{
								$slider_items.css('left', 0);

								for (i = 0; i < options.slide; i++)
								{
									$slider_items.children(options.item).first().remove();
								}
							});
						} else
						{
							$slider_items.animate
							({
								'top': '-=' + $slider_items.children(options.item).outerHeight(true) * options.slide
							}, options.speed, options.easing, function()
							{
								$slider_items.css('top', 0);
								
								for (i = 0; i < options.slide; i++)
								{
									$slider_items.children(options.item).first().remove();
								}
							});
						}
					}
				}
					
				return false;
			};
			
			$slider.data('options', options);
			$slider_nav.children('li.prev').data('slider', $slider).click(slide_left);
			$slider_nav.children('li.next').data('slider', $slider).click(slide_right);
			
			$slider_pagination.children(options.item).data('slider', $slider).click(slider_item);
				
			if (options.auto_slide)
			{
				auto_timeout = setTimeout( function()
				{
					setInterval( function()
					{
						if (options.auto_direction == 'left')
						{
							$slider.children(options.nav).children('li.prev').click();
						} else
						{
							$slider.children(options.nav).children('li.next').click();
						}
					}, options.auto_delay);
					
					clearTimeout(auto_timeout);
				}, options.auto_delay);
			}
		});
	
		return this;
	}
})(jQuery);

(function($)
{
	$.fn.tabs = function()
	{
		$(this).each( function()
		{
			var $nav = $(this).children('ul.nav').children('li');
			var $content = $(this).children('ul.content ').children('li');
			var index = 0;
			
			if ($nav.filter('.current').length > 0)
			{
				index = $nav.index($nav.filter('.current'));
			}
			
			$nav.removeClass('current').eq(index).addClass('current');
			$content.hide().eq(index).show();
			
			$nav.click( function()
			{
				$nav = $(this).parent('ul.nav').children('li');
				$content = $(this).parents('.tabs').children('ul.content').children('li');
				
				index = $nav.index(this);

				$nav.removeClass('current').eq(index).addClass('current');
				$content.hide().eq(index).show();
				
				return false;
			});
		});

		return this;
	}
})(jQuery);


(function($)
{
	$.imgload_images = [];

	$.fn.imgload = function(src, options)
	{
		var options = $.extend
		({
			path: '',
			use_parent: false
		}, options);

		$(this).each( function()
		{
			var exists = false;

			for (var i = 0; i < $.imgload_images.length; i++)
			{
				if ($.imgload_images[i] == src)
				{
					exists = true;
					break;
				}
			}
			
			if ($(this).attr('src') == src)
			{
				exists = true;
			}
			
			if ( ! exists)
			{
				$img = $(this);

				var left = $img.position().left;
				var top = $img.position().top;
				
				if (options.use_parent)
				{
					var width = $img.parent().width();
					var height = $img.parent().height();
				} else
				{
					var width = $img.width();
					var height = $img.height();
				}

				$(this).next('.loading').remove();
				$(this).next('.loading-wrapper').remove();
				
				$loading_wrapper = $('<div />');
				$loading_wrapper.addClass('loading-wrapper');
				$loading_wrapper.css
				({
					'background': '#000',
					'position': 'absolute',
					'left': (options.use_parent ? 0 : left),
					'top': (options.use_parent ? 0 : top),
					'width': width,
					'height': height,
					'z-index': 6666,
					'opacity': 0.8
				}).hide();
				
				$loading = $('<div />');
				$loading.addClass('loading');
				
				$loading.css
				({
					'background': 'transparent url(' + options.path + 'loading.gif) 50% 50% no-repeat',
					'position': 'absolute',
					'left': (options.use_parent ? 0 : left),
					'top': (options.use_parent ? 0 : top),
					'width': width,
					'height': height,
					'z-index': 6667
				}).hide();
				
				$loading.show();
				$loading_wrapper.show();
				
				$(this).after($loading_wrapper);
				$(this).after($loading);
				
				if (typeof src == 'undefined')
				{
					src = $(this).attr('src');
				}

				$(this).after($('<img />').addClass('loading-img').attr('src', src).bind('load', function()
				{
					$(this).prev('img').trigger('loaded');
					$.imgload_images.push($(this).attr('src').split('?')[0]);
				}).hide())

				$(this).bind('loaded', function()
				{
					$(this).attr('src', $(this).nextAll('img.loading-img').attr('src'));
					$(this).unbind('loaded');
					
					$(this).nextAll('img.loading-img').remove();
					$(this).nextAll('.loading').fadeOut(function() { $(this).remove(); });
					$(this).nextAll('.loading-wrapper').fadeOut(function() { $(this).remove(); });
				});
			} else
			{
				$(this).attr('src', src);
			}
		});
		
		return this;
	};
})(jQuery);

(function($)
{
	$.fn.load_begin = function(options)
	{
		var options = $.extend
		({
			path: '',
			use_parent: false,
			opacity: 0.8,
			absolute: true,
			follow: false,
			width: null,
			height: null,
			image: 'loading.gif'
		}, options);

		$(this).each( function()
		{
			$element = $(this);

			var left = $element.position().left;
			var top = $element.position().top;
			
			if (options.use_parent)
			{
				var width = $element.parent().width();
				var height = $element.parent().height();
			} else
			{
				var width = (options.width != null ? options.width : $element.width());
				var height = (options.height != null ? options.height: $element.height());
			}

			$(this).next('.loading').remove();
			$(this).next('.loading-wrapper').remove();
			
			$loading_wrapper = $('<div />');
			$loading_wrapper.addClass('loading-wrapper');
			$loading_wrapper.css
			({
				'background': '#000',
				'position': options.absolute ? 'absolute' : 'fixed',
				'left': (options.use_parent ? 0 : left),
				'top': (options.use_parent ? 0 : top),
				'width': width,
				'height': height,
				'z-index': 9998,
				'opacity': options.opacity
			}).hide();
			
			$loading = $('<div />');
			$loading.addClass('loading');
			
			$loading.css
			({
				'background': 'transparent url(' + options.path + options.image + ') 50% 50% no-repeat',
				'position': options.absolute ? 'absolute' : 'fixed',
				'left': (options.use_parent ? 0 : left),
				'top': (options.use_parent ? 0 : top),
				'width': width,
				'height': height,
				'z-index': 9999
			}).hide();
			
			$loading.fadeIn();
			$loading_wrapper.fadeIn();
			
			$(this).after($loading_wrapper);
			$(this).after($loading);
		});
		
		return this;
	};

	$.fn.load_end = function(options)
	{
		var options = $.extend
		({
			path: '',
			use_parent: false
		}, options);

		$(this).each( function()
		{
			$(this).next('.loading').remove();
			$(this).next('.loading-wrapper').remove();
		});
		
		return this;
	};
})(jQuery);

(function($)
{
	$.fn.tooltip = function(options)
	{
		var $tooltips = $(this);
		
		var options = $.extend
		({
			index: 9999,
			tooltip: 'div.tooltip',
			fade: false,
			fade_speed: 500,
			x: 0,
			y: 0,
			sub_width: false,
			sub_height: false,
			follow: true,
			offset: true,
			standalone: false
		}, options);
		
		var $tooltip_trigger = $tooltips;
			
		if (options.follow)
		{
			$tooltip_trigger.css
			({
				'position': 'relative'
			});
				
			$tooltip_trigger.children(options.tooltip).css
			({
				'position': 'absolute',
				'z-index': options.index
			});
		}
			
		$tooltip_trigger.live('mouseenter', function(event)
		{
			var $tooltip = $(this).children(options.tooltip);
	
			if (options.standalone)
			{
				$('body').append($tooltip.clone());
					
				$tooltip = $('body > ' + options.tooltip).last();
			}
				
			if (options.fade)
			{
				$tooltip.not(':animated').fadeIn(options.fade_speed);
			} else
			{
				$tooltip.show();
			}
		});
			
		$tooltip_trigger.live('mouseleave', function()
		{
			var $tooltip = $(this).children(options.tooltip);
				
			if (options.standalone)
			{
				$tooltip = $('body > ' + options.tooltip).last();
			}
				
			if (options.fade)
			{
				$tooltip.fadeOut(options.speed, function()
				{
					if (options.standalone)
					{
						$tooltip.remove();
					}
				});
			} else
			{
				$tooltip.hide();
					
				if (options.standalone)
				{
					$tooltip.remove();
				}
			}
		});
			
		if (options.follow)
		{
			$tooltip_trigger.live('mousemove', function(event)
			{
				var $this = $(this);
				var $tooltip = $(this).children(options.tooltip);
				
				if (options.standalone)
				{
					$tooltip = $('body > ' + options.tooltip).last();
				}

				$tooltip.css
				({
					'top': event.pageY + (options.offset && ! options.standalone ? -$this.offset().top : 0) + options.y - (options.sub_height ? $tooltip.height() : 0),
					'left': event.pageX + (options.offset && ! options.standalone ? -$this.offset().left : 0) + options.x - (options.sub_width ? $tooltip.width() : 0)
				});
			});
		}
	
		return this;
	}
})(jQuery);
