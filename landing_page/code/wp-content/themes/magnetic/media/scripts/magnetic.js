/*
 * Shadowbox.js, version 3.0.3
 * http://shadowbox-js.com/
 *
 * Copyright 2007-2010, Michael J. I. Jackson
 * Date: 2011-01-14 08:48:37 +0000
 */
(function(window,undefined){var S={version:"3.0.3"};var ua=navigator.userAgent.toLowerCase();if(ua.indexOf("windows")>-1||ua.indexOf("win32")>-1){S.isWindows=true}else{if(ua.indexOf("macintosh")>-1||ua.indexOf("mac os x")>-1){S.isMac=true}else{if(ua.indexOf("linux")>-1){S.isLinux=true}}}S.isIE=ua.indexOf("msie")>-1;S.isIE6=ua.indexOf("msie 6")>-1;S.isIE7=ua.indexOf("msie 7")>-1;S.isGecko=ua.indexOf("gecko")>-1&&ua.indexOf("safari")==-1;S.isWebKit=ua.indexOf("applewebkit/")>-1;var inlineId=/#(.+)$/,galleryName=/^(light|shadow)box\[(.*?)\]/i,inlineParam=/\s*([a-z_]*?)\s*=\s*(.+)\s*/,fileExtension=/[0-9a-z]+$/i,scriptPath=/(.+\/)shadowbox\.js/i;var open=false,initialized=false,lastOptions={},slideDelay=0,slideStart,slideTimer;S.current=-1;S.dimensions=null;S.ease=function(state){return 1+Math.pow(state-1,3)};S.errorInfo={fla:{name:"Flash",url:"http://www.adobe.com/products/flashplayer/"},qt:{name:"QuickTime",url:"http://www.apple.com/quicktime/download/"},wmp:{name:"Windows Media Player",url:"http://www.microsoft.com/windows/windowsmedia/"},f4m:{name:"Flip4Mac",url:"http://www.flip4mac.com/wmv_download.htm"}};S.gallery=[];S.onReady=noop;S.path=null;S.player=null;S.playerId="sb-player";S.options={animate:true,animateFade:true,autoplayMovies:true,continuous:false,enableKeys:true,flashParams:{bgcolor:"#000000",allowfullscreen:true},flashVars:{},flashVersion:"9.0.115",handleOversize:"resize",handleUnsupported:"link",onChange:noop,onClose:noop,onFinish:noop,onOpen:noop,showMovieControls:true,skipSetup:false,slideshowDelay:0,viewportPadding:20};S.getCurrent=function(){return S.current>-1?S.gallery[S.current]:null};S.hasNext=function(){return S.gallery.length>1&&(S.current!=S.gallery.length-1||S.options.continuous)};S.isOpen=function(){return open};S.isPaused=function(){return slideTimer=="pause"};S.applyOptions=function(options){lastOptions=apply({},S.options);apply(S.options,options)};S.revertOptions=function(){apply(S.options,lastOptions)};S.init=function(options,callback){if(initialized){return}initialized=true;if(S.skin.options){apply(S.options,S.skin.options)}if(options){apply(S.options,options)}if(!S.path){var path,scripts=document.getElementsByTagName("script");for(var i=0,len=scripts.length;i<len;++i){path=scriptPath.exec(scripts[i].src);if(path){S.path=path[1];break}}}if(callback){S.onReady=callback}bindLoad()};S.open=function(obj){if(open){return}var gc=S.makeGallery(obj);S.gallery=gc[0];S.current=gc[1];obj=S.getCurrent();if(obj==null){return}S.applyOptions(obj.options||{});filterGallery();if(S.gallery.length){obj=S.getCurrent();if(S.options.onOpen(obj)===false){return}open=true;S.skin.onOpen(obj,load)}};S.close=function(){if(!open){return}open=false;if(S.player){S.player.remove();S.player=null}if(typeof slideTimer=="number"){clearTimeout(slideTimer);slideTimer=null}slideDelay=0;listenKeys(false);S.options.onClose(S.getCurrent());S.skin.onClose();S.revertOptions()};S.play=function(){if(!S.hasNext()){return}if(!slideDelay){slideDelay=S.options.slideshowDelay*1000}if(slideDelay){slideStart=now();slideTimer=setTimeout(function(){slideDelay=slideStart=0;S.next()},slideDelay);if(S.skin.onPlay){S.skin.onPlay()}}};S.pause=function(){if(typeof slideTimer!="number"){return}slideDelay=Math.max(0,slideDelay-(now()-slideStart));if(slideDelay){clearTimeout(slideTimer);slideTimer="pause";if(S.skin.onPause){S.skin.onPause()}}};S.change=function(index){if(!(index in S.gallery)){if(S.options.continuous){index=(index<0?S.gallery.length+index:0);if(!(index in S.gallery)){return}}else{return}}S.current=index;if(typeof slideTimer=="number"){clearTimeout(slideTimer);slideTimer=null;slideDelay=slideStart=0}S.options.onChange(S.getCurrent());load(true)};S.next=function(){S.change(S.current+1)};S.previous=function(){S.change(S.current-1)};S.setDimensions=function(height,width,maxHeight,maxWidth,topBottom,leftRight,padding,preserveAspect){var originalHeight=height,originalWidth=width;var extraHeight=2*padding+topBottom;if(height+extraHeight>maxHeight){height=maxHeight-extraHeight}var extraWidth=2*padding+leftRight;if(width+extraWidth>maxWidth){width=maxWidth-extraWidth}var changeHeight=(originalHeight-height)/originalHeight,changeWidth=(originalWidth-width)/originalWidth,oversized=(changeHeight>0||changeWidth>0);if(preserveAspect&&oversized){if(changeHeight>changeWidth){width=Math.round((originalWidth/originalHeight)*height)}else{if(changeWidth>changeHeight){height=Math.round((originalHeight/originalWidth)*width)}}}S.dimensions={height:height+topBottom,width:width+leftRight,innerHeight:height,innerWidth:width,top:Math.floor((maxHeight-(height+extraHeight))/2+padding),left:Math.floor((maxWidth-(width+extraWidth))/2+padding),oversized:oversized};return S.dimensions};S.makeGallery=function(obj){var gallery=[],current=-1;if(typeof obj=="string"){obj=[obj]}if(typeof obj.length=="number"){each(obj,function(i,o){if(o.content){gallery[i]=o}else{gallery[i]={content:o}}});current=0}else{if(obj.tagName){var cacheObj=S.getCache(obj);obj=cacheObj?cacheObj:S.makeObject(obj)}if(obj.gallery){gallery=[];var o;for(var key in S.cache){o=S.cache[key];if(o.gallery&&o.gallery==obj.gallery){if(current==-1&&o.content==obj.content){current=gallery.length}gallery.push(o)}}if(current==-1){gallery.unshift(obj);current=0}}else{gallery=[obj];current=0}}each(gallery,function(i,o){gallery[i]=apply({},o)});return[gallery,current]};S.makeObject=function(link,options){var obj={content:link.href,title:link.getAttribute("title")||"",link:link};if(options){options=apply({},options);each(["player","title","height","width","gallery"],function(i,o){if(typeof options[o]!="undefined"){obj[o]=options[o];delete options[o]}});obj.options=options}else{obj.options={}}if(!obj.player){obj.player=S.getPlayer(obj.content)}var rel=link.getAttribute("rel");if(rel){var match=rel.match(galleryName);if(match){obj.gallery=escape(match[2])}each(rel.split(";"),function(i,p){match=p.match(inlineParam);if(match){obj[match[1]]=match[2]}})}return obj};S.getPlayer=function(content){if(content.indexOf("#")>-1&&content.indexOf(document.location.href)==0){return"inline"}var q=content.indexOf("?");if(q>-1){content=content.substring(0,q)}var ext,m=content.match(fileExtension);if(m){ext=m[0].toLowerCase()}if(ext){if(S.img&&S.img.ext.indexOf(ext)>-1){return"img"}if(S.swf&&S.swf.ext.indexOf(ext)>-1){return"swf"}if(S.flv&&S.flv.ext.indexOf(ext)>-1){return"flv"}if(S.qt&&S.qt.ext.indexOf(ext)>-1){if(S.wmp&&S.wmp.ext.indexOf(ext)>-1){return"qtwmp"}else{return"qt"}}if(S.wmp&&S.wmp.ext.indexOf(ext)>-1){return"wmp"}}return"iframe"};function filterGallery(){var err=S.errorInfo,plugins=S.plugins,obj,remove,needed,m,format,replace,inlineEl,flashVersion;for(var i=0;i<S.gallery.length;++i){obj=S.gallery[i];remove=false;needed=null;switch(obj.player){case"flv":case"swf":if(!plugins.fla){needed="fla"}break;case"qt":if(!plugins.qt){needed="qt"}break;case"wmp":if(S.isMac){if(plugins.qt&&plugins.f4m){obj.player="qt"}else{needed="qtf4m"}}else{if(!plugins.wmp){needed="wmp"}}break;case"qtwmp":if(plugins.qt){obj.player="qt"}else{if(plugins.wmp){obj.player="wmp"}else{needed="qtwmp"}}break}if(needed){if(S.options.handleUnsupported=="link"){switch(needed){case"qtf4m":format="shared";replace=[err.qt.url,err.qt.name,err.f4m.url,err.f4m.name];break;case"qtwmp":format="either";replace=[err.qt.url,err.qt.name,err.wmp.url,err.wmp.name];break;default:format="single";replace=[err[needed].url,err[needed].name]}obj.player="html";obj.content='<div class="sb-message">'+sprintf(S.lang.errors[format],replace)+"</div>"}else{remove=true}}else{if(obj.player=="inline"){m=inlineId.exec(obj.content);if(m){inlineEl=get(m[1]);if(inlineEl){obj.content=inlineEl.innerHTML}else{remove=true}}else{remove=true}}else{if(obj.player=="swf"||obj.player=="flv"){flashVersion=(obj.options&&obj.options.flashVersion)||S.options.flashVersion;if(S.flash&&!S.flash.hasFlashPlayerVersion(flashVersion)){obj.width=310;obj.height=177}}}}if(remove){S.gallery.splice(i,1);if(i<S.current){--S.current}else{if(i==S.current){S.current=i>0?i-1:i}}--i}}}function listenKeys(on){if(!S.options.enableKeys){return}(on?addEvent:removeEvent)(document,"keydown",handleKey)}function handleKey(e){if(e.metaKey||e.shiftKey||e.altKey||e.ctrlKey){return}var code=keyCode(e),handler;switch(code){case 81:case 88:case 27:handler=S.close;break;case 37:handler=S.previous;break;case 39:handler=S.next;break;case 32:handler=typeof slideTimer=="number"?S.pause:S.play;break}if(handler){preventDefault(e);handler()}}function load(changing){listenKeys(false);var obj=S.getCurrent();var player=(obj.player=="inline"?"html":obj.player);if(typeof S[player]!="function"){throw"unknown player "+player}if(changing){S.player.remove();S.revertOptions();S.applyOptions(obj.options||{})}S.player=new S[player](obj,S.playerId);if(S.gallery.length>1){var next=S.gallery[S.current+1]||S.gallery[0];if(next.player=="img"){var a=new Image();a.src=next.content}var prev=S.gallery[S.current-1]||S.gallery[S.gallery.length-1];if(prev.player=="img"){var b=new Image();b.src=prev.content}}S.skin.onLoad(changing,waitReady)}function waitReady(){if(!open){return}if(typeof S.player.ready!="undefined"){var timer=setInterval(function(){if(open){if(S.player.ready){clearInterval(timer);timer=null;S.skin.onReady(show)}}else{clearInterval(timer);timer=null}},10)}else{S.skin.onReady(show)}}function show(){if(!open){return}S.player.append(S.skin.body,S.dimensions);S.skin.onShow(finish)}function finish(){if(!open){return}if(S.player.onLoad){S.player.onLoad()}S.options.onFinish(S.getCurrent());if(!S.isPaused()){S.play()}listenKeys(true)}if(!Array.prototype.indexOf){Array.prototype.indexOf=function(obj,from){var len=this.length>>>0;from=from||0;if(from<0){from+=len}for(;from<len;++from){if(from in this&&this[from]===obj){return from}}return -1}}function now(){return(new Date).getTime()}function apply(original,extension){for(var property in extension){original[property]=extension[property]}return original}function each(obj,callback){var i=0,len=obj.length;for(var value=obj[0];i<len&&callback.call(value,i,value)!==false;value=obj[++i]){}}function sprintf(str,replace){return str.replace(/\{(\w+?)\}/g,function(match,i){return replace[i]})}function noop(){}function get(id){return document.getElementById(id)}function remove(el){el.parentNode.removeChild(el)}var supportsOpacity=true,supportsFixed=true;function checkSupport(){var body=document.body,div=document.createElement("div");supportsOpacity=typeof div.style.opacity==="string";div.style.position="fixed";div.style.margin=0;div.style.top="20px";body.appendChild(div,body.firstChild);supportsFixed=div.offsetTop==20;body.removeChild(div)}S.getStyle=(function(){var opacity=/opacity=([^)]*)/,getComputedStyle=document.defaultView&&document.defaultView.getComputedStyle;return function(el,style){var ret;if(!supportsOpacity&&style=="opacity"&&el.currentStyle){ret=opacity.test(el.currentStyle.filter||"")?(parseFloat(RegExp.$1)/100)+"":"";return ret===""?"1":ret}if(getComputedStyle){var computedStyle=getComputedStyle(el,null);if(computedStyle){ret=computedStyle[style]}if(style=="opacity"&&ret==""){ret="1"}}else{ret=el.currentStyle[style]}return ret}})();S.appendHTML=function(el,html){if(el.insertAdjacentHTML){el.insertAdjacentHTML("BeforeEnd",html)}else{if(el.lastChild){var range=el.ownerDocument.createRange();range.setStartAfter(el.lastChild);var frag=range.createContextualFragment(html);el.appendChild(frag)}else{el.innerHTML=html}}};S.getWindowSize=function(dimension){if(document.compatMode==="CSS1Compat"){return document.documentElement["client"+dimension]}return document.body["client"+dimension]};S.setOpacity=function(el,opacity){var style=el.style;if(supportsOpacity){style.opacity=(opacity==1?"":opacity)}else{style.zoom=1;if(opacity==1){if(typeof style.filter=="string"&&(/alpha/i).test(style.filter)){style.filter=style.filter.replace(/\s*[\w\.]*alpha\([^\)]*\);?/gi,"")}}else{style.filter=(style.filter||"").replace(/\s*[\w\.]*alpha\([^\)]*\)/gi,"")+" alpha(opacity="+(opacity*100)+")"}}};S.clearOpacity=function(el){S.setOpacity(el,1)};function getTarget(e){return e.target}function getPageXY(e){return[e.pageX,e.pageY]}function preventDefault(e){e.preventDefault()}function keyCode(e){return e.keyCode}function addEvent(el,type,handler){jQuery(el).bind(type,handler)}function removeEvent(el,type,handler){jQuery(el).unbind(type,handler)}jQuery.fn.shadowbox=function(options){return this.each(function(){var el=jQuery(this);var opts=jQuery.extend({},options||{},jQuery.metadata?el.metadata():jQuery.meta?el.data():{});var cls=this.className||"";opts.width=parseInt((cls.match(/w:(\d+)/)||[])[1])||opts.width;opts.height=parseInt((cls.match(/h:(\d+)/)||[])[1])||opts.height;Shadowbox.setup(el,opts)})};var loaded=false,DOMContentLoaded;if(document.addEventListener){DOMContentLoaded=function(){document.removeEventListener("DOMContentLoaded",DOMContentLoaded,false);S.load()}}else{if(document.attachEvent){DOMContentLoaded=function(){if(document.readyState==="complete"){document.detachEvent("onreadystatechange",DOMContentLoaded);S.load()}}}}function doScrollCheck(){if(loaded){return}try{document.documentElement.doScroll("left")}catch(e){setTimeout(doScrollCheck,1);return}S.load()}function bindLoad(){if(document.readyState==="complete"){return S.load()}if(document.addEventListener){document.addEventListener("DOMContentLoaded",DOMContentLoaded,false);window.addEventListener("load",S.load,false)}else{if(document.attachEvent){document.attachEvent("onreadystatechange",DOMContentLoaded);window.attachEvent("onload",S.load);var topLevel=false;try{topLevel=window.frameElement===null}catch(e){}if(document.documentElement.doScroll&&topLevel){doScrollCheck()}}}}S.load=function(){if(loaded){return}if(!document.body){return setTimeout(S.load,13)}loaded=true;checkSupport();S.onReady();if(!S.options.skipSetup){S.setup()}S.skin.init()};S.plugins={};if(navigator.plugins&&navigator.plugins.length){var names=[];each(navigator.plugins,function(i,p){names.push(p.name)});names=names.join(",");var f4m=names.indexOf("Flip4Mac")>-1;S.plugins={fla:names.indexOf("Shockwave Flash")>-1,qt:names.indexOf("QuickTime")>-1,wmp:!f4m&&names.indexOf("Windows Media")>-1,f4m:f4m}}else{var detectPlugin=function(name){var axo;try{axo=new ActiveXObject(name)}catch(e){}return !!axo};S.plugins={fla:detectPlugin("ShockwaveFlash.ShockwaveFlash"),qt:detectPlugin("QuickTime.QuickTime"),wmp:detectPlugin("wmplayer.ocx"),f4m:false}}var relAttr=/^(light|shadow)box/i,expando="shadowboxCacheKey",cacheKey=1;S.cache={};S.select=function(selector){var links=[];if(!selector){var rel;each(document.getElementsByTagName("a"),function(i,el){rel=el.getAttribute("rel");if(rel&&relAttr.test(rel)){links.push(el)}})}else{var length=selector.length;if(length){if(typeof selector=="string"){if(S.find){links=S.find(selector)}}else{if(length==2&&typeof selector[0]=="string"&&selector[1].nodeType){if(S.find){links=S.find(selector[0],selector[1])}}else{for(var i=0;i<length;++i){links[i]=selector[i]}}}}else{links.push(selector)}}return links};S.setup=function(selector,options){each(S.select(selector),function(i,link){S.addCache(link,options)})};S.teardown=function(selector){each(S.select(selector),function(i,link){S.removeCache(link)})};S.addCache=function(link,options){var key=link[expando];if(key==undefined){key=cacheKey++;link[expando]=key;addEvent(link,"click",handleClick)}S.cache[key]=S.makeObject(link,options)};S.removeCache=function(link){removeEvent(link,"click",handleClick);delete S.cache[link[expando]];link[expando]=null};S.getCache=function(link){var key=link[expando];return(key in S.cache&&S.cache[key])};S.clearCache=function(){for(var key in S.cache){S.removeCache(S.cache[key].link)}S.cache={}};function handleClick(e){S.open(this);if(S.gallery.length){preventDefault(e)}}
/*
 * Sizzle CSS Selector Engine - v1.0
 *  Copyright 2009, The Dojo Foundation
 *  Released under the MIT, BSD, and GPL Licenses.
 *  More information: http://sizzlejs.com/
 *
 * Modified for inclusion in Shadowbox.js
 */
S.find=(function(){var chunker=/((?:\((?:\([^()]+\)|[^()]+)+\)|\[(?:\[[^[\]]*\]|['"][^'"]*['"]|[^[\]'"]+)+\]|\\.|[^ >+~,(\[\\]+)+|[>+~])(\s*,\s*)?((?:.|\r|\n)*)/g,done=0,toString=Object.prototype.toString,hasDuplicate=false,baseHasDuplicate=true;[0,0].sort(function(){baseHasDuplicate=false;return 0});var Sizzle=function(selector,context,results,seed){results=results||[];var origContext=context=context||document;if(context.nodeType!==1&&context.nodeType!==9){return[]}if(!selector||typeof selector!=="string"){return results}var parts=[],m,set,checkSet,extra,prune=true,contextXML=isXML(context),soFar=selector;while((chunker.exec(""),m=chunker.exec(soFar))!==null){soFar=m[3];parts.push(m[1]);if(m[2]){extra=m[3];break}}if(parts.length>1&&origPOS.exec(selector)){if(parts.length===2&&Expr.relative[parts[0]]){set=posProcess(parts[0]+parts[1],context)}else{set=Expr.relative[parts[0]]?[context]:Sizzle(parts.shift(),context);while(parts.length){selector=parts.shift();if(Expr.relative[selector]){selector+=parts.shift()}set=posProcess(selector,set)}}}else{if(!seed&&parts.length>1&&context.nodeType===9&&!contextXML&&Expr.match.ID.test(parts[0])&&!Expr.match.ID.test(parts[parts.length-1])){var ret=Sizzle.find(parts.shift(),context,contextXML);context=ret.expr?Sizzle.filter(ret.expr,ret.set)[0]:ret.set[0]}if(context){var ret=seed?{expr:parts.pop(),set:makeArray(seed)}:Sizzle.find(parts.pop(),parts.length===1&&(parts[0]==="~"||parts[0]==="+")&&context.parentNode?context.parentNode:context,contextXML);set=ret.expr?Sizzle.filter(ret.expr,ret.set):ret.set;if(parts.length>0){checkSet=makeArray(set)}else{prune=false}while(parts.length){var cur=parts.pop(),pop=cur;if(!Expr.relative[cur]){cur=""}else{pop=parts.pop()}if(pop==null){pop=context}Expr.relative[cur](checkSet,pop,contextXML)}}else{checkSet=parts=[]}}if(!checkSet){checkSet=set}if(!checkSet){throw"Syntax error, unrecognized expression: "+(cur||selector)}if(toString.call(checkSet)==="[object Array]"){if(!prune){results.push.apply(results,checkSet)}else{if(context&&context.nodeType===1){for(var i=0;checkSet[i]!=null;i++){if(checkSet[i]&&(checkSet[i]===true||checkSet[i].nodeType===1&&contains(context,checkSet[i]))){results.push(set[i])}}}else{for(var i=0;checkSet[i]!=null;i++){if(checkSet[i]&&checkSet[i].nodeType===1){results.push(set[i])}}}}}else{makeArray(checkSet,results)}if(extra){Sizzle(extra,origContext,results,seed);Sizzle.uniqueSort(results)}return results};Sizzle.uniqueSort=function(results){if(sortOrder){hasDuplicate=baseHasDuplicate;results.sort(sortOrder);if(hasDuplicate){for(var i=1;i<results.length;i++){if(results[i]===results[i-1]){results.splice(i--,1)}}}}return results};Sizzle.matches=function(expr,set){return Sizzle(expr,null,null,set)};Sizzle.find=function(expr,context,isXML){var set,match;if(!expr){return[]}for(var i=0,l=Expr.order.length;i<l;i++){var type=Expr.order[i],match;if((match=Expr.leftMatch[type].exec(expr))){var left=match[1];match.splice(1,1);if(left.substr(left.length-1)!=="\\"){match[1]=(match[1]||"").replace(/\\/g,"");set=Expr.find[type](match,context,isXML);if(set!=null){expr=expr.replace(Expr.match[type],"");break}}}}if(!set){set=context.getElementsByTagName("*")}return{set:set,expr:expr}};Sizzle.filter=function(expr,set,inplace,not){var old=expr,result=[],curLoop=set,match,anyFound,isXMLFilter=set&&set[0]&&isXML(set[0]);while(expr&&set.length){for(var type in Expr.filter){if((match=Expr.match[type].exec(expr))!=null){var filter=Expr.filter[type],found,item;anyFound=false;if(curLoop===result){result=[]}if(Expr.preFilter[type]){match=Expr.preFilter[type](match,curLoop,inplace,result,not,isXMLFilter);if(!match){anyFound=found=true}else{if(match===true){continue}}}if(match){for(var i=0;(item=curLoop[i])!=null;i++){if(item){found=filter(item,match,i,curLoop);var pass=not^!!found;if(inplace&&found!=null){if(pass){anyFound=true}else{curLoop[i]=false}}else{if(pass){result.push(item);anyFound=true}}}}}if(found!==undefined){if(!inplace){curLoop=result}expr=expr.replace(Expr.match[type],"");if(!anyFound){return[]}break}}}if(expr===old){if(anyFound==null){throw"Syntax error, unrecognized expression: "+expr}else{break}}old=expr}return curLoop};var Expr=Sizzle.selectors={order:["ID","NAME","TAG"],match:{ID:/#((?:[\w\u00c0-\uFFFF-]|\\.)+)/,CLASS:/\.((?:[\w\u00c0-\uFFFF-]|\\.)+)/,NAME:/\[name=['"]*((?:[\w\u00c0-\uFFFF-]|\\.)+)['"]*\]/,ATTR:/\[\s*((?:[\w\u00c0-\uFFFF-]|\\.)+)\s*(?:(\S?=)\s*(['"]*)(.*?)\3|)\s*\]/,TAG:/^((?:[\w\u00c0-\uFFFF\*-]|\\.)+)/,CHILD:/:(only|nth|last|first)-child(?:\((even|odd|[\dn+-]*)\))?/,POS:/:(nth|eq|gt|lt|first|last|even|odd)(?:\((\d*)\))?(?=[^-]|$)/,PSEUDO:/:((?:[\w\u00c0-\uFFFF-]|\\.)+)(?:\((['"]*)((?:\([^\)]+\)|[^\2\(\)]*)+)\2\))?/},leftMatch:{},attrMap:{"class":"className","for":"htmlFor"},attrHandle:{href:function(elem){return elem.getAttribute("href")}},relative:{"+":function(checkSet,part){var isPartStr=typeof part==="string",isTag=isPartStr&&!/\W/.test(part),isPartStrNotTag=isPartStr&&!isTag;if(isTag){part=part.toLowerCase()}for(var i=0,l=checkSet.length,elem;i<l;i++){if((elem=checkSet[i])){while((elem=elem.previousSibling)&&elem.nodeType!==1){}checkSet[i]=isPartStrNotTag||elem&&elem.nodeName.toLowerCase()===part?elem||false:elem===part}}if(isPartStrNotTag){Sizzle.filter(part,checkSet,true)}},">":function(checkSet,part){var isPartStr=typeof part==="string";if(isPartStr&&!/\W/.test(part)){part=part.toLowerCase();for(var i=0,l=checkSet.length;i<l;i++){var elem=checkSet[i];if(elem){var parent=elem.parentNode;checkSet[i]=parent.nodeName.toLowerCase()===part?parent:false}}}else{for(var i=0,l=checkSet.length;i<l;i++){var elem=checkSet[i];if(elem){checkSet[i]=isPartStr?elem.parentNode:elem.parentNode===part}}if(isPartStr){Sizzle.filter(part,checkSet,true)}}},"":function(checkSet,part,isXML){var doneName=done++,checkFn=dirCheck;if(typeof part==="string"&&!/\W/.test(part)){var nodeCheck=part=part.toLowerCase();checkFn=dirNodeCheck}checkFn("parentNode",part,doneName,checkSet,nodeCheck,isXML)},"~":function(checkSet,part,isXML){var doneName=done++,checkFn=dirCheck;if(typeof part==="string"&&!/\W/.test(part)){var nodeCheck=part=part.toLowerCase();checkFn=dirNodeCheck}checkFn("previousSibling",part,doneName,checkSet,nodeCheck,isXML)}},find:{ID:function(match,context,isXML){if(typeof context.getElementById!=="undefined"&&!isXML){var m=context.getElementById(match[1]);return m?[m]:[]}},NAME:function(match,context){if(typeof context.getElementsByName!=="undefined"){var ret=[],results=context.getElementsByName(match[1]);for(var i=0,l=results.length;i<l;i++){if(results[i].getAttribute("name")===match[1]){ret.push(results[i])}}return ret.length===0?null:ret}},TAG:function(match,context){return context.getElementsByTagName(match[1])}},preFilter:{CLASS:function(match,curLoop,inplace,result,not,isXML){match=" "+match[1].replace(/\\/g,"")+" ";if(isXML){return match}for(var i=0,elem;(elem=curLoop[i])!=null;i++){if(elem){if(not^(elem.className&&(" "+elem.className+" ").replace(/[\t\n]/g," ").indexOf(match)>=0)){if(!inplace){result.push(elem)}}else{if(inplace){curLoop[i]=false}}}}return false},ID:function(match){return match[1].replace(/\\/g,"")},TAG:function(match,curLoop){return match[1].toLowerCase()},CHILD:function(match){if(match[1]==="nth"){var test=/(-?)(\d*)n((?:\+|-)?\d*)/.exec(match[2]==="even"&&"2n"||match[2]==="odd"&&"2n+1"||!/\D/.test(match[2])&&"0n+"+match[2]||match[2]);match[2]=(test[1]+(test[2]||1))-0;match[3]=test[3]-0}match[0]=done++;return match},ATTR:function(match,curLoop,inplace,result,not,isXML){var name=match[1].replace(/\\/g,"");if(!isXML&&Expr.attrMap[name]){match[1]=Expr.attrMap[name]}if(match[2]==="~="){match[4]=" "+match[4]+" "}return match},PSEUDO:function(match,curLoop,inplace,result,not){if(match[1]==="not"){if((chunker.exec(match[3])||"").length>1||/^\w/.test(match[3])){match[3]=Sizzle(match[3],null,null,curLoop)}else{var ret=Sizzle.filter(match[3],curLoop,inplace,true^not);if(!inplace){result.push.apply(result,ret)}return false}}else{if(Expr.match.POS.test(match[0])||Expr.match.CHILD.test(match[0])){return true}}return match},POS:function(match){match.unshift(true);return match}},filters:{enabled:function(elem){return elem.disabled===false&&elem.type!=="hidden"},disabled:function(elem){return elem.disabled===true},checked:function(elem){return elem.checked===true},selected:function(elem){elem.parentNode.selectedIndex;return elem.selected===true},parent:function(elem){return !!elem.firstChild},empty:function(elem){return !elem.firstChild},has:function(elem,i,match){return !!Sizzle(match[3],elem).length},header:function(elem){return/h\d/i.test(elem.nodeName)},text:function(elem){return"text"===elem.type},radio:function(elem){return"radio"===elem.type},checkbox:function(elem){return"checkbox"===elem.type},file:function(elem){return"file"===elem.type},password:function(elem){return"password"===elem.type},submit:function(elem){return"submit"===elem.type},image:function(elem){return"image"===elem.type},reset:function(elem){return"reset"===elem.type},button:function(elem){return"button"===elem.type||elem.nodeName.toLowerCase()==="button"},input:function(elem){return/input|select|textarea|button/i.test(elem.nodeName)}},setFilters:{first:function(elem,i){return i===0},last:function(elem,i,match,array){return i===array.length-1},even:function(elem,i){return i%2===0},odd:function(elem,i){return i%2===1},lt:function(elem,i,match){return i<match[3]-0},gt:function(elem,i,match){return i>match[3]-0},nth:function(elem,i,match){return match[3]-0===i},eq:function(elem,i,match){return match[3]-0===i}},filter:{PSEUDO:function(elem,match,i,array){var name=match[1],filter=Expr.filters[name];if(filter){return filter(elem,i,match,array)}else{if(name==="contains"){return(elem.textContent||elem.innerText||getText([elem])||"").indexOf(match[3])>=0}else{if(name==="not"){var not=match[3];for(var i=0,l=not.length;i<l;i++){if(not[i]===elem){return false}}return true}else{throw"Syntax error, unrecognized expression: "+name}}}},CHILD:function(elem,match){var type=match[1],node=elem;switch(type){case"only":case"first":while((node=node.previousSibling)){if(node.nodeType===1){return false}}if(type==="first"){return true}node=elem;case"last":while((node=node.nextSibling)){if(node.nodeType===1){return false}}return true;case"nth":var first=match[2],last=match[3];if(first===1&&last===0){return true}var doneName=match[0],parent=elem.parentNode;if(parent&&(parent.sizcache!==doneName||!elem.nodeIndex)){var count=0;for(node=parent.firstChild;node;node=node.nextSibling){if(node.nodeType===1){node.nodeIndex=++count}}parent.sizcache=doneName}var diff=elem.nodeIndex-last;if(first===0){return diff===0}else{return(diff%first===0&&diff/first>=0)}}},ID:function(elem,match){return elem.nodeType===1&&elem.getAttribute("id")===match},TAG:function(elem,match){return(match==="*"&&elem.nodeType===1)||elem.nodeName.toLowerCase()===match},CLASS:function(elem,match){return(" "+(elem.className||elem.getAttribute("class"))+" ").indexOf(match)>-1},ATTR:function(elem,match){var name=match[1],result=Expr.attrHandle[name]?Expr.attrHandle[name](elem):elem[name]!=null?elem[name]:elem.getAttribute(name),value=result+"",type=match[2],check=match[4];return result==null?type==="!=":type==="="?value===check:type==="*="?value.indexOf(check)>=0:type==="~="?(" "+value+" ").indexOf(check)>=0:!check?value&&result!==false:type==="!="?value!==check:type==="^="?value.indexOf(check)===0:type==="$="?value.substr(value.length-check.length)===check:type==="|="?value===check||value.substr(0,check.length+1)===check+"-":false},POS:function(elem,match,i,array){var name=match[2],filter=Expr.setFilters[name];if(filter){return filter(elem,i,match,array)}}}};var origPOS=Expr.match.POS;for(var type in Expr.match){Expr.match[type]=new RegExp(Expr.match[type].source+/(?![^\[]*\])(?![^\(]*\))/.source);Expr.leftMatch[type]=new RegExp(/(^(?:.|\r|\n)*?)/.source+Expr.match[type].source)}var makeArray=function(array,results){array=Array.prototype.slice.call(array,0);if(results){results.push.apply(results,array);return results}return array};try{Array.prototype.slice.call(document.documentElement.childNodes,0)}catch(e){makeArray=function(array,results){var ret=results||[];if(toString.call(array)==="[object Array]"){Array.prototype.push.apply(ret,array)}else{if(typeof array.length==="number"){for(var i=0,l=array.length;i<l;i++){ret.push(array[i])}}else{for(var i=0;array[i];i++){ret.push(array[i])}}}return ret}}var sortOrder;if(document.documentElement.compareDocumentPosition){sortOrder=function(a,b){if(!a.compareDocumentPosition||!b.compareDocumentPosition){if(a==b){hasDuplicate=true}return a.compareDocumentPosition?-1:1}var ret=a.compareDocumentPosition(b)&4?-1:a===b?0:1;if(ret===0){hasDuplicate=true}return ret}}else{if("sourceIndex" in document.documentElement){sortOrder=function(a,b){if(!a.sourceIndex||!b.sourceIndex){if(a==b){hasDuplicate=true}return a.sourceIndex?-1:1}var ret=a.sourceIndex-b.sourceIndex;if(ret===0){hasDuplicate=true}return ret}}else{if(document.createRange){sortOrder=function(a,b){if(!a.ownerDocument||!b.ownerDocument){if(a==b){hasDuplicate=true}return a.ownerDocument?-1:1}var aRange=a.ownerDocument.createRange(),bRange=b.ownerDocument.createRange();aRange.setStart(a,0);aRange.setEnd(a,0);bRange.setStart(b,0);bRange.setEnd(b,0);var ret=aRange.compareBoundaryPoints(Range.START_TO_END,bRange);if(ret===0){hasDuplicate=true}return ret}}}}function getText(elems){var ret="",elem;for(var i=0;elems[i];i++){elem=elems[i];if(elem.nodeType===3||elem.nodeType===4){ret+=elem.nodeValue}else{if(elem.nodeType!==8){ret+=getText(elem.childNodes)}}}return ret}(function(){var form=document.createElement("div"),id="script"+(new Date).getTime();form.innerHTML="<a name='"+id+"'/>";var root=document.documentElement;root.insertBefore(form,root.firstChild);if(document.getElementById(id)){Expr.find.ID=function(match,context,isXML){if(typeof context.getElementById!=="undefined"&&!isXML){var m=context.getElementById(match[1]);return m?m.id===match[1]||typeof m.getAttributeNode!=="undefined"&&m.getAttributeNode("id").nodeValue===match[1]?[m]:undefined:[]}};Expr.filter.ID=function(elem,match){var node=typeof elem.getAttributeNode!=="undefined"&&elem.getAttributeNode("id");return elem.nodeType===1&&node&&node.nodeValue===match}}root.removeChild(form);root=form=null})();(function(){var div=document.createElement("div");div.appendChild(document.createComment(""));if(div.getElementsByTagName("*").length>0){Expr.find.TAG=function(match,context){var results=context.getElementsByTagName(match[1]);if(match[1]==="*"){var tmp=[];for(var i=0;results[i];i++){if(results[i].nodeType===1){tmp.push(results[i])}}results=tmp}return results}}div.innerHTML="<a href='#'></a>";if(div.firstChild&&typeof div.firstChild.getAttribute!=="undefined"&&div.firstChild.getAttribute("href")!=="#"){Expr.attrHandle.href=function(elem){return elem.getAttribute("href",2)}}div=null})();if(document.querySelectorAll){(function(){var oldSizzle=Sizzle,div=document.createElement("div");div.innerHTML="<p class='TEST'></p>";if(div.querySelectorAll&&div.querySelectorAll(".TEST").length===0){return}Sizzle=function(query,context,extra,seed){context=context||document;if(!seed&&context.nodeType===9&&!isXML(context)){try{return makeArray(context.querySelectorAll(query),extra)}catch(e){}}return oldSizzle(query,context,extra,seed)};for(var prop in oldSizzle){Sizzle[prop]=oldSizzle[prop]}div=null})()}(function(){var div=document.createElement("div");div.innerHTML="<div class='test e'></div><div class='test'></div>";if(!div.getElementsByClassName||div.getElementsByClassName("e").length===0){return}div.lastChild.className="e";if(div.getElementsByClassName("e").length===1){return}Expr.order.splice(1,0,"CLASS");Expr.find.CLASS=function(match,context,isXML){if(typeof context.getElementsByClassName!=="undefined"&&!isXML){return context.getElementsByClassName(match[1])}};div=null})();function dirNodeCheck(dir,cur,doneName,checkSet,nodeCheck,isXML){for(var i=0,l=checkSet.length;i<l;i++){var elem=checkSet[i];if(elem){elem=elem[dir];var match=false;while(elem){if(elem.sizcache===doneName){match=checkSet[elem.sizset];break}if(elem.nodeType===1&&!isXML){elem.sizcache=doneName;elem.sizset=i}if(elem.nodeName.toLowerCase()===cur){match=elem;break}elem=elem[dir]}checkSet[i]=match}}}function dirCheck(dir,cur,doneName,checkSet,nodeCheck,isXML){for(var i=0,l=checkSet.length;i<l;i++){var elem=checkSet[i];if(elem){elem=elem[dir];var match=false;while(elem){if(elem.sizcache===doneName){match=checkSet[elem.sizset];break}if(elem.nodeType===1){if(!isXML){elem.sizcache=doneName;elem.sizset=i}if(typeof cur!=="string"){if(elem===cur){match=true;break}}else{if(Sizzle.filter(cur,[elem]).length>0){match=elem;break}}}elem=elem[dir]}checkSet[i]=match}}}var contains=document.compareDocumentPosition?function(a,b){return a.compareDocumentPosition(b)&16}:function(a,b){return a!==b&&(a.contains?a.contains(b):true)};var isXML=function(elem){var documentElement=(elem?elem.ownerDocument||elem:0).documentElement;return documentElement?documentElement.nodeName!=="HTML":false};var posProcess=function(selector,context){var tmpSet=[],later="",match,root=context.nodeType?[context]:context;while((match=Expr.match.PSEUDO.exec(selector))){later+=match[0];selector=selector.replace(Expr.match.PSEUDO,"")}selector=Expr.relative[selector]?selector+"*":selector;for(var i=0,l=root.length;i<l;i++){Sizzle(selector,root[i],tmpSet)}return Sizzle.filter(later,tmpSet)};return Sizzle})();
/*
 * SWFObject v2.1 <http://code.google.com/p/swfobject/>
 * Copyright (c) 2007-2008 Geoff Stearns, Michael Williams, and Bobby van der Sluis
 * This software is released under the MIT License <http://www.opensource.org/licenses/mit-license.php>
 *
 * Modified for inclusion in Shadowbox.js
 */
S.flash=(function(){var swfobject=function(){var UNDEF="undefined",OBJECT="object",SHOCKWAVE_FLASH="Shockwave Flash",SHOCKWAVE_FLASH_AX="ShockwaveFlash.ShockwaveFlash",FLASH_MIME_TYPE="application/x-shockwave-flash",EXPRESS_INSTALL_ID="SWFObjectExprInst",win=window,doc=document,nav=navigator,domLoadFnArr=[],regObjArr=[],objIdArr=[],listenersArr=[],script,timer=null,storedAltContent=null,storedAltContentId=null,isDomLoaded=false,isExpressInstallActive=false;var ua=function(){var w3cdom=typeof doc.getElementById!=UNDEF&&typeof doc.getElementsByTagName!=UNDEF&&typeof doc.createElement!=UNDEF,playerVersion=[0,0,0],d=null;if(typeof nav.plugins!=UNDEF&&typeof nav.plugins[SHOCKWAVE_FLASH]==OBJECT){d=nav.plugins[SHOCKWAVE_FLASH].description;if(d&&!(typeof nav.mimeTypes!=UNDEF&&nav.mimeTypes[FLASH_MIME_TYPE]&&!nav.mimeTypes[FLASH_MIME_TYPE].enabledPlugin)){d=d.replace(/^.*\s+(\S+\s+\S+$)/,"$1");playerVersion[0]=parseInt(d.replace(/^(.*)\..*$/,"$1"),10);playerVersion[1]=parseInt(d.replace(/^.*\.(.*)\s.*$/,"$1"),10);playerVersion[2]=/r/.test(d)?parseInt(d.replace(/^.*r(.*)$/,"$1"),10):0}}else{if(typeof win.ActiveXObject!=UNDEF){var a=null,fp6Crash=false;try{a=new ActiveXObject(SHOCKWAVE_FLASH_AX+".7")}catch(e){try{a=new ActiveXObject(SHOCKWAVE_FLASH_AX+".6");playerVersion=[6,0,21];a.AllowScriptAccess="always"}catch(e){if(playerVersion[0]==6){fp6Crash=true}}if(!fp6Crash){try{a=new ActiveXObject(SHOCKWAVE_FLASH_AX)}catch(e){}}}if(!fp6Crash&&a){try{d=a.GetVariable("$version");if(d){d=d.split(" ")[1].split(",");playerVersion=[parseInt(d[0],10),parseInt(d[1],10),parseInt(d[2],10)]}}catch(e){}}}}var u=nav.userAgent.toLowerCase(),p=nav.platform.toLowerCase(),webkit=/webkit/.test(u)?parseFloat(u.replace(/^.*webkit\/(\d+(\.\d+)?).*$/,"$1")):false,ie=false,windows=p?/win/.test(p):/win/.test(u),mac=p?/mac/.test(p):/mac/.test(u);
/*@cc_on
			ie = true;
			@if (@_win32)
				windows = true;
			@elif (@_mac)
				mac = true;
			@end
		@*/
return{w3cdom:w3cdom,pv:playerVersion,webkit:webkit,ie:ie,win:windows,mac:mac}}();var onDomLoad=function(){if(!ua.w3cdom){return}addDomLoadEvent(main);if(ua.ie&&ua.win){try{doc.write("<script id=__ie_ondomload defer=true src=//:><\/script>");script=getElementById("__ie_ondomload");if(script){addListener(script,"onreadystatechange",checkReadyState)}}catch(e){}}if(ua.webkit&&typeof doc.readyState!=UNDEF){timer=setInterval(function(){if(/loaded|complete/.test(doc.readyState)){callDomLoadFunctions()}},10)}if(typeof doc.addEventListener!=UNDEF){doc.addEventListener("DOMContentLoaded",callDomLoadFunctions,null)}addLoadEvent(callDomLoadFunctions)}();function checkReadyState(){if(script.readyState=="complete"){script.parentNode.removeChild(script);callDomLoadFunctions()}}function callDomLoadFunctions(){if(isDomLoaded){return}if(ua.ie&&ua.win){var s=createElement("span");try{var t=doc.getElementsByTagName("body")[0].appendChild(s);t.parentNode.removeChild(t)}catch(e){return}}isDomLoaded=true;if(timer){clearInterval(timer);timer=null}var dl=domLoadFnArr.length;for(var i=0;i<dl;i++){domLoadFnArr[i]()}}function addDomLoadEvent(fn){if(isDomLoaded){fn()}else{domLoadFnArr[domLoadFnArr.length]=fn}}function addLoadEvent(fn){if(typeof win.addEventListener!=UNDEF){win.addEventListener("load",fn,false)}else{if(typeof doc.addEventListener!=UNDEF){doc.addEventListener("load",fn,false)}else{if(typeof win.attachEvent!=UNDEF){addListener(win,"onload",fn)}else{if(typeof win.onload=="function"){var fnOld=win.onload;win.onload=function(){fnOld();fn()}}else{win.onload=fn}}}}}function main(){var rl=regObjArr.length;for(var i=0;i<rl;i++){var id=regObjArr[i].id;if(ua.pv[0]>0){var obj=getElementById(id);if(obj){regObjArr[i].width=obj.getAttribute("width")?obj.getAttribute("width"):"0";regObjArr[i].height=obj.getAttribute("height")?obj.getAttribute("height"):"0";if(hasPlayerVersion(regObjArr[i].swfVersion)){if(ua.webkit&&ua.webkit<312){fixParams(obj)}setVisibility(id,true)}else{if(regObjArr[i].expressInstall&&!isExpressInstallActive&&hasPlayerVersion("6.0.65")&&(ua.win||ua.mac)){showExpressInstall(regObjArr[i])}else{displayAltContent(obj)}}}}else{setVisibility(id,true)}}}function fixParams(obj){var nestedObj=obj.getElementsByTagName(OBJECT)[0];if(nestedObj){var e=createElement("embed"),a=nestedObj.attributes;if(a){var al=a.length;for(var i=0;i<al;i++){if(a[i].nodeName=="DATA"){e.setAttribute("src",a[i].nodeValue)}else{e.setAttribute(a[i].nodeName,a[i].nodeValue)}}}var c=nestedObj.childNodes;if(c){var cl=c.length;for(var j=0;j<cl;j++){if(c[j].nodeType==1&&c[j].nodeName=="PARAM"){e.setAttribute(c[j].getAttribute("name"),c[j].getAttribute("value"))}}}obj.parentNode.replaceChild(e,obj)}}function showExpressInstall(regObj){isExpressInstallActive=true;var obj=getElementById(regObj.id);if(obj){if(regObj.altContentId){var ac=getElementById(regObj.altContentId);if(ac){storedAltContent=ac;storedAltContentId=regObj.altContentId}}else{storedAltContent=abstractAltContent(obj)}if(!(/%$/.test(regObj.width))&&parseInt(regObj.width,10)<310){regObj.width="310"}if(!(/%$/.test(regObj.height))&&parseInt(regObj.height,10)<137){regObj.height="137"}doc.title=doc.title.slice(0,47)+" - Flash Player Installation";var pt=ua.ie&&ua.win?"ActiveX":"PlugIn",dt=doc.title,fv="MMredirectURL="+win.location+"&MMplayerType="+pt+"&MMdoctitle="+dt,replaceId=regObj.id;if(ua.ie&&ua.win&&obj.readyState!=4){var newObj=createElement("div");replaceId+="SWFObjectNew";newObj.setAttribute("id",replaceId);obj.parentNode.insertBefore(newObj,obj);obj.style.display="none";var fn=function(){obj.parentNode.removeChild(obj)};addListener(win,"onload",fn)}createSWF({data:regObj.expressInstall,id:EXPRESS_INSTALL_ID,width:regObj.width,height:regObj.height},{flashvars:fv},replaceId)}}function displayAltContent(obj){if(ua.ie&&ua.win&&obj.readyState!=4){var el=createElement("div");obj.parentNode.insertBefore(el,obj);el.parentNode.replaceChild(abstractAltContent(obj),el);obj.style.display="none";var fn=function(){obj.parentNode.removeChild(obj)};addListener(win,"onload",fn)}else{obj.parentNode.replaceChild(abstractAltContent(obj),obj)}}function abstractAltContent(obj){var ac=createElement("div");if(ua.win&&ua.ie){ac.innerHTML=obj.innerHTML}else{var nestedObj=obj.getElementsByTagName(OBJECT)[0];if(nestedObj){var c=nestedObj.childNodes;if(c){var cl=c.length;for(var i=0;i<cl;i++){if(!(c[i].nodeType==1&&c[i].nodeName=="PARAM")&&!(c[i].nodeType==8)){ac.appendChild(c[i].cloneNode(true))}}}}}return ac}function createSWF(attObj,parObj,id){var r,el=getElementById(id);if(el){if(typeof attObj.id==UNDEF){attObj.id=id}if(ua.ie&&ua.win){var att="";for(var i in attObj){if(attObj[i]!=Object.prototype[i]){if(i.toLowerCase()=="data"){parObj.movie=attObj[i]}else{if(i.toLowerCase()=="styleclass"){att+=' class="'+attObj[i]+'"'}else{if(i.toLowerCase()!="classid"){att+=" "+i+'="'+attObj[i]+'"'}}}}}var par="";for(var j in parObj){if(parObj[j]!=Object.prototype[j]){par+='<param name="'+j+'" value="'+parObj[j]+'" />'}}el.outerHTML='<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"'+att+">"+par+"</object>";objIdArr[objIdArr.length]=attObj.id;r=getElementById(attObj.id)}else{if(ua.webkit&&ua.webkit<312){var e=createElement("embed");e.setAttribute("type",FLASH_MIME_TYPE);for(var k in attObj){if(attObj[k]!=Object.prototype[k]){if(k.toLowerCase()=="data"){e.setAttribute("src",attObj[k])}else{if(k.toLowerCase()=="styleclass"){e.setAttribute("class",attObj[k])}else{if(k.toLowerCase()!="classid"){e.setAttribute(k,attObj[k])}}}}}for(var l in parObj){if(parObj[l]!=Object.prototype[l]){if(l.toLowerCase()!="movie"){e.setAttribute(l,parObj[l])}}}el.parentNode.replaceChild(e,el);r=e}else{var o=createElement(OBJECT);o.setAttribute("type",FLASH_MIME_TYPE);for(var m in attObj){if(attObj[m]!=Object.prototype[m]){if(m.toLowerCase()=="styleclass"){o.setAttribute("class",attObj[m])}else{if(m.toLowerCase()!="classid"){o.setAttribute(m,attObj[m])}}}}for(var n in parObj){if(parObj[n]!=Object.prototype[n]&&n.toLowerCase()!="movie"){createObjParam(o,n,parObj[n])}}el.parentNode.replaceChild(o,el);r=o}}}return r}function createObjParam(el,pName,pValue){var p=createElement("param");p.setAttribute("name",pName);p.setAttribute("value",pValue);el.appendChild(p)}function removeSWF(id){var obj=getElementById(id);if(obj&&(obj.nodeName=="OBJECT"||obj.nodeName=="EMBED")){if(ua.ie&&ua.win){if(obj.readyState==4){removeObjectInIE(id)}else{win.attachEvent("onload",function(){removeObjectInIE(id)})}}else{obj.parentNode.removeChild(obj)}}}function removeObjectInIE(id){var obj=getElementById(id);if(obj){for(var i in obj){if(typeof obj[i]=="function"){obj[i]=null}}obj.parentNode.removeChild(obj)}}function getElementById(id){var el=null;try{el=doc.getElementById(id)}catch(e){}return el}function createElement(el){return doc.createElement(el)}function addListener(target,eventType,fn){target.attachEvent(eventType,fn);listenersArr[listenersArr.length]=[target,eventType,fn]}function hasPlayerVersion(rv){var pv=ua.pv,v=rv.split(".");v[0]=parseInt(v[0],10);v[1]=parseInt(v[1],10)||0;v[2]=parseInt(v[2],10)||0;return(pv[0]>v[0]||(pv[0]==v[0]&&pv[1]>v[1])||(pv[0]==v[0]&&pv[1]==v[1]&&pv[2]>=v[2]))?true:false}function createCSS(sel,decl){if(ua.ie&&ua.mac){return}var h=doc.getElementsByTagName("head")[0],s=createElement("style");s.setAttribute("type","text/css");s.setAttribute("media","screen");if(!(ua.ie&&ua.win)&&typeof doc.createTextNode!=UNDEF){s.appendChild(doc.createTextNode(sel+" {"+decl+"}"))}h.appendChild(s);if(ua.ie&&ua.win&&typeof doc.styleSheets!=UNDEF&&doc.styleSheets.length>0){var ls=doc.styleSheets[doc.styleSheets.length-1];if(typeof ls.addRule==OBJECT){ls.addRule(sel,decl)}}}function setVisibility(id,isVisible){var v=isVisible?"visible":"hidden";if(isDomLoaded&&getElementById(id)){getElementById(id).style.visibility=v}else{createCSS("#"+id,"visibility:"+v)}}function urlEncodeIfNecessary(s){var regex=/[\\\"<>\.;]/;var hasBadChars=regex.exec(s)!=null;return hasBadChars?encodeURIComponent(s):s}var cleanup=function(){if(ua.ie&&ua.win){window.attachEvent("onunload",function(){var ll=listenersArr.length;for(var i=0;i<ll;i++){listenersArr[i][0].detachEvent(listenersArr[i][1],listenersArr[i][2])}var il=objIdArr.length;for(var j=0;j<il;j++){removeSWF(objIdArr[j])}for(var k in ua){ua[k]=null}ua=null;for(var l in swfobject){swfobject[l]=null}swfobject=null})}}();return{registerObject:function(objectIdStr,swfVersionStr,xiSwfUrlStr){if(!ua.w3cdom||!objectIdStr||!swfVersionStr){return}var regObj={};regObj.id=objectIdStr;regObj.swfVersion=swfVersionStr;regObj.expressInstall=xiSwfUrlStr?xiSwfUrlStr:false;regObjArr[regObjArr.length]=regObj;setVisibility(objectIdStr,false)},getObjectById:function(objectIdStr){var r=null;if(ua.w3cdom){var o=getElementById(objectIdStr);if(o){var n=o.getElementsByTagName(OBJECT)[0];if(!n||(n&&typeof o.SetVariable!=UNDEF)){r=o}else{if(typeof n.SetVariable!=UNDEF){r=n}}}}return r},embedSWF:function(swfUrlStr,replaceElemIdStr,widthStr,heightStr,swfVersionStr,xiSwfUrlStr,flashvarsObj,parObj,attObj){if(!ua.w3cdom||!swfUrlStr||!replaceElemIdStr||!widthStr||!heightStr||!swfVersionStr){return}widthStr+="";heightStr+="";if(hasPlayerVersion(swfVersionStr)){setVisibility(replaceElemIdStr,false);var att={};if(attObj&&typeof attObj===OBJECT){for(var i in attObj){if(attObj[i]!=Object.prototype[i]){att[i]=attObj[i]}}}att.data=swfUrlStr;att.width=widthStr;att.height=heightStr;var par={};if(parObj&&typeof parObj===OBJECT){for(var j in parObj){if(parObj[j]!=Object.prototype[j]){par[j]=parObj[j]}}}if(flashvarsObj&&typeof flashvarsObj===OBJECT){for(var k in flashvarsObj){if(flashvarsObj[k]!=Object.prototype[k]){if(typeof par.flashvars!=UNDEF){par.flashvars+="&"+k+"="+flashvarsObj[k]}else{par.flashvars=k+"="+flashvarsObj[k]}}}}addDomLoadEvent(function(){createSWF(att,par,replaceElemIdStr);if(att.id==replaceElemIdStr){setVisibility(replaceElemIdStr,true)}})}else{if(xiSwfUrlStr&&!isExpressInstallActive&&hasPlayerVersion("6.0.65")&&(ua.win||ua.mac)){isExpressInstallActive=true;setVisibility(replaceElemIdStr,false);addDomLoadEvent(function(){var regObj={};regObj.id=regObj.altContentId=replaceElemIdStr;regObj.width=widthStr;regObj.height=heightStr;regObj.expressInstall=xiSwfUrlStr;showExpressInstall(regObj)})}}},getFlashPlayerVersion:function(){return{major:ua.pv[0],minor:ua.pv[1],release:ua.pv[2]}},hasFlashPlayerVersion:hasPlayerVersion,createSWF:function(attObj,parObj,replaceElemIdStr){if(ua.w3cdom){return createSWF(attObj,parObj,replaceElemIdStr)}else{return undefined}},removeSWF:function(objElemIdStr){if(ua.w3cdom){removeSWF(objElemIdStr)}},createCSS:function(sel,decl){if(ua.w3cdom){createCSS(sel,decl)}},addDomLoadEvent:addDomLoadEvent,addLoadEvent:addLoadEvent,getQueryParamValue:function(param){var q=doc.location.search||doc.location.hash;if(param==null){return urlEncodeIfNecessary(q)}if(q){var pairs=q.substring(1).split("&");for(var i=0;i<pairs.length;i++){if(pairs[i].substring(0,pairs[i].indexOf("="))==param){return urlEncodeIfNecessary(pairs[i].substring((pairs[i].indexOf("=")+1)))}}}return""},expressInstallCallback:function(){if(isExpressInstallActive&&storedAltContent){var obj=getElementById(EXPRESS_INSTALL_ID);if(obj){obj.parentNode.replaceChild(storedAltContent,obj);if(storedAltContentId){setVisibility(storedAltContentId,true);if(ua.ie&&ua.win){storedAltContent.style.display="block"}}storedAltContent=null;storedAltContentId=null;isExpressInstallActive=false}}}}}();return swfobject})();S.lang={code:"en",of:"of",loading:"loading",cancel:"Cancel",next:"Next",previous:"Previous",play:"Play",pause:"Pause",close:"Close",errors:{single:'You must install the <a href="{0}">{1}</a> browser plugin to view this content.',shared:'You must install both the <a href="{0}">{1}</a> and <a href="{2}">{3}</a> browser plugins to view this content.',either:'You must install either the <a href="{0}">{1}</a> or the <a href="{2}">{3}</a> browser plugin to view this content.'}};var pre,proxyId="sb-drag-proxy",dragData,dragProxy,dragTarget;function resetDrag(){dragData={x:0,y:0,startX:null,startY:null}}function updateProxy(){var dims=S.dimensions;apply(dragProxy.style,{height:dims.innerHeight+"px",width:dims.innerWidth+"px"})}function enableDrag(){resetDrag();var style=["position:absolute","cursor:"+(S.isGecko?"-moz-grab":"move"),"background-color:"+(S.isIE?"#fff;filter:alpha(opacity=0)":"transparent")].join(";");S.appendHTML(S.skin.body,'<div id="'+proxyId+'" style="'+style+'"></div>');dragProxy=get(proxyId);updateProxy();addEvent(dragProxy,"mousedown",startDrag)}function disableDrag(){if(dragProxy){removeEvent(dragProxy,"mousedown",startDrag);remove(dragProxy);dragProxy=null}dragTarget=null}function startDrag(e){preventDefault(e);var xy=getPageXY(e);dragData.startX=xy[0];dragData.startY=xy[1];dragTarget=get(S.player.id);addEvent(document,"mousemove",positionDrag);addEvent(document,"mouseup",endDrag);if(S.isGecko){dragProxy.style.cursor="-moz-grabbing"}}function positionDrag(e){var player=S.player,dims=S.dimensions,xy=getPageXY(e);var moveX=xy[0]-dragData.startX;dragData.startX+=moveX;dragData.x=Math.max(Math.min(0,dragData.x+moveX),dims.innerWidth-player.width);var moveY=xy[1]-dragData.startY;dragData.startY+=moveY;dragData.y=Math.max(Math.min(0,dragData.y+moveY),dims.innerHeight-player.height);apply(dragTarget.style,{left:dragData.x+"px",top:dragData.y+"px"})}function endDrag(){removeEvent(document,"mousemove",positionDrag);removeEvent(document,"mouseup",endDrag);if(S.isGecko){dragProxy.style.cursor="-moz-grab"}}S.img=function(obj,id){this.obj=obj;this.id=id;this.ready=false;var self=this;pre=new Image();pre.onload=function(){self.height=obj.height?parseInt(obj.height,10):pre.height;self.width=obj.width?parseInt(obj.width,10):pre.width;self.ready=true;pre.onload=null;pre=null};pre.src=obj.content};S.img.ext=["bmp","gif","jpg","jpeg","png"];S.img.prototype={append:function(body,dims){var img=document.createElement("img");img.id=this.id;img.src=this.obj.content;img.style.position="absolute";var height,width;if(dims.oversized&&S.options.handleOversize=="resize"){height=dims.innerHeight;width=dims.innerWidth}else{height=this.height;width=this.width}img.setAttribute("height",height);img.setAttribute("width",width);body.appendChild(img)},remove:function(){var el=get(this.id);if(el){remove(el)}disableDrag();if(pre){pre.onload=null;pre=null}},onLoad:function(){var dims=S.dimensions;if(dims.oversized&&S.options.handleOversize=="drag"){enableDrag()}},onWindowResize:function(){var dims=S.dimensions;switch(S.options.handleOversize){case"resize":var el=get(this.id);el.height=dims.innerHeight;el.width=dims.innerWidth;break;case"drag":if(dragTarget){var top=parseInt(S.getStyle(dragTarget,"top")),left=parseInt(S.getStyle(dragTarget,"left"));if(top+this.height<dims.innerHeight){dragTarget.style.top=dims.innerHeight-this.height+"px"}if(left+this.width<dims.innerWidth){dragTarget.style.left=dims.innerWidth-this.width+"px"}updateProxy()}break}}};S.iframe=function(obj,id){this.obj=obj;this.id=id;var overlay=get("sb-overlay");this.height=obj.height?parseInt(obj.height,10):overlay.offsetHeight;this.width=obj.width?parseInt(obj.width,10):overlay.offsetWidth};S.iframe.prototype={append:function(body,dims){var html='<iframe id="'+this.id+'" name="'+this.id+'" height="100%" width="100%" frameborder="0" marginwidth="0" marginheight="0" style="visibility:hidden" onload="this.style.visibility=\'visible\'" scrolling="auto"';if(S.isIE){html+=' allowtransparency="true"';if(S.isIE6){html+=" src=\"javascript:false;document.write('');\""}}html+="></iframe>";body.innerHTML=html},remove:function(){var el=get(this.id);if(el){remove(el);if(S.isGecko){delete window.frames[this.id]}}},onLoad:function(){var win=S.isIE?get(this.id).contentWindow:window.frames[this.id];win.location.href=this.obj.content}};S.html=function(obj,id){this.obj=obj;this.id=id;this.height=obj.height?parseInt(obj.height,10):300;this.width=obj.width?parseInt(obj.width,10):500};S.html.prototype={append:function(body,dims){var div=document.createElement("div");div.id=this.id;div.className="html";div.innerHTML=this.obj.content;body.appendChild(div)},remove:function(){var el=get(this.id);if(el){remove(el)}}};var jwControllerHeight=20;S.flv=function(obj,id){this.obj=obj;this.id=id;this.height=obj.height?parseInt(obj.height,10):300;if(S.options.showMovieControls){this.height+=jwControllerHeight}this.width=obj.width?parseInt(obj.width,10):300};S.flv.ext=["flv","m4v"];S.flv.prototype={append:function(body,dims){var tmp=document.createElement("div");tmp.id=this.id;body.appendChild(tmp);var height=dims.innerHeight,width=dims.innerWidth,swf=S.path+"player.swf",version=S.options.flashVersion,express=S.path+"expressInstall.swf",flashvars=apply({file:this.obj.content,height:height,width:width,autostart:(S.options.autoplayMovies?"true":"false"),controlbar:(S.options.showMovieControls?"bottom":"none"),backcolor:"0x000000",frontcolor:"0xCCCCCC",lightcolor:"0x557722"},S.options.flashVars),params=S.options.flashParams;S.flash.embedSWF(swf,this.id,width,height,version,express,flashvars,params)},remove:function(){S.flash.expressInstallCallback();S.flash.removeSWF(this.id)},onWindowResize:function(){var dims=S.dimensions,el=get(this.id);el.height=dims.innerHeight;el.width=dims.innerWidth}};var overlayOn=false,visibilityCache=[],pngIds=["sb-nav-close","sb-nav-next","sb-nav-play","sb-nav-pause","sb-nav-previous"],container,overlay,wrapper,doWindowResize=true;function animate(el,property,to,duration,callback){var isOpacity=(property=="opacity"),anim=isOpacity?S.setOpacity:function(el,value){el.style[property]=""+value+"px"};if(duration==0||(!isOpacity&&!S.options.animate)||(isOpacity&&!S.options.animateFade)){anim(el,to);if(callback){callback()}return}var from=parseFloat(S.getStyle(el,property))||0;var delta=to-from;if(delta==0){if(callback){callback()}return}duration*=1000;var begin=now(),ease=S.ease,end=begin+duration,time;var interval=setInterval(function(){time=now();if(time>=end){clearInterval(interval);interval=null;anim(el,to);if(callback){callback()}}else{anim(el,from+ease((time-begin)/duration)*delta)}},10)}function setSize(){container.style.height=S.getWindowSize("Height")+"px";container.style.width=S.getWindowSize("Width")+"px"}function setPosition(){container.style.top=document.documentElement.scrollTop+"px";container.style.left=document.documentElement.scrollLeft+"px"}function toggleTroubleElements(on){if(on){each(visibilityCache,function(i,el){el[0].style.visibility=el[1]||""})}else{visibilityCache=[];each(S.options.troubleElements,function(i,tag){each(document.getElementsByTagName(tag),function(j,el){visibilityCache.push([el,el.style.visibility]);el.style.visibility="hidden"})})}}function toggleNav(id,on){var el=get("sb-nav-"+id);if(el){el.style.display=on?"":"none"}}function toggleLoading(on,callback){var loading=get("sb-loading"),playerName=S.getCurrent().player,anim=(playerName=="img"||playerName=="html");if(on){S.setOpacity(loading,0);loading.style.display="block";var wrapped=function(){S.clearOpacity(loading);if(callback){callback()}};if(anim){animate(loading,"opacity",1,S.options.fadeDuration,wrapped)}else{wrapped()}}else{var wrapped=function(){loading.style.display="none";S.clearOpacity(loading);if(callback){callback()}};if(anim){animate(loading,"opacity",0,S.options.fadeDuration,wrapped)}else{wrapped()}}}function buildBars(callback){var obj=S.getCurrent();get("sb-title-inner").innerHTML=obj.title||"";var close,next,play,pause,previous;if(S.options.displayNav){close=true;var len=S.gallery.length;if(len>1){if(S.options.continuous){next=previous=true}else{next=(len-1)>S.current;previous=S.current>0}}if(S.options.slideshowDelay>0&&S.hasNext()){pause=!S.isPaused();play=!pause}}else{close=next=play=pause=previous=false}toggleNav("close",close);toggleNav("next",next);toggleNav("play",play);toggleNav("pause",pause);toggleNav("previous",previous);var counter="";if(S.options.displayCounter&&S.gallery.length>1){var len=S.gallery.length;if(S.options.counterType=="skip"){var i=0,end=len,limit=parseInt(S.options.counterLimit)||0;if(limit<len&&limit>2){var h=Math.floor(limit/2);i=S.current-h;if(i<0){i+=len}end=S.current+(limit-h);if(end>len){end-=len}}while(i!=end){if(i==len){i=0}counter+='<a onclick="Shadowbox.change('+i+');"';if(i==S.current){counter+=' class="sb-counter-current"'}counter+=">"+(++i)+"</a>"}}else{counter=[S.current+1,S.lang.of,len].join(" ")}}get("sb-counter").innerHTML=counter;callback()}function showBars(callback){var titleInner=get("sb-title-inner"),infoInner=get("sb-info-inner"),duration=0.35;titleInner.style.visibility=infoInner.style.visibility="";if(titleInner.innerHTML!=""){animate(titleInner,"marginTop",0,duration)}animate(infoInner,"marginTop",0,duration,callback)}function hideBars(anim,callback){var title=get("sb-title"),info=get("sb-info"),titleHeight=title.offsetHeight,infoHeight=info.offsetHeight,titleInner=get("sb-title-inner"),infoInner=get("sb-info-inner"),duration=(anim?0.35:0);animate(titleInner,"marginTop",titleHeight,duration);animate(infoInner,"marginTop",infoHeight*-1,duration,function(){titleInner.style.visibility=infoInner.style.visibility="hidden";callback()})}function adjustHeight(height,top,anim,callback){var wrapperInner=get("sb-wrapper-inner"),duration=(anim?S.options.resizeDuration:0);animate(wrapper,"top",top,duration);animate(wrapperInner,"height",height,duration,callback)}function adjustWidth(width,left,anim,callback){var duration=(anim?S.options.resizeDuration:0);animate(wrapper,"left",left,duration);animate(wrapper,"width",width,duration,callback)}function setDimensions(height,width){var bodyInner=get("sb-body-inner"),height=parseInt(height),width=parseInt(width),topBottom=wrapper.offsetHeight-bodyInner.offsetHeight,leftRight=wrapper.offsetWidth-bodyInner.offsetWidth,maxHeight=overlay.offsetHeight,maxWidth=overlay.offsetWidth,padding=parseInt(S.options.viewportPadding)||20,preserveAspect=(S.player&&S.options.handleOversize!="drag");return S.setDimensions(height,width,maxHeight,maxWidth,topBottom,leftRight,padding,preserveAspect)}var K={};K.markup='<div id="sb-container"><div id="sb-overlay"></div><div id="sb-wrapper"><div id="sb-title"><div id="sb-title-inner"></div></div><div id="sb-wrapper-inner"><div id="sb-body"><div id="sb-body-inner"></div><div id="sb-loading"><div id="sb-loading-inner"><span>{loading}</span></div></div></div></div><div id="sb-info"><div id="sb-info-inner"><div id="sb-counter"></div><div id="sb-nav"><a id="sb-nav-close" title="{close}" onclick="Shadowbox.close()"></a><a id="sb-nav-next" title="{next}" onclick="Shadowbox.next()"></a><a id="sb-nav-play" title="{play}" onclick="Shadowbox.play()"></a><a id="sb-nav-pause" title="{pause}" onclick="Shadowbox.pause()"></a><a id="sb-nav-previous" title="{previous}" onclick="Shadowbox.previous()"></a></div></div></div></div></div>';K.options={animSequence:"sync",counterLimit:10,counterType:"default",displayCounter:true,displayNav:true,fadeDuration:0.35,initialHeight:160,initialWidth:320,modal:false,overlayColor:"#000",overlayOpacity:0.5,resizeDuration:0.35,showOverlay:true,troubleElements:["select","object","embed","canvas"]};K.init=function(){S.appendHTML(document.body,sprintf(K.markup,S.lang));K.body=get("sb-body-inner");container=get("sb-container");overlay=get("sb-overlay");wrapper=get("sb-wrapper");if(!supportsFixed){container.style.position="absolute"}if(!supportsOpacity){var el,m,re=/url\("(.*\.png)"\)/;each(pngIds,function(i,id){el=get(id);if(el){m=S.getStyle(el,"backgroundImage").match(re);if(m){el.style.backgroundImage="none";el.style.filter="progid:DXImageTransform.Microsoft.AlphaImageLoader(enabled=true,src="+m[1]+",sizingMethod=scale);"}}})}var timer;addEvent(window,"resize",function(){if(timer){clearTimeout(timer);timer=null}if(open){timer=setTimeout(K.onWindowResize,10)}})};K.onOpen=function(obj,callback){doWindowResize=false;container.style.display="block";setSize();var dims=setDimensions(S.options.initialHeight,S.options.initialWidth);adjustHeight(dims.innerHeight,dims.top);adjustWidth(dims.width,dims.left);if(S.options.showOverlay){overlay.style.backgroundColor=S.options.overlayColor;S.setOpacity(overlay,0);if(!S.options.modal){addEvent(overlay,"click",S.close)}overlayOn=true}if(!supportsFixed){setPosition();addEvent(window,"scroll",setPosition)}toggleTroubleElements();container.style.visibility="visible";if(overlayOn){animate(overlay,"opacity",S.options.overlayOpacity,S.options.fadeDuration,callback)}else{callback()}};K.onLoad=function(changing,callback){toggleLoading(true);while(K.body.firstChild){remove(K.body.firstChild)}hideBars(changing,function(){if(!open){return}if(!changing){wrapper.style.visibility="visible"}buildBars(callback)})};K.onReady=function(callback){if(!open){return}var player=S.player,dims=setDimensions(player.height,player.width);var wrapped=function(){showBars(callback)};switch(S.options.animSequence){case"hw":adjustHeight(dims.innerHeight,dims.top,true,function(){adjustWidth(dims.width,dims.left,true,wrapped)});break;case"wh":adjustWidth(dims.width,dims.left,true,function(){adjustHeight(dims.innerHeight,dims.top,true,wrapped)});break;default:adjustWidth(dims.width,dims.left,true);adjustHeight(dims.innerHeight,dims.top,true,wrapped)}};K.onShow=function(callback){toggleLoading(false,callback);doWindowResize=true};K.onClose=function(){if(!supportsFixed){removeEvent(window,"scroll",setPosition)}removeEvent(overlay,"click",S.close);wrapper.style.visibility="hidden";var callback=function(){container.style.visibility="hidden";container.style.display="none";toggleTroubleElements(true)};if(overlayOn){animate(overlay,"opacity",0,S.options.fadeDuration,callback)}else{callback()}};K.onPlay=function(){toggleNav("play",false);toggleNav("pause",true)};K.onPause=function(){toggleNav("pause",false);toggleNav("play",true)};K.onWindowResize=function(){if(!doWindowResize){return}setSize();var player=S.player,dims=setDimensions(player.height,player.width);adjustWidth(dims.width,dims.left);adjustHeight(dims.innerHeight,dims.top);if(player.onWindowResize){player.onWindowResize()}};S.skin=K;window.Shadowbox=S})(window);

/**
 * jQuery Roundabout - v1.1
 * http://fredhq.com/projects/roundabout/
 *
 * Moves list-items of enabled ordered and unordered lists long
 * a chosen path. Includes the default "lazySusan" path, that
 * moves items long a spinning turntable.
 *
 * Terms of Use // jQuery Roundabout
 * 
 * Open source under the BSD license
 *
 * Copyright (c) 2010, Fred LeBlanc
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met:
 * 
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   - Redistributions in binary form must reproduce the above 
 *     copyright notice, this list of conditions and the following 
 *     disclaimer in the documentation and/or other materials provided 
 *     with the distribution.
 *   - Neither the name of the author nor the names of its contributors 
 *     may be used to endorse or promote products derived from this 
 *     software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
 * POSSIBILITY OF SUCH DAMAGE.
 */

jQuery.extend({roundabout_shape:{def:'lazySusan',lazySusan:function(r,a,t){return{x:Math.sin(r+a),y:(Math.sin(r+3*Math.PI/2+a)/8)*t,z:(Math.cos(r+a)+1)/2,scale:(Math.sin(r+Math.PI/2+a)/2)+0.5}}}});jQuery.fn.roundabout=function(){var options=(typeof arguments[0]!='object')?{}:arguments[0];options={bearing:(typeof options.bearing=='undefined')?0.0:jQuery.roundabout_toFloat(options.bearing%360.0),tilt:(typeof options.tilt=='undefined')?0.0:jQuery.roundabout_toFloat(options.tilt),minZ:(typeof options.minZ=='undefined')?100:parseInt(options.minZ,10),maxZ:(typeof options.maxZ=='undefined')?400:parseInt(options.maxZ,10),minOpacity:(typeof options.minOpacity=='undefined')?0.40:jQuery.roundabout_toFloat(options.minOpacity),maxOpacity:(typeof options.maxOpacity=='undefined')?1.00:jQuery.roundabout_toFloat(options.maxOpacity),minScale:(typeof options.minScale=='undefined')?0.40:jQuery.roundabout_toFloat(options.minScale),maxScale:(typeof options.maxScale=='undefined')?1.00:jQuery.roundabout_toFloat(options.maxScale),duration:(typeof options.duration=='undefined')?600:parseInt(options.duration,10),btnNext:options.btnNext||null,btnPrev:options.btnPrev||null,easing:options.easing||'swing',clickToFocus:(options.clickToFocus!==false),focusBearing:(typeof options.focusBearing=='undefined')?0.0:jQuery.roundabout_toFloat(options.focusBearing%360.0),shape:options.shape||'lazySusan',debug:options.debug||false,childSelector:options.childSelector||'li',startingChild:(typeof options.startingChild=='undefined')?null:parseInt(options.startingChild,10),reflect:(typeof options.reflect=='undefined'||options.reflect===false)?false:true};this.each(function(i){var ref=jQuery(this);var period=jQuery.roundabout_toFloat(360.0/ref.children(options.childSelector).length);var startingBearing=(options.startingChild===null)?options.bearing:options.startingChild*period;ref.addClass('roundabout-holder').css('padding',0).css('position','relative').css('z-index',options.minZ);ref.data('roundabout',{'bearing':startingBearing,'tilt':options.tilt,'minZ':options.minZ,'maxZ':options.maxZ,'minOpacity':options.minOpacity,'maxOpacity':options.maxOpacity,'minScale':options.minScale,'maxScale':options.maxScale,'duration':options.duration,'easing':options.easing,'clickToFocus':options.clickToFocus,'focusBearing':options.focusBearing,'animating':0,'childInFocus':-1,'shape':options.shape,'period':period,'debug':options.debug,'childSelector':options.childSelector,'reflect':options.reflect});if(options.clickToFocus===true){ref.children(options.childSelector).each(function(i){jQuery(this).click(function(e){var degrees=(options.reflect===true)?360.0-(period*i):period*i;degrees=jQuery.roundabout_toFloat(degrees);if(!jQuery.roundabout_isInFocus(ref,degrees)){e.preventDefault();if(ref.data('roundabout').animating===0){ref.roundabout_animateAngleToFocus(degrees)}return false}})})}if(options.btnNext){jQuery(options.btnNext).bind('click.roundabout',function(e){e.preventDefault();if(ref.data('roundabout').animating===0){ref.roundabout_animateToNextChild()}return false})}if(options.btnPrev){jQuery(options.btnPrev).bind('click.roundabout',function(e){e.preventDefault();if(ref.data('roundabout').animating===0){ref.roundabout_animateToPreviousChild()}return false})}});this.roundabout_startChildren();if(typeof arguments[1]==='function'){var callback=arguments[1],ref=this;setTimeout(function(){callback(ref)},0)}return this};jQuery.fn.roundabout_startChildren=function(){this.each(function(i){var ref=jQuery(this);var data=ref.data('roundabout');var children=ref.children(data.childSelector);children.each(function(i){var degrees=(data.reflect===true)?360.0-(data.period*i):data.period*i;jQuery(this).addClass('roundabout-moveable-item').css('position','absolute');jQuery(this).data('roundabout',{'startWidth':jQuery(this).width(),'startHeight':jQuery(this).height(),'startFontSize':parseInt(jQuery(this).css('font-size'),10),'degrees':degrees})});ref.roundabout_updateChildPositions()});return this};jQuery.fn.roundabout_setTilt=function(newTilt){this.each(function(i){jQuery(this).data('roundabout').tilt=newTilt;jQuery(this).roundabout_updateChildPositions()});if(typeof arguments[1]==='function'){var callback=arguments[1],ref=this;setTimeout(function(){callback(ref)},0)}return this};jQuery.fn.roundabout_setBearing=function(newBearing){this.each(function(i){jQuery(this).data('roundabout').bearing=jQuery.roundabout_toFloat(newBearing%360,2);jQuery(this).roundabout_updateChildPositions()});if(typeof arguments[1]==='function'){var callback=arguments[1],ref=this;setTimeout(function(){callback(ref)},0)}return this};jQuery.fn.roundabout_adjustBearing=function(delta){delta=jQuery.roundabout_toFloat(delta);if(delta!==0){this.each(function(i){jQuery(this).data('roundabout').bearing=jQuery.roundabout_getBearing(jQuery(this))+delta;jQuery(this).roundabout_updateChildPositions()})}if(typeof arguments[1]==='function'){var callback=arguments[1],ref=this;setTimeout(function(){callback(ref)},0)}return this};jQuery.fn.roundabout_adjustTilt=function(delta){delta=jQuery.roundabout_toFloat(delta);if(delta!==0){this.each(function(i){jQuery(this).data('roundabout').tilt=jQuery.roundabout_toFloat(jQuery(this).roundabout_get('tilt')+delta);jQuery(this).roundabout_updateChildPositions()})}if(typeof arguments[1]==='function'){var callback=arguments[1],ref=this;setTimeout(function(){callback(ref)},0)}return this};jQuery.fn.roundabout_animateToBearing=function(bearing){bearing=jQuery.roundabout_toFloat(bearing);var currentTime=new Date();var duration=(typeof arguments[1]=='undefined')?null:arguments[1];var easingType=(typeof arguments[2]=='undefined')?null:arguments[2];var passedData=(typeof arguments[3]!=='object')?null:arguments[3];this.each(function(i){var ref=jQuery(this),data=ref.data('roundabout'),timer,easingFn,newBearing;var thisDuration=(duration===null)?data.duration:duration;var thisEasingType=(easingType!==null)?easingType:data.easing||'swing';if(passedData===null){passedData={timerStart:currentTime,start:jQuery.roundabout_getBearing(ref),totalTime:thisDuration}}timer=currentTime-passedData.timerStart;if(timer<thisDuration){data.animating=1;if(typeof jQuery.easing.def=='string'){easingFn=jQuery.easing[thisEasingType]||jQuery.easing[jQuery.easing.def];newBearing=easingFn(null,timer,passedData.start,bearing-passedData.start,passedData.totalTime)}else{newBearing=jQuery.easing[thisEasingType]((timer/passedData.totalTime),timer,passedData.start,bearing-passedData.start,passedData.totalTime)}ref.roundabout_setBearing(newBearing,function(){ref.roundabout_animateToBearing(bearing,thisDuration,thisEasingType,passedData)})}else{bearing=(bearing<0)?bearing+360:bearing%360;data.animating=0;ref.roundabout_setBearing(bearing)}});return this};jQuery.fn.roundabout_animateToDelta=function(delta){var duration=arguments[1],easing=arguments[2];this.each(function(i){delta=jQuery.roundabout_getBearing(jQuery(this))+jQuery.roundabout_toFloat(delta);jQuery(this).roundabout_animateToBearing(delta,duration,easing)});return this};jQuery.fn.roundabout_animateToChild=function(childPos){var duration=arguments[1],easing=arguments[2];this.each(function(i){var ref=jQuery(this),data=ref.data('roundabout');if(data.childInFocus!==childPos&&data.animating===0){var child=jQuery(ref.children(data.childSelector)[childPos]);ref.roundabout_animateAngleToFocus(child.data('roundabout').degrees,duration,easing)}});return this};jQuery.fn.roundabout_animateToNearbyChild=function(passedArgs,which){var duration=passedArgs[0],easing=passedArgs[1];this.each(function(i){var data=jQuery(this).data('roundabout');var bearing=jQuery.roundabout_toFloat(360.0-jQuery.roundabout_getBearing(jQuery(this)));var period=data.period,j=0,range;var reflect=data.reflect;var length=jQuery(this).children(data.childSelector).length;bearing=(reflect===true)?bearing%360.0:bearing;if(data.animating===0){if((reflect===false&&which==='next')||(reflect===true&&which!=='next')){bearing=(bearing===0)?360:bearing;while(true&&j<length){range={lower:jQuery.roundabout_toFloat(period*j),upper:jQuery.roundabout_toFloat(period*(j+1))};range.upper=(j==length-1)?360.0:range.upper;if(bearing<=range.upper&&bearing>range.lower){jQuery(this).roundabout_animateToDelta(bearing-range.lower,duration,easing);break}j++}}else{while(true){range={lower:jQuery.roundabout_toFloat(period*j),upper:jQuery.roundabout_toFloat(period*(j+1))};range.upper=(j==length-1)?360.0:range.upper;if(bearing>=range.lower&&bearing<range.upper){jQuery(this).roundabout_animateToDelta(bearing-range.upper,duration,easing);break}j++}}}});return this};jQuery.fn.roundabout_animateToNextChild=function(){return this.roundabout_animateToNearbyChild(arguments,'next')};jQuery.fn.roundabout_animateToPreviousChild=function(){return this.roundabout_animateToNearbyChild(arguments,'previous')};jQuery.fn.roundabout_animateAngleToFocus=function(target){var duration=arguments[1],easing=arguments[2];this.each(function(i){var delta=jQuery.roundabout_getBearing(jQuery(this))-target;delta=(Math.abs(360.0-delta)<Math.abs(0.0-delta))?360.0-delta:0.0-delta;delta=(delta>180)?-(360.0-delta):delta;if(delta!==0){jQuery(this).roundabout_animateToDelta(delta,duration,easing)}});return this};jQuery.fn.roundabout_updateChildPositions=function(){this.each(function(i){var ref=jQuery(this),data=ref.data('roundabout');var inFocus=-1;var info={bearing:jQuery.roundabout_getBearing(ref),tilt:data.tilt,stage:{width:Math.floor(ref.width()*0.9),height:Math.floor(ref.height()*0.9)},animating:data.animating,inFocus:data.childInFocus,focusBearingRad:jQuery.roundabout_degToRad(data.focusBearing),shape:jQuery.roundabout_shape[data.shape]||jQuery.roundabout_shape[jQuery.roundabout_shape.def]};info.midStage={width:info.stage.width/2,height:info.stage.height/2};info.nudge={width:info.midStage.width+info.stage.width*0.05,height:info.midStage.height+info.stage.height*0.05};info.zValues={min:data.minZ,max:data.maxZ,diff:data.maxZ-data.minZ};info.opacity={min:data.minOpacity,max:data.maxOpacity,diff:data.maxOpacity-data.minOpacity};info.scale={min:data.minScale,max:data.maxScale,diff:data.maxScale-data.minScale};ref.children(data.childSelector).each(function(i){if(jQuery.roundabout_updateChildPosition(jQuery(this),ref,info,i)&&info.animating===0){inFocus=i;jQuery(this).addClass('roundabout-in-focus')}else{jQuery(this).removeClass('roundabout-in-focus')}});if(inFocus!==info.inFocus){jQuery.roundabout_triggerEvent(ref,info.inFocus,'blur');if(inFocus!==-1){jQuery.roundabout_triggerEvent(ref,inFocus,'focus')}data.childInFocus=inFocus}});return this};jQuery.roundabout_getBearing=function(el){return jQuery.roundabout_toFloat(el.data('roundabout').bearing)%360};jQuery.roundabout_degToRad=function(degrees){return(degrees%360.0)*Math.PI/180.0};jQuery.roundabout_isInFocus=function(el,target){return(jQuery.roundabout_getBearing(el)%360===(target%360))};jQuery.roundabout_triggerEvent=function(el,child,eventType){return(child<0)?this:jQuery(el.children(el.data('roundabout').childSelector)[child]).trigger(eventType)};jQuery.roundabout_toFloat=function(number){number=Math.round(parseFloat(number)*1000)/1000;return parseFloat(number.toFixed(2))};jQuery.roundabout_updateChildPosition=function(child,container,info,childPos){var ref=jQuery(child),data=ref.data('roundabout'),out=[];var rad=jQuery.roundabout_degToRad((360.0-ref.data('roundabout').degrees)+info.bearing);while(rad<0){rad=rad+Math.PI*2}while(rad>Math.PI*2){rad=rad-Math.PI*2}var factors=info.shape(rad,info.focusBearingRad,info.tilt);factors.scale=(factors.scale>1)?1:factors.scale;factors.adjustedScale=(info.scale.min+(info.scale.diff*factors.scale)).toFixed(4);factors.width=(factors.adjustedScale*data.startWidth).toFixed(4);factors.height=(factors.adjustedScale*data.startHeight).toFixed(4);ref.css('left',((factors.x*info.midStage.width+info.nudge.width)-factors.width/2.0).toFixed(1)+'px').css('top',((factors.y*info.midStage.height+info.nudge.height)-factors.height/2.0).toFixed(1)+'px').css('width',factors.width+'px').css('height',factors.height+'px').css('opacity',(info.opacity.min+(info.opacity.diff*factors.scale)).toFixed(2)).css('z-index',Math.round(info.zValues.min+(info.zValues.diff*factors.z))).css('font-size',(factors.adjustedScale*data.startFontSize).toFixed(2)+'px').attr('current-scale',factors.adjustedScale);if(container.data('roundabout').debug===true){out.push('<div style="font-weight: normal; font-size: 10px; padding: 2px; width: '+ref.css('width')+'; background-color: #ffc;">');out.push('<strong style="font-size: 12px; white-space: nowrap;">Child '+childPos+'</strong><br />');out.push('<strong>left:</strong> '+ref.css('left')+'<br /><strong>top:</strong> '+ref.css('top')+'<br />');out.push('<strong>width:</strong> '+ref.css('width')+'<br /><strong>opacity:</strong> '+ref.css('opacity')+'<br />');out.push('<strong>z-index:</strong> '+ref.css('z-index')+'<br /><strong>font-size:</strong> '+ref.css('font-size')+'<br />');out.push('<strong>scale:</strong> '+ref.attr('current-scale'));out.push('</div>');ref.html(out.join(''))}return jQuery.roundabout_isInFocus(container,ref.data('roundabout').degrees)};

/*
 * jQuery Easing v1.3 - http://gsgd.co.uk/sandbox/jquery/easing/
 *
 * Uses the built in easing capabilities added In jQuery 1.1
 * to offer multiple easing options
 *
 * TERMS OF USE - jQuery Easing
 * 
 * Open source under the BSD License. 
 * 
 * Copyright © 2008 George McGinley Smith
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 * 
 * Redistributions of source code must retain the above copyright notice, this list of 
 * conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice, this list 
 * of conditions and the following disclaimer in the documentation and/or other materials 
 * provided with the distribution.
 * 
 * Neither the name of the author nor the names of contributors may be used to endorse 
 * or promote products derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 *  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED 
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
 * OF THE POSSIBILITY OF SUCH DAMAGE. 
 *
*/

// t: current time, b: begInnIng value, c: change In value, d: duration
jQuery.easing['jswing']=jQuery.easing['swing'];jQuery.extend(jQuery.easing,{def:'easeOutQuad',swing:function(x,t,b,c,d){return jQuery.easing[jQuery.easing.def](x,t,b,c,d)},easeInQuad:function(x,t,b,c,d){return c*(t/=d)*t+b},easeOutQuad:function(x,t,b,c,d){return-c*(t/=d)*(t-2)+b},easeInOutQuad:function(x,t,b,c,d){if((t/=d/2)<1)return c/2*t*t+b;return-c/2*((--t)*(t-2)-1)+b},easeInCubic:function(x,t,b,c,d){return c*(t/=d)*t*t+b},easeOutCubic:function(x,t,b,c,d){return c*((t=t/d-1)*t*t+1)+b},easeInOutCubic:function(x,t,b,c,d){if((t/=d/2)<1)return c/2*t*t*t+b;return c/2*((t-=2)*t*t+2)+b},easeInQuart:function(x,t,b,c,d){return c*(t/=d)*t*t*t+b},easeOutQuart:function(x,t,b,c,d){return-c*((t=t/d-1)*t*t*t-1)+b},easeInOutQuart:function(x,t,b,c,d){if((t/=d/2)<1)return c/2*t*t*t*t+b;return-c/2*((t-=2)*t*t*t-2)+b},easeInQuint:function(x,t,b,c,d){return c*(t/=d)*t*t*t*t+b},easeOutQuint:function(x,t,b,c,d){return c*((t=t/d-1)*t*t*t*t+1)+b},easeInOutQuint:function(x,t,b,c,d){if((t/=d/2)<1)return c/2*t*t*t*t*t+b;return c/2*((t-=2)*t*t*t*t+2)+b},easeInSine:function(x,t,b,c,d){return-c*Math.cos(t/d*(Math.PI/2))+c+b},easeOutSine:function(x,t,b,c,d){return c*Math.sin(t/d*(Math.PI/2))+b},easeInOutSine:function(x,t,b,c,d){return-c/2*(Math.cos(Math.PI*t/d)-1)+b},easeInExpo:function(x,t,b,c,d){return(t==0)?b:c*Math.pow(2,10*(t/d-1))+b},easeOutExpo:function(x,t,b,c,d){return(t==d)?b+c:c*(-Math.pow(2,-10*t/d)+1)+b},easeInOutExpo:function(x,t,b,c,d){if(t==0)return b;if(t==d)return b+c;if((t/=d/2)<1)return c/2*Math.pow(2,10*(t-1))+b;return c/2*(-Math.pow(2,-10*--t)+2)+b},easeInCirc:function(x,t,b,c,d){return-c*(Math.sqrt(1-(t/=d)*t)-1)+b},easeOutCirc:function(x,t,b,c,d){return c*Math.sqrt(1-(t=t/d-1)*t)+b},easeInOutCirc:function(x,t,b,c,d){if((t/=d/2)<1)return-c/2*(Math.sqrt(1-t*t)-1)+b;return c/2*(Math.sqrt(1-(t-=2)*t)+1)+b},easeInElastic:function(x,t,b,c,d){var s=1.70158;var p=0;var a=c;if(t==0)return b;if((t/=d)==1)return b+c;if(!p)p=d*.3;if(a<Math.abs(c)){a=c;var s=p/4}else var s=p/(2*Math.PI)*Math.asin(c/a);return-(a*Math.pow(2,10*(t-=1))*Math.sin((t*d-s)*(2*Math.PI)/p))+b},easeOutElastic:function(x,t,b,c,d){var s=1.70158;var p=0;var a=c;if(t==0)return b;if((t/=d)==1)return b+c;if(!p)p=d*.3;if(a<Math.abs(c)){a=c;var s=p/4}else var s=p/(2*Math.PI)*Math.asin(c/a);return a*Math.pow(2,-10*t)*Math.sin((t*d-s)*(2*Math.PI)/p)+c+b},easeInOutElastic:function(x,t,b,c,d){var s=1.70158;var p=0;var a=c;if(t==0)return b;if((t/=d/2)==2)return b+c;if(!p)p=d*(.3*1.5);if(a<Math.abs(c)){a=c;var s=p/4}else var s=p/(2*Math.PI)*Math.asin(c/a);if(t<1)return-.5*(a*Math.pow(2,10*(t-=1))*Math.sin((t*d-s)*(2*Math.PI)/p))+b;return a*Math.pow(2,-10*(t-=1))*Math.sin((t*d-s)*(2*Math.PI)/p)*.5+c+b},easeInBack:function(x,t,b,c,d,s){if(s==undefined)s=1.70158;return c*(t/=d)*t*((s+1)*t-s)+b},easeOutBack:function(x,t,b,c,d,s){if(s==undefined)s=1.70158;return c*((t=t/d-1)*t*((s+1)*t+s)+1)+b},easeInOutBack:function(x,t,b,c,d,s){if(s==undefined)s=1.70158;if((t/=d/2)<1)return c/2*(t*t*(((s*=(1.525))+1)*t-s))+b;return c/2*((t-=2)*t*(((s*=(1.525))+1)*t+s)+2)+b},easeInBounce:function(x,t,b,c,d){return c-jQuery.easing.easeOutBounce(x,d-t,0,c,d)+b},easeOutBounce:function(x,t,b,c,d){if((t/=d)<(1/2.75)){return c*(7.5625*t*t)+b}else if(t<(2/2.75)){return c*(7.5625*(t-=(1.5/2.75))*t+.75)+b}else if(t<(2.5/2.75)){return c*(7.5625*(t-=(2.25/2.75))*t+.9375)+b}else{return c*(7.5625*(t-=(2.625/2.75))*t+.984375)+b}},easeInOutBounce:function(x,t,b,c,d){if(t<d/2)return jQuery.easing.easeInBounce(x,t*2,0,c,d)*.5+b;return jQuery.easing.easeOutBounce(x,t*2-d,0,c,d)*.5+c*.5+b}});

/*
 * jQuery Nivo Slider v2.4
 * http://nivo.dev7studios.com
 *
 * Copyright 2011, Gilbert Pellegrom
 * Free to use and abuse under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
 */

(function(a){var A=function(s,v){var f=a.extend({},a.fn.nivoSlider.defaults,v),g={currentSlide:0,currentImage:"",totalSlides:0,randAnim:"",running:false,paused:false,stop:false},e=a(s);e.data("nivo:vars",g);e.css("position","relative");e.addClass("nivoSlider");var j=e.children();j.each(function(){var b=a(this),h="";if(!b.is("img")){if(b.is("a")){b.addClass("nivo-imageLink");h=b}b=b.find("img:first")}var c=b.width();if(c==0)c=b.attr("width");var o=b.height();if(o==0)o=b.attr("height");c>e.width()&&
e.width(c);o>e.height()&&e.height(o);h!=""&&h.css("display","none");b.css("display","none");g.totalSlides++});if(f.startSlide>0){if(f.startSlide>=g.totalSlides)f.startSlide=g.totalSlides-1;g.currentSlide=f.startSlide}g.currentImage=a(j[g.currentSlide]).is("img")?a(j[g.currentSlide]):a(j[g.currentSlide]).find("img:first");a(j[g.currentSlide]).is("a")&&a(j[g.currentSlide]).css("display","block");e.css("background",'url("'+g.currentImage.attr("src")+'") no-repeat');for(var k=0;k<f.slices;k++){var p=
Math.round(e.width()/f.slices);k==f.slices-1?e.append(a('<div class="nivo-slice"></div>').css({left:p*k+"px",width:e.width()-p*k+"px"})):e.append(a('<div class="nivo-slice"></div>').css({left:p*k+"px",width:p+"px"}))}e.append(a('<div class="nivo-caption"><p></p></div>').css({display:"none",opacity:f.captionOpacity}));if(g.currentImage.attr("title")!=""){k=g.currentImage.attr("title");if(k.substr(0,1)=="#")k=a(k).html();a(".nivo-caption p",e).html(k);a(".nivo-caption",e).fadeIn(f.animSpeed)}var l=
0;if(!f.manualAdvance&&j.length>1)l=setInterval(function(){r(e,j,f,false)},f.pauseTime);if(f.directionNav){e.append('<div class="nivo-directionNav"><a class="nivo-prevNav">Prev</a><a class="nivo-nextNav">Next</a></div>');if(f.directionNavHide){a(".nivo-directionNav",e).hide();e.hover(function(){a(".nivo-directionNav",e).show()},function(){a(".nivo-directionNav",e).hide()})}a("a.nivo-prevNav",e).live("click",function(){if(g.running)return false;clearInterval(l);l="";g.currentSlide-=2;r(e,j,f,"prev")});
a("a.nivo-nextNav",e).live("click",function(){if(g.running)return false;clearInterval(l);l="";r(e,j,f,"next")})}if(f.controlNav){p=a('<div class="nivo-controlNav"></div>');e.append(p);for(k=0;k<j.length;k++)if(f.controlNavThumbs){var t=j.eq(k);t.is("img")||(t=t.find("img:first"));f.controlNavThumbsFromRel?p.append('<a class="nivo-control" rel="'+k+'"><img src="'+t.attr("rel")+'" alt="" /></a>'):p.append('<a class="nivo-control" rel="'+k+'"><img src="'+t.attr("src").replace(f.controlNavThumbsSearch,
f.controlNavThumbsReplace)+'" alt="" /></a>')}else p.append('<a class="nivo-control" rel="'+k+'">'+(k+1)+"</a>");a(".nivo-controlNav a:eq("+g.currentSlide+")",e).addClass("active");a(".nivo-controlNav a",e).live("click",function(){if(g.running)return false;if(a(this).hasClass("active"))return false;clearInterval(l);l="";e.css("background",'url("'+g.currentImage.attr("src")+'") no-repeat');g.currentSlide=a(this).attr("rel")-1;r(e,j,f,"control")})}f.keyboardNav&&a(window).keypress(function(b){if(b.keyCode==
"37"){if(g.running)return false;clearInterval(l);l="";g.currentSlide-=2;r(e,j,f,"prev")}if(b.keyCode=="39"){if(g.running)return false;clearInterval(l);l="";r(e,j,f,"next")}});f.pauseOnHover&&e.hover(function(){g.paused=true;clearInterval(l);l=""},function(){g.paused=false;if(l==""&&!f.manualAdvance)l=setInterval(function(){r(e,j,f,false)},f.pauseTime)});e.bind("nivo:animFinished",function(){g.running=false;a(j).each(function(){a(this).is("a")&&a(this).css("display","none")});a(j[g.currentSlide]).is("a")&&
a(j[g.currentSlide]).css("display","block");if(l==""&&!g.paused&&!f.manualAdvance)l=setInterval(function(){r(e,j,f,false)},f.pauseTime);f.afterChange.call(this)});var w=function(b,h){var c=0;a(".nivo-slice",b).each(function(){var o=a(this),d=Math.round(b.width()/h.slices);c==h.slices-1?o.css("width",b.width()-d*c+"px"):o.css("width",d+"px");c++})},r=function(b,h,c,o){var d=b.data("nivo:vars");d&&d.currentSlide==d.totalSlides-1&&c.lastSlide.call(this);if((!d||d.stop)&&!o)return false;c.beforeChange.call(this);
if(o){o=="prev"&&b.css("background",'url("'+d.currentImage.attr("src")+'") no-repeat');o=="next"&&b.css("background",'url("'+d.currentImage.attr("src")+'") no-repeat')}else b.css("background",'url("'+d.currentImage.attr("src")+'") no-repeat');d.currentSlide++;if(d.currentSlide==d.totalSlides){d.currentSlide=0;c.slideshowEnd.call(this)}if(d.currentSlide<0)d.currentSlide=d.totalSlides-1;d.currentImage=a(h[d.currentSlide]).is("img")?a(h[d.currentSlide]):a(h[d.currentSlide]).find("img:first");if(c.controlNav){a(".nivo-controlNav a",
b).removeClass("active");a(".nivo-controlNav a:eq("+d.currentSlide+")",b).addClass("active")}if(d.currentImage.attr("title")!=""){var u=d.currentImage.attr("title");if(u.substr(0,1)=="#")u=a(u).html();a(".nivo-caption",b).css("display")=="block"?a(".nivo-caption p",b).fadeOut(c.animSpeed,function(){a(this).html(u);a(this).fadeIn(c.animSpeed)}):a(".nivo-caption p",b).html(u);a(".nivo-caption",b).fadeIn(c.animSpeed)}else a(".nivo-caption",b).fadeOut(c.animSpeed);var m=0;a(".nivo-slice",b).each(function(){var i=
Math.round(b.width()/c.slices);a(this).css({height:"0px",opacity:"0",background:'url("'+d.currentImage.attr("src")+'") no-repeat -'+(i+m*i-i)+"px 0%"});m++});if(c.effect=="random"){h=["sliceDownRight","sliceDownLeft","sliceUpRight","sliceUpLeft","sliceUpDown","sliceUpDownLeft","fold","fade","slideInRight","slideInLeft"];d.randAnim=h[Math.floor(Math.random()*(h.length+1))];if(d.randAnim==undefined)d.randAnim="fade"}if(c.effect.indexOf(",")!=-1){h=c.effect.split(",");d.randAnim=h[Math.floor(Math.random()*
h.length)];if(d.randAnim==undefined)d.randAnim="fade"}d.running=true;if(c.effect=="sliceDown"||c.effect=="sliceDownRight"||d.randAnim=="sliceDownRight"||c.effect=="sliceDownLeft"||d.randAnim=="sliceDownLeft"){var n=0;m=0;w(b,c);h=a(".nivo-slice",b);if(c.effect=="sliceDownLeft"||d.randAnim=="sliceDownLeft")h=a(".nivo-slice",b)._reverse();h.each(function(){var i=a(this);i.css({top:"0px"});m==c.slices-1?setTimeout(function(){i.animate({height:"100%",opacity:"1.0"},c.animSpeed,"",function(){b.trigger("nivo:animFinished")})},
100+n):setTimeout(function(){i.animate({height:"100%",opacity:"1.0"},c.animSpeed)},100+n);n+=50;m++})}else if(c.effect=="sliceUp"||c.effect=="sliceUpRight"||d.randAnim=="sliceUpRight"||c.effect=="sliceUpLeft"||d.randAnim=="sliceUpLeft"){m=n=0;w(b,c);h=a(".nivo-slice",b);if(c.effect=="sliceUpLeft"||d.randAnim=="sliceUpLeft")h=a(".nivo-slice",b)._reverse();h.each(function(){var i=a(this);i.css({bottom:"0px"});m==c.slices-1?setTimeout(function(){i.animate({height:"100%",opacity:"1.0"},c.animSpeed,"",
function(){b.trigger("nivo:animFinished")})},100+n):setTimeout(function(){i.animate({height:"100%",opacity:"1.0"},c.animSpeed)},100+n);n+=50;m++})}else if(c.effect=="sliceUpDown"||c.effect=="sliceUpDownRight"||d.randAnim=="sliceUpDown"||c.effect=="sliceUpDownLeft"||d.randAnim=="sliceUpDownLeft"){var x=m=n=0;w(b,c);h=a(".nivo-slice",b);if(c.effect=="sliceUpDownLeft"||d.randAnim=="sliceUpDownLeft")h=a(".nivo-slice",b)._reverse();h.each(function(){var i=a(this);if(m==0){i.css("top","0px");m++}else{i.css("bottom",
"0px");m=0}x==c.slices-1?setTimeout(function(){i.animate({height:"100%",opacity:"1.0"},c.animSpeed,"",function(){b.trigger("nivo:animFinished")})},100+n):setTimeout(function(){i.animate({height:"100%",opacity:"1.0"},c.animSpeed)},100+n);n+=50;x++})}else if(c.effect=="fold"||d.randAnim=="fold"){m=n=0;w(b,c);a(".nivo-slice",b).each(function(){var i=a(this),y=i.width();i.css({top:"0px",height:"100%",width:"0px"});m==c.slices-1?setTimeout(function(){i.animate({width:y,opacity:"1.0"},c.animSpeed,"",function(){b.trigger("nivo:animFinished")})},
100+n):setTimeout(function(){i.animate({width:y,opacity:"1.0"},c.animSpeed)},100+n);n+=50;m++})}else if(c.effect=="fade"||d.randAnim=="fade"){var q=a(".nivo-slice:first",b);q.css({height:"100%",width:b.width()+"px"});q.animate({opacity:"1.0"},c.animSpeed*2,"",function(){b.trigger("nivo:animFinished")})}else if(c.effect=="slideInRight"||d.randAnim=="slideInRight"){q=a(".nivo-slice:first",b);q.css({height:"100%",width:"0px",opacity:"1"});q.animate({width:b.width()+"px"},c.animSpeed*2,"",function(){b.trigger("nivo:animFinished")})}else if(c.effect==
"slideInLeft"||d.randAnim=="slideInLeft"){q=a(".nivo-slice:first",b);q.css({height:"100%",width:"0px",opacity:"1",left:"",right:"0px"});q.animate({width:b.width()+"px"},c.animSpeed*2,"",function(){q.css({left:"0px",right:""});b.trigger("nivo:animFinished")})}},z=function(b){this.console&&typeof console.log!="undefined"&&console.log(b)};this.stop=function(){if(!a(s).data("nivo:vars").stop){a(s).data("nivo:vars").stop=true;z("Stop Slider")}};this.start=function(){if(a(s).data("nivo:vars").stop){a(s).data("nivo:vars").stop=
false;z("Start Slider")}};f.afterLoad.call(this)};a.fn.nivoSlider=function(s){return this.each(function(){var v=a(this);if(!v.data("nivoslider")){var f=new A(this,s);v.data("nivoslider",f)}})};a.fn.nivoSlider.defaults={effect:"random",slices:15,animSpeed:500,pauseTime:3E3,startSlide:0,directionNav:true,directionNavHide:true,controlNav:true,controlNavThumbs:false,controlNavThumbsFromRel:false,controlNavThumbsSearch:".jpg",controlNavThumbsReplace:"_thumb.jpg",keyboardNav:true,pauseOnHover:true,manualAdvance:false,
captionOpacity:0.8,beforeChange:function(){},afterChange:function(){},slideshowEnd:function(){},lastSlide:function(){},afterLoad:function(){}};a.fn._reverse=[].reverse})(jQuery);


/*
 * Superfish v1.4.8 - jQuery menu widget
 * Copyright (c) 2008 Joel Birch
 *
 * Dual licensed under the MIT and GPL licenses:
 * 	http://www.opensource.org/licenses/mit-license.php
 * 	http://www.gnu.org/licenses/gpl.html
 *
 * CHANGELOG: http://users.tpg.com.au/j_birch/plugins/superfish/changelog.txt
 */
 
(function($){$.fn.superfish=function(op){var sf=$.fn.superfish,c=sf.c,$arrow=$(['<span class="',c.arrowClass,'"> &#187;</span>'].join('')),over=function(){var $$=$(this),menu=getMenu($$);clearTimeout(menu.sfTimer);$$.showSuperfishUl().siblings().hideSuperfishUl()},out=function(){var $$=$(this),menu=getMenu($$),o=sf.op;clearTimeout(menu.sfTimer);menu.sfTimer=setTimeout(function(){o.retainPath=($.inArray($$[0],o.$path)>-1);$$.hideSuperfishUl();if(o.$path.length&&$$.parents(['li.',o.hoverClass].join('')).length<1){over.call(o.$path)}},o.delay)},getMenu=function($menu){var menu=$menu.parents(['ul.',c.menuClass,':first'].join(''))[0];sf.op=sf.o[menu.serial];return menu},addArrow=function($a){$a.addClass(c.anchorClass).append($arrow.clone())};return this.each(function(){var s=this.serial=sf.o.length;var o=$.extend({},sf.defaults,op);o.$path=$('li.'+o.pathClass,this).slice(0,o.pathLevels).each(function(){$(this).addClass([o.hoverClass,c.bcClass].join(' ')).filter('li:has(ul)').removeClass(o.pathClass)});sf.o[s]=sf.op=o;$('li:has(ul)',this)[($.fn.hoverIntent&&!o.disableHI)?'hoverIntent':'hover'](over,out).each(function(){if(o.autoArrows)addArrow($('>a:first-child',this))}).not('.'+c.bcClass).hideSuperfishUl();var $a=$('a',this);$a.each(function(i){var $li=$a.eq(i).parents('li');$a.eq(i).focus(function(){over.call($li)}).blur(function(){out.call($li)})});o.onInit.call(this)}).each(function(){var menuClasses=[c.menuClass];if(sf.op.dropShadows&&!($.browser.msie&&$.browser.version<7))menuClasses.push(c.shadowClass);$(this).addClass(menuClasses.join(' '))})};var sf=$.fn.superfish;sf.o=[];sf.op={};sf.IE7fix=function(){var o=sf.op;if($.browser.msie&&$.browser.version>6&&o.dropShadows&&o.animation.opacity!=undefined)this.toggleClass(sf.c.shadowClass+'-off')};sf.c={bcClass:'sf-breadcrumb',menuClass:'sf-js-enabled',anchorClass:'sf-with-ul',arrowClass:'sf-sub-indicator',shadowClass:'sf-shadow'};sf.defaults={hoverClass:'sfHover',pathClass:'overideThisToUse',pathLevels:1,delay:800,animation:{opacity:'show'},speed:'normal',autoArrows:true,dropShadows:true,disableHI:false,onInit:function(){},onBeforeShow:function(){},onShow:function(){},onHide:function(){}};$.fn.extend({hideSuperfishUl:function(){var o=sf.op,not=(o.retainPath===true)?o.$path:'';o.retainPath=false;var $ul=$(['li.',o.hoverClass].join(''),this).add(this).not(not).removeClass(o.hoverClass).find('>ul').hide().css('visibility','hidden');o.onHide.call($ul);return this},showSuperfishUl:function(){var o=sf.op,sh=sf.c.shadowClass+'-off',$ul=this.addClass(o.hoverClass).find('>ul:hidden').css('visibility','visible');sf.IE7fix.call($ul);o.onBeforeShow.call($ul);$ul.animate(o.animation,o.speed,function(){sf.IE7fix.call($ul);o.onShow.call($ul)});return this}})})(jQuery);

/*

Quicksand 1.2.2

Reorder and filter items with a nice shuffling animation.

Copyright (c) 2010 Jacek Galanciak (razorjack.net) and agilope.com
Big thanks for Piotr Petrus (riddle.pl) for deep code review and wonderful docs & demos.

Dual licensed under the MIT and GPL version 2 licenses.
http://github.com/jquery/jquery/blob/master/MIT-LICENSE.txt
http://github.com/jquery/jquery/blob/master/GPL-LICENSE.txt

Project site: http://razorjack.net/quicksand
Github site: http://github.com/razorjack/quicksand

*/
(function($){$.fn.quicksand=function(y,z){var A={duration:750,easing:'swing',attribute:'data-id',adjustHeight:'auto',useScaling:true,enhancement:function(c){},selector:'> *',dx:0,dy:0};$.extend(A,z);if($.browser.msie||(typeof($.fn.scale)=='undefined')){A.useScaling=false}var B;if(typeof(arguments[1])=='function'){var B=arguments[1]}else if(typeof(arguments[2]=='function')){var B=arguments[2]}return this.each(function(i){var f;var g=[];var h=$(y).clone();var j=$(this);var k=$(this).css('height');var l;var m=false;var n=$(j).offset();var o=[];var p=$(this).find(A.selector);if($.browser.msie&&$.browser.version.substr(0,1)<7){j.html('').append(h);return}var q=0;var r=function(){if(!q){q=1;$toDelete=j.find('> *');j.prepend(w.find('> *'));$toDelete.remove();if(m){j.css('height',l)}A.enhancement(j);if(typeof B=='function'){B.call(this)}}};var s=j.offsetParent();var t=s.offset();if(s.css('position')=='relative'){if(s.get(0).nodeName.toLowerCase()=='body'){}else{t.top+=(parseFloat(s.css('border-top-width'))||0);t.left+=(parseFloat(s.css('border-left-width'))||0)}}else{t.top-=(parseFloat(s.css('border-top-width'))||0);t.left-=(parseFloat(s.css('border-left-width'))||0);t.top-=(parseFloat(s.css('margin-top'))||0);t.left-=(parseFloat(s.css('margin-left'))||0)}if(isNaN(t.left)){t.left=0}if(isNaN(t.top)){t.top=0}t.left-=A.dx;t.top-=A.dy;j.css('height',$(this).height());p.each(function(i){o[i]=$(this).offset()});$(this).stop();var u=0;var v=0;p.each(function(i){$(this).stop();var a=$(this).get(0);if(a.style.position=='absolute'){u=-A.dx;v=-A.dy}else{u=A.dx;v=A.dy}a.style.position='absolute';a.style.margin='0';a.style.top=(o[i].top-parseFloat(a.style.marginTop)-t.top+v)+'px';a.style.left=(o[i].left-parseFloat(a.style.marginLeft)-t.left+u)+'px'});var w=$(j).clone();var x=w.get(0);x.innerHTML='';x.setAttribute('id','');x.style.height='auto';x.style.width=j.width()+'px';w.append(h);w.insertBefore(j);w.css('opacity',0.0);x.style.zIndex=-1;x.style.margin='0';x.style.position='absolute';x.style.top=n.top-t.top+'px';x.style.left=n.left-t.left+'px';if(A.adjustHeight==='dynamic'){j.animate({height:w.height()},A.duration,A.easing)}else if(A.adjustHeight==='auto'){l=w.height();if(parseFloat(k)<parseFloat(l)){j.css('height',l)}else{m=true}}p.each(function(i){var a=[];if(typeof(A.attribute)=='function'){f=A.attribute($(this));h.each(function(){if(A.attribute(this)==f){a=$(this);return false}})}else{a=h.filter('['+A.attribute+'='+$(this).attr(A.attribute)+']')}if(a.length){if(!A.useScaling){g.push({element:$(this),animation:{top:a.offset().top-t.top,left:a.offset().left-t.left,opacity:1.0}})}else{g.push({element:$(this),animation:{top:a.offset().top-t.top,left:a.offset().left-t.left,opacity:1.0,scale:'1.0'}})}}else{if(!A.useScaling){g.push({element:$(this),animation:{opacity:'0.0'}})}else{g.push({element:$(this),animation:{opacity:'0.0',scale:'0.0'}})}}});h.each(function(i){var a=[];var b=[];if(typeof(A.attribute)=='function'){f=A.attribute($(this));p.each(function(){if(A.attribute(this)==f){a=$(this);return false}});h.each(function(){if(A.attribute(this)==f){b=$(this);return false}})}else{a=p.filter('['+A.attribute+'='+$(this).attr(A.attribute)+']');b=h.filter('['+A.attribute+'='+$(this).attr(A.attribute)+']')}var c;if(a.length===0){if(!A.useScaling){c={opacity:'1.0'}}else{c={opacity:'1.0',scale:'1.0'}}d=b.clone();var e=d.get(0);e.style.position='absolute';e.style.margin='0';e.style.top=b.offset().top-t.top+'px';e.style.left=b.offset().left-t.left+'px';d.css('opacity',0.0);if(A.useScaling){d.css('transform','scale(0.0)')}d.appendTo(j);g.push({element:$(d),animation:c})}});w.remove();A.enhancement(j);for(i=0;i<g.length;i++){g[i].element.animate(g[i].animation,A.duration,A.easing,r)}})}})(jQuery);

/*placeholder.js*/
(function($){$.fn.placeholder=function(b){$(this).each(function(){if($(this).attr('name')!=''&&$(this).attr('name')in b){var a=b[$(this).attr('name')];$(this).data('placeholder',a);if($(this).val()==''){$(this).val(a);$(this).addClass('placeholder')}}else{$(this).data('placeholder','')}});$(this).focus(function(){if($(this).val()==$(this).data('placeholder')){$(this).val('');$(this).removeClass('placeholder')}});$(this).blur(function(){if($(this).val()==''){$(this).val($(this).data('placeholder'));$(this).addClass('placeholder')}});$(this).parents('form').submit(function(){$(this).find('input, textarea').each(function(){if($(this).data('placeholder')!=''){if($(this).val()==$(this).data('placeholder')){$(this).val('')}}$(this).blur()})});return this}})(jQuery);

/*slider.js*/
//(function($){$.fn.slider=function(r){var s=$(this);var r=$.extend({nav:'ul.nav',pagination:'ul.pagination',items:'ul.items',item:r.item,visible:3,slide:1,speed:200,auto_width:true,width:null,height:null,wrapper_class:'slider-wrapper',fade:false,auto_slide:false,auto_delay:500,auto_direction:'right',easing:'swing',auto_height:true,auto_height_parent:false,count:null,axis:'x',on_show:null},r);s.each(function(){var l=$(this);var m=$(this).children(r.nav);var n=$(this).children(r.pagination);var o=$(this).children(r.items);o.after(o.clone().hide());var p=$(this).children(r.items).eq(1);o.css({'position':'absolute','width':(r.axis=='x'?9999:'auto'),'height':(r.axis=='y'?9999:'auto')}).wrap($('<div />').addClass(r.wrapper_class).css({'position':'relative','overflow':'hidden','height':(r.axis=='x'?o.outerHeight(true):'auto'),'width':(r.auto_width?'auto':r.width)})).children(r.item).css({'float':'left'});if(r.count==null){r.count=o.children(r.item).length}o.children(r.item).remove();o.data('last-index',r.visible);var q=0;for(i=0;i<r.visible;i++){$item=p.children(r.item).eq(i).clone();if(r.on_show!=null){r.on_show($item)}o.append($item);if(r.auto_height){if(r.axis=='x'){if(o.children(r.item).last().outerHeight(true)>q){q=o.children(r.item).last().outerHeight(true)}}else{q+=o.children(r.item).last().outerHeight(true)}}}if(r.auto_height&&q!=0){if(r.auto_height_parent){o.parent('div.'+r.wrapper_class).parent().height(q)}else{o.parent('div.'+r.wrapper_class).height(q)}}if(!r.auto_height&&r.height!=null){o.parent('div.'+r.wrapper_class).height(r.height)}slider_item=function(){var a=$(this).data('slider');var b=a.data('options');var c=a.children(b.pagination);var d=a.children('div.'+b.wrapper_class).children(b.items);var e=a.children(b.items);var f=c.children(b.item).index(this);if(b.count>b.visible){if(d.is(':not(:animated)')){var g=d.data('last-index')-1;var h=0;if(f<g){var j=e.children(b.item).eq(f).clone();if(b.on_show){b.on_show(j)}d.prepend(j);if(b.auto_height){if(b.axis=='x'){if(d.children(b.item).first().outerHeight(true)){h=d.children(b.item).first().outerHeight(true)}}else{h+=d.children(b.item).first().outerHeight(true)}}d.data('last-index',(f+1>=b.count?0:f+1))}else if(f>g){var j=e.children(b.item).eq(f).clone();if(b.on_show!=null){b.on_show(j)}d.append(j);if(b.auto_height){if(b.axis=='x'){if(d.children(b.item).last().outerHeight(true)){h=d.children(b.item).last().outerHeight(true)}}else{h+=d.children(b.item).last().outerHeight(true)}}d.data('last-index',(f+1>=b.count?0:f+1))}if(f!=g){c.children(b.item).removeClass('current').eq(f).addClass('current');if(f<g){d.css((b.axis=='x'?'left':'top'),-(b.axis=='x'?d.children(b.item).outerWidth(true):d.children(b.item).outerHeight(true))*b.slide)}if(b.fade){if(f<g){d.children(b.item).last().fadeOut(b.speed)}else{d.children(b.item).first().fadeOut(b.speed)}}if(b.auto_height&&h!=0){if(b.auto_height_parent){d.parent('div.'+b.wrapper_class).parent().animate({'height':h},b.speed)}else{d.parent('div.'+b.wrapper_class).animate({'height':h},b.speed)}}if(f<g){if(b.axis=='x'){d.animate({'left':'+='+d.children(b.item).outerWidth(true)*b.slide},b.speed,b.easing,function(){for(i=0;i<b.slide;i++){d.children(b.item).last().remove()}})}else{d.animate({'top':'+='+d.children(b.item).outerHeight(true)*b.slide},b.speed,b.easing,function(){for(i=0;i<b.slide;i++){d.children(b.item).last().remove()}})}}else{if(b.axis=='x'){d.animate({'left':'-='+d.children(b.item).outerWidth(true)*b.slide},b.speed,b.easing,function(){d.css('left',0);for(i=0;i<b.slide;i++){d.children(b.item).first().remove()}})}else{d.animate({'top':'-='+d.children(b.item).outerHeight(true)*b.slide},b.speed,b.easing,function(){d.css('top',0);for(i=0;i<b.slide;i++){d.children(b.item).first().remove()}})}}}}}return false}slide_left=function(){var a=$(this).data('slider');var b=a.data('options');var c=a.children(b.nav);var d=a.children(b.pagination);var e=a.children('div.'+b.wrapper_class).children(b.items);var f=a.children(b.items);if(b.count>b.visible){if(e.is(':not(:animated)')){var g=e.data('last-index')-b.visible-1;var h=0;for(i=g;i>g-b.slide;i--){if(i<0){var j=f.children(b.item).length+i}else{var j=i}var k=f.children(b.item).eq(j).clone();if(b.on_show!=null){b.on_show(k)}e.prepend(k);if(b.auto_height){if(b.axis=='x'){if(e.children(b.item).first().outerHeight(true)){h=e.children(b.item).first().outerHeight(true)}}else{h+=e.children(b.item).first().outerHeight(true)}}e.data('last-index',j+b.visible)}d.children(b.item).removeClass('current').eq(j).addClass('current');e.css((b.axis=='x'?'left':'top'),-(b.axis=='x'?e.children(b.item).outerWidth(true):e.children(b.item).outerHeight(true))*b.slide);if(b.fade){for(i=e.children(b.item).length-1;i>e.children(b.item).length-b.slide-1;i--){e.children(b.item).eq(i).fadeOut(b.speed)}}if(b.auto_height&&h!=0){if(b.auto_height_parent){e.parent('div.'+b.wrapper_class).parent().animate({'height':h},b.speed)}else{e.parent('div.'+b.wrapper_class).animate({'height':h},b.speed)}}if(b.axis=='x'){e.animate({'left':'+='+e.children(b.item).outerWidth(true)*b.slide},b.speed,b.easing,function(){for(i=0;i<b.slide;i++){e.children(b.item).last().remove()}})}else{e.animate({'top':'+='+e.children(b.item).outerHeight(true)*b.slide},b.speed,b.easing,function(){for(i=0;i<b.slide;i++){e.children(b.item).last().remove()}})}}}return false};slide_right=function(){var a=$(this).data('slider');var b=a.data('options');var c=a.children(b.nav);var d=a.children('div.'+b.wrapper_class).children(b.items);var e=a.children(b.items);if(b.count>b.visible){if(d.is(':not(:animated)')){var f=d.data('last-index');var g=0;for(i=f;i<f+b.slide;i++){if(i>=e.children(b.item).length){var h=i-e.children(b.item).length}else{var h=i}var j=e.children(b.item).eq(h).clone();if(b.on_show!=null){b.on_show(j)}d.append(j);if(b.auto_height){if(b.axis=='x'){if(d.children(b.item).last().outerHeight(true)){g=d.children(b.item).last().outerHeight(true)}}else{g+=d.children(b.item).last().outerHeight(true)}}d.data('last-index',h+1)}n.children(b.item).removeClass('current').eq(h).addClass('current');if(b.fade){for(i=0;i<b.slide;i++){d.children(b.item).eq(i).fadeOut(b.speed)}}if(b.auto_height&&g!=0){if(b.auto_height_parent){d.parent('div.'+b.wrapper_class).parent().animate({'height':g},b.speed)}else{d.parent('div.'+b.wrapper_class).animate({'height':g},b.speed)}}if(b.axis=='x'){d.animate({'left':'-='+d.children(b.item).outerWidth(true)*b.slide},b.speed,b.easing,function(){d.css('left',0);for(i=0;i<b.slide;i++){d.children(b.item).first().remove()}})}else{d.animate({'top':'-='+d.children(b.item).outerHeight(true)*b.slide},b.speed,b.easing,function(){d.css('top',0);for(i=0;i<b.slide;i++){d.children(b.item).first().remove()}})}}}return false};l.data('options',r);m.children('li.prev').data('slider',l).click(slide_left);m.children('li.next').data('slider',l).click(slide_right);n.children(r.item).data('slider',l).click(slider_item);if(r.auto_slide){auto_timeout=setTimeout(function(){setInterval(function(){if(r.auto_direction=='left'){l.children(r.nav).children('li.prev').click()}else{l.children(r.nav).children('li.next').click()}},r.auto_delay);clearTimeout(auto_timeout)},r.auto_delay)}});return this}})(jQuery);
(function($)
{
	$.fn.slider = function(options)
	{
		var $sliders = $(this);
		
		var options = $.extend
		({
			nav: 'ul.nav',
			pagination: 'ul.pagination',
			items: 'ul.items',
			item: options.item,
			visible: 3,
			slide: 1,
			speed: 200,
			auto_width: true,
			width: null,
			height: null,
			wrapper_class: 'slider-wrapper',
			fade: false,
			auto_slide: false,
			auto_delay: 500,
			auto_direction: 'right',
			easing: 'swing',
			auto_height: true,
			auto_height_parent: false,
			count: null,
			axis: 'x',
			on_show: null
		}, options);
		
		$sliders.each( function()
		{
			var $slider = $(this);
			
			var $slider_nav = $(this).children(options.nav);
			var $slider_pagination = $(this).children(options.pagination);
			var $slider_items = $(this).children(options.items);
				
			$slider_items.after($slider_items.clone().hide());
				
			var $slider_items_content = $(this).children(options.items).eq(1);
			
			$slider_items.css
			({
				'position': 'absolute',
				'width': (options.axis == 'x' ? 9999 : 'auto'),
				'height': (options.axis == 'y' ? 9999 : 'auto')
			}).wrap
			(
				$('<div />').addClass(options.wrapper_class).css
				({
					'position': 'relative',
					'overflow': 'hidden',
					'height': (options.axis == 'x' ? $slider_items.outerHeight(true) : 'auto'),
					'width': (options.auto_width ? 'auto': options.width)
				})
			).children(options.item).css
			({
				'float': 'left'
			});
			
			if (options.count == null)
			{
				options.count = $slider_items.children(options.item).length;
			}
				
			$slider_items.children(options.item).remove();
			$slider_items.data('last-index', options.visible);
			
			var height = 0;
			
			for (i = 0; i < options.visible; i++)
			{
				$item = $slider_items_content.children(options.item).eq(i).clone();
				
				if (options.on_show != null)
				{
					options.on_show($item);
				}
				
				$slider_items.append($item);
				
				if (options.auto_height)
				{
					if (options.axis == 'x')
					{
						if ($slider_items.children(options.item).last().outerHeight(true) > height)
						{
							height = $slider_items.children(options.item).last().outerHeight(true);
						}
					} else
					{
						height += $slider_items.children(options.item).last().outerHeight(true);
					}
				}
			}

			if (options.auto_height && height != 0)
			{
				if (options.auto_height_parent)
				{
					$slider_items.parent('div.' + options.wrapper_class).parent().height(height);
				} else
				{
					$slider_items.parent('div.' + options.wrapper_class).height(height);
				}
			}
			
			if ( ! options.auto_height && options.height != null)
			{
				$slider_items.parent('div.' + options.wrapper_class).height(options.height);
			}
			
			slider_item = function()
			{
				var $slider = $(this).data('slider');
				var options = $slider.data('options');
				var $slider_pagination = $slider.children(options.pagination);
				var $slider_items = $slider.children('div.' + options.wrapper_class).children(options.items);				
				var $slider_items_content = $slider.children(options.items);
				var item = $slider_pagination.children(options.item).index(this);
				
				if (options.count > options.visible)
				{
					if ($slider_items.is(':not(:animated)'))
					{
						var start_index = $slider_items.data('last-index') - 1;
						var height = 0;

						if (item < start_index)
						{
							var $item = $slider_items_content.children(options.item).eq(item).clone();
							
							if (options.on_show)
							{
								options.on_show($item);
							}
							
							$slider_items.prepend($item);
								
							if (options.auto_height)
							{
								if (options.axis == 'x')
								{
									if ($slider_items.children(options.item).first().outerHeight(true))
									{
										height = $slider_items.children(options.item).first().outerHeight(true);
									}
								} else
								{
									height += $slider_items.children(options.item).first().outerHeight(true);
								}
							}
								
							$slider_items.data('last-index', (item + 1 >= options.count ? 0 : item + 1));
						} else if (item > start_index)
						{
							var $item = $slider_items_content.children(options.item).eq(item).clone();
							
							if (options.on_show != null)
							{
								options.on_show($item);
							}
							
							$slider_items.append($item);
							
							if (options.auto_height)
							{
								if (options.axis == 'x')
								{
									if ($slider_items.children(options.item).last().outerHeight(true))
									{
										height = $slider_items.children(options.item).last().outerHeight(true);
									}
								} else
								{
									height += $slider_items.children(options.item).last().outerHeight(true);
								}
							}
							
							$slider_items.data('last-index', (item + 1 >= options.count ? 0 : item + 1));
						}
						
						if (item != start_index)
						{
							$slider_pagination.children(options.item).removeClass('current').eq(item).addClass('current');
							
							if (item < start_index)
							{
								$slider_items.css((options.axis == 'x' ? 'left' : 'top'), -(options.axis == 'x' ? $slider_items.children(options.item).outerWidth(true) : $slider_items.children(options.item).outerHeight(true)) * options.slide);
							}
								
							if (options.fade)
							{
								if (item < start_index)
								{
									$slider_items.children(options.item).last().fadeOut(options.speed);
								} else
								{
									$slider_items.children(options.item).first().fadeOut(options.speed);
								}
							}
							
							if (options.auto_height && height != 0)
							{
								if (options.auto_height_parent)
								{
									$slider_items.parent('div.' + options.wrapper_class).parent().animate
									({
										'height': height
									}, options.speed);
								} else
								{
									$slider_items.parent('div.' + options.wrapper_class).animate
									({
										'height': height
									}, options.speed);
								}
							}
							
							if (item < start_index)
							{
								if (options.axis == 'x')
								{
									$slider_items.animate
									({
										'left': '+=' + $slider_items.children(options.item).outerWidth(true) * options.slide
									}, options.speed, options.easing, function()
									{
										for (i = 0; i < options.slide; i++)
										{
											$slider_items.children(options.item).last().remove();
										}
									});
								} else
								{
									$slider_items.animate
									({
										'top': '+=' + $slider_items.children(options.item).outerHeight(true) * options.slide
									}, options.speed, options.easing, function()
									{
										for (i = 0; i < options.slide; i++)
										{
											$slider_items.children(options.item).last().remove();
										}
									});
								}
							} else
							{
								if (options.axis == 'x')
								{
									$slider_items.animate
									({
										'left': '-=' + $slider_items.children(options.item).outerWidth(true) * options.slide
									}, options.speed, options.easing, function()
									{
										$slider_items.css('left', 0);

										for (i = 0; i < options.slide; i++)
										{
											$slider_items.children(options.item).first().remove();
										}
									});
								} else
								{
									$slider_items.animate
									({
										'top': '-=' + $slider_items.children(options.item).outerHeight(true) * options.slide
									}, options.speed, options.easing, function()
									{
										$slider_items.css('top', 0);

										for (i = 0; i < options.slide; i++)
										{
											$slider_items.children(options.item).first().remove();
										}
									});
								}
							}
						}
					}
				}
					
				return false;
			}
				
			slide_left = function()
			{
				var $slider = $(this).data('slider');
				var options = $slider.data('options');
				var $slider_nav = $slider.children(options.nav);
				var $slider_pagination = $slider.children(options.pagination);
				var $slider_items = $slider.children('div.' + options.wrapper_class).children(options.items);				
				var $slider_items_content = $slider.children(options.items);

				if (options.count > options.visible)
				{
					if ($slider_items.is(':not(:animated)'))
					{
						var start_index = $slider_items.data('last-index') - options.visible - 1;
						var height = 0;
						
						for (i = start_index; i > start_index - options.slide ; i--)
						{
							if (i < 0)
							{
								var index = $slider_items_content.children(options.item).length + i;
							} else
							{
								var index = i;
							}
							
							var $item = $slider_items_content.children(options.item).eq(index).clone();
							
							if (options.on_show != null)
							{
								options.on_show($item);
							}
			
							$slider_items.prepend($item);
							
							if (options.auto_height)
							{
								if (options.axis == 'x')
								{
									if ($slider_items.children(options.item).first().outerHeight(true))
									{
										height = $slider_items.children(options.item).first().outerHeight(true);
									}
								} else
								{
									height += $slider_items.children(options.item).first().outerHeight(true);
								}
							}
							
							$slider_items.data('last-index', index + options.visible);
						}
						
						$slider_pagination.children(options.item).removeClass('current').eq(index).addClass('current');
						
						$slider_items.css((options.axis == 'x' ? 'left' : 'top'), -(options.axis == 'x' ? $slider_items.children(options.item).outerWidth(true) : $slider_items.children(options.item).outerHeight(true)) * options.slide);
							
						if (options.fade)
						{
							for (i = $slider_items.children(options.item).length - 1; i > $slider_items.children(options.item).length - options.slide - 1; i--)
							{
								$slider_items.children(options.item).eq(i).fadeOut(options.speed);
							}
						}
						
						if (options.auto_height && height != 0)
						{
							if (options.auto_height_parent)
							{
								$slider_items.parent('div.' + options.wrapper_class).parent().animate
								({
									'height': height
								}, options.speed);
							} else
							{
								$slider_items.parent('div.' + options.wrapper_class).animate
								({
									'height': height
								}, options.speed);
							}
						}
						
						if (options.axis == 'x')
						{
							$slider_items.animate
							({
								'left': '+=' + $slider_items.children(options.item).outerWidth(true) * options.slide
							}, options.speed, options.easing, function()
							{
								for (i = 0; i < options.slide; i++)
								{
									$slider_items.children(options.item).last().remove();
								}
							});
						} else
						{
							$slider_items.animate
							({
								'top': '+=' + $slider_items.children(options.item).outerHeight(true) * options.slide
							}, options.speed, options.easing, function()
							{
								for (i = 0; i < options.slide; i++)
								{
									$slider_items.children(options.item).last().remove();
								}
							});
						}
					}
				}
					
				return false;
			};
			
			slide_right = function()
			{
				var $slider = $(this).data('slider');
				var options = $slider.data('options');
				var $slider_nav = $slider.children(options.nav);
				var $slider_items = $slider.children('div.' + options.wrapper_class).children(options.items);
				var $slider_items_content = $slider.children(options.items);
				
				if (options.count > options.visible)
				{
					if ($slider_items.is(':not(:animated)'))
					{
						var start_index = $slider_items.data('last-index');
						var height = 0;
						
						for (i = start_index; i < start_index + options.slide ; i++)
						{
							if (i >= $slider_items_content.children(options.item).length)
							{
								var index = i - $slider_items_content.children(options.item).length;
							} else
							{
								var index = i;
							}
							
							var $item = $slider_items_content.children(options.item).eq(index).clone();
							
							if (options.on_show != null)
							{
								options.on_show($item);
							}
							
							$slider_items.append($item);
							
							if (options.auto_height)
							{
								if (options.axis == 'x')
								{
									if ($slider_items.children(options.item).last().outerHeight(true))
									{
										height = $slider_items.children(options.item).last().outerHeight(true);
									}
								} else
								{
									height += $slider_items.children(options.item).last().outerHeight(true);
								}
							}
							
							$slider_items.data('last-index', index + 1);
						}
						
						$slider_pagination.children(options.item).removeClass('current').eq(index).addClass('current');
							
						if (options.fade)
						{
							for (i = 0; i < options.slide; i++)
							{
								$slider_items.children(options.item).eq(i).fadeOut(options.speed);
							}
						}
						
						if (options.auto_height && height != 0)
						{
							if (options.auto_height_parent)
							{
								$slider_items.parent('div.' + options.wrapper_class).parent().animate
								({
									'height': height
								}, options.speed);
							} else
							{
								$slider_items.parent('div.' + options.wrapper_class).animate
								({
									'height': height
								}, options.speed);
							}
						}
						
						if (options.axis == 'x')
						{
							$slider_items.animate
							({
								'left': '-=' + $slider_items.children(options.item).outerWidth(true) * options.slide
							}, options.speed, options.easing, function()
							{
								$slider_items.css('left', 0);

								for (i = 0; i < options.slide; i++)
								{
									$slider_items.children(options.item).first().remove();
								}
							});
						} else
						{
							$slider_items.animate
							({
								'top': '-=' + $slider_items.children(options.item).outerHeight(true) * options.slide
							}, options.speed, options.easing, function()
							{
								$slider_items.css('top', 0);
								
								for (i = 0; i < options.slide; i++)
								{
									$slider_items.children(options.item).first().remove();
								}
							});
						}
					}
				}
					
				return false;
			};
			
			$slider.data('options', options);
			$slider_nav.children('li.prev').data('slider', $slider).click(slide_left);
			$slider_nav.children('li.next').data('slider', $slider).click(slide_right);
			
			$slider_pagination.children(options.item).data('slider', $slider).click(slider_item);
				
			if (options.auto_slide)
			{
				auto_timeout = setTimeout( function()
				{
					setInterval( function()
					{
						if (options.auto_direction == 'left')
						{
							$slider.children(options.nav).children('li.prev').click();
						} else
						{
							$slider.children(options.nav).children('li.next').click();
						}
					}, options.auto_delay);
					
					clearTimeout(auto_timeout);
				}, options.auto_delay);
			}
		});
	
		return this;
	}
})(jQuery);


/*imgload.js*/
//(function($){$.imgload_images=[];$.fn.imgload=function(f,g){var g=$.extend({path:'',use_parent:false},g);$(this).each(function(){var a=false;for(var i=0;i<$.imgload_images.length;i++){if($.imgload_images[i]==f){a=true;break}}if($(this).attr('src')==f){a=true}if(!a){$img=$(this);var b=$img.position().left;var c=$img.position().top;if(g.use_parent){var d=$img.parent().width();var e=$img.parent().height()}else{var d=$img.width();var e=$img.height()}$(this).next('.loading').remove();$(this).next('.loading-wrapper').remove();$loading_wrapper=$('<div />');$loading_wrapper.addClass('loading-wrapper');$loading_wrapper.css({'background':'#000','position':'absolute','left':(g.use_parent?0:b),'top':(g.use_parent?0:c),'width':d,'height':e,'z-index':6666,'opacity':0.8}).hide();$loading=$('<div />');$loading.addClass('loading');$loading.css({'background':'transparent url('+g.path+'loading.gif) 50% 50% no-repeat','position':'absolute','left':(g.use_parent?0:b),'top':(g.use_parent?0:c),'width':d,'height':e,'z-index':6667}).hide();$loading.show();$loading_wrapper.show();$(this).after($loading_wrapper);$(this).after($loading);if(typeof f=='undefined'){f=$(this).attr('src')}$(this).after($('<img />').addClass('loading-img').attr('src',f).bind('load',function(){$(this).prev('img').trigger('loaded');$.imgload_images.push($(this).attr('src').split('?')[0])}).hide())$(this).bind('loaded',function(){$(this).attr('src',$(this).nextAll('img.loading-img').attr('src'));$(this).unbind('loaded');$(this).nextAll('img.loading-img').remove();$(this).nextAll('.loading').fadeOut(function(){$(this).remove()});$(this).nextAll('.loading-wrapper').fadeOut(function(){$(this).remove()})})}else{$(this).attr('src',f)}});return this}})(jQuery);
(function($)
{
	$.imgload_images = [];

	$.fn.imgload = function(src, options)
	{
		var options = $.extend
		({
			path: '',
			use_parent: false
		}, options);

		$(this).each( function()
		{
			var exists = false;

			for (var i = 0; i < $.imgload_images.length; i++)
			{
				if ($.imgload_images[i] == src)
				{
					exists = true;
					break;
				}
			}
			
			if ($(this).attr('src') == src)
			{
				exists = true;
			}
			
			if ( ! exists)
			{
				$img = $(this);

				var left = $img.position().left;
				var top = $img.position().top;
				
				if (options.use_parent)
				{
					var width = $img.parent().width();
					var height = $img.parent().height();
				} else
				{
					var width = $img.width();
					var height = $img.height();
				}

				$(this).next('.loading').remove();
				$(this).next('.loading-wrapper').remove();
				
				$loading_wrapper = $('<div />');
				$loading_wrapper.addClass('loading-wrapper');
				$loading_wrapper.css
				({
					'background': '#000',
					'position': 'absolute',
					'left': (options.use_parent ? 0 : left),
					'top': (options.use_parent ? 0 : top),
					'width': width,
					'height': height,
					'z-index': 6666,
					'opacity': 0.8
				}).hide();
				
				$loading = $('<div />');
				$loading.addClass('loading');
				
				$loading.css
				({
					'background': 'transparent url(' + options.path + 'loading.gif) 50% 50% no-repeat',
					'position': 'absolute',
					'left': (options.use_parent ? 0 : left),
					'top': (options.use_parent ? 0 : top),
					'width': width,
					'height': height,
					'z-index': 6667
				}).hide();
				
				$loading.show();
				$loading_wrapper.show();
				
				$(this).after($loading_wrapper);
				$(this).after($loading);
				
				if (typeof src == 'undefined')
				{
					src = $(this).attr('src');
				}

				$(this).after($('<img />').addClass('loading-img').attr('src', src).bind('load', function()
				{
					$(this).prev('img').trigger('loaded');
					$.imgload_images.push($(this).attr('src').split('?')[0]);
				}).hide())

				$(this).bind('loaded', function()
				{
					$(this).attr('src', $(this).nextAll('img.loading-img').attr('src'));
					$(this).unbind('loaded');
					
					$(this).nextAll('img.loading-img').remove();
					$(this).nextAll('.loading').fadeOut(function() { $(this).remove(); });
					$(this).nextAll('.loading-wrapper').fadeOut(function() { $(this).remove(); });
				});
			} else
			{
				$(this).attr('src', src);
			}
		});
		
		return this;
	};
})(jQuery);


/*load.js*/
(function($){$.fn.load_begin=function(e){var e=$.extend({path:'',use_parent:false,opacity:0.8,absolute:true,follow:false,width:null,height:null,image:'loading.gif'},e);$(this).each(function(){$element=$(this);var a=$element.position().left;var b=$element.position().top;if(e.use_parent){var c=$element.parent().width();var d=$element.parent().height()}else{var c=(e.width!=null?e.width:$element.width());var d=(e.height!=null?e.height:$element.height())}$(this).next('.loading').remove();$(this).next('.loading-wrapper').remove();$loading_wrapper=$('<div />');$loading_wrapper.addClass('loading-wrapper');$loading_wrapper.css({'background':'#000','position':e.absolute?'absolute':'fixed','left':(e.use_parent?0:a),'top':(e.use_parent?0:b),'width':c,'height':d,'z-index':9998,'opacity':e.opacity}).hide();$loading=$('<div />');$loading.addClass('loading');$loading.css({'background':'transparent url('+e.path+e.image+') 50% 50% no-repeat','position':e.absolute?'absolute':'fixed','left':(e.use_parent?0:a),'top':(e.use_parent?0:b),'width':c,'height':d,'z-index':9999}).hide();$loading.fadeIn();$loading_wrapper.fadeIn();$(this).after($loading_wrapper);$(this).after($loading)});return this};$.fn.load_end=function(a){var a=$.extend({path:'',use_parent:false},a);$(this).each(function(){$(this).next('.loading').remove();$(this).next('.loading-wrapper').remove()});return this}})(jQuery);

/*tooltip.js*/
(function($){$.fn.tooltip=function(d){var e=$(this);var d=$.extend({index:9999,tooltip:'div.tooltip',fade:false,fade_speed:500,x:0,y:0,sub_width:false,sub_height:false,follow:true,offset:true,standalone:false},d);var f=e;if(d.follow){f.css({'position':'relative'});f.children(d.tooltip).css({'position':'absolute','z-index':d.index})}f.live('mouseenter',function(a){var b=$(this).children(d.tooltip);if(d.standalone){$('body').append(b.clone());b=$('body > '+d.tooltip).last()}if(d.fade){b.not(':animated').fadeIn(d.fade_speed)}else{b.show()}});f.live('mouseleave',function(){var a=$(this).children(d.tooltip);if(d.standalone){a=$('body > '+d.tooltip).last()}if(d.fade){a.fadeOut(d.speed,function(){if(d.standalone){a.remove()}})}else{a.hide();if(d.standalone){a.remove()}}});if(d.follow){f.live('mousemove',function(a){var b=$(this);var c=$(this).children(d.tooltip);if(d.standalone){c=$('body > '+d.tooltip).last()}c.css({'top':a.pageY+(d.offset&&!d.standalone?-b.offset().top:0)+d.y-(d.sub_height?c.height():0),'left':a.pageX+(d.offset&&!d.standalone?-b.offset().left:0)+d.x-(d.sub_width?c.width():0)})})}return this}})(jQuery);

/*config.js*/
var config={images_dir:'media/images/',input_placeholder:'input[name=search], input[name=name], input[name=email], input[name=s], input[name=author], textarea[name=message], textarea[name=comment], input[name=nick]',form_validate:'input[name=name], input[name=email], textarea[name=message], input[name=author], textarea[name=comment], input[name=nick]',placeholder:{name:'Name...',author:'Name...',email:'Email...',message:'Message...',search:'Search...',s:'Search...',comment:'Name...',nick:'Name...'}};

/*common.js*/
if(top!==self)top.location.replace(self.location.href);Shadowbox.init({language:'en',players:['img','html','iframe','qt','wmp','swf','flv']});var $jq=jQuery.noConflict();$jq(function(){$jq('body').addClass('js');$jq('a.toggle').each(function(){if(!$jq(this).hasClass('hide')){$jq(this).next().hide()}else{$jq(this).next().show()}});$jq('a.toggle').click(function(){$jq(this).toggleClass('hide');if($jq(this).hasClass('hide')){$jq(this).next().show()}else{$jq(this).next().hide()}return false});$jq('a.frame-holder > img').each(function(){if($jq(this).attr('title')!=''&&$jq(this).parents('.flickr-feed').length==0){title=$jq(this).attr('title');$jq(this).after($jq('<span />').addClass('title-description').text(title));if($jq(this).hasClass('aligncenter')||$jq(this).parent('a.frame-holder').hasClass('aligncenter')){$jq(this).load(function(){$img=$jq(this);$jq(this).parent('a.frame-holder').css({'width':$img.width()})})}}});$jq('.frame-holder > *').each(function(){if($jq(this)[0].tagName.toLowerCase()!='img'&&$jq(this)[0].tagName.toLowerCase()!='span'){$jq(this).parent('.frame-holder').width($jq(this).width())}});$jq('a[href=#header], .separator').click(function(){$jq('html,body').animate({'scrollTop':0},300);return false});$jq(config.form_validate).blur(function(){var a=$jq(this).val();$jq(this).removeClass('valid');if(a==''||a==$jq(this).data('placeholder')){$jq(this).addClass('error')}else{if($jq(this).attr('name')=='email'||$jq(this).attr('id')=='email'||$jq(this).hasClass('email')){if(/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(a)){$jq(this).removeClass('error').addClass('valid')}else{$jq(this).addClass('error')}}else{$jq(this).removeClass('error').addClass('valid')}}});$jq('form').submit(function(){if($jq(this).find('input.error, textarea.error').length>0){return false}});$jq(config.input_placeholder).placeholder(config.placeholder);$jq('div.zoombox').each(function(){var a=$jq(this).children('ul.images').children('li').width();var b=$jq(this).children('div.enlarged');var c=$jq(this).children('div.enlarged').find('img');b.children('a').css({'display':'block','width':b.width(),'height':b.height()});if($jq(this).parent().width()<b.width()){b.width($jq(this).parent().width())}c.load(function(){$jq(this).css({'position':'absolute','top':b.height()/2-c.height()/2,'left':b.width()/2-c.width()/2})});$jq(this).slider({nav:'ul.slider-nav',items:'ul.images',visible:Math.round(($jq(this).width()/a))-2,slide:2,speed:600,auto_width:false,width:b.width()-a})});$jq('div.zoombox div.enlarged').mousemove(function(e){var a=$jq(this);var b=a.find('img');var c=a.width();var d=a.height();var f=a.outerWidth();var g=a.outerHeight();var h=a.offset().left;var i=a.offset().top;var j=b.width();var k=b.height();if(j>c||k>d){var x=j/2-((e.pageX-h)/f*j);var y=k/2-((e.pageY-i)/g*k);var l=f/2-(e.pageX-h);var m=g/2-(e.pageY-i);b.css({'top':g/2-k/2+y-m,'left':f/2-j/2+x-l})}});$jq('div.zoombox ul.images li').live('click',function(){var a=$jq(this);var b=$jq(this).parents('div.zoombox');var c=b.children('div.enlarged');var d=c.find('img');d.imgload(a.children('a').attr('href'),{path:config.images_dir,use_parent:true}).load(function(){$jq(this).css({'top':c.height()/2-$jq(this).height()/2,'left':c.width()/2-$jq(this).width()/2});$jq(this).parent('a').attr('href',$jq(this).attr('src'));Shadowbox.setup()});return false});$jq('div.pip ul.images').each(function(){$jq(this).css('width','auto');$jq(this).css('margin-left',-($jq(this).width()/2))});$jq('div.pip div.enlarged a').each(function(){$jq(this).css({'display':'block','width':$jq(this).parent('div.enlarged').width(),'height':$jq(this).parent('div.enlarged').height()})});$jq('div.pip ul.images li').click(function(){var a=$jq(this);var b=$jq(this).parents('div.pip');var c=b.children('div.enlarged');var d=c.find('img');d.imgload(a.children('a').attr('href'),{path:config.images_dir,use_parent:true}).load(function(){$jq(this).parent('a').attr('href',$jq(this).attr('src'));Shadowbox.setup()});return false});$jq('div.sidebar div.testimonials.slider ul.items li').show();$jq('div.sidebar div.testimonials.slider').each(function(){width=$jq(this).width();$jq(this).children('ul.items').children('li').css('width',width-2)});$jq('div.sidebar div.testimonials.slider').slider({nav:'ul.slider-nav',items:'ul.items',visible:1,slide:1,auto_height_parent:true});$jq('div.success-stories ul.items li').show();$jq('div.success-stories').each(function(){width=$jq(this).width();$jq(this).children('ul.items').children('li').css('width',width).css('display','list-item')});$jq('div.success-stories').slider({nav:'ul.slider-nav',items:'ul.items',pagination:'ul.slider-pagination',wrapper_class:'success-stories-wrapper',visible:1,slide:1});$jq('div.clients ul.items li').show();$jq('div.clients').slider({nav:'ul.slider-nav',items:'ul.items',wrapper_class:'clients-wrapper',visible:2,slide:1});$jq('ul.roundabout').css({'margin':'0 auto','display':'block','float':'none'});$jq('ul.roundabout li').css({'margin':0,'overflow':'hidden',});$jq('ul.roundabout li > a > img').css({'width':'100%','height':'100%'});$jq('.has-tooltip').tooltip({tooltip:'.tooltip',x:20,y:-30,offset:true,sub_width:true,sub_height:true,standalone:true});$jq('.has-tooltip').hover(function(){$jq(this).children('.tooltip').hide()});$jq('td').each(function(){if(typeof $jq(this).attr('custom-description')!='undefined'&&$jq(this).attr('custom-description')!=''){desc=$jq(this).attr('custom-description');$jq(this).append($jq('<span />').hide().addClass('tooltip').text(desc))}});$jq('td').tooltip({tooltip:'.tooltip',x:20,y:-30,offset:false,sub_width:true,sub_height:true});$jq('ul.menu-1 li:not(.current) ul').hide();$jq('ul.menu-1 li.current').parents('ul').show();$jq('ul.menu-1 li > a').click(function(e){var a=$jq(this).parent('li');var b=$jq(this).parent('li').siblings();a.addClass('current');a.children('ul').slideDown(500).children('li.current').removeClass('current');if(b.find('ul:visible').length>0){b.find('ul:visible').slideUp(500,function(){$jq(this).parent('li').removeClass('current')})}else{b.removeClass('current')}a.parents('li').each(function(){$jq(this).removeClass('current');$jq(this).children('ul').show()});if($jq(e.target).next('ul').length>0){return false}});$jq('div.widget div.featured ul.items li').show();$jq('div.widget div.featured').each(function(){var a=$jq(this).width()-30;$jq(this).children('ul.items').children('li').css('width',a).css('display','list-item')});$jq('div.widget div.featured').slider({nav:'ul.slider-nav',items:'ul.items',visible:1,slide:1,auto_height:true,auto_height_parent:false});$jq('div.media-slider ul.items li').show();$jq('div.media-slider').slider({items:'ul.items',visible:1,slide:1,pagination:'ul.slider-pagination'});$jq('#nav > ul').superfish({autoArrows:false,dropShadows:false});$jq('#nav').prepend($jq('<div />').addClass('tip'));$nav_current=$jq('#nav > ul > li.current');if($nav_current.length==0){$nav_current=$jq('#nav > ul > li.current-menu-parent');if($nav_current.length==0){$nav_current=$jq('#nav > ul > li:eq(0)')}}$jq('#nav div.tip').css('left',($nav_current.offset().left-(($jq(document).width()-$jq('#nav').width())/2)+($nav_current.width()/2)-8));$jq('#nav ul').hover(function(){},function(){var a=$jq(this).children('li.current');$jq('#nav div.tip').stop().animate({left:(a.offset().left-(($jq(document).width()-$jq('#nav').width())/2)+(a.width()/2)-8)},300,'linear')});$jq('#nav > ul > li').hover(function(e){$li=$jq(this);$ul=$li.parent('ul');$jq('#nav div.tip').stop().animate({left:($li.offset().left-(($jq(document).width()-$jq('#nav').width())/2)+($li.width()/2)-8)},300,'linear')},function(){});if($jq('#main-heading').length==0){$jq('head').prepend($jq('<style />').attr('type','text/css').text('div.tip:after { background-color: #fff !important; }'))}$jq('.main div.flickr-feed, .sidebar div.flickr-feed').slider({nav:'ul.slider-nav',items:'ul.items',visible:2,slide:2,speed:500});$jq('.main #twitter_div').slider({items:'ul.items',visible:2,slide:1,nav:'ul.slider-nav',axis:'y',auto_height:false,height:122*2});$jq('.sidebar #twitter_div').slider({items:'ul.items',visible:3,slide:3,nav:'ul.slider-nav',axis:'y',auto_height:true});$jq('.featured-ribbon').each(function(){$jq(this).css('top',$jq(this).parent('.featured').height()/2)});$jq('input[name=contact]').click(function(){var a=$jq(this).parents('form');a.find('input, textarea').blur();if(a.find('input.error, textarea.error').length>0){return 0}if(!a.hasClass('sent')){var b=$jq('<p class="box info"><span>'+a.find('input[name=success-message]').val()+'</span></p>');b.hide();b.click(function(){$jq(this).fadeOut(function(){$jq(this).remove()})});$jq.post(window.location.href,a.serialize()+'&contact=true',function(){a.before(b);b.fadeIn()});a.addClass('sent')}return false})});
