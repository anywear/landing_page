/**
 * Por Design Load
 *
 * Copyright (c) 2010 Por Design (pordesign.eu)
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
 * 
 *
 */

(function($)
{
	$.fn.load_begin = function(options)
	{
		var options = $.extend
		({
			path: '',
			use_parent: false,
			opacity: 0.8,
			absolute: true,
			follow: false,
			width: null,
			height: null,
			image: 'loading.gif'
		}, options);

		$(this).each( function()
		{
			$element = $(this);

			var left = $element.position().left;
			var top = $element.position().top;
			
			if (options.use_parent)
			{
				var width = $element.parent().width();
				var height = $element.parent().height();
			} else
			{
				var width = (options.width != null ? options.width : $element.width());
				var height = (options.height != null ? options.height: $element.height());
			}

			$(this).next('.loading').remove();
			$(this).next('.loading-wrapper').remove();
			
			$loading_wrapper = $('<div />');
			$loading_wrapper.addClass('loading-wrapper');
			$loading_wrapper.css
			({
				'background': '#000',
				'position': options.absolute ? 'absolute' : 'fixed',
				'left': (options.use_parent ? 0 : left),
				'top': (options.use_parent ? 0 : top),
				'width': width,
				'height': height,
				'z-index': 9998,
				'opacity': options.opacity
			}).hide();
			
			$loading = $('<div />');
			$loading.addClass('loading');
			
			$loading.css
			({
				'background': 'transparent url(' + options.path + options.image + ') 50% 50% no-repeat',
				'position': options.absolute ? 'absolute' : 'fixed',
				'left': (options.use_parent ? 0 : left),
				'top': (options.use_parent ? 0 : top),
				'width': width,
				'height': height,
				'z-index': 9999
			}).hide();
			
			$loading.fadeIn();
			$loading_wrapper.fadeIn();
			
			$(this).after($loading_wrapper);
			$(this).after($loading);
		});
		
		return this;
	};

	$.fn.load_end = function(options)
	{
		var options = $.extend
		({
			path: '',
			use_parent: false
		}, options);

		$(this).each( function()
		{
			$(this).next('.loading').remove();
			$(this).next('.loading-wrapper').remove();
		});
		
		return this;
	};
})(jQuery);
