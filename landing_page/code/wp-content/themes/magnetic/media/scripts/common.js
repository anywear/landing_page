if (top !== self) top.location.replace(self.location.href);

// SHADOWBOX BEGIN

Shadowbox.init({ language: 'en', players: ['img', 'html', 'iframe', 'qt', 'wmp', 'swf', 'flv'] });

// SHADOWBOX END

var $jq = jQuery.noConflict();

$jq( function()
{
	// JS SUPPORT BEGIN

	$jq('body').addClass('js');
	
	// JS SUPPORT END
	
	// TOGGLE BEGIN
	
	$jq('.toggle > a').each( function()
	{
		if ($jq(this).hasClass('closed'))
		{
			$jq(this).next('.toggle-content').hide();
		} else
		{
			$jq(this).next('.toggle-content').show();
		}
	});
	
	$jq('.toggle > a').click( function()
	{
		$jq(this).toggleClass('closed');
		
		if ( ! $jq(this).hasClass('closed'))
		{
			$jq(this).next('.toggle-content').show();
		} else
		{
			$jq(this).next('.toggle-content').hide();
		}
		
		return false;
	});
	
	// TOGGLE END
	
	// ALERT BOX BEGIN
	
	$jq('.box a.close').click( function()
	{
		$jq(this).parent().fadeOut(500, function()
		{
			$jq(this).remove();
		});

		return false;
	});
	
	// ALERT BOX END
	
	$jq('.tabs').tabs();
	
	// DESCRIPTION FOR IMAGES FROM TITLE ATTRIBUTE BEGIN
	
	$jq('a.frame-holder > img').each( function()
	{
		if ($jq(this).attr('title') != '' && $jq(this).parents('.flickr-feed').length == 0)
		{
			title = $jq(this).attr('title');

			$jq(this).after
			(
				$jq('<span />').addClass('title-description').text(title)
			);
			
			if ($jq(this).hasClass('aligncenter') || $jq(this).parent('a.frame-holder').hasClass('aligncenter'))
			{
				$jq(this).load( function()
				{
					$img = $jq(this);
					
					$jq(this).parent('a.frame-holder').css
					({
						'width': $img.width()
					});
				});
			}
		}
	});
	
	$jq('.frame-holder > *').each( function()
	{
		if ($jq(this)[0].tagName.toLowerCase() != 'img' && $jq(this)[0].tagName.toLowerCase() != 'span')
		{
			$jq(this).parent('.frame-holder').width($jq(this).width());
		}
	});
	
	// DESCRIPTION FOR IMAGES FROM TITLE ATTRIBUTE END
	
	// SCROLL TOP BEGIN
	
	$jq('a[href=#header], .separator').click( function()
	{
		$jq('html,body').animate
		({
			'scrollTop': 0
		}, 300);
		
		return false;
	});
	
	// SCROLL TOP END
	
	// FORM VALIDATION BEGIN
	
	$jq(config.form_validate).blur( function()
	{
		var val = $jq(this).val();

		$jq(this).removeClass('valid');
			
		if (val == '' || val == $jq(this).data('placeholder'))
		{
			$jq(this).addClass('error');
		} else
		{
			if ($jq(this).attr('name') == 'email' || $jq(this).attr('id') == 'email' || $jq(this).hasClass('email'))
			{
				if (/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(val))
				{
					$jq(this).removeClass('error').addClass('valid');
				} else
				{
					$jq(this).addClass('error');
				}
			} else
			{
				$jq(this).removeClass('error').addClass('valid');
			}
		}
	});
	
	$jq('form').submit( function()
	{
		if ($jq(this).find('input.error, textarea.error').length > 0)
		{
			return false;
		}
	});

	// FORM VALIDATION END
	
	// FORM PLACEHOLDERS BEGIN
	
	$jq(config.input_placeholder).placeholder(config.placeholder);
		
	// FORM PLACEHOLDERS END
	
	// ZOOMBOX BEGIN

	$jq('div.zoombox').each( function()
	{
		var item_width = $jq(this).children('ul.images').children('li').width();

		var $enlarged = $jq(this).children('div.enlarged');
		var $img = $jq(this).children('div.enlarged').find('img');
		
		$enlarged.children('a').css
		({
			'display': 'block',
			'width': $enlarged.width(),
			'height': $enlarged.height()
		});
		
		if ($jq(this).parent().width() < $enlarged.width())
		{
			$enlarged.width($jq(this).parent().width());
		}
		
		$img.load( function()
		{
			$jq(this).css
			({
				'position': 'absolute',
				'top': $enlarged.height()/2 - $img.height()/2,
				'left': $enlarged.width()/2 - $img.width()/2
			});
		});
		
		$jq(this).slider
		({
			nav: 'ul.slider-nav',
			items: 'ul.images',
			visible: Math.round(($jq(this).width() / item_width)) - 2,
			slide: 2,
			speed: 600,
			auto_width: false,
			width: $enlarged.width() - item_width
		});
	});
	
	$jq('div.zoombox div.enlarged').mousemove( function(e)
	{
		var $enlarged = $jq(this);
		var $img = $enlarged.find('img');
		
		var e_width = $enlarged.width();
		var e_height = $enlarged.height();
		var e_owidth = $enlarged.outerWidth();
		var e_oheight = $enlarged.outerHeight();
		var e_offsetl = $enlarged.offset().left;
		var e_offsett = $enlarged.offset().top;
		var i_width = $img.width();
		var i_height = $img.height();
		
		if (i_width > e_width || i_height > e_height)
		{
			var x = i_width/2 - ((e.pageX - e_offsetl) / e_owidth * i_width);
			var y = i_height/2 - ((e.pageY - e_offsett) / e_oheight * i_height);
			var offsetx = e_owidth/2 - (e.pageX - e_offsetl);
			var offsety = e_oheight/2 - (e.pageY - e_offsett);

			$img.css
			({
				'top': e_oheight/2 - i_height / 2 + y - offsety, 
				'left': e_owidth/2 - i_width / 2 + x - offsetx 
			});
		}
	});

	$jq('div.zoombox ul.images li').live('click', function()
	{
		var $this = $jq(this);

		var $zoombox = $jq(this).parents('div.zoombox');
		var $enlarged = $zoombox.children('div.enlarged');
		
		var $img = $enlarged.find('img');
		
		$img.imgload($this.children('a').attr('href'), { path: config.images_dir, use_parent: true }).load( function()
		{
			$jq(this).css
			({
				'top': $enlarged.height()/2 - $jq(this).height()/2,
				'left': $enlarged.width()/2 - $jq(this).width()/2
			});
			
			$jq(this).parent('a').attr('href', $jq(this).attr('src'));
			
			Shadowbox.setup();
			//$img.attr('width', $jq(this).width());
			//$img.attr('height', $jq(this).height());
		});

		return false;
	});
	
	// ZOOMBOX END
	
	// PIP BEGIN
	
	$jq('div.pip ul.images').each( function()
	{
		$jq(this).css('width', 'auto');
		$jq(this).css('margin-left', -($jq(this).width() / 2));
	});

	$jq('div.pip div.enlarged a').each( function()
	{
		$jq(this).css
		({
			'display': 'block',
			'width': $jq(this).parent('div.enlarged').width(),
			'height': $jq(this).parent('div.enlarged').height()
		});
	});
	
	$jq('div.pip ul.images li').click( function()
	{
		var $this = $jq(this);

		var $pip = $jq(this).parents('div.pip');
		var $enlarged = $pip.children('div.enlarged');
		
		var $img = $enlarged.find('img');
		
		$img.imgload($this.children('a').attr('href'), { path: config.images_dir, use_parent: true }).load( function()
		{
			$jq(this).parent('a').attr('href', $jq(this).attr('src'));

			Shadowbox.setup();
		});

		return false;
	});
	
	// PIP END
	
	// TESTIMONIALS BEGIN
	
	$jq('div.sidebar div.testimonials.slider ul.items li').show();
	
	$jq('div.sidebar div.testimonials.slider').each( function()
	{
		width = $jq(this).width();
		
		$jq(this).children('ul.items').children('li').css('width', width - 2);
	});

	$jq('div.sidebar div.testimonials.slider').slider
	({
		nav: 'ul.slider-nav',
		items: 'ul.items',
		visible: 1,
		slide: 1,
		auto_height_parent: true
	});

	// TESTIMONIALS END
	
	// SUCCESS STORIES BEGIN

	$jq('div.success-stories ul.items li').show();
	
	$jq('div.success-stories').each( function()
	{
		width = $jq(this).width();
		
		$jq(this).children('ul.items').children('li').css('width', width).css('display', 'list-item');
	});

	$jq('div.success-stories').slider
	({
		nav: 'ul.slider-nav',
		items: 'ul.items',
		pagination: 'ul.slider-pagination',
		wrapper_class: 'success-stories-wrapper',
		visible: 1,
		slide: 1
	});
	
	// SUCCESS STORIES END
	
	// CLIENTS SLIDER BEGIN
	
	$jq('div.clients ul.items li').show();
	
	$jq('div.clients').slider
	({
		nav: 'ul.slider-nav',
		items: 'ul.items',
		wrapper_class: 'clients-wrapper',
		visible: 2,
		slide: 1
	});
	
	// CLIENTS SLIDER END
	
	// ROUNDABOUT BEGIN
	
	$jq('ul.roundabout').css
	({
		'margin': '0 auto',
		'display': 'block',
		'float': 'none'
	});

	$jq('ul.roundabout li').css
	({
		'margin': 0,
		'overflow': 'hidden',
	});

	// ROUNDABOUT END
	
	// TOOLTIPS BEGIN
	
	$jq('.has-tooltip').tooltip
	({
		tooltip: '.tooltip',
		x: 20,
		y: -30,
		offset: true,
		sub_width: true,
		sub_height: true,
		standalone: true
	});
		
	$jq('.has-tooltip').hover(function() { $jq(this).children('.tooltip').hide();});
	
	// TOOLTIPS END
	
	// TOOLTIPS FOR TABLES BEGIN
	
	$jq('td').each( function()
	{
		if (typeof $jq(this).attr('custom-description') != 'undefined' && $jq(this).attr('custom-description') != '')
		{
			desc = $jq(this).attr('custom-description');
			$jq(this).append($jq('<span />').hide().addClass('tooltip').text(desc));
		}
	});
	
	$jq('td').tooltip
	({
		tooltip: '.tooltip',
		x: 20,
		y: -30,
		offset: false,
		sub_width: true,
		sub_height: true
	});
	
	// TOOLTIPS FOR TABLES END
	
	// CUSTOM MENU BEGIN

	$jq('ul.menu-1 li:not(.current) ul').hide();
	$jq('ul.menu-1 li.current').parents('ul').show();

	$jq('ul.menu-1 li > a').click( function(e)
	{
		var $current = $jq(this).parent('li');
		var $siblings = $jq(this).parent('li').siblings();
	
		$current.addClass('current');
		$current.children('ul').slideDown(500).children('li.current').removeClass('current');
		
		if ($siblings.find('ul:visible').length > 0)
		{
			$siblings.find('ul:visible').slideUp(500, function()
			{
				$jq(this).parent('li').removeClass('current');
			});
		} else
		{
			$siblings.removeClass('current');
		}
		
		$current.parents('li').each( function()
		{
			$jq(this).removeClass('current');
			$jq(this).children('ul').show();
		});
	
		if ($jq(e.target).next('ul').length > 0)
		{
			return false;
		}
	});

	// CUSTOM MENU END

	// FEATURED POSTS BEGIN
	
	$jq('div.widget div.featured ul.items li').show();
	$jq('div.widget div.featured').each( function()
	{
		//$jq(this).width($jq(this).parent().width());
		var width = $jq(this).width() - 30;

		$jq(this).children('ul.items').children('li').css('width', width).css('display', 'list-item');
	});

	$jq('div.widget div.featured').slider
	({
		nav: 'ul.slider-nav',
		items: 'ul.items',
		visible: 1,
		slide: 1,
		auto_height: true,
		auto_height_parent: false
	});
	
	// FEATURED POSTS END
	
	$jq('div.media-slider ul.items li').show();
	$jq('div.media-slider').slider
	({
		items: 'ul.items',
		visible: 1,
		slide: 1,
		pagination: 'ul.slider-pagination'
	});
	
	// SUPERFISH BEGIN
	
	$jq('#nav > ul').superfish
	({
		autoArrows: false,
		dropShadows : false
	});
	
	$jq('#nav').prepend($jq('<div />').addClass('tip'));
	$nav_current = $jq('#nav > ul > li.current');
	
	if ($nav_current.length == 0)
	{
		$nav_current = $jq('#nav > ul > li.current-menu-parent');
		
		if ($nav_current.length == 0)
		{
			$nav_current = $jq('#nav > ul > li:eq(0)');
		}
	}
	
	$jq('#nav div.tip').css('left', ($nav_current.offset().left - (($jq(document).width() - $jq('#nav').width()) / 2) + ($nav_current.width() / 2) - 8));
	
	$jq('#nav ul').hover( function() {},
	function()
	{
		var $current = $jq(this).children('li.current');
		
		$jq('#nav div.tip').stop().animate
		({
			left: ($current.offset().left - (($jq(document).width() - $jq('#nav').width()) / 2) + ($current.width() / 2) - 8)
		}, 300, 'linear');
	});

	$jq('#nav > ul > li').hover( function(e)
	{
		$li = $jq(this);
		$ul = $li.parent('ul');

		$jq('#nav div.tip').stop().animate
		({
			left: ($li.offset().left - (($jq(document).width() - $jq('#nav').width()) / 2) + ($li.width() / 2) - 8)
		}, 300, 'linear');
	}, function() {});
	
	// SUPERFISH END
	
	if ($jq('#main-heading').length == 0)
	{
		$jq('head').prepend($jq('<style />').attr('type', 'text/css').text('div.tip:after { background-color: #fff !important; }'));
	}
	
	$jq('.main div.flickr-feed, .sidebar div.flickr-feed').slider
	({
		nav: 'ul.slider-nav',
		items: 'ul.items',
		visible: 2,
		slide: 2,
		speed: 500
	});
	
	$jq('.main #twitter_div').slider
	({
		items: 'ul.items',
		visible: 2,
		slide: 1,
		nav: 'ul.slider-nav',
		axis: 'y',
		auto_height: false,
		height: 122 * 2
	});
	
	$jq('.sidebar #twitter_div').slider
	({
		items: 'ul.items',
		visible: 3,
		slide: 3,
		nav: 'ul.slider-nav',
		axis: 'y',
		auto_height: true
	});
	
	$jq('.featured-ribbon').each( function()
	{
		$jq(this).css('top', $jq(this).parent('.featured').height() / 2);
	});
	
	$jq('input[name=contact]').click( function()
	{
		var $form = $jq(this).parents('form');
		
		$form.find('input, textarea').blur();
		
		if ($form.find('input.error, textarea.error').length > 0)
		{
			return 0;
		}

		if ( ! $form.hasClass('sent'))
		{
			var $info = $jq('<p class="box info"><span>' + $form.find('input[name=success-message]').val() + '</span></p>');
			$info.hide();

			$info.click( function()
			{
				$jq(this).fadeOut( function()
				{
					$jq(this).remove();
				});
			});			
			
			$jq.post(window.location.href, $form.serialize() + '&contact=true', function()
			{
				$form.before($info);
				
				$info.fadeIn();
			});
			
			
			$form.addClass('sent');
		}
			
		return false;
	});
});
