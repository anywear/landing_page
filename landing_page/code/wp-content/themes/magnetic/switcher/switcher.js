switcher = 
{
	themes:
	[
		{
			name: 'Openlogic',
			url: 'http://goo.gl/ZQ4es',
			thumbnail: 'http://s3.envato.com/files/1761243/0-preview-html.__large_preview.jpg'
		},
		{
			name: 'Magnetic',
			url: 'http://goo.gl/ua5Vy',
			thumbnail: 'http://s3.envato.com/files/3914121/0-preview-wp.__large_preview.jpg'
		},
		{
			name: 'ChilliBox',
			url: 'http://goo.gl/47NxA',
			thumbnail: 'http://s3.envato.com/files/1415277/00-preview-html.__large_preview.jpg'
		},
		{
			name: 'WebPoint',
			url: 'http://goo.gl/K18Fv',
			thumbnail: 'http://s3.envato.com/files/1225775/00_preview.__large_preview.jpg'
		},
		{
			name: 'Aurora',
			url: 'http://goo.gl/tgYLa',
			thumbnail: 'http://s3.envato.com/files/368343/screens/01_Preview.__large_preview.jpg'
		},
		{
			name: 'Designer',
			url: 'http://goo.gl/KzlhP',
			thumbnail: 'http://s3.envato.com/files/367184/screenshots/01_Theme_preview.__large_preview.jpg'
		},
		{
			name: 'DigitalArt',
			url: 'http://goo.gl/M8hAz',
			thumbnail: 'http://s3.envato.com/files/341417/screens%20html/01_Theme_preview.__large_preview.jpg'
		},
		{
			name: 'Creato',
			url: 'http://goo.gl/9fgSd',
			thumbnail: 'http://s3.envato.com/files/341438/screens%20html/01_theme_preview.__large_preview.jpg'
		}
	],
	sliders:
	[
		{
			name: 'Default',
			url: 'http://demo.themefields.net/magnetic/wordpress/theme/'
		},
		{
			name: 'Carusel',
			url: 'http://demo.themefields.net/magnetic/wordpress/theme/gallery/typo/'
		},
		{
			name: 'Nivo',
			url: 'http://demo.themefields.net/magnetic/wordpress/theme/widgets-buttons/'
		},
		{
			name: '3D',
			url: 'http://demo.themefields.net/magnetic/wordpress/theme/easycolumn-system/'
		}
	],
	fonts:
	[
		'Allerta',
		'Allerta Stencil',
		'Arimo',
		'Arvo',
		'Bentham',
		'Cantarell',
		'Cardo',
		'Coda',
		'Cousine',
		'Covered By Your Grace',
		'Crisomo Text',
		'Cuprum',
		'Droid Sans Mono',
		'Droid Sans',
		'Droid Serif',
		'Geo',
		'IM Fell',
		'Inconsolata',
		'Jangerine',
		'Josefin Sans',
		'Josefin Slab',
		'Just Me Again Down Here',
		'Lobster',
		'Molengo',
		'Neucha',
		'Neuton',
		'Nobile',
		'OFL Sorts Mill',
		'Old Standard TT',
		'Philosopher',
		'PT Sans',
		'Puritan',
		'Raleway',
		'Reenie Beanie',
		'Tinos',
		'Unifraktur Cook',
		'Unifraktur Magunita',
		'Vollkorn',
		'Yanone Kaffeesatz'
	],
	font_pattern:
	[
		'h1',
		'h2',
		'h3',
		'h4',
		'h5',
		'h6'
	],
	color_pattern:
	{
		'background-color':
		[
			'#main-heading',
			'#nav #left-bg',
			'#nav #right-bg',
			'#nav',
			'#nav ul ul',
			'.tip:after'
		],
		'color':
		[
			'.title h2',
			'.title h3'
		]
	},
	color_pattern2:
	{
		'color':
		[
			'a',
			'a:hover',
			'a:focus',
			'h4',
			'h6',
			'.pricing .alt',
			'#main-heading #twitter_update_list li a',
			'.menu-1 li.current > a',
			'blockquote.style-1 p',
			'.services h4 a',
			'.success-stories h4 a',
			'.testimonials a',
			'#twitter_update_list a'
		],
		'background-color':
		[
			'.testimonials .read-more',
			'.button.default'
		],
		'border-color':
		[
			'.testimonials .read-more:after',
			'.frame-holder:hover img'
		],
		'border-right-color':
		[
			'.right-sidebar .menu-1 li.current > a'
		],
		'border-left-color':
		[
			'.left-sidebar .menu-1 li.current > a'
		]
	},
	psd_url: 'http://goo.gl/xXs5i',
	html_url: 'http://goo.gl/YCqVr',
	wp_url: '#',
	purchase_url: 'http://goo.gl/zzKYh'
};

$j = jQuery.noConflict();

function load_script(path)
{
	$j('head').append($j('<script />').attr('type', 'text/javascript').attr('src', path));
}

function load_stylesheet(path)
{
	var style = $j('<link />').attr('rel', 'stylesheet').attr('href', path).attr('type', 'text/css');
	document.getElementsByTagName('head')[0].appendChild(style[0]);
}

function hex(x)
{
	return ("0" + parseInt(x).toString(16)).slice(-2);
}
	
function rgb2hex(rgb)
{
	rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);

	return hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
}

$j( function()
{
	load_script('http://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js');
	load_script(ether.path + 'switcher/media/scripts/colorpicker.js');
	load_stylesheet(ether.path + 'switcher/media/stylesheets/switcher.css');
	load_stylesheet(ether.path + 'switcher/media/stylesheets/colorpicker.css');
	
	var $switcher = $j('<div />').attr('id', 'switcher-holder').addClass('closed js').load(ether.path + 'switcher/switcher.html #switcher-holder > *', function()
	{
		$switcher.find('img').each( function()
		{
			$j(this).attr('src', ether.path + $j(this).attr('src'));
		});

		$j('body').prepend($switcher);
		
		$j('#switcher-holder li.toggle').click( function()
		{
			if ($j('#switcher-holder').hasClass('closed'))
			{
				$j('#switcher-holder').animate
				({
					'margin-top': 0
				}, 'slow', function()
				{
					$j(this).removeClass('closed');
				});
				
				
				$j(this).animate
				({
					'margin-top': 14,
					'margin-bottom': 0
				}, 'slow', function()
				{
					$j(this).find('a').attr('title', 'Hide theme options');
				});
			} else
			{
					$j('#switcher-holder').animate
				({
					'margin-top': -55
				}, 'slow', function()
				{
					$j(this).addClass('closed');
				});
				
				$j(this).animate
				({
					'margin-top': 55,
					'margin-bottom': -55
				}, 'slow', function()
				{
					$j(this).find('a').attr('title', 'Customize theme');
				});
			}
		});
		
		$j('#switcher li.psd a').attr('href', switcher.psd_url);
		$j('#switcher li.html a').attr('href', switcher.html_url);
		$j('#switcher li.wp a').attr('href', switcher.wp_url);
		$j('#switcher a.purchase').attr('href', switcher.purchase_url);

		$j.each(switcher.themes, function(i, theme)
		{
			$j('#switcher li.select-theme ul.items').append($j('<li />').append($j('<a />').attr('href', theme.url).text(theme.name)).append($j('<img />').attr('src', (theme.thumbnail.substring(0, 7) == 'http://' ? theme.thumbnail : ether.path + theme.thumbnail)).attr('width', 590).attr('height', 300)));
		});
		
		$j.each(switcher.sliders, function(i, slider)
		{
			$j('#switcher li.change-slider ul.items').append($j('<li />').append($j('<a />').attr('href', (slider.url.substring(0, 7) == 'http://' ? slider.url : ether.path + slider.url)).text(slider.name)));
		});

		$j('#switcher li.change-font div.dropdown ul.items').css('overflow-x', 'hidden');
		
		$j.each(switcher.fonts, function(i, font)
		{
			var $font = $j('<li />');
			$font.append($j('<a />').append($j('<img />').attr('src', ether.path + 'switcher/media/fonts/' + font.toLowerCase().replace(/ /g, '-') + '.png').attr('title', font).attr('alt', font.toLowerCase().replace(/ /g, '-'))));
			
			if (i == 0)
			{
				$font.addClass('selected');
			}
			
			$j('#switcher li.change-font div.dropdown ul.items').append($font);
		});
		
		$j('#switcher li.change-font').hover( function()
		{
			$j(this).children('div.dropdown').show();
		}, function()
		{
			$j(this).children('div.dropdown').hide();
		});

		$j('#switcher li.change-font div.dropdown ul.items li').click( function()
		{
			$j(this).parent('ul.items').children('li').removeClass('selected');
			$j(this).addClass('selected');
				
			var style = '';
			var font = $j(this).find('img').attr('title');
			var variants = 'regular';
				
			$j('style.fonts').remove();
			$j("link[href^='http://fonts.googleapis.com']").remove();
				
			if (font.toLowerCase() == 'diavlo')
			{

			} else
			{
				WebFont.load
				({
					google:
					{
						families: [ font.replace(/ /g,"+") + ':' + 'regular,bold' ]
					}
				});	
					
				$j.each(switcher.font_pattern, function(index, selector)
				{
					style += selector + ' { font-family: \'' + font + '\';}';
				});

				$j('<style />').attr('type', 'text/css').text(style).addClass('fonts').addClass('custom-rules').appendTo('head');
				$j('#switcher li.change-font div.dropdown').hide();
			}
				
			return false;
		});

	});
	
});

$j(window).load( function()
{
	$j('#switcher li.change-color-1').data('color', '#000000')
	$j('#switcher li.change-color-2').data('color', '#000000')

	$j('#switcher li.change-color-1 div.dropdown').ColorPicker
	({
		flat: true,
		onSubmit: function(hsb, hex, rgb, el)
		{
			
		},
		onBeforeShow: function()
		{
			$j(this).ColorPickerSetColor($j('#switcher li.change-color-1').data('color'));
		},
		onChange: function(hsb, hex, rgb)
		{
			var style = '';
			var color = '#' + hex;

			$j.each(switcher.color_pattern, function(key, elements)
			{
				$j.each(elements, function(index, selector)
				{
					style += selector + ' {' + key + ':' + color + (selector == '.tip:after' ? ' !important' : '') + ';}';
				});
			});

			$j('style.colors1').remove();
			$j('<style />').attr('type', 'text/css').text(style).addClass('colors1').addClass('custom-rules').appendTo('head');
			$j('#switcher li.change-color-1').data('color', color);
		}
	}).hover( function()
	{
		//$j(this).ColorPickerShow();
	}, function()
	{
		//$j(this).ColorPickerHide();
	});
	
	$j('#switcher li.change-color-2 div.dropdown').ColorPicker
	({
		flat: true,
		onSubmit: function(hsb, hex, rgb, el)
		{
			
		},
		onBeforeShow: function()
		{
			$j(this).ColorPickerSetColor($j('#switcher li.change-color-2').data('color'));
		},
		onChange: function(hsb, hex, rgb, el)
		{
			var style = '';
			var color = '#' + hex;

			$j.each(switcher.color_pattern2, function(key, elements)
			{
				$j.each(elements, function(index, selector)
				{
					style += selector + ' {' + key + ':' + color + (selector == '.testimonials .read-more:after' ? ' transparent' : '') + ';}';
				});
			});

			$j('style.colors2').remove();
			$j('<style />').attr('type', 'text/css').text(style).addClass('colors2').addClass('custom-rules').appendTo('head');
			$j('#switcher li.change-color-2').data('color', color);
		}
	}).hover( function()
	{
		//$j(this).ColorPickerShow();
	}, function()
	{
		//$j(this).ColorPickerHide();
	});
		
	$j('div#switcher').show();
	$j('#switcher-holder').removeClass('closed');
});
