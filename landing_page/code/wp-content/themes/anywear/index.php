<?php
/**
* The main template file
*
* This is the most generic template file in a WordPress theme
* and one of the two required files for a theme (the other being style.css).
* It is used to display a page when nothing more specific matches a query.
* For example, it puts together the home page when no home.php file exists.
*
* @link http://codex.wordpress.org/Template_Hierarchy
*
* @package WordPress
* @subpackage Twenty_Twelve
* @since Twenty Twelve 1.0
*/
$site_url = get_option('siteurl');

get_header(); ?>

	
<div id="section-1">
   <div class="page page2">
	<div class="page-block" id="page_block_footer" style="height: 650px; overflow:hidden; margin-bottom:0;">
    
    	<div class="page-element widget-container page-element-type-headline widget-headline" id="element-8" style="height: 96px;width: auto;left: 50px;top: 155px;z-index: 7;">
            <div class="contents">
              <h1>
                <p style="text-align: center;"><span style=""><span style="" class="cnt_head">Shop Only Where It's Right For You!</span></span></p>
              </h1>
            </div>
        </div>
        
        <div class="page-element widget-container page-element-type-headline widget-headline" id="element-20" style="height: 126px;width: 518px;left: 50px;top: 228px;z-index: 19;">
            <div class="contents">
              <h1>
                <p style="font-size: 30px; line-height: 75%; text-align: left;"> <span><span style="" class="cnt_desc">Discover a Tailored List of Fashion Boutiques Worldwide that Match Your Personal Profile &amp; Style</span>  </span> </p>
              </h1>
            </div>
      	</div>
        
        <div class="page-element widget-container page-element-type-headline widget-headline" id="element-24" style="height: 126px;width: 415px;left: 50px;top: 303px;z-index: 23;">
            <div class="contents">
              <h1>
                <p style="font-size: 30px; line-height: 75%; "> <span><span class="cnt_desc">Visit the Boutiques Near you or Shop online from your smartphone</span> </em> </span> </p>
              </h1>
            </div>
            <!--<div class="mybtn">
                <a class="shop_btn" id="shopregister" href="#shopregisterdiv">Shop Register</a>
              <a class="shop_btn" id="shopcontactus" href="#shopcontactusdiv">Contact Us</a>
            </div>-->
        </div>
        
         <div class="page-element widget-container page-element-type-image widget-image" id="element-6" style="height: 526px;width: 264px;left: 572px;top: 18px;z-index: 6;">
            <div class="contents"> <img src="<?php echo get_template_directory_uri(); ?>/images/white-phone3.jpg" alt=""> </div>
         </div>
        
        <div class="page-element widget-container page-element-type-video widget-video" id="element-7" style="height: 405px;width: 230px;left: 630px;top: 95px;z-index: 7;">
            <div class="video_wrapper contents">
              <ul id="site-iphone5" class="muslide">
                <li><img alt="Slide1" src="<?php echo get_template_directory_uri(); ?>/images/Screen_1.png"></li>
                <li><img alt="Slide2" src="<?php echo get_template_directory_uri(); ?>/images/Screen_2.png"></li>
                <li><img alt="Slide3" src="<?php echo get_template_directory_uri(); ?>/images/Screen_3.png"></li>
                <li><img alt="Slide4" src="<?php echo get_template_directory_uri(); ?>/images/Screen_4.png"></li>
                <li><img alt="Slide5" src="<?php echo get_template_directory_uri(); ?>/images/Screen_5.png"></li>
                <li><img alt="Slide4" src="<?php echo get_template_directory_uri(); ?>/images/Screen_6.png"></li>
                <li><img alt="Slide5" src="<?php echo get_template_directory_uri(); ?>/images/Screen_7.png"></li>
              </ul>
            </div>
        </div>
        
    </div>
    
	</div>
</div>
	
    <!-- End of Section-1 -->
<div id="section-2">    
  <div class="page page2">
	<div class="page-block section-2" id="page_block_footer" style="height: 950px; overflow:hidden; margin-bottom:0;">

    	
        <div class="page-element widget-container page-element-type-headline widget-headline" id="element-2" style="height: 100px;width: 279px;left: 321px;top: 30px;z-index: 4;">
            <div class="contents">
              <h1>
                <p style="text-align: center; font-size: 36px; line-height: 140%;"><span class="cnt_head">Tell us:</span> </p>
              </h1>
            </div>
         </div>
         
         <div class="page-element widget-container page-element-type-headline widget-headline" id="element-12" style="height: 33px;width: 370px;left: 140px;top: 80px;z-index: 11;">
            <div class="contents">
              <h1>
                <p style="font-size: 24px; line-height: 140%;"><span class="cnt_sub_head">Your Style</span></p>
              </h1>
            </div>
         </div>
  	  	
        <div class="page-element widget-container page-element-type-image widget-image" id="element-4" style="height: 453px;width: 228px;left: 30px;top: 124px;z-index: 5;">
            <div class="contents"> <img src="<?php echo get_template_directory_uri(); ?>/images/white-phone1.png" alt=""> </div>
        </div>
        
        <div class="page-element widget-container page-element-type-headline widget-headline" id="element-11" style="height: 33px;width: 370px;left: 400px;top: 100px;z-index: 10;">
            <div class="contents">
              <h1>
                <p style="font-size: 24px; line-height: 140%;"><span class="cnt_sub_head">Your Size</span></p>
              </h1>
            </div>
        </div>
        
        <div class="page-element widget-container page-element-type-image widget-image" id="element-1" style="height: 410px;width: 207px;left: 285px;top: 150px;z-index: 10;">
        	<div class="contents"> <img src="<?php echo get_template_directory_uri(); ?>/images/white-phone4.png" alt=""> </div>
      	</div>
      
      	<div class="page-element widget-container page-element-type-headline widget-headline" id="element-10" style="height: 33px;width: 370px;left: 635px;top: 85px;z-index: 9;">
            <div class="contents">
              <h1>
                <p style="font-size: 24px; line-height: 140%;"><span class="cnt_sub_head">Your Budget &nbsp;</span></p>
              </h1>
            </div>
      	</div>
  
  		<div class="page-element widget-container page-element-type-image widget-image" id="element-3" style="height: 460px;width: 231px;left: 540px;top: 125px;z-index: 5;">
        	<div class="contents"> <img src="<?php echo get_template_directory_uri(); ?>/images/white-phone2.png" alt=""> </div>
      	</div>
        
        <div class="page-element widget-container page-element-type-headline widget-headline" id="element-13" style="height: 168px;width: 738px;left: 115px;top: 750px;z-index: 12;">
            <div class="contents">
              <h1>
                <p style="font-size: 30px; line-height: 95%; text-align: center;"><span class="cnt_sub_head" style="color:#4f5050; font-weight:bold">Then...<br>
                  Boutiqall will find the boutiques that <br>
                  you are looking for,&nbsp;in your price and size range, and <br> most importantly, in the styles you love!</span></p>
              </h1>
            </div>
        </div>
      
      
	</div>
   </div>
 </div>
    
    
    <!-- End of Section-2 -->
    
<div id="section-3">
  <div class="page page2">
	<div class="page-block" id="page_block_footer" style="height: 675px; overflow:hidden; margin-bottom:0;">

        
        <div class="page-element widget-container page-element-type-image widget-image" id="element-28" style="height: 467px;width: 237px;left: 30px;top: 50px;z-index: 27;">
            <div class="contents"> <img src="<?php echo get_template_directory_uri(); ?>/images/white-phone5.jpg" alt=""> </div>
        </div>
        
         <div class="page-element widget-container page-element-type-headline widget-headline" id="element-14" style="height: 42px;width: 188px;left: 450px;top: 100px;z-index: 13;">
            <div class="contents">
              <h1>
                <p><span class="cnt_head">Explore</span></p>
              </h1>
            </div>
         </div>
         
          <div class="page-element widget-container page-element-type-headline widget-headline" id="element-17" style="height: 25px;width: 428px;left: 452px;top: 150px;z-index: 16;">
            <div class="contents">
                <p><span class="cnt_desc cnt_desc_clr">New Boutiques, New Designers, New Trends&nbsp;</span></p>
            </div>
          </div>
          
          <div class="page-element widget-container page-element-type-headline widget-headline" id="element-14" style="height: 42px;width: 225px;left: 450px;top: 200px;z-index: 13;">
            <div class="contents">
              <h1>
                <p><span class="cnt_head">SHOP AROUND</span></p>
              </h1>
            </div>
         </div>
          
          <div class="page-element widget-container page-element-type-headline widget-headline" id="element-21" style="height: 25px;width: 370px;left: 452px;top: 248px;z-index: 20;">
            <div class="contents">
              	 <p><span class="cnt_desc cnt_desc_clr">In the Boutiques Near You or Online</span></p>
            </div>
          </div>
          
          <div class="page-element widget-container page-element-type-headline widget-headline" id="element-22" style="height: 42px;width: 188px;left: 450px;top: 296px;z-index: 21;">
            <div class="contents">
              <h1>
              	<p><span class="cnt_head">Collect</span></p>
              </h1>
            </div>
          </div>
          
           <div class="page-element widget-container page-element-type-headline widget-headline" id="element-23" style="height: 25px;width: 428px;left: 452px;top: 340px;z-index: 22;">
                <div class="contents">
                    <p><span class="cnt_desc cnt_desc_clr">Points, Rewards and Monthly Possibilities</span></p>
                </div>
          </div>
          
          <div class="page-element widget-container page-element-type-headline widget-headline" id="element-15" style="height: 42px;width: 188px;left: 450px;top: 384px;z-index: 14;">
            <div class="contents">
              <h1>
              	<p><span class="cnt_head">Recommend</span></p>
              </h1>
            </div>
          </div>
          
          <div class="page-element widget-container page-element-type-headline widget-headline" id="element-18" style="height: 25px;width: 370px;left: 452px;top: 435px;z-index: 17;">
            <div class="contents">
                <p><span class="cnt_desc cnt_desc_clr">What you Love, What you Want</span></p>
            </div>
          </div>
          
          <div class="page-element widget-container page-element-type-headline widget-headline" id="element-30" style="height: 42px;width: 188px;left: 450px;top: 478px;z-index: 29;">
            <div class="contents">
              <h1>
              	<p><span class="cnt_head">Become</span></p>
              </h1>
            </div>
          </div>
          
          <div class="page-element widget-container page-element-type-headline widget-headline" id="element-31" style="height: 25px;width: 370px;left: 452px;top: 527px;z-index: 30;">
            <div class="contents">
              	<p><span class="cnt_desc cnt_desc_clr">A Trendsetter, an Editor, a Fashionista</span></p>
            </div>
          </div>
          
    
    </div>
 </div>
</div>
  
  	<!-- End of Section-3 -->
    
<div id="section-4">
   <div class="page page2">
	 <div class="page-block section-4" id="page_block_footer" style="height: 350px; overflow:hidden; margin-bottom:0;">   
        <div class="page-element widget-container page-element-type-text widget-text" id="element-9" style="height: 132px;width: 801px;left: 50px;top: 0px;z-index: 8;">
            <div class="contents">
              <p style="font-size: 24px; line-height: 140%; color: #000000;"><em><strong>Boutiqall</strong> <strong>is coming soon!&nbsp;<br>
                </strong><span style="font-size: 24px;">Sign up and be the first to know when to download! Start receiving monthly newsletters, stay up to date with fashion trends, events, sales, new boutiques <br>
                and editor's picks!</span></em></p>
            </div>
            <div>
	       <!--<form action=""  method="post" id="frmsubmit" name="frmsubmit" enctype="multipart/form-data">
            	<div class="txt_box">
                	<input type="text" id="user_nicename" name="user_nicename" class="txt_sub" placeholder="Name"><br>
                	<input type="text" id="user_email" name="user_email" class="txt_sub" placeholder="Email">
                </div>-->
                
                <div>
                	<!--<button class="submit-button" style="line-height: 20.8px; font-size: 17.333333333333px;">Submit</button>-->
		    	<!--<input type="button" value="Sign Up" class="shop_btn submit-button_1"  onclick="saveuser();">-->
			<!--<input type="button" value="Sign Up" class="shop_btn submit-button_1"  id="signup" href="#signupDiv">-->
			<a class="shop_btn submit-button_1" id="signup" href="#signupDiv">Sign Up</a>
                        <a class="shop_btn submit-button_2" id="shopregister" href="#shopregisterdiv">Shop Registration</a>
                      	<a class="shop_btn submit-button_3" id="shopcontactus" href="#shopcontactusdiv">Contact Us</a>
                   
                </div>
	       <!--</form>-->
            </div>
            
            <div style="text-align:center;top: 80px; position: relative; "><?php echo do_shortcode ('[shareaholic app="share_buttons" title="" id="dba1d5aed6561461918c0ad8acbcab22"]'); ?> </div>
        
         </div>
    </div>
    
    
    <!-- End of Section-4 -->
  
  
  
  
  <div class="page-element widget-container page-element-type-form widget-form" id="element-5" style="height: 256px;width: 306px;left: 145px;top: 2143px;z-index: 6;"> </div>

</div>


<div style="display: none;">
  <div class="seemappopup" id="shopregisterdiv">
      <div class="mainseemapdiv">
        <div class="main_massage_box">
	 <form action=""  method="post" id="frmsubmit" name="frmsubmit" enctype="multipart/form-data">
          <div class="massage_box">
            <div align="center" id="messageid" class="error-msg"></div>
            <div class="flt_left">
                <div class="to_inputbox">
                  <div class="popup_label"><span>*</span>&nbsp;Contact Name:</div>
                  <input type="text" name="vContactName" id="vContactName" class="massage_input" value="">
                </div>
                <div class="cl"></div>
                <div class="to_inputbox">
                  <div class="popup_label"><span>*</span>&nbsp;Contact Email:</div>
                  <input type="text" name="vContactEmail" id="vContactEmail" class="massage_input" value="">
                </div>
                <div class="cl"></div>
                <div class="to_inputbox">
                  <div class="popup_label"><span>*</span>&nbsp;Contact Phone:</div>
                  <input type="text" name="vPhone" id="vPhone" class="massage_input" value="">
                </div>
                <div class="cl"></div>
                <div class="to_inputbox">
                  <div class="popup_label"><span>*</span>&nbsp;Shop Name:</div>
                  <input type="text" name="vStoreName" id="vStoreName" class="massage_input" value="">
                </div>
                
                <div class="to_inputbox">
                  <div class="popup_label"><span>*</span>&nbsp;Shop URL:</div>
                  <input type="text" name="vShopUrl" id="vShopUrl" class="massage_input" value="">
                </div>
                
            </div>
            <div class="flt_right">
                <div class="cl"></div>
                <div class="to_inputbox">
                  <div class="popup_label"><span>*</span>&nbsp;Shop City:</div>
                  <input type="text" name="vCity" id="vCity" class="massage_input" value="">
                </div>
                <div class="to_inputbox2">
                  <div class="popup_label"><span></span>&nbsp;Comment:</div>
                  <textarea id="tComment" name="tComment" class="massage_input2"></textarea>
                </div>
		
                <div class="cl"></div>
                <div class="check_box_txt">
                  <input align="absmiddle" type="checkbox" id="Checkemail" name="Checkemail" value="" class="check_box">
                  Send a copy of this email to yourself 
                </div>
                <div class="flt_clear"></div>
		<div id="loderid2" style="display: none;z-index: 999999; margin-left: -11%;margin-top: -23%;position: absolute;">
		  <img src="<?php echo get_template_directory_uri(); ?>/images/blackloader.gif" alt="">
	       </div>
	    <div class="flt_clear"></div>
	    
               <div style="margin-top: 30px; text-align: center;">
            	<!--<img style="cursor: pointer;" onclick="emailtofriend();" title="Submit" src="<?php //echo get_template_directory_uri(); ?>/images/submit-btn.png" alt="">-->
                	<input type="button" value="Send Request" class="shop_btn shop_btn_req"  onclick="shopregiater();" style="cursor: pointer; background: #181818; padding: 10px 15px;">
               </div>
             </div>
             <div class="flt_clear"></div>
            
          </div>
        </div>
      </div>
    </div>
</div>
</form>

<div style="display: none;">
  <div class="seemappopup" id="shopcontactusdiv">
      <div class="mainseemapdiv">
        <div class="main_massage_box">
	 <form action=""  method="post" id="frmsubmit" name="frmsubmit" enctype="multipart/form-data">
          <div class="massage_box">
            <div align="center" id="messageid" class="error-msg"></div>
            <div class="flt_left">
                <div class="to_inputbox">
                  <div class="popup_label"><span>*</span>&nbsp;Contact Name:</div>
                  <input type="text" name="vContectName" id="vContectName" class="massage_input" value="">
                </div>
                <div class="cl"></div>
                <div class="to_inputbox">
                  <div class="popup_label"><span>*</span>&nbsp;Contact Email:</div>
                  <input type="text" name="vContetcEmail" id="vContetcEmail" class="massage_input" value="">
                </div>
                <div class="cl"></div>
                <div class="to_inputbox">
                  <div class="popup_label"><span>*</span>&nbsp;Subject:</div>
                  <input type="text" name="vSubject" id="vSubject" class="massage_input" value="">
                </div>
            </div>
            <div class="flt_right">
                <div class="to_inputbox2">
                  <div class="popup_label"><span></span>&nbsp;Comment:</div>
                  <textarea id="tCommentqq" name="tCommentqq" class="massage_input2" style="height:152px;"></textarea>
                </div>
             </div>
             <div class="flt_clear"></div>
	     
	    <div id="loderid1" style="display: none;z-index: 999999; margin-left: 38%;margin-top: -23%;position: absolute;">
		  <img src="<?php echo get_template_directory_uri(); ?>/images/blackloader.gif" alt="">
	       </div>
	    <div class="flt_clear"></div>
	    
            <div style="margin-top: 10px; text-align: center;">
                <input type="button" value="Submit" class="shop_btn shop_btn_req" id="shopregister"  onclick="sendcontectemail();" style="cursor: pointer;background: #181818; padding: 10px 15px;">
            </div> 
            <!--<div style="margin-top: 10px; text-align: center;"><img style="cursor: pointer;" onclick="emailtofriend();" title="Submit" src="<?php //echo get_template_directory_uri(); ?>/images/submit-btn.png" alt=""></div>-->
          </div>
        </div>
      </div>
    </div>
</div>
</div>
</form>


<div style="display: none;">
  <div class="seemappopup" id="signupDiv">
      <div class="mainseemapdiv">
        <div class="main_massage_box">
	 <form action=""  method="post" id="frmsubmit" name="frmsubmit" enctype="multipart/form-data">
          <div class="massage_box">
            <div align="center" id="messageid" class="error-msg"></div>
            <div class="flt_left">
                <div class="to_inputbox">
                  <div class="popup_label"><span>*</span>&nbsp; Name:</div>
                  <input type="text" id="user_nicename" name="user_nicename" class="massage_input">
                </div>
                <div class="cl"></div>
            </div>
	    
	    <div class="flt_right">
                <div class="to_inputbox2">
                  <div class="popup_label"><span>*</span>&nbsp;Email:</div>
                  <input type="text" id="user_email" name="user_email" class="massage_input">
                </div>
            </div>
	    
            <div class="flt_clear"></div>
	       <div id="loderid" style="display: none;z-index: 999999; margin-left: 37%;margin-top: -12%; position: absolute;">
		  <img src="<?php echo get_template_directory_uri(); ?>/images/blackloader.gif" alt="">
	       </div>
	    <div class="flt_clear"></div>
            <div style="margin-top: 10px; text-align: center;">
	       <input type="button" value="Submit" class="shop_btn shop_btn_req"  onclick="saveuser();" style="cursor: pointer;background: #181818; padding: 10px 15px;">
                <!--<input type="button" value="Submit" class="shop_btn shop_btn_req" id="shopregister"  onclick="sendcontectemail();" style="cursor: pointer;background: #181818; padding: 10px 15px;">-->
            </div> 
          </div>
	  </form>
        </div>
      </div>
    </div>
</div>




<script language="javascript">
$(document).ready(function(){
	$('#site-iphone5') .cycle({
		fx: 'fade', //'scrollLeft,scrollDown,scrollRight,scrollUp',blindX, blindY, blindZ, cover, curtainX, curtainY, fade, fadeZoom, growX, growY, none, scrollUp,scrollDown,scrollLeft,scrollRight,scrollHorz,scrollVert,shuffle,slideX,slideY,toss,turnUp,turnDown,turnLeft,turnRight,uncover,ipe ,zoom
		speed:  'slow', 
   		timeout: 2000 
	});
});	
</script>
<script>
$(document).ready(function(){
  $('#shopregister').fancybox({
        'overlayShow' : true,
        'transitionIn'  : 'elastic',
        'transitionOut' : 'elastic',
        'margin' : '0',
        'padding' : '0',
        'scrolling' : 'no'
        });


  $('#shopcontactus').fancybox({
        'overlayShow' : true,
        'transitionIn'  : 'elastic',
        'transitionOut' : 'elastic',
        'margin' : '0',
        'padding' : '0',
        'scrolling' : 'no'
        });
  
  $('#signup').fancybox({
        'overlayShow' : true,
        'transitionIn'  : 'elastic',
        'transitionOut' : 'elastic',
        'margin' : '0',
        'padding' : '0',
        'scrolling' : 'no'
        });
  
});






function shopregiater(){
   var vContactName=$("#vContactName").val();
   var vContactEmail=$("#vContactEmail").val();
   var vPhone=$("#vPhone").val();
   var vStoreName=$("#vStoreName").val();
   var vCity=$("#vCity").val();
   var tComment=$("#tComment").val();
   var Checkemail=document.getElementById("Checkemail").checked;
   var vShopUrl=$("#vShopUrl").val();

   var valid=true;

   if(vContactName.trim() == ''){      
      $("#vContactName").addClass("required");        
      valid=false;
   }    
   else{
      $("#vContactName").removeClass("required");
   }
   
   if(vContactEmail.trim() == ''){
       $("#vContactEmail").addClass("required");        
       valid=false;
   }    
   else{
       $("#vContactEmail").removeClass("required");
   }   
   if (vContactEmail.trim() != '') {        
       var emailRegexStr = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
       var isvalid = emailRegexStr.test(vContactEmail);
       if (!isvalid) {            
	   $("#vContactEmail").addClass("required");
	   valid=false;
       }else{            
	   $("#vContactEmail").removeClass("required");
       }
   }
   
   if(vPhone.trim() == ''){      
      $("#vPhone").addClass("required");        
      valid=false;
   }    
   else{
      $("#vPhone").removeClass("required");
   }
   
   if(vStoreName.trim() == ''){      
      $("#vStoreName").addClass("required");        
      valid=false;
   }    
   else{
      $("#vStoreName").removeClass("required");
   }
   
   if(vShopUrl.trim() == ''){      
      $("#vShopUrl").addClass("required");        
      valid=false;
   }    
   else{
      $("#vShopUrl").removeClass("required");
   }
   
   if(vCity.trim() == ''){      
      $("#vCity").addClass("required");        
      valid=false;
   }    
   else{
      $("#vCity").removeClass("required");
   }
   
   if (valid == true) {
      $("#loderid2").show();
      
      var url = site_url+"shopregister.php";
   
      var extra='';
      extra+='?vContactName='+vContactName;
      extra+='&vContactEmail='+vContactEmail;
      extra+='&vPhone='+vPhone;
      extra+='&vStoreName='+vStoreName;
      extra+='&vCity='+vCity;
      extra+='&tComment='+tComment;
      extra+='&vShopUrl='+vShopUrl;
      extra+='&Checkemail='+Checkemail;
      var pars = extra;
      //alert(url+pars);
      $.post(url+pars,
	 function(data) {
	    $("#loderid2").hide();
	    if (data == "Message has been sent.") {
	       $("#vContactName").val('');
	       $("#vContactEmail").val('');
	       $("#vPhone").val('');
	       $("#vStoreName").val('');
	       $("#vCity").val('');
	       $("#tComment").val('');
	       $("#vShopUrl").val('');
	       //$.fancybox.close();
	       var html='';
			html+='<div class="seemappopup" style="height:auto;">';
			html+='<div class="mainseemapdiv">';
			html+='<div  style="text-align:center;line-height:24px;" class="errormsg"><b>Your Shop has been Successfully Registered in Boutiqall</b></div>';
			html+='</div>';
			html+='</div>';
			$(document).ready(function () {				
				$.fancybox(html,{'modal':false,'margin' : '0','padding' : '0','scrolling' : 'no'});
			});
		  }else{
		     $("#vContactName").val('');
		     $("#vContactEmail").val('');
		     $("#vPhone").val('');
		     $("#vStoreName").val('');
		     $("#vCity").val('');
		     $("#tComment").val('');
		     $("#vShopUrl").val('');
			var html='';
			html+='<div class="seemappopup" style="height:auto;">';
			html+='<div class="mainseemapdiv">';
			html+='<div  style="text-align:center;line-height:24px;" class="errormsg"><b>Email id already exists.Please use another email id for shop register</b></div>';
			html+='</div>';
			html+='</div>';
			$(document).ready(function () {				
				$.fancybox(html,{'modal':false,'margin' : '0','padding' : '0','scrolling' : 'no'});
			});
	    }
	 });   
   }
   
}

function sendcontectemail(){
   var vContectName=$("#vContectName").val();
   var vContetcEmail=$("#vContetcEmail").val();
   var vSubject=$("#vSubject").val();
   var tCommentqq=$("#tCommentqq").val();
   //alert(tCommentqq);return false;
   var valid=true;
   

   if(vContectName.trim() == ''){      
      $("#vContectName").addClass("required");        
      valid=false;
   }    
   else{
      $("#vContectName").removeClass("required");
   }
   
   if(vContetcEmail.trim() == ''){
       $("#vContetcEmail").addClass("required");        
       valid=false;
   }    
   else{
       $("#vContetcEmail").removeClass("required");
   }   
   if (vContetcEmail.trim() != '') {        
       var emailRegexStr = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
       var isvalid = emailRegexStr.test(vContetcEmail);
       if (!isvalid) {            
	   $("#vContetcEmail").addClass("required");
	   valid=false;
       }else{            
	   $("#vContetcEmail").removeClass("required");
       }
   }
   
   if(vSubject.trim() == ''){      
      $("#vSubject").addClass("required");        
      valid=false;
   }    
   else{
      $("#vSubject").removeClass("required");
   }
   
   if (valid == true) {
      $("#loderid1").show();
      
      var url = site_url+"contectemail.php";
   
      var extra='';
      extra+='?vContectName='+vContectName;
      extra+='&vContetcEmail='+vContetcEmail;
      extra+='&vSubject='+vSubject;
      extra+='&tCommentqq='+tCommentqq;
      
      var pars = extra;
      //alert(url+pars);
      $.post(url+pars,
	 function(data) {
	    $("#loderid1").hide();
	    if (data == "Message has been sent.") {
	       $("#vContectName").val('');
	       $("#vContetcEmail").val('');
	       $("#vSubject").val('');
	       $("#tCommentqq").val('');
	      // $.fancybox.close();
	       var html='';
			html+='<div class="seemappopup" style="height:auto;">';
			html+='<div class="mainseemapdiv">';
			html+='<div  style="text-align:center;line-height:24px;" class="errormsg"><b>Thank You for Contact us.We will contact you soon</b></div>';
			html+='</div>';
			html+='</div>';
			$(document).ready(function () {				
				$.fancybox(html,{'modal':false,'margin' : '0','padding' : '0','scrolling' : 'no'});
			});
	    }
	 });   
   }
   
}


function saveuser(){
   var user_nicename=$("#user_nicename").val();
   var user_email=$("#user_email").val();
   
   var valid=true;
   

   if(user_nicename.trim() == ''){      
      $("#user_nicename").addClass("required");        
      valid=false;
   }    
   else{
      $("#user_nicename").removeClass("required");
   }
   
   if(user_email.trim() == ''){
       $("#user_email").addClass("required");        
       valid=false;
   }    
   else{
       $("#user_email").removeClass("required");
   }   
   if (user_email.trim() != '') {        
       var emailRegexStr = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
       var isvalid = emailRegexStr.test(user_email);
       if (!isvalid) {            
	   $("#user_email").addClass("required");
	   valid=false;
       }else{            
	   $("#user_email").removeClass("required");
       }
   }
   
   
   if (valid == true) {
      $("#loderid").show();
      
      var url = site_url+"registeruser.php";
      var extra='';
      extra+='?user_nicename='+user_nicename;
      extra+='&user_email='+user_email;
      var pars = extra;
      //alert(url+pars);
      $.post(url+pars,
	 function(data) {
	    //alert(data);
	    $("#loderid").hide();
	    if (data == "Message has been sent.") {
	       $("#user_nicename").val('');
	       $("#user_email").val('');
	       
	       var html='';
			html+='<div class="seemappopup" style="height:auto;">';
			html+='<div class="mainseemapdiv">';
			html+='<div  style="text-align:center;line-height:24px;" class="errormsg"><b>Thank You for Register in Boutiqall</b></div>';
			html+='</div>';
			html+='</div>';
			$(document).ready(function () {				
				$.fancybox(html,{'modal':false,'margin' : '0','padding' : '0','scrolling' : 'no'});
			});
	    }else{
	       $("#user_nicename").val('');
	       $("#user_email").val('');
	       var html='';
			html+='<div class="seemappopup" style="height:auto;">';
			html+='<div class="mainseemapdiv">';
			html+='<div  style="text-align:center;line-height:24px;" class="errormsg"><b>Email id already exists.Please use another email id for register</b></div>';
			html+='</div>';
			html+='</div>';
			$(document).ready(function () {				
				$.fancybox(html,{'modal':false,'margin' : '0','padding' : '0','scrolling' : 'no'});
			});
	    }
	 });   
   }
   
}



</script>
<?php get_footer(); ?>
