<?php
/**
 * The Header template for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
?>
<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<title>
<?php wp_title( '|', true, 'right' ); ?>
</title>
<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon_2.ico" type="image/x-icon">
<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon_2.ico" type="image/x-icon">
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php // Loads HTML5 JavaScript file to add support for HTML5 elements in older IE versions. ?>
<!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
<![endif]-->
<?php wp_head(); ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.cycle.all.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/js/fancybox/jquery.fancybox-1.3.4.css" media="screen" />
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>

</head>
<?php
    $site_url = get_option('siteurl');
?>

<script type="text/javascript">
  site_url = '<?php echo get_template_directory_uri(); ?>/';
</script>

<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/mycss.css" media="screen">

<body data-twttr-rendered="true" style="display: block;">
<div class="notification">
  <div class="notification-overlay"></div>
  <div class="notification-inner"> <img src="http://instapage-static-v25.s3.amazonaws.com/img-loading-461.gif" class="loading" alt=""> <span class="message"></span> <span class="close-button" onclick="jQuery(this).parent().parent().hide()">Close</span> </div>
</div>

<div id="">
<div class="header_bg">
<div class="page page2">
<div class="page-block" id="page_block_above_fold" style="height: 205px;"> </div>
<div class="page-block" id="page_block_above_fold" style="height: 333px;">
  <div class="page-element widget-container page-element-type-image widget-image" id="element-27" style="height: 146px;width: 450px;left: 277px;top: 134px;z-index: 26;">
    <div class="contents"> <img src="<?php echo get_template_directory_uri(); ?>/images/anywear-logo.jpg" alt=""> </div>
  </div>
  <div class="page-element widget-container page-element-type-image widget-image" id="element-25" style="height: 639px;width: 959px;left: 0px;top: -298px;z-index: 24;">
    <div class="contents"> <img src="<?php echo get_template_directory_uri(); ?>/images/banner.jpg" alt=""> </div>
  </div>
  <div class="page-element widget-container page-element-type-headline widget-headline" id="element-0" style="height: 47px;width: 561px;left: 347px;top: -8px;z-index: 3;">
    <div class="contents">
      <h1>
        <p><span style="font-size: 36px; color: #ffffff;"><strong>Why should they try your app?</strong></span></p>
      </h1>
    </div>
  </div>
  <div class="page-element widget-container page-element-type-box widget-box" id="element-26" style="height: 558px;width: 960px;left: 0px;top: -216px;z-index: 25;">
    <div class="box" style="width: 958px; height: 556px; background-color: #55343d; border: 1px solid transparent; border-radius: 0px 0px 0px 0px; opacity: 0.18; filter:alpha(opacity=18);"></div>
  </div>
  <div class="page-element widget-container page-element-type-headline widget-headline" id="element-29" style="height: 134px;width: 967px;left: 47px;top: 10px;z-index: 28;">
    <div class="contents">
      <h1>
        <p style="font-size: 48px; line-height: 140%;"><span style="color: #ffffff; font-size: 48px;"><em class="tiredtit">Tired of walking into the wrong shops?</em></span><span style="color: #ffffff;"><em><br>
          <strong><br>
          </strong></em></span></p>
      </h1>
    </div>
  </div>
</div>
</div>
</div>
