<?php
/**
* The main template file
*
* This is the most generic template file in a WordPress theme
* and one of the two required files for a theme (the other being style.css).
* It is used to display a page when nothing more specific matches a query.
* For example, it puts together the home page when no home.php file exists.
*
* @link http://codex.wordpress.org/Template_Hierarchy
*
* @package WordPress
* @subpackage Twenty_Twelve
* @since Twenty Twelve 1.0
*/

get_header(); ?>


<div class="page-block" id="page_block_footer" style="height: 2491px;">
																					<div class="page-element widget-container page-element-type-social widget-social" id="element-16" style="height: 36px;width: 605px;left: 284px;top: 2419px;z-index: 99999;">
	<div class="social buttons">

	
					<div id="___plusone_0" style="text-indent: 0px; margin: 0px; padding: 0px; background-color: transparent; border-style: none; float: none; line-height: normal; font-size: 1px; vertical-align: baseline; display: inline-block; width: 32px; height: 20px; background-position: initial initial; background-repeat: initial initial;"><iframe frameborder="0" hspace="0" marginheight="0" marginwidth="0" scrolling="no" style="position: static; top: 0px; width: 32px; margin: 0px; border-style: none; left: 0px; visibility: visible; height: 20px;" tabindex="0" vspace="0" width="100%" id="I0_1409063148956" name="I0_1409063148956" src="https://apis.google.com/u/0/se/0/_/+1/fastbutton?usegapi=1&amp;size=medium&amp;annotation=none&amp;origin=http%3A%2F%2Fanywearwpdemo.myinstapage.com&amp;url=http%3A%2F%2Fanywearwpdemo.myinstapage.com%2F&amp;gsrc=3p&amp;ic=1&amp;jsh=m%3B%2F_%2Fscs%2Fapps-static%2F_%2Fjs%2Fk%3Doz.gapi.en.2yxj2SLg-Ik.O%2Fm%3D__features__%2Fam%3DAQ%2Frt%3Dj%2Fd%3D1%2Ft%3Dzcms%2Frs%3DAItRSTOrG1ECpkXX65HyMjlQQzq2qWX_rQ#_methods=onPlusOne%2C_ready%2C_close%2C_open%2C_resizeMe%2C_renderstart%2Concircled%2Cdrefresh%2Cerefresh%2Conload&amp;id=I0_1409063148956&amp;parent=http%3A%2F%2Fanywearwpdemo.myinstapage.com&amp;pfname=&amp;rpctoken=24840724" data-gapiattached="true" title="+1"></iframe></div>
		
					<iframe id="twitter-widget-0" scrolling="no" frameborder="0" allowtransparency="true" src="http://platform.twitter.com/widgets/tweet_button.1409007440.html#_=1409063149720&amp;count=none&amp;id=twitter-widget-0&amp;lang=en&amp;original_referer=http%3A%2F%2Fanywearwpdemo.myinstapage.com%2F&amp;size=m&amp;text=Tired%20of%20walking%20into%20the%20wrong%20shops%3F%20Shop%20Only%20Where%20It%27s%20Right%20For%20You!%20%23Anywear%20is%20the%20perfect%20choice%20to%20find%20boutiques%20near%20by%20you.&amp;url=http%3A%2F%2Fanywearwpdemo.myinstapage.com%2F" class="twitter-share-button twitter-tweet-button twitter-share-button twitter-count-none" title="Twitter Tweet Button" data-twttr-rendered="true" style="width: 55px; height: 20px;"></iframe>
		
					<span class="IN-widget" style="line-height: 1; vertical-align: baseline; display: inline-block; text-align: center;"><span style="padding: 0px !important; margin: 0px !important; text-indent: 0px !important; display: inline-block !important; vertical-align: baseline !important; font-size: 1px !important;"><span id="li_ui_li_gen_1409063148779_0"><a id="li_ui_li_gen_1409063148779_0-link" href="javascript:void(0);"><span id="li_ui_li_gen_1409063148779_0-logo">in</span><span id="li_ui_li_gen_1409063148779_0-title"><span id="li_ui_li_gen_1409063148779_0-mark"></span><span id="li_ui_li_gen_1409063148779_0-title-text">Share</span></span></a></span></span></span><script type="IN/Share+init"></script>
		
					 <div class="fb-like fb_iframe_widget" data-send="false" data-width="450" data-show-faces="false" fb-xfbml-state="rendered" fb-iframe-plugin-query="app_id=284491111573568&amp;href=http%3A%2F%2Fanywearwpdemo.myinstapage.com%2F&amp;locale=en_US&amp;sdk=joey&amp;send=false&amp;show_faces=false&amp;width=450"><span style="vertical-align: bottom; width: 450px; height: 20px;"><iframe name="f2266e9964" width="450px" height="1000px" frameborder="0" allowtransparency="true" scrolling="no" title="fb:like Facebook Social Plugin" src="http://www.facebook.com/plugins/like.php?app_id=284491111573568&amp;channel=http%3A%2F%2Fstatic.ak.facebook.com%2Fconnect%2Fxd_arbiter%2FZIZ-G4f9LgK.js%3Fversion%3D41%23cb%3Df4ca35518%26domain%3Danywearwpdemo.myinstapage.com%26origin%3Dhttp%253A%252F%252Fanywearwpdemo.myinstapage.com%252Ff1f444192%26relation%3Dparent.parent&amp;href=http%3A%2F%2Fanywearwpdemo.myinstapage.com%2F&amp;locale=en_US&amp;sdk=joey&amp;send=false&amp;show_faces=false&amp;width=450" style="border: none; visibility: visible; width: 450px; height: 20px;" class=""></iframe></span></div>
		
	</div>
</div>

															<div class="page-element widget-container page-element-type-headline widget-headline" id="element-2" style="height: 100px;width: 279px;left: 321px;top: 584px;z-index: 4;">
	
<div class="contents"><h1><p style="text-align: center; font-size: 36px; line-height: 140%;"><span style="color: #808080;"><span style="font-size: 36px;"><strong><em><span style="color: #000000;">Tell us:</span></em></strong></span><br><br></span></p></h1></div>
</div>

															<div class="page-element widget-container page-element-type-headline widget-headline" id="element-24" style="height: 126px;width: 415px;left: 69px;top: 354px;z-index: 23;">
	
<div class="contents"><h1><p style="font-size: 30px; line-height: 140%; text-align: center;"><span style="font-size: 30px;"><em><span style="color: #808080;">Visit the boutiques near you or Shop online from your &nbsp;smartphone</span></em></span></p></h1></div>
</div>

															<div class="page-element widget-container page-element-type-image widget-image" id="element-3" style="height: 460px;width: 231px;left: 521px;top: 715px;z-index: 5;">
	<div class="contents">
		<img src="http://instapage-thumbnail.s3.amazonaws.com/134482-260973-231x460-BUDGET.jpg" alt="">
	</div>
</div>

															<div class="page-element widget-container page-element-type-headline widget-headline" id="element-20" style="height: 126px;width: 518px;left: 18px;top: 201px;z-index: 19;">
	
<div class="contents"><h1><p style="font-size: 30px; line-height: 140%; text-align: center;"><span style="font-size: 30px;"><em><span style="color: #808080;">Discover a Tailored List of <br>Fashion Boutiques Worldwide that Match Your Personal Profile &amp; Style</span></em></span></p></h1></div>
</div>

															<div class="page-element widget-container page-element-type-image widget-image" id="element-4" style="height: 453px;width: 228px;left: 172px;top: 714px;z-index: 5;">
	<div class="contents">
		<img src="http://instapage-thumbnail.s3.amazonaws.com/134482-260963-228x453-Styles.jpg" alt="">
	</div>
</div>

															<div class="page-element widget-container page-element-type-image widget-image" id="element-1" style="height: 410px;width: 207px;left: 363px;top: 740px;z-index: 4;">
	<div class="contents">
		<img src="http://instapage-thumbnail.s3.amazonaws.com/134482-262675-207x410-Phonewsizesnew.jpg" alt="">
	</div>
</div>

															<div class="page-element widget-container page-element-type-form widget-form" id="element-5" style="height: 256px;width: 306px;left: 145px;top: 2143px;z-index: 6;">
	
<style type="text/css">
		#element-5 .submit-button
	{
		background-color: #000000;

					color: #ffffff;
			    	}

	#element-5 label
	{
		color: #000000;
	}

	::-webkit-input-placeholder
	{ /* WebKit browsers */
		color: #000000	}

	:-moz-placeholder
	{ /* Mozilla Firefox 4 to 18 */
		color: #000000;
	}

	::-moz-placeholder
	{ /* Mozilla Firefox 19+ */
		color: #000000;
	}

	:-ms-input-placeholder
	{ /* Internet Explorer 10+ */
		color: #000000;
	}

	#element-5 label.error
	{
		color: #C00;
	}

	#element-5 input[type="text"],
	#element-5 input[type="password"],
	#element-5 input[type="email"],
	#element-5 textarea
	{
		color: #000000;
		background-color: #ffffff;
		border-color: #555555;
	}
	 #element-5 select
	 {
	 	border-color: #555555;
	 }

			#element-5 .dynamic-button:hover
		{
			background-color: #000000;
		}
	
					#element-5 .submit-holder
		{
			position: relative;
		}

		#element-5 .dynamic-button, #element-5 .image-button
		{
			position: absolute;
			margin: 0;
			top: 195px;
			left: 334.5px;
			width: 300px;
			height: 52px;
			z-index: 6;
		}
	
					#element-5 .dynamic-button.glossy
		{
			background-image: linear-gradient(bottom, #000000, rgb(26, 26, 26) );
			background-image: -o-linear-gradient(bottom, #000000, rgb(26, 26, 26) );
			background-image: -moz-linear-gradient(bottom, #000000, rgb(26, 26, 26) );
			background-image: -webkit-linear-gradient(bottom, #000000, rgb(26, 26, 26) );
			background-image: -ms-linear-gradient(bottom, #000000, rgb(26, 26, 26) );
			background-image: -webkit-gradient(
				linear,
				right bottom,
				right top,
				color-stop(0, #000000),
				color-stop(0.66, rgb(26, 26, 26))
			);
			border: #000000 solid 1px;
			
		}

								#element-5 .dynamic-button.glossy:hover
			{
				background-image: linear-gradient(bottom, #000000, rgb(26, 26, 26));
				background-image: -o-linear-gradient(bottom, #000000, rgb(26, 26, 26));
				background-image: -moz-linear-gradient(bottom, #000000, rgb(26, 26, 26));
				background-image: -webkit-linear-gradient(bottom, #000000, rgb(26, 26, 26));
				background-image: -ms-linear-gradient(bottom, #000000, rgb(26, 26, 26));
				background-image: -webkit-gradient(
					linear,
					right bottom,
					right top,
					color-stop(0, #000000),
					color-stop(0.66, rgb(26, 26, 26))
				);
				border: #000000 solid 1px;
				text-shadow: #000000 0px 1px 1px;
			}
		
	</style>

<div class="widget-container widget-form">
	<div class="contents">
		<form method="post" action="http://anywearwpdemo.myinstapage.com/api/email/134482" class="email-form" novalidate="novalidate">
			<div class="email-form-messagebox-wrapper">
				<div class="email-form-messagebox-header">Please Fix Following Errors</div>
				<div class="email-form-messagebox"></div>
			</div>
			<input type="hidden" name="variant" value="F">

			
						
							<div class="input-holder field-text">
					
											
						<div class="field-element ">
							<input id="498384dcea792b1d39a1fca17f5ebbaa-0" type="text" name="TmFtZQ==" value="" placeholder="Name" class="shortnice form-input required  ">
						</div>
									</div>
							<div class="input-holder field-text">
					
											
						<div class="field-element ">
							<input id="498384dcea792b1d39a1fca17f5ebbaa-1" type="email" name="RW1haWw=" value="" placeholder="Email" class="shortnice form-input required  ">
						</div>
									</div>
							<div class="input-holder field-textarea">
					
																																				<div class="field-element ">
							<textarea id="498384dcea792b1d39a1fca17f5ebbaa-2" type="text" name="Q29tbWVudA==" class="shortnice form-input required " placeholder="Comment"></textarea>
						</div>
									</div>
			
			
			
			
			
			
			
			
			
			
												<button class="btn submit-button button_submit dynamic-button  corners glossy " style="line-height: 20.8px; font-size: 17.333333333333px;">Submit</button>
							
							<input type="hidden" name="validation" value="Tzo4OiJzdGRDbGFzcyI6Mjp7czo2OiJmaWVsZHMiO2E6Mzp7czo0OiJOYW1lIjtPOjg6InN0ZENsYXNzIjoxOntzOjg6InJlcXVpcmVkIjtiOjE7fXM6NToiRW1haWwiO086ODoic3RkQ2xhc3MiOjE6e3M6ODoicmVxdWlyZWQiO2I6MTt9czo3OiJDb21tZW50IjtPOjg6InN0ZENsYXNzIjoxOntzOjg6InJlcXVpcmVkIjtiOjE7fX1zOjc6ImZvcm1faWQiO3M6MzI6IjQ5ODM4NGRjZWE3OTJiMWQzOWExZmNhMTdmNWViYmFhIjt9">
			
			
		</form>
	</div>
	<div style="clear: both"></div>
</div>

</div>

															<div class="page-element widget-container page-element-type-headline widget-headline" id="element-18" style="height: 25px;width: 370px;left: 425px;top: 1825px;z-index: 17;">
	
<div class="contents"><h1><p style="font-size: 18px; line-height: 140%;"><span style="font-size: 18px;"><strong><em><span style="color: #808080;">What you Love, What you Want</span></em></strong></span></p></h1></div>
</div>

															<div class="page-element widget-container page-element-type-image widget-image" id="element-6" style="height: 526px;width: 264px;left: 572px;top: 18px;z-index: 6;">
	<div class="contents">
		<img src="http://instapage-thumbnail.s3.amazonaws.com/134482-260973-264x526-BUDGET.jpg" alt="">
	</div>
</div>

															<div class="page-element widget-container page-element-type-headline widget-headline" id="element-22" style="height: 42px;width: 188px;left: 385px;top: 1684px;z-index: 21;">
	
<div class="contents"><h1><p style="font-size: 30px; line-height: 140%; text-align: center;"><em><span style="color: #000000; font-size: 30px;"><strong>Collect</strong></span></em></p></h1></div>
</div>

															<div class="page-element widget-container page-element-type-video widget-video" id="element-7" style="height: 364px;width: 230px;left: 590px;top: 82px;z-index: 7;">
	<div class="video_wrapper contents">
			<iframe class="youtube_object" type="text/html" width="230" height="364" src="http://www.youtube.com/embed/UF8uR6Z6KLc?&amp;wmode=transparent&amp;rel=0&amp;showinfo=0" allowfullscreen="" frameborder="0"></iframe>
	</div>
</div>

															<div class="page-element widget-container page-element-type-headline widget-headline" id="element-30" style="height: 42px;width: 188px;left: 390px;top: 1868px;z-index: 29;">
	
<div class="contents"><h1><p style="font-size: 30px; line-height: 140%; text-align: center;"><em><span style="color: #000000; font-size: 30px;"><strong>Become</strong></span></em></p></h1></div>
</div>

															<div class="page-element widget-container page-element-type-headline widget-headline" id="element-8" style="height: 96px;width: 506px;left: 18px;top: 62px;z-index: 7;">
	
<div class="contents"><h1><p style="text-align: center;"><span style="color: #000000;"><strong><span style="font-size: 36px;"><em>Shop OnlyWhere&nbsp;<br>It's Right For You!</em></span></strong></span></p></h1></div>
</div>

															<div class="page-element widget-container page-element-type-headline widget-headline" id="element-15" style="height: 42px;width: 188px;left: 422px;top: 1774px;z-index: 14;">
	
<div class="contents"><h1><p style="font-size: 30px; line-height: 140%; text-align: center;"><em><span style="color: #000000; font-size: 30px;"><strong>Recommend</strong></span></em></p></h1></div>
</div>

															<div class="page-element widget-container page-element-type-text widget-text" id="element-9" style="height: 132px;width: 801px;left: 120px;top: 1979px;z-index: 8;">
	
<div class="contents"><p style="font-size: 24px; line-height: 140%; color: #000000;"><em><strong>AnyWear</strong> <strong>is coming soon!&nbsp;<br></strong><span style="font-size: 24px;">Sign up and be the first to know when to download! Start receiving monthly newsletters, stay up to date with fashion trends, events, sales, new boutiques <br>and editor's picks!</span></em></p></div>
</div>

															<div class="page-element widget-container page-element-type-headline widget-headline" id="element-17" style="height: 25px;width: 428px;left: 430px;top: 1538px;z-index: 16;">
	
<div class="contents"><h1><p style="font-size: 18px; line-height: 140%;"><span style="font-size: 18px;"><strong><em><span style="color: #808080;">New Boutiques, New Designers, New Trends&nbsp;</span></em></strong></span></p></h1></div>
</div>

															<div class="page-element widget-container page-element-type-headline widget-headline" id="element-10" style="height: 33px;width: 370px;left: 562px;top: 680px;z-index: 9;">
	
<div class="contents"><h1><p style="font-size: 24px; line-height: 140%;"><strong><em><span style="color: #808080; font-size: 24px;">Your Budget &nbsp;</span></em></strong><span style="color: #808080; font-size: 24px;"><strong><br></strong></span></p></h1></div>
</div>

															<div class="page-element widget-container page-element-type-headline widget-headline" id="element-19" style="height: 42px;width: 274px;left: 390px;top: 1589px;z-index: 18;">
	
<div class="contents"><h1><p style="font-size: 30px; line-height: 140%; text-align: center;"><em><span style="color: #000000; font-size: 30px;"><strong>Shop Around</strong></span></em></p></h1></div>
</div>

															<div class="page-element widget-container page-element-type-headline widget-headline" id="element-11" style="height: 33px;width: 370px;left: 405px;top: 706px;z-index: 10;">
	
<div class="contents"><h1><p style="font-size: 24px; line-height: 140%;"><strong><em><span style="color: #808080; font-size: 24px;">Your Size</span></em></strong></p></h1></div>
</div>

															<div class="page-element widget-container page-element-type-headline widget-headline" id="element-21" style="height: 25px;width: 370px;left: 430px;top: 1638px;z-index: 20;">
	
<div class="contents"><h1><p style="font-size: 18px; line-height: 140%;"><span style="font-size: 18px;"><strong><em><span style="color: #808080;">In the Boutiques Near You or Online</span></em></strong></span></p></h1></div>
</div>

															<div class="page-element widget-container page-element-type-headline widget-headline" id="element-12" style="height: 33px;width: 370px;left: 213px;top: 679px;z-index: 11;">
	
<div class="contents"><h1><p style="font-size: 24px; line-height: 140%;"><strong><span style="color: #808080; font-size: 24px;">&nbsp;</span><em><span style="color: #808080; font-size: 24px;">Your Style</span></em></strong></p></h1></div>
</div>

															<div class="page-element widget-container page-element-type-headline widget-headline" id="element-23" style="height: 25px;width: 428px;left: 430px;top: 1730px;z-index: 22;">
	
<div class="contents"><h1><p style="font-size: 18px; line-height: 140%;"><span style="font-size: 18px;"><strong><em><span style="color: #808080;">Points, Rewards and Monthly Possibilities</span></em></strong></span></p></h1></div>
</div>

															<div class="page-element widget-container page-element-type-headline widget-headline" id="element-13" style="height: 168px;width: 738px;left: 115px;top: 1232px;z-index: 12;">
	
<div class="contents"><h1><p style="font-size: 30px; line-height: 140%; text-align: center;"><em><span style="color: #000000; font-size: 30px;"><strong>Then...<br>AnyWear will find the boutiques that <br>you are looking for,&nbsp;in your price and size range, and most importantly, in the styles you love!</strong></span></em></p></h1></div>
</div>

															<div class="page-element widget-container page-element-type-image widget-image" id="element-28" style="height: 467px;width: 237px;left: 116px;top: 1484px;z-index: 27;">
	<div class="contents">
		<img src="http://instapage-thumbnail.s3.amazonaws.com/134482-262683-237x467-PhoneExplore.jpg" alt="">
	</div>
</div>

															<div class="page-element widget-container page-element-type-headline widget-headline" id="element-14" style="height: 42px;width: 188px;left: 399px;top: 1490px;z-index: 13;">
	
<div class="contents"><h1><p style="font-size: 30px; line-height: 140%; text-align: center;"><em><span style="color: #000000; font-size: 30px;"><strong>Explore</strong></span></em></p></h1></div>
</div>

															<div class="page-element widget-container page-element-type-headline widget-headline" id="element-31" style="height: 25px;width: 370px;left: 422px;top: 1917px;z-index: 30;">
	
<div class="contents"><h1><p style="font-size: 18px; line-height: 140%;"><span style="font-size: 18px;"><strong><em><span style="color: #808080;">A Trendsetter, an Editor, a Fashionista</span></em></strong></span></p></h1></div>
</div>

																		</div>

	
	


<?php get_footer(); ?>